﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000005 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000007 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000008 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000B System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000000C TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000E System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000F System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000010 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000011 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000014 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000015 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000016 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000018 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000019 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000001B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000001E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000020 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000022 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000023 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000024 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000025 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000026 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000027 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000028 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000033 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000036 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000037 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000038 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000039 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000003A System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000003B TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000003C System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000003D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000003E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000003F System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000040 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000041 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000042 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000043 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000044 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000045 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000046 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000047 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000048 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000049 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000004A System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000004B System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000004C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000004D System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000004E System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000004F System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000050 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000051 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000052 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000053 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000054 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000055 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000056 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000057 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000058 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
// 0x00000059 System.Void System.Collections.Generic.ICollectionDebugView`1::.ctor(System.Collections.Generic.ICollection`1<T>)
// 0x0000005A T[] System.Collections.Generic.ICollectionDebugView`1::get_Items()
static Il2CppMethodPointer s_methodPointers[90] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[90] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[27] = 
{
	{ 0x02000004, { 40, 4 } },
	{ 0x02000005, { 44, 9 } },
	{ 0x02000006, { 55, 7 } },
	{ 0x02000007, { 64, 10 } },
	{ 0x02000008, { 76, 11 } },
	{ 0x02000009, { 90, 9 } },
	{ 0x0200000A, { 102, 12 } },
	{ 0x0200000B, { 117, 1 } },
	{ 0x0200000C, { 118, 2 } },
	{ 0x0200000D, { 120, 4 } },
	{ 0x0200000E, { 124, 21 } },
	{ 0x02000010, { 145, 2 } },
	{ 0x02000011, { 147, 2 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 10 } },
	{ 0x06000005, { 20, 5 } },
	{ 0x06000006, { 25, 5 } },
	{ 0x06000007, { 30, 3 } },
	{ 0x06000008, { 33, 3 } },
	{ 0x06000009, { 36, 1 } },
	{ 0x0600000A, { 37, 3 } },
	{ 0x0600001A, { 53, 2 } },
	{ 0x0600001F, { 62, 2 } },
	{ 0x06000024, { 74, 2 } },
	{ 0x0600002A, { 87, 3 } },
	{ 0x0600002F, { 99, 3 } },
	{ 0x06000034, { 114, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[149] = 
{
	{ (Il2CppRGCTXDataType)2, 12647 },
	{ (Il2CppRGCTXDataType)3, 6472 },
	{ (Il2CppRGCTXDataType)2, 12648 },
	{ (Il2CppRGCTXDataType)2, 12649 },
	{ (Il2CppRGCTXDataType)3, 6473 },
	{ (Il2CppRGCTXDataType)2, 12650 },
	{ (Il2CppRGCTXDataType)2, 12651 },
	{ (Il2CppRGCTXDataType)3, 6474 },
	{ (Il2CppRGCTXDataType)2, 12652 },
	{ (Il2CppRGCTXDataType)3, 6475 },
	{ (Il2CppRGCTXDataType)2, 12653 },
	{ (Il2CppRGCTXDataType)3, 6476 },
	{ (Il2CppRGCTXDataType)2, 12654 },
	{ (Il2CppRGCTXDataType)2, 12655 },
	{ (Il2CppRGCTXDataType)3, 6477 },
	{ (Il2CppRGCTXDataType)2, 12656 },
	{ (Il2CppRGCTXDataType)2, 12657 },
	{ (Il2CppRGCTXDataType)3, 6478 },
	{ (Il2CppRGCTXDataType)2, 12658 },
	{ (Il2CppRGCTXDataType)3, 6479 },
	{ (Il2CppRGCTXDataType)2, 12659 },
	{ (Il2CppRGCTXDataType)3, 6480 },
	{ (Il2CppRGCTXDataType)3, 6481 },
	{ (Il2CppRGCTXDataType)2, 9495 },
	{ (Il2CppRGCTXDataType)3, 6482 },
	{ (Il2CppRGCTXDataType)2, 12660 },
	{ (Il2CppRGCTXDataType)3, 6483 },
	{ (Il2CppRGCTXDataType)3, 6484 },
	{ (Il2CppRGCTXDataType)2, 9502 },
	{ (Il2CppRGCTXDataType)3, 6485 },
	{ (Il2CppRGCTXDataType)2, 12661 },
	{ (Il2CppRGCTXDataType)3, 6486 },
	{ (Il2CppRGCTXDataType)3, 6487 },
	{ (Il2CppRGCTXDataType)2, 9506 },
	{ (Il2CppRGCTXDataType)2, 12662 },
	{ (Il2CppRGCTXDataType)3, 6488 },
	{ (Il2CppRGCTXDataType)2, 9509 },
	{ (Il2CppRGCTXDataType)2, 9511 },
	{ (Il2CppRGCTXDataType)2, 12663 },
	{ (Il2CppRGCTXDataType)3, 6489 },
	{ (Il2CppRGCTXDataType)3, 6490 },
	{ (Il2CppRGCTXDataType)3, 6491 },
	{ (Il2CppRGCTXDataType)2, 9516 },
	{ (Il2CppRGCTXDataType)3, 6492 },
	{ (Il2CppRGCTXDataType)3, 6493 },
	{ (Il2CppRGCTXDataType)2, 9528 },
	{ (Il2CppRGCTXDataType)2, 12664 },
	{ (Il2CppRGCTXDataType)3, 6494 },
	{ (Il2CppRGCTXDataType)3, 6495 },
	{ (Il2CppRGCTXDataType)2, 9530 },
	{ (Il2CppRGCTXDataType)2, 12567 },
	{ (Il2CppRGCTXDataType)3, 6496 },
	{ (Il2CppRGCTXDataType)3, 6497 },
	{ (Il2CppRGCTXDataType)2, 12665 },
	{ (Il2CppRGCTXDataType)3, 6498 },
	{ (Il2CppRGCTXDataType)3, 6499 },
	{ (Il2CppRGCTXDataType)2, 9540 },
	{ (Il2CppRGCTXDataType)2, 12666 },
	{ (Il2CppRGCTXDataType)3, 6500 },
	{ (Il2CppRGCTXDataType)3, 6501 },
	{ (Il2CppRGCTXDataType)3, 6131 },
	{ (Il2CppRGCTXDataType)3, 6502 },
	{ (Il2CppRGCTXDataType)2, 12667 },
	{ (Il2CppRGCTXDataType)3, 6503 },
	{ (Il2CppRGCTXDataType)3, 6504 },
	{ (Il2CppRGCTXDataType)2, 9552 },
	{ (Il2CppRGCTXDataType)2, 12668 },
	{ (Il2CppRGCTXDataType)3, 6505 },
	{ (Il2CppRGCTXDataType)3, 6506 },
	{ (Il2CppRGCTXDataType)3, 6507 },
	{ (Il2CppRGCTXDataType)3, 6508 },
	{ (Il2CppRGCTXDataType)3, 6509 },
	{ (Il2CppRGCTXDataType)3, 6137 },
	{ (Il2CppRGCTXDataType)3, 6510 },
	{ (Il2CppRGCTXDataType)2, 12669 },
	{ (Il2CppRGCTXDataType)3, 6511 },
	{ (Il2CppRGCTXDataType)3, 6512 },
	{ (Il2CppRGCTXDataType)2, 9565 },
	{ (Il2CppRGCTXDataType)2, 12670 },
	{ (Il2CppRGCTXDataType)3, 6513 },
	{ (Il2CppRGCTXDataType)3, 6514 },
	{ (Il2CppRGCTXDataType)2, 9567 },
	{ (Il2CppRGCTXDataType)2, 12671 },
	{ (Il2CppRGCTXDataType)3, 6515 },
	{ (Il2CppRGCTXDataType)3, 6516 },
	{ (Il2CppRGCTXDataType)2, 12672 },
	{ (Il2CppRGCTXDataType)3, 6517 },
	{ (Il2CppRGCTXDataType)3, 6518 },
	{ (Il2CppRGCTXDataType)2, 12673 },
	{ (Il2CppRGCTXDataType)3, 6519 },
	{ (Il2CppRGCTXDataType)3, 6520 },
	{ (Il2CppRGCTXDataType)2, 9582 },
	{ (Il2CppRGCTXDataType)2, 12674 },
	{ (Il2CppRGCTXDataType)3, 6521 },
	{ (Il2CppRGCTXDataType)3, 6522 },
	{ (Il2CppRGCTXDataType)3, 6523 },
	{ (Il2CppRGCTXDataType)3, 6148 },
	{ (Il2CppRGCTXDataType)2, 12675 },
	{ (Il2CppRGCTXDataType)3, 6524 },
	{ (Il2CppRGCTXDataType)3, 6525 },
	{ (Il2CppRGCTXDataType)2, 12676 },
	{ (Il2CppRGCTXDataType)3, 6526 },
	{ (Il2CppRGCTXDataType)3, 6527 },
	{ (Il2CppRGCTXDataType)2, 9598 },
	{ (Il2CppRGCTXDataType)2, 12677 },
	{ (Il2CppRGCTXDataType)3, 6528 },
	{ (Il2CppRGCTXDataType)3, 6529 },
	{ (Il2CppRGCTXDataType)3, 6530 },
	{ (Il2CppRGCTXDataType)3, 6531 },
	{ (Il2CppRGCTXDataType)3, 6532 },
	{ (Il2CppRGCTXDataType)3, 6533 },
	{ (Il2CppRGCTXDataType)3, 6154 },
	{ (Il2CppRGCTXDataType)2, 12678 },
	{ (Il2CppRGCTXDataType)3, 6534 },
	{ (Il2CppRGCTXDataType)3, 6535 },
	{ (Il2CppRGCTXDataType)2, 12679 },
	{ (Il2CppRGCTXDataType)3, 6536 },
	{ (Il2CppRGCTXDataType)3, 6537 },
	{ (Il2CppRGCTXDataType)3, 6538 },
	{ (Il2CppRGCTXDataType)3, 6539 },
	{ (Il2CppRGCTXDataType)2, 12680 },
	{ (Il2CppRGCTXDataType)2, 9628 },
	{ (Il2CppRGCTXDataType)2, 9626 },
	{ (Il2CppRGCTXDataType)2, 12681 },
	{ (Il2CppRGCTXDataType)3, 6540 },
	{ (Il2CppRGCTXDataType)2, 12682 },
	{ (Il2CppRGCTXDataType)3, 6541 },
	{ (Il2CppRGCTXDataType)3, 6542 },
	{ (Il2CppRGCTXDataType)3, 6543 },
	{ (Il2CppRGCTXDataType)2, 9632 },
	{ (Il2CppRGCTXDataType)3, 6544 },
	{ (Il2CppRGCTXDataType)3, 6545 },
	{ (Il2CppRGCTXDataType)2, 9635 },
	{ (Il2CppRGCTXDataType)3, 6546 },
	{ (Il2CppRGCTXDataType)1, 12683 },
	{ (Il2CppRGCTXDataType)2, 9634 },
	{ (Il2CppRGCTXDataType)3, 6547 },
	{ (Il2CppRGCTXDataType)1, 9634 },
	{ (Il2CppRGCTXDataType)1, 9632 },
	{ (Il2CppRGCTXDataType)2, 12684 },
	{ (Il2CppRGCTXDataType)2, 9634 },
	{ (Il2CppRGCTXDataType)3, 6548 },
	{ (Il2CppRGCTXDataType)3, 6549 },
	{ (Il2CppRGCTXDataType)3, 6550 },
	{ (Il2CppRGCTXDataType)2, 9633 },
	{ (Il2CppRGCTXDataType)3, 6551 },
	{ (Il2CppRGCTXDataType)2, 9646 },
	{ (Il2CppRGCTXDataType)2, 9657 },
	{ (Il2CppRGCTXDataType)2, 9659 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	90,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	27,
	s_rgctxIndices,
	149,
	s_rgctxValues,
	NULL,
};
