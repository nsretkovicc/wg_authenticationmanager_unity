﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void GameMenuHandler::SetVisible(System.Boolean)
extern void GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C (void);
// 0x00000002 System.Void GameMenuHandler::SetupAppleData(System.String,AppleAuth.Interfaces.ICredential)
extern void GameMenuHandler_SetupAppleData_m5878285CA5056522A0FD6BDC58739DCEC59E6BC2 (void);
// 0x00000003 System.Void GameMenuHandler::.ctor()
extern void GameMenuHandler__ctor_m4A869F8A31ADAE4B56BE371C18A75C926184DA1A (void);
// 0x00000004 System.Void LoginMenuHandler::SetVisible(System.Boolean)
extern void LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069 (void);
// 0x00000005 System.Void LoginMenuHandler::SetLoadingMessage(System.Boolean,System.String)
extern void LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B (void);
// 0x00000006 System.Void LoginMenuHandler::SetSignInWithAppleButton(System.Boolean,System.Boolean)
extern void LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608 (void);
// 0x00000007 System.Void LoginMenuHandler::UpdateLoadingMessage(System.Single)
extern void LoginMenuHandler_UpdateLoadingMessage_m9F88D8943762FB49A8DCB56CDEC6BC63F35E7083 (void);
// 0x00000008 System.Void LoginMenuHandler::.ctor()
extern void LoginMenuHandler__ctor_m7363D92585401D5357294C6D50A145D5F9905FD5 (void);
// 0x00000009 System.Void MainMenu::Start()
extern void MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821 (void);
// 0x0000000A System.Void MainMenu::Update()
extern void MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866 (void);
// 0x0000000B System.Void MainMenu::SignInWithAppleButtonPressed()
extern void MainMenu_SignInWithAppleButtonPressed_mE369BF33E96F249C0B362FB5F5CF020951ADCFFB (void);
// 0x0000000C System.Void MainMenu::InitializeLoginMenu()
extern void MainMenu_InitializeLoginMenu_m6DF9DFCD30D84C572C0FAE153A293E972409D333 (void);
// 0x0000000D System.Void MainMenu::SetupLoginMenuForUnsupportedPlatform()
extern void MainMenu_SetupLoginMenuForUnsupportedPlatform_m80C029D8C208B629AF2849559B8D653AD4FCF374 (void);
// 0x0000000E System.Void MainMenu::SetupLoginMenuForSignInWithApple()
extern void MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC (void);
// 0x0000000F System.Void MainMenu::SetupLoginMenuForCheckingCredentials()
extern void MainMenu_SetupLoginMenuForCheckingCredentials_m16AEE25404D91E530C121EFFFF3257C074EEBF25 (void);
// 0x00000010 System.Void MainMenu::SetupLoginMenuForQuickLoginAttempt()
extern void MainMenu_SetupLoginMenuForQuickLoginAttempt_m351D9B48275D2D2587C3FFD3F5F4D6BF002C948F (void);
// 0x00000011 System.Void MainMenu::SetupLoginMenuForAppleSignIn()
extern void MainMenu_SetupLoginMenuForAppleSignIn_m314FDA3B2F46F68904F84841840033848ADB585D (void);
// 0x00000012 System.Void MainMenu::SetupGameMenu(System.String,AppleAuth.Interfaces.ICredential)
extern void MainMenu_SetupGameMenu_m9141BCE4B7637E206CF1718C18189AC0D6DDCFF9 (void);
// 0x00000013 System.Void MainMenu::CheckCredentialStatusForUserId(System.String)
extern void MainMenu_CheckCredentialStatusForUserId_m33A8D3E7479359851349A1532CC5031D03AFE3B2 (void);
// 0x00000014 System.Void MainMenu::AttemptQuickLogin()
extern void MainMenu_AttemptQuickLogin_m1C4F040B20F22A88A09469378F69540EFEAFB883 (void);
// 0x00000015 System.Void MainMenu::SignInWithApple()
extern void MainMenu_SignInWithApple_m54E769319FF0899CCD150E6BB1602E3E5ECFBE94 (void);
// 0x00000016 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5 (void);
// 0x00000017 System.Void MainMenu::<InitializeLoginMenu>b__7_0(System.String)
extern void MainMenu_U3CInitializeLoginMenuU3Eb__7_0_mC33C351ABC83249E18FA1ED7E2C2010992174DFE (void);
// 0x00000018 System.Void MainMenu::<AttemptQuickLogin>b__15_0(AppleAuth.Interfaces.ICredential)
extern void MainMenu_U3CAttemptQuickLoginU3Eb__15_0_mE971ABB7EF757755E9980F507531CAB5D16A2549 (void);
// 0x00000019 System.Void MainMenu::<AttemptQuickLogin>b__15_1(AppleAuth.Interfaces.IAppleError)
extern void MainMenu_U3CAttemptQuickLoginU3Eb__15_1_m3FFF75DA7F2BA4DB719F829F8C4623FFC639B467 (void);
// 0x0000001A System.Void MainMenu::<SignInWithApple>b__16_0(AppleAuth.Interfaces.ICredential)
extern void MainMenu_U3CSignInWithAppleU3Eb__16_0_mE59B75DF412205BE9CF77711BDBB75A56461D83F (void);
// 0x0000001B System.Void MainMenu::<SignInWithApple>b__16_1(AppleAuth.Interfaces.IAppleError)
extern void MainMenu_U3CSignInWithAppleU3Eb__16_1_m2E75C508FE4FBF8F3789A238CD92E5CC8516620B (void);
// 0x0000001C System.Void WebelinxAppleAuthController::Start()
extern void WebelinxAppleAuthController_Start_mC5EE45F17610B28F7978FD18AFC8504770675FA2 (void);
// 0x0000001D System.Void WebelinxAppleAuthController::Update()
extern void WebelinxAppleAuthController_Update_m6DCB8BF987E305E3FE6BDFFF0857C5AB7349C25D (void);
// 0x0000001E System.Void WebelinxAppleAuthController::AppleAuthHandler(System.Boolean)
extern void WebelinxAppleAuthController_AppleAuthHandler_m9C86C49F406E5269A87D21CDF0E43AEF6280BA07 (void);
// 0x0000001F System.Void WebelinxAppleAuthController::.ctor()
extern void WebelinxAppleAuthController__ctor_m353F2BF00DFF1AAA58B346725B2553036BC95C9E (void);
// 0x00000020 System.Void Authentication.WebelinxAnonymousAuthController::Awake()
extern void WebelinxAnonymousAuthController_Awake_mDC1A3C9F3989E599312109DF59CA727B807DFE96 (void);
// 0x00000021 System.Void Authentication.WebelinxAnonymousAuthController::LoginAnonymously()
extern void WebelinxAnonymousAuthController_LoginAnonymously_mB1FDA14714B88436EF79AB2C95EDC6293EE3C602 (void);
// 0x00000022 System.Void Authentication.WebelinxAnonymousAuthController::.ctor()
extern void WebelinxAnonymousAuthController__ctor_m19A3027562B3E2BE1D838024499B58647B3A4F81 (void);
// 0x00000023 System.Void Authentication.WebelinxFirebaseAuthManager::Awake()
extern void WebelinxFirebaseAuthManager_Awake_m23637A0CEA31F6BD70DBECA038DE4D90573A89A2 (void);
// 0x00000024 System.Void Authentication.WebelinxFirebaseAuthManager::InitializeFirebase()
extern void WebelinxFirebaseAuthManager_InitializeFirebase_mC06C2FF001E3CE03236086DF841C3AA2B729802F (void);
// 0x00000025 System.Void Authentication.WebelinxFirebaseAuthManager::SignInThroughProvider(System.String,Authentication.Providers)
extern void WebelinxFirebaseAuthManager_SignInThroughProvider_m053BD42E82A8F70CD2BF5E4487A977E37B18623C (void);
// 0x00000026 System.Void Authentication.WebelinxFirebaseAuthManager::LinkAccountWithAnotherProvider(System.String,Authentication.Providers)
extern void WebelinxFirebaseAuthManager_LinkAccountWithAnotherProvider_m520CC13E2A4771D5DF7167E773A41EA0B619E2AB (void);
// 0x00000027 System.Void Authentication.WebelinxFirebaseAuthManager::SignInAnonymously()
extern void WebelinxFirebaseAuthManager_SignInAnonymously_m871FBA002D82959565F2E81BD8E28CB7E4D4D2F7 (void);
// 0x00000028 System.Void Authentication.WebelinxFirebaseAuthManager::SignOut()
extern void WebelinxFirebaseAuthManager_SignOut_mB3FA9243EEFD54BCC0B77A337DD18027954406A5 (void);
// 0x00000029 Firebase.Auth.Credential Authentication.WebelinxFirebaseAuthManager::GetCredential(System.String,Authentication.Providers)
extern void WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3 (void);
// 0x0000002A System.Void Authentication.WebelinxFirebaseAuthManager::AuthStateChanged(System.Object,System.EventArgs)
extern void WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A (void);
// 0x0000002B System.Void Authentication.WebelinxFirebaseAuthManager::SetUserInformations(Firebase.Auth.FirebaseUser)
extern void WebelinxFirebaseAuthManager_SetUserInformations_mEF76DEF1EEA9516F497E6F12D673AC56F68AC85B (void);
// 0x0000002C System.Void Authentication.WebelinxFirebaseAuthManager::SyncFirebaseUserDataWithProviderData()
extern void WebelinxFirebaseAuthManager_SyncFirebaseUserDataWithProviderData_m4F5FAF541ED3A1D4F9F72E0B2247C3B4EE92BA36 (void);
// 0x0000002D System.Void Authentication.WebelinxFirebaseAuthManager::OnDestroy()
extern void WebelinxFirebaseAuthManager_OnDestroy_mBC21EF9D772490890B413F03E5224062C68B12AD (void);
// 0x0000002E System.Void Authentication.WebelinxFirebaseAuthManager::.ctor()
extern void WebelinxFirebaseAuthManager__ctor_m527265DC58D5B4B7EF12D3DB821C4CB5CFAF8549 (void);
// 0x0000002F System.Void Authentication.WebelinxFirebaseAuthManager::<SignInThroughProvider>b__9_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void WebelinxFirebaseAuthManager_U3CSignInThroughProviderU3Eb__9_0_m30F8227F6F0FEE4CECB0C0B592AA3E9203669A0C (void);
// 0x00000030 System.Void Authentication.WebelinxFirebaseAuthManager::<LinkAccountWithAnotherProvider>b__10_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void WebelinxFirebaseAuthManager_U3CLinkAccountWithAnotherProviderU3Eb__10_0_m531EBED0E89D1B164F16D95AC18B0F7198D57ED7 (void);
// 0x00000031 System.Void Authentication.WebelinxFirebaseAuthManager::<SignInAnonymously>b__11_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void WebelinxFirebaseAuthManager_U3CSignInAnonymouslyU3Eb__11_0_m42C7B73B3A78BCB3B5D7BF9C5599F6B8256D5B21 (void);
// 0x00000032 System.String Authentication.SignedInUserFirebaseInfo::get_UserUUID()
extern void SignedInUserFirebaseInfo_get_UserUUID_m1BA830CB5DC3B933B6DA95CF15A6D1883D78629C (void);
// 0x00000033 System.Void Authentication.SignedInUserFirebaseInfo::set_UserUUID(System.String)
extern void SignedInUserFirebaseInfo_set_UserUUID_mC1824612BC320BA142FDBE242726D2712BFCD902 (void);
// 0x00000034 System.String Authentication.SignedInUserFirebaseInfo::get_DisplayName()
extern void SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46 (void);
// 0x00000035 System.Void Authentication.SignedInUserFirebaseInfo::set_DisplayName(System.String)
extern void SignedInUserFirebaseInfo_set_DisplayName_m4F58543D0F421804556204E391108297C3C51D50 (void);
// 0x00000036 System.String Authentication.SignedInUserFirebaseInfo::get_Email()
extern void SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF (void);
// 0x00000037 System.Void Authentication.SignedInUserFirebaseInfo::set_Email(System.String)
extern void SignedInUserFirebaseInfo_set_Email_m69A025C44E648B762D68463C85FA74144897CE93 (void);
// 0x00000038 System.Boolean Authentication.SignedInUserFirebaseInfo::get_IsAnonymous()
extern void SignedInUserFirebaseInfo_get_IsAnonymous_m064F66405EE6B554D78E364FD555E180BDF2F0DB (void);
// 0x00000039 System.Void Authentication.SignedInUserFirebaseInfo::set_IsAnonymous(System.Boolean)
extern void SignedInUserFirebaseInfo_set_IsAnonymous_mEEC1E03C2960F04B8A8DCE046775D85100AC522D (void);
// 0x0000003A System.Uri Authentication.SignedInUserFirebaseInfo::get_PhotoUri()
extern void SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8 (void);
// 0x0000003B System.Void Authentication.SignedInUserFirebaseInfo::set_PhotoUri(System.Uri)
extern void SignedInUserFirebaseInfo_set_PhotoUri_m2A865C067ACE928BE0C30A959AF3EB5727157E6A (void);
// 0x0000003C System.Void Authentication.SignedInUserFirebaseInfo::.ctor()
extern void SignedInUserFirebaseInfo__ctor_m3814EC958E68B7047E0E3157273D4F46C17934CC (void);
// 0x0000003D System.Void Authentication.SignedInUserFirebaseInfo::SetData(Firebase.Auth.FirebaseUser)
extern void SignedInUserFirebaseInfo_SetData_m3CF4935FE552BD2AA83C83465FE1973147C12FB9 (void);
// 0x0000003E System.String Authentication.SignedInUserFirebaseInfo::ToString()
extern void SignedInUserFirebaseInfo_ToString_mDA7E55088D45FA5DDD17F6C5EA660A1B45EE3983 (void);
// 0x0000003F System.String Authentication.SignedInUserProviderInfo::get_ProviderNameID()
extern void SignedInUserProviderInfo_get_ProviderNameID_mD11C21B821492EF3EDC289324BDE73FB5E427E3E (void);
// 0x00000040 System.Void Authentication.SignedInUserProviderInfo::set_ProviderNameID(System.String)
extern void SignedInUserProviderInfo_set_ProviderNameID_m8422083B4FAAB0D2C1FB9F208CB1A7BB7C559828 (void);
// 0x00000041 System.String Authentication.SignedInUserProviderInfo::get_UserProviderID()
extern void SignedInUserProviderInfo_get_UserProviderID_m4D91634A20FC5A4B6D78749E72CC14153B9C962D (void);
// 0x00000042 System.Void Authentication.SignedInUserProviderInfo::set_UserProviderID(System.String)
extern void SignedInUserProviderInfo_set_UserProviderID_mC93C141881ABA67D15BCF271FE0D0F546D01C165 (void);
// 0x00000043 System.String Authentication.SignedInUserProviderInfo::get_Email()
extern void SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D (void);
// 0x00000044 System.Void Authentication.SignedInUserProviderInfo::set_Email(System.String)
extern void SignedInUserProviderInfo_set_Email_m851CCA9B5D5F346D62D5EF68A9F9E9A2B7DAFE74 (void);
// 0x00000045 System.String Authentication.SignedInUserProviderInfo::get_DisplayName()
extern void SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B (void);
// 0x00000046 System.Void Authentication.SignedInUserProviderInfo::set_DisplayName(System.String)
extern void SignedInUserProviderInfo_set_DisplayName_mDB53CCA9AB396016EF447DE6440A54CE0928EB81 (void);
// 0x00000047 System.Uri Authentication.SignedInUserProviderInfo::get_PhotoUri()
extern void SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355 (void);
// 0x00000048 System.Void Authentication.SignedInUserProviderInfo::set_PhotoUri(System.Uri)
extern void SignedInUserProviderInfo_set_PhotoUri_mE69209E187C563DC763A3349D67E7DDCA0D60AFC (void);
// 0x00000049 System.Void Authentication.SignedInUserProviderInfo::.ctor()
extern void SignedInUserProviderInfo__ctor_mE5F7CDFB0E15F3FC967D59B05E4995D2269225E6 (void);
// 0x0000004A System.Void Authentication.SignedInUserProviderInfo::SetData(Firebase.Auth.FirebaseUser)
extern void SignedInUserProviderInfo_SetData_mD617F76287BBA2E8C70996A7B1100C8855D01F2D (void);
// 0x0000004B System.String Authentication.SignedInUserProviderInfo::ToString()
extern void SignedInUserProviderInfo_ToString_m0CB989A3CA76D9F627FE4DA65A3884C2D44742E2 (void);
// 0x0000004C System.Void Authentication.WebelinxFirebaseAuthView::Awake()
extern void WebelinxFirebaseAuthView_Awake_m4D3CA124DE180FF292DAA935E31F5CFD616F5A95 (void);
// 0x0000004D System.Void Authentication.WebelinxFirebaseAuthView::Start()
extern void WebelinxFirebaseAuthView_Start_m82368ADFCEF22D8B4E7779B2C09A2C10BC48E4D4 (void);
// 0x0000004E System.Void Authentication.WebelinxFirebaseAuthView::UpdateUserStatusText(Firebase.Auth.FirebaseUser,System.String)
extern void WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A (void);
// 0x0000004F System.Void Authentication.WebelinxFirebaseAuthView::PrintUserInformations()
extern void WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C (void);
// 0x00000050 System.Void Authentication.WebelinxFirebaseAuthView::DisplayUserProfilePicture()
extern void WebelinxFirebaseAuthView_DisplayUserProfilePicture_mCFB8F1344ACD38DB02A6C872D137A27AC2C5618E (void);
// 0x00000051 System.Void Authentication.WebelinxFirebaseAuthView::OnUserSignedOut()
extern void WebelinxFirebaseAuthView_OnUserSignedOut_m496B5B915654C58C956EEAF9D5271A62620EE580 (void);
// 0x00000052 System.Void Authentication.WebelinxFirebaseAuthView::.ctor()
extern void WebelinxFirebaseAuthView__ctor_mB431C330881E3F1F0EAB7F346E3518DB21BDA144 (void);
// 0x00000053 System.Void GooglePlayGames.PluginVersion::.ctor()
extern void PluginVersion__ctor_m492A36F5A6C804F8254096C80B59312EAFE1AC76 (void);
// 0x00000054 System.Boolean GooglePlayGames.OurUtils.Logger::get_DebugLogEnabled()
extern void Logger_get_DebugLogEnabled_m3953BFF21DD3B54D640C75656E846C07EF02FB20 (void);
// 0x00000055 System.Void GooglePlayGames.OurUtils.Logger::set_DebugLogEnabled(System.Boolean)
extern void Logger_set_DebugLogEnabled_m5787BDC789E038FC7142BC2EF160318D0D1A918E (void);
// 0x00000056 System.Boolean GooglePlayGames.OurUtils.Logger::get_WarningLogEnabled()
extern void Logger_get_WarningLogEnabled_m2E33359F4E5A78915FD65DD1C51BDCA7AB54F833 (void);
// 0x00000057 System.Void GooglePlayGames.OurUtils.Logger::set_WarningLogEnabled(System.Boolean)
extern void Logger_set_WarningLogEnabled_mB416AC86C073873489EA2EFB51C2A70A93E148BF (void);
// 0x00000058 System.Void GooglePlayGames.OurUtils.Logger::d(System.String)
extern void Logger_d_m95C0BD29B60442754276E97D1FB7ED05E4AC31CE (void);
// 0x00000059 System.Void GooglePlayGames.OurUtils.Logger::w(System.String)
extern void Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB (void);
// 0x0000005A System.Void GooglePlayGames.OurUtils.Logger::e(System.String)
extern void Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8 (void);
// 0x0000005B System.String GooglePlayGames.OurUtils.Logger::describe(System.Byte[])
extern void Logger_describe_m6CF7621BCB43B5A4F721ECB71FE509F1EAFD0EB1 (void);
// 0x0000005C System.String GooglePlayGames.OurUtils.Logger::ToLogMessage(System.String,System.String,System.String)
extern void Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C (void);
// 0x0000005D System.Void GooglePlayGames.OurUtils.Logger::.ctor()
extern void Logger__ctor_mEC78EBB35D3E40205D9EFF7CEE10E40FBE187CAF (void);
// 0x0000005E System.Void GooglePlayGames.OurUtils.Logger::.cctor()
extern void Logger__cctor_m42BF51365CFF03BA353B071F75D18DE0C7F277FD (void);
// 0x0000005F System.Boolean GooglePlayGames.OurUtils.Misc::BuffersAreIdentical(System.Byte[],System.Byte[])
extern void Misc_BuffersAreIdentical_mC0F680186480CD64115596AD689421C6E0C38387 (void);
// 0x00000060 System.Byte[] GooglePlayGames.OurUtils.Misc::GetSubsetBytes(System.Byte[],System.Int32,System.Int32)
extern void Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965 (void);
// 0x00000061 T GooglePlayGames.OurUtils.Misc::CheckNotNull(T)
// 0x00000062 T GooglePlayGames.OurUtils.Misc::CheckNotNull(T,System.String)
// 0x00000063 System.Boolean GooglePlayGames.OurUtils.Misc::IsApiException(UnityEngine.AndroidJavaObject)
extern void Misc_IsApiException_m303491E5CA3BFA339C9A5EBFFE55F7C0C3BB9F99 (void);
// 0x00000064 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::CreateObject()
extern void PlayGamesHelperObject_CreateObject_m35E4ACCC42D970BFD0BA33D133F35B51A2137601 (void);
// 0x00000065 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Awake()
extern void PlayGamesHelperObject_Awake_m38AAE71B9B1F9F4D3F98E863119E2235A3B8251F (void);
// 0x00000066 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnDisable()
extern void PlayGamesHelperObject_OnDisable_m395CECEDE06F5F33F23B85418FCFB7FDBFE8B82C (void);
// 0x00000067 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunCoroutine(System.Collections.IEnumerator)
extern void PlayGamesHelperObject_RunCoroutine_mCDC6CBF287B6D8EBE869A970C5770808A95DC194 (void);
// 0x00000068 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunOnGameThread(System.Action)
extern void PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE (void);
// 0x00000069 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Update()
extern void PlayGamesHelperObject_Update_m803AA51E52E865F91C722354EE7156072705F5EE (void);
// 0x0000006A System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationFocus(System.Boolean)
extern void PlayGamesHelperObject_OnApplicationFocus_m9A19C80383E682429043888202B65FD0BD998994 (void);
// 0x0000006B System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationPause(System.Boolean)
extern void PlayGamesHelperObject_OnApplicationPause_mADFF6C814E67628969732FC37BCC560B296D4AB6 (void);
// 0x0000006C System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::AddFocusCallback(System.Action`1<System.Boolean>)
extern void PlayGamesHelperObject_AddFocusCallback_m6E1E5A60B199070EC622D2D47B50D8C20319B82E (void);
// 0x0000006D System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::RemoveFocusCallback(System.Action`1<System.Boolean>)
extern void PlayGamesHelperObject_RemoveFocusCallback_mEA5550B390C0301AB6AE9CFB43379E80E9326979 (void);
// 0x0000006E System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::AddPauseCallback(System.Action`1<System.Boolean>)
extern void PlayGamesHelperObject_AddPauseCallback_m97D958B110BFCA92A31FA362904CEF6AA1FCD00A (void);
// 0x0000006F System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::RemovePauseCallback(System.Action`1<System.Boolean>)
extern void PlayGamesHelperObject_RemovePauseCallback_m941669D42C479896AE60A61F2800C97DABE2EA5F (void);
// 0x00000070 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.ctor()
extern void PlayGamesHelperObject__ctor_m6F2894860B6C363E51DFB53E692ADCDCCB10D9B1 (void);
// 0x00000071 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.cctor()
extern void PlayGamesHelperObject__cctor_mED148D40A157EBA3FC978F09D87A3225C30ADF32 (void);
// 0x00000072 System.Boolean GooglePlayGames.BasicApi.CommonTypesUtil::StatusIsSuccess(GooglePlayGames.BasicApi.ResponseStatus)
extern void CommonTypesUtil_StatusIsSuccess_mD31DC8C3A99B06F08FE225CCE67855296D362060 (void);
// 0x00000073 System.Void GooglePlayGames.BasicApi.CommonTypesUtil::.ctor()
extern void CommonTypesUtil__ctor_mF73564494B79C6E17AD874B199D6086DEBA24ADC (void);
// 0x00000074 GooglePlayGames.BasicApi.SignInStatus GooglePlayGames.BasicApi.SignInHelper::ToSignInStatus(System.Int32)
extern void SignInHelper_ToSignInStatus_m859368773DCD252BDA21574CEFAE8B1D55DF08E0 (void);
// 0x00000075 System.Void GooglePlayGames.BasicApi.SignInHelper::SetPromptUiSignIn(System.Boolean)
extern void SignInHelper_SetPromptUiSignIn_mFF3C68D64C52D79EB92293B4929FD6D4FF96AE43 (void);
// 0x00000076 System.Boolean GooglePlayGames.BasicApi.SignInHelper::ShouldPromptUiSignIn()
extern void SignInHelper_ShouldPromptUiSignIn_m057F98856138682BB850BAFCB4E5FC918527563B (void);
// 0x00000077 System.Void GooglePlayGames.BasicApi.SignInHelper::.ctor()
extern void SignInHelper__ctor_mD4FBF450A975966ABA22A41BFD0787001FAF72BA (void);
// 0x00000078 System.Void GooglePlayGames.BasicApi.SignInHelper::.cctor()
extern void SignInHelper__cctor_m362E7EFFA0C88CB9F48C1072547815CE968EB927 (void);
// 0x00000079 System.Void GooglePlayGames.BasicApi.Video.CaptureOverlayStateListener::OnCaptureOverlayStateChanged(GooglePlayGames.BasicApi.VideoCaptureOverlayState)
// 0x0000007A System.Void GooglePlayGames.BasicApi.Video.IVideoClient::GetCaptureCapabilities(System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Video.VideoCapabilities>)
// 0x0000007B System.Void GooglePlayGames.BasicApi.Video.IVideoClient::ShowCaptureOverlay()
// 0x0000007C System.Void GooglePlayGames.BasicApi.Video.IVideoClient::GetCaptureState(System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Video.VideoCaptureState>)
// 0x0000007D System.Void GooglePlayGames.BasicApi.Video.IVideoClient::IsCaptureAvailable(GooglePlayGames.BasicApi.VideoCaptureMode,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>)
// 0x0000007E System.Boolean GooglePlayGames.BasicApi.Video.IVideoClient::IsCaptureSupported()
// 0x0000007F System.Void GooglePlayGames.BasicApi.Video.IVideoClient::RegisterCaptureOverlayStateChangedListener(GooglePlayGames.BasicApi.Video.CaptureOverlayStateListener)
// 0x00000080 System.Void GooglePlayGames.BasicApi.Video.IVideoClient::UnregisterCaptureOverlayStateChangedListener()
// 0x00000081 System.Void GooglePlayGames.BasicApi.Video.VideoCapabilities::.ctor(System.Boolean,System.Boolean,System.Boolean,System.Boolean[],System.Boolean[])
extern void VideoCapabilities__ctor_m93DB6D1B1295CF5B2E31AD7480AE282A62386CAD (void);
// 0x00000082 System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::get_IsCameraSupported()
extern void VideoCapabilities_get_IsCameraSupported_mF707F3BB960508F74817A00941D220C1E2B9ABCA (void);
// 0x00000083 System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::get_IsMicSupported()
extern void VideoCapabilities_get_IsMicSupported_m75E44B8B14E41C6CA5FCD57F3D9773A40ADAF75C (void);
// 0x00000084 System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::get_IsWriteStorageSupported()
extern void VideoCapabilities_get_IsWriteStorageSupported_m25F2CA6BB587E105C189E6A8E6D11505EEEA233D (void);
// 0x00000085 System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::SupportsCaptureMode(GooglePlayGames.BasicApi.VideoCaptureMode)
extern void VideoCapabilities_SupportsCaptureMode_m014CA89403FB9C0A684A10ACF384763EB1C4DBFE (void);
// 0x00000086 System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::SupportsQualityLevel(GooglePlayGames.BasicApi.VideoQualityLevel)
extern void VideoCapabilities_SupportsQualityLevel_mD6477AF89B8A9C2456B93EF605CD58BC26BDF402 (void);
// 0x00000087 System.String GooglePlayGames.BasicApi.Video.VideoCapabilities::ToString()
extern void VideoCapabilities_ToString_m794CEF4D0F7C3177434876703967E3C20343CF7B (void);
// 0x00000088 System.Void GooglePlayGames.BasicApi.Video.VideoCaptureState::.ctor(System.Boolean,GooglePlayGames.BasicApi.VideoCaptureMode,GooglePlayGames.BasicApi.VideoQualityLevel,System.Boolean,System.Boolean)
extern void VideoCaptureState__ctor_m9FB8FE52A8A8758BB5ABCF1EAEAE522FD93378E5 (void);
// 0x00000089 System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::get_IsCapturing()
extern void VideoCaptureState_get_IsCapturing_m7C38813203EB92BE4D9B82285039FE14D22C81C0 (void);
// 0x0000008A GooglePlayGames.BasicApi.VideoCaptureMode GooglePlayGames.BasicApi.Video.VideoCaptureState::get_CaptureMode()
extern void VideoCaptureState_get_CaptureMode_m7D36F460E825166FBE22C385EB811B9A723BEBD9 (void);
// 0x0000008B GooglePlayGames.BasicApi.VideoQualityLevel GooglePlayGames.BasicApi.Video.VideoCaptureState::get_QualityLevel()
extern void VideoCaptureState_get_QualityLevel_m1965EE6230E442EC1A852D5D620272D6F2F472E9 (void);
// 0x0000008C System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::get_IsOverlayVisible()
extern void VideoCaptureState_get_IsOverlayVisible_mA7A531329DBDBD0DCA777A5984455850011F5A6C (void);
// 0x0000008D System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::get_IsPaused()
extern void VideoCaptureState_get_IsPaused_m67EE8C8DFA66ED88D594F52CC483295CE334ED89 (void);
// 0x0000008E System.String GooglePlayGames.BasicApi.Video.VideoCaptureState::ToString()
extern void VideoCaptureState_ToString_m31D72AE6DCABEE2EB09502165175970030712606 (void);
// 0x0000008F System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::.ctor(System.Object,System.IntPtr)
extern void ConflictCallback__ctor_m7AA035B001255FC625B211DE6C1BBD2E85B66F8F (void);
// 0x00000090 System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern void ConflictCallback_Invoke_mF267F1669328BC164AF8E5D305A52D0EABAEBDD2 (void);
// 0x00000091 System.IAsyncResult GooglePlayGames.BasicApi.SavedGame.ConflictCallback::BeginInvoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],System.AsyncCallback,System.Object)
extern void ConflictCallback_BeginInvoke_mB3EF38DC068E36F9ADB2D538E09FE2FCAFBF4763 (void);
// 0x00000092 System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::EndInvoke(System.IAsyncResult)
extern void ConflictCallback_EndInvoke_mD934D9D128B5ECA394A7E59A8A7A9069EE454015 (void);
// 0x00000093 System.Void GooglePlayGames.BasicApi.SavedGame.ISavedGameClient::OpenWithAutomaticConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
// 0x00000094 System.Void GooglePlayGames.BasicApi.SavedGame.ISavedGameClient::OpenWithManualConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
// 0x00000095 System.Void GooglePlayGames.BasicApi.SavedGame.ISavedGameClient::ReadBinaryData(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>)
// 0x00000096 System.Void GooglePlayGames.BasicApi.SavedGame.ISavedGameClient::ShowSelectSavedGameUI(System.String,System.UInt32,System.Boolean,System.Boolean,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
// 0x00000097 System.Void GooglePlayGames.BasicApi.SavedGame.ISavedGameClient::CommitUpdate(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate,System.Byte[],System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
// 0x00000098 System.Void GooglePlayGames.BasicApi.SavedGame.ISavedGameClient::FetchAllSavedGames(GooglePlayGames.BasicApi.DataSource,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>)
// 0x00000099 System.Void GooglePlayGames.BasicApi.SavedGame.ISavedGameClient::Delete(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
// 0x0000009A System.Void GooglePlayGames.BasicApi.SavedGame.IConflictResolver::ChooseMetadata(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
// 0x0000009B System.Void GooglePlayGames.BasicApi.SavedGame.IConflictResolver::ResolveConflict(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate,System.Byte[])
// 0x0000009C System.Boolean GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_IsOpen()
// 0x0000009D System.String GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_Filename()
// 0x0000009E System.String GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_Description()
// 0x0000009F System.String GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_CoverImageURL()
// 0x000000A0 System.TimeSpan GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_TotalTimePlayed()
// 0x000000A1 System.DateTime GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata::get_LastModifiedTimestamp()
// 0x000000A2 System.Void GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::.ctor(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder)
extern void SavedGameMetadataUpdate__ctor_m5B508D8C6829985292C4AFC6D5A99F3A9E9AE259_AdjustorThunk (void);
// 0x000000A3 System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsDescriptionUpdated()
extern void SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1CBC7A1976D25DC56DA9D85A358BDF4CED9A26C8_AdjustorThunk (void);
// 0x000000A4 System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedDescription()
extern void SavedGameMetadataUpdate_get_UpdatedDescription_m8B5AD589A240684AE03B31448BA9808E4FAF399C_AdjustorThunk (void);
// 0x000000A5 System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsCoverImageUpdated()
extern void SavedGameMetadataUpdate_get_IsCoverImageUpdated_m8D0478AA3953E8790FC4AF2B8EDEFDD9AB58B7CB_AdjustorThunk (void);
// 0x000000A6 System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPngCoverImage()
extern void SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2E4D77C771ABD4A2E4EDD225A6F59FD05E4D32F1_AdjustorThunk (void);
// 0x000000A7 System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsPlayedTimeUpdated()
extern void SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F_AdjustorThunk (void);
// 0x000000A8 System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPlayedTime()
extern void SavedGameMetadataUpdate_get_UpdatedPlayedTime_m610F974D3E0EAA3F1476BF985C47ED6726012A00_AdjustorThunk (void);
// 0x000000A9 System.Void GooglePlayGames.BasicApi.Nearby.AdvertisingResult::.ctor(GooglePlayGames.BasicApi.ResponseStatus,System.String)
extern void AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB_AdjustorThunk (void);
// 0x000000AA System.Boolean GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Succeeded()
extern void AdvertisingResult_get_Succeeded_mB18393EF2668F7055A946844069C3FFD5C114427_AdjustorThunk (void);
// 0x000000AB GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Status()
extern void AdvertisingResult_get_Status_m9D257F1C520D8D7B3D75709985B2C6D59061BF89_AdjustorThunk (void);
// 0x000000AC System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_LocalEndpointName()
extern void AdvertisingResult_get_LocalEndpointName_mAED984971DC4130D6A7A2716C262B124F313DA3D_AdjustorThunk (void);
// 0x000000AD System.Void GooglePlayGames.BasicApi.Nearby.ConnectionRequest::.ctor(System.String,System.String,System.String,System.Byte[])
extern void ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43_AdjustorThunk (void);
// 0x000000AE GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_RemoteEndpoint()
extern void ConnectionRequest_get_RemoteEndpoint_mFB71214059AC49D631EEB34A4073B05BB9F098D9_AdjustorThunk (void);
// 0x000000AF System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_Payload()
extern void ConnectionRequest_get_Payload_m5FF54BC4CB9C49835FE68F6EFE6A853A824A37F0_AdjustorThunk (void);
// 0x000000B0 System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.ctor(System.Int64,System.String,GooglePlayGames.BasicApi.Nearby.ConnectionResponse_Status,System.Byte[])
extern void ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20_AdjustorThunk (void);
// 0x000000B1 System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_LocalClientId()
extern void ConnectionResponse_get_LocalClientId_mB134B291B50B17DD632719CC808C47F1B5128E94_AdjustorThunk (void);
// 0x000000B2 System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_RemoteEndpointId()
extern void ConnectionResponse_get_RemoteEndpointId_m767C97E62BC21CE88908266C7FA8947CE98BFC18_AdjustorThunk (void);
// 0x000000B3 GooglePlayGames.BasicApi.Nearby.ConnectionResponse_Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_ResponseStatus()
extern void ConnectionResponse_get_ResponseStatus_m592CB9981EE99165FDE5009096E61D37D9D0DF04_AdjustorThunk (void);
// 0x000000B4 System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_Payload()
extern void ConnectionResponse_get_Payload_mACC8A0806D0DD6E10E21F1977B42F9BCB26D1C7F_AdjustorThunk (void);
// 0x000000B5 GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Rejected(System.Int64,System.String)
extern void ConnectionResponse_Rejected_mA6985CBE633FF1E10C02C1E89B8752520816F4CB (void);
// 0x000000B6 GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::NetworkNotConnected(System.Int64,System.String)
extern void ConnectionResponse_NetworkNotConnected_m1F2A5E374AB38812517E2908D8423C5444AC8D7A (void);
// 0x000000B7 GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::InternalError(System.Int64,System.String)
extern void ConnectionResponse_InternalError_mDF78FF322A6E590BD4D524645B41A9A1DC800DD5 (void);
// 0x000000B8 GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::EndpointNotConnected(System.Int64,System.String)
extern void ConnectionResponse_EndpointNotConnected_m77D7E68783C9BAF4ADEE6C7675AF7185DF452D18 (void);
// 0x000000B9 GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Accepted(System.Int64,System.String,System.Byte[])
extern void ConnectionResponse_Accepted_mAEB28D825CEE048F714DE6DA3E571938B32E527F (void);
// 0x000000BA GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::AlreadyConnected(System.Int64,System.String)
extern void ConnectionResponse_AlreadyConnected_mB3BBBB13B00069C8B7691638AA5F542D2B325A5B (void);
// 0x000000BB System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.cctor()
extern void ConnectionResponse__cctor_m98A4E442ECE05DE88094B57CE48D5647EE345B4D (void);
// 0x000000BC System.Void GooglePlayGames.BasicApi.Nearby.EndpointDetails::.ctor(System.String,System.String,System.String)
extern void EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F_AdjustorThunk (void);
// 0x000000BD System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_EndpointId()
extern void EndpointDetails_get_EndpointId_m560A17AA21D05B3256DA8F0D07ED1FC55FD4A3BD_AdjustorThunk (void);
// 0x000000BE System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_Name()
extern void EndpointDetails_get_Name_m502483C84E998E62F76F450450F4D3D805E69E07_AdjustorThunk (void);
// 0x000000BF System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_ServiceId()
extern void EndpointDetails_get_ServiceId_mDB50FBA4F7F40740C49B63E76D92CE37F9090C63_AdjustorThunk (void);
// 0x000000C0 System.Void GooglePlayGames.BasicApi.Nearby.IMessageListener::OnMessageReceived(System.String,System.Byte[],System.Boolean)
// 0x000000C1 System.Void GooglePlayGames.BasicApi.Nearby.IMessageListener::OnRemoteEndpointDisconnected(System.String)
// 0x000000C2 System.Void GooglePlayGames.BasicApi.Nearby.IDiscoveryListener::OnEndpointFound(GooglePlayGames.BasicApi.Nearby.EndpointDetails)
// 0x000000C3 System.Void GooglePlayGames.BasicApi.Nearby.IDiscoveryListener::OnEndpointLost(System.String)
// 0x000000C4 System.Void GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::.ctor(System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>,System.Int64)
extern void NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C_AdjustorThunk (void);
// 0x000000C5 System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_LocalClientId()
extern void NearbyConnectionConfiguration_get_LocalClientId_m70F6E0E0867D4BE7FCDB0A157AC0DDA2F61B0D7D_AdjustorThunk (void);
// 0x000000C6 System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_InitializationCallback()
extern void NearbyConnectionConfiguration_get_InitializationCallback_m0E59187AC709946F6C28F84A0C80A0B36539990B_AdjustorThunk (void);
// 0x000000C7 System.Void GooglePlayGames.BasicApi.Events.Event::.ctor(System.String,System.String,System.String,System.String,System.UInt64,GooglePlayGames.BasicApi.Events.EventVisibility)
extern void Event__ctor_mB8BAC967B425C74E21ED3FA177061AEC433B669E (void);
// 0x000000C8 System.String GooglePlayGames.BasicApi.Events.Event::get_Id()
extern void Event_get_Id_m1B22AD589B9AB7AD75E6E1C9BE5ADC64436220CD (void);
// 0x000000C9 System.String GooglePlayGames.BasicApi.Events.Event::get_Name()
extern void Event_get_Name_mB8A76B3994A62C91CAFED8552A2407FB2E11B8CE (void);
// 0x000000CA System.String GooglePlayGames.BasicApi.Events.Event::get_Description()
extern void Event_get_Description_m8FA22DDBB79A4F7E4948B3E4CAA7F624A493F054 (void);
// 0x000000CB System.String GooglePlayGames.BasicApi.Events.Event::get_ImageUrl()
extern void Event_get_ImageUrl_m3C8358B358E199DE35AB1F80B5EED76274C54C93 (void);
// 0x000000CC System.UInt64 GooglePlayGames.BasicApi.Events.Event::get_CurrentCount()
extern void Event_get_CurrentCount_m3F7CD36B22DC6ADE3347D76D5684180D9B9F364A (void);
// 0x000000CD GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.BasicApi.Events.Event::get_Visibility()
extern void Event_get_Visibility_m883BC24B32C07A45EFF52FA0F26E467637071F0C (void);
// 0x000000CE System.String GooglePlayGames.BasicApi.Events.IEvent::get_Id()
// 0x000000CF System.String GooglePlayGames.BasicApi.Events.IEvent::get_Name()
// 0x000000D0 System.String GooglePlayGames.BasicApi.Events.IEvent::get_Description()
// 0x000000D1 System.String GooglePlayGames.BasicApi.Events.IEvent::get_ImageUrl()
// 0x000000D2 System.UInt64 GooglePlayGames.BasicApi.Events.IEvent::get_CurrentCount()
// 0x000000D3 GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.BasicApi.Events.IEvent::get_Visibility()
// 0x000000D4 System.Void MainMenu_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m036E1E18C115482EC760CB2A2851B38E75EAA653 (void);
// 0x000000D5 System.Void MainMenu_<>c__DisplayClass14_0::<CheckCredentialStatusForUserId>b__0(AppleAuth.Enums.CredentialState)
extern void U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__0_m0E51F1E824212F9985EDD569F5D0E2947979B370 (void);
// 0x000000D6 System.Void MainMenu_<>c__DisplayClass14_0::<CheckCredentialStatusForUserId>b__1(AppleAuth.Interfaces.IAppleError)
extern void U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__1_m2FAFDE5F0F9B6B2AEDFE777D0850E452BFB896B6 (void);
// 0x000000D7 System.Void WebelinxAppleAuthController_<>c::.cctor()
extern void U3CU3Ec__cctor_m18EE7693094C336A370250B287B562DF94875253 (void);
// 0x000000D8 System.Void WebelinxAppleAuthController_<>c::.ctor()
extern void U3CU3Ec__ctor_m7D4447F7EDCE4B591674E4A6048A309B5758BA08 (void);
// 0x000000D9 System.Void WebelinxAppleAuthController_<>c::<AppleAuthHandler>b__3_0(AppleAuth.Interfaces.ICredential)
extern void U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_0_m7CB32875FC16DF9BC460CA91228508118DC534AD (void);
// 0x000000DA System.Void WebelinxAppleAuthController_<>c::<AppleAuthHandler>b__3_1(AppleAuth.Interfaces.IAppleError)
extern void U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_1_m7A93743A0A6D77967CFF416B59225ED7EDAF2E93 (void);
// 0x000000DB System.Void Authentication.WebelinxFirebaseAuthManager_<>c::.cctor()
extern void U3CU3Ec__cctor_mEB73EA82F54AD404E29A52A9FED6395C77CF9BAF (void);
// 0x000000DC System.Void Authentication.WebelinxFirebaseAuthManager_<>c::.ctor()
extern void U3CU3Ec__ctor_mC540C513C869C7F6E8137A2F2BF6AD336599DDD1 (void);
// 0x000000DD System.Void Authentication.WebelinxFirebaseAuthManager_<>c::<SyncFirebaseUserDataWithProviderData>b__16_0(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_0_m361BD96B14EE16FCC87B6958136DB824453515A5 (void);
// 0x000000DE System.Void Authentication.WebelinxFirebaseAuthManager_<>c::<SyncFirebaseUserDataWithProviderData>b__16_1(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_1_m0F1243EEA002F490F1766EF845A67A40C7E060F7 (void);
// 0x000000DF System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m8C4C8977B9B5F82E13A6EFAE0F54ECE849EC214B (void);
// 0x000000E0 System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass8_0::<d>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CdU3Eb__0_m48020CDD6BCC8C1325960C7B8ACAF21C9C46590A (void);
// 0x000000E1 System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m7FE9968DBCD1D95BA7D9CE20A30FDF7976C1DDE1 (void);
// 0x000000E2 System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass9_0::<w>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CwU3Eb__0_m357B91D2A7DFEF1DA7865CAF4E998B09A0A120E7 (void);
// 0x000000E3 System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mA3755E25A067B9D324C45DF55C4088DD1480E0C6 (void);
// 0x000000E4 System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass10_0::<e>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CeU3Eb__0_m0B9AE2FE606892EF2909B280223B84E4646E8F73 (void);
// 0x000000E5 System.Void GooglePlayGames.OurUtils.Logger_<>c::.cctor()
extern void U3CU3Ec__cctor_mE9D701C70C5874BD30D40926128A74E673F87ED1 (void);
// 0x000000E6 System.Void GooglePlayGames.OurUtils.Logger_<>c::.ctor()
extern void U3CU3Ec__ctor_mAF62846782BCBABBC4CE9AD9D5A1EC351B71CE33 (void);
// 0x000000E7 System.Void GooglePlayGames.OurUtils.Logger_<>c::<ToLogMessage>b__12_0()
extern void U3CU3Ec_U3CToLogMessageU3Eb__12_0_m1B59C28B2C712520830759AEE37ECA1FFE239DF0 (void);
// 0x000000E8 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mC187ED07653BC33D8A6BA353FC0F6A74C1628F91 (void);
// 0x000000E9 System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject_<>c__DisplayClass10_0::<RunCoroutine>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CRunCoroutineU3Eb__0_m4C5D12A9EFD2B686E793203491194E13C6C5A46D (void);
// 0x000000EA System.Void GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::.cctor()
extern void U3CU3Ec__cctor_m570ADC6E83A04B439A89A6F2CA68567764DC2A40 (void);
// 0x000000EB System.Void GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::.ctor()
extern void U3CU3Ec__ctor_m4BD30BD945191F668A932130240743B6CCFA046E (void);
// 0x000000EC System.String GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::<ToString>b__14_0(System.Boolean)
extern void U3CU3Ec_U3CToStringU3Eb__14_0_m75FE11CA93BBBB7EE560F5115E951A2DE484A04B (void);
// 0x000000ED System.String GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::<ToString>b__14_1(System.Boolean)
extern void U3CU3Ec_U3CToStringU3Eb__14_1_m7F792E66E4DB839ECE1667C2895A77226FBE14BF (void);
// 0x000000EE GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::WithUpdatedDescription(System.String)
extern void Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B_AdjustorThunk (void);
// 0x000000EF GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::WithUpdatedPngCoverImage(System.Byte[])
extern void Builder_WithUpdatedPngCoverImage_mEF27FDD278DFFEF63F224B254933CB2127BBE223_AdjustorThunk (void);
// 0x000000F0 GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::WithUpdatedPlayedTime(System.TimeSpan)
extern void Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB_AdjustorThunk (void);
// 0x000000F1 GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::Build()
extern void Builder_Build_m0E1D8B5A4CEDDBEBA294C8FC744DE04DA810D803_AdjustorThunk (void);
static Il2CppMethodPointer s_methodPointers[241] = 
{
	GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C,
	GameMenuHandler_SetupAppleData_m5878285CA5056522A0FD6BDC58739DCEC59E6BC2,
	GameMenuHandler__ctor_m4A869F8A31ADAE4B56BE371C18A75C926184DA1A,
	LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069,
	LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B,
	LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608,
	LoginMenuHandler_UpdateLoadingMessage_m9F88D8943762FB49A8DCB56CDEC6BC63F35E7083,
	LoginMenuHandler__ctor_m7363D92585401D5357294C6D50A145D5F9905FD5,
	MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821,
	MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866,
	MainMenu_SignInWithAppleButtonPressed_mE369BF33E96F249C0B362FB5F5CF020951ADCFFB,
	MainMenu_InitializeLoginMenu_m6DF9DFCD30D84C572C0FAE153A293E972409D333,
	MainMenu_SetupLoginMenuForUnsupportedPlatform_m80C029D8C208B629AF2849559B8D653AD4FCF374,
	MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC,
	MainMenu_SetupLoginMenuForCheckingCredentials_m16AEE25404D91E530C121EFFFF3257C074EEBF25,
	MainMenu_SetupLoginMenuForQuickLoginAttempt_m351D9B48275D2D2587C3FFD3F5F4D6BF002C948F,
	MainMenu_SetupLoginMenuForAppleSignIn_m314FDA3B2F46F68904F84841840033848ADB585D,
	MainMenu_SetupGameMenu_m9141BCE4B7637E206CF1718C18189AC0D6DDCFF9,
	MainMenu_CheckCredentialStatusForUserId_m33A8D3E7479359851349A1532CC5031D03AFE3B2,
	MainMenu_AttemptQuickLogin_m1C4F040B20F22A88A09469378F69540EFEAFB883,
	MainMenu_SignInWithApple_m54E769319FF0899CCD150E6BB1602E3E5ECFBE94,
	MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5,
	MainMenu_U3CInitializeLoginMenuU3Eb__7_0_mC33C351ABC83249E18FA1ED7E2C2010992174DFE,
	MainMenu_U3CAttemptQuickLoginU3Eb__15_0_mE971ABB7EF757755E9980F507531CAB5D16A2549,
	MainMenu_U3CAttemptQuickLoginU3Eb__15_1_m3FFF75DA7F2BA4DB719F829F8C4623FFC639B467,
	MainMenu_U3CSignInWithAppleU3Eb__16_0_mE59B75DF412205BE9CF77711BDBB75A56461D83F,
	MainMenu_U3CSignInWithAppleU3Eb__16_1_m2E75C508FE4FBF8F3789A238CD92E5CC8516620B,
	WebelinxAppleAuthController_Start_mC5EE45F17610B28F7978FD18AFC8504770675FA2,
	WebelinxAppleAuthController_Update_m6DCB8BF987E305E3FE6BDFFF0857C5AB7349C25D,
	WebelinxAppleAuthController_AppleAuthHandler_m9C86C49F406E5269A87D21CDF0E43AEF6280BA07,
	WebelinxAppleAuthController__ctor_m353F2BF00DFF1AAA58B346725B2553036BC95C9E,
	WebelinxAnonymousAuthController_Awake_mDC1A3C9F3989E599312109DF59CA727B807DFE96,
	WebelinxAnonymousAuthController_LoginAnonymously_mB1FDA14714B88436EF79AB2C95EDC6293EE3C602,
	WebelinxAnonymousAuthController__ctor_m19A3027562B3E2BE1D838024499B58647B3A4F81,
	WebelinxFirebaseAuthManager_Awake_m23637A0CEA31F6BD70DBECA038DE4D90573A89A2,
	WebelinxFirebaseAuthManager_InitializeFirebase_mC06C2FF001E3CE03236086DF841C3AA2B729802F,
	WebelinxFirebaseAuthManager_SignInThroughProvider_m053BD42E82A8F70CD2BF5E4487A977E37B18623C,
	WebelinxFirebaseAuthManager_LinkAccountWithAnotherProvider_m520CC13E2A4771D5DF7167E773A41EA0B619E2AB,
	WebelinxFirebaseAuthManager_SignInAnonymously_m871FBA002D82959565F2E81BD8E28CB7E4D4D2F7,
	WebelinxFirebaseAuthManager_SignOut_mB3FA9243EEFD54BCC0B77A337DD18027954406A5,
	WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3,
	WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A,
	WebelinxFirebaseAuthManager_SetUserInformations_mEF76DEF1EEA9516F497E6F12D673AC56F68AC85B,
	WebelinxFirebaseAuthManager_SyncFirebaseUserDataWithProviderData_m4F5FAF541ED3A1D4F9F72E0B2247C3B4EE92BA36,
	WebelinxFirebaseAuthManager_OnDestroy_mBC21EF9D772490890B413F03E5224062C68B12AD,
	WebelinxFirebaseAuthManager__ctor_m527265DC58D5B4B7EF12D3DB821C4CB5CFAF8549,
	WebelinxFirebaseAuthManager_U3CSignInThroughProviderU3Eb__9_0_m30F8227F6F0FEE4CECB0C0B592AA3E9203669A0C,
	WebelinxFirebaseAuthManager_U3CLinkAccountWithAnotherProviderU3Eb__10_0_m531EBED0E89D1B164F16D95AC18B0F7198D57ED7,
	WebelinxFirebaseAuthManager_U3CSignInAnonymouslyU3Eb__11_0_m42C7B73B3A78BCB3B5D7BF9C5599F6B8256D5B21,
	SignedInUserFirebaseInfo_get_UserUUID_m1BA830CB5DC3B933B6DA95CF15A6D1883D78629C,
	SignedInUserFirebaseInfo_set_UserUUID_mC1824612BC320BA142FDBE242726D2712BFCD902,
	SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46,
	SignedInUserFirebaseInfo_set_DisplayName_m4F58543D0F421804556204E391108297C3C51D50,
	SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF,
	SignedInUserFirebaseInfo_set_Email_m69A025C44E648B762D68463C85FA74144897CE93,
	SignedInUserFirebaseInfo_get_IsAnonymous_m064F66405EE6B554D78E364FD555E180BDF2F0DB,
	SignedInUserFirebaseInfo_set_IsAnonymous_mEEC1E03C2960F04B8A8DCE046775D85100AC522D,
	SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8,
	SignedInUserFirebaseInfo_set_PhotoUri_m2A865C067ACE928BE0C30A959AF3EB5727157E6A,
	SignedInUserFirebaseInfo__ctor_m3814EC958E68B7047E0E3157273D4F46C17934CC,
	SignedInUserFirebaseInfo_SetData_m3CF4935FE552BD2AA83C83465FE1973147C12FB9,
	SignedInUserFirebaseInfo_ToString_mDA7E55088D45FA5DDD17F6C5EA660A1B45EE3983,
	SignedInUserProviderInfo_get_ProviderNameID_mD11C21B821492EF3EDC289324BDE73FB5E427E3E,
	SignedInUserProviderInfo_set_ProviderNameID_m8422083B4FAAB0D2C1FB9F208CB1A7BB7C559828,
	SignedInUserProviderInfo_get_UserProviderID_m4D91634A20FC5A4B6D78749E72CC14153B9C962D,
	SignedInUserProviderInfo_set_UserProviderID_mC93C141881ABA67D15BCF271FE0D0F546D01C165,
	SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D,
	SignedInUserProviderInfo_set_Email_m851CCA9B5D5F346D62D5EF68A9F9E9A2B7DAFE74,
	SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B,
	SignedInUserProviderInfo_set_DisplayName_mDB53CCA9AB396016EF447DE6440A54CE0928EB81,
	SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355,
	SignedInUserProviderInfo_set_PhotoUri_mE69209E187C563DC763A3349D67E7DDCA0D60AFC,
	SignedInUserProviderInfo__ctor_mE5F7CDFB0E15F3FC967D59B05E4995D2269225E6,
	SignedInUserProviderInfo_SetData_mD617F76287BBA2E8C70996A7B1100C8855D01F2D,
	SignedInUserProviderInfo_ToString_m0CB989A3CA76D9F627FE4DA65A3884C2D44742E2,
	WebelinxFirebaseAuthView_Awake_m4D3CA124DE180FF292DAA935E31F5CFD616F5A95,
	WebelinxFirebaseAuthView_Start_m82368ADFCEF22D8B4E7779B2C09A2C10BC48E4D4,
	WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A,
	WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C,
	WebelinxFirebaseAuthView_DisplayUserProfilePicture_mCFB8F1344ACD38DB02A6C872D137A27AC2C5618E,
	WebelinxFirebaseAuthView_OnUserSignedOut_m496B5B915654C58C956EEAF9D5271A62620EE580,
	WebelinxFirebaseAuthView__ctor_mB431C330881E3F1F0EAB7F346E3518DB21BDA144,
	PluginVersion__ctor_m492A36F5A6C804F8254096C80B59312EAFE1AC76,
	Logger_get_DebugLogEnabled_m3953BFF21DD3B54D640C75656E846C07EF02FB20,
	Logger_set_DebugLogEnabled_m5787BDC789E038FC7142BC2EF160318D0D1A918E,
	Logger_get_WarningLogEnabled_m2E33359F4E5A78915FD65DD1C51BDCA7AB54F833,
	Logger_set_WarningLogEnabled_mB416AC86C073873489EA2EFB51C2A70A93E148BF,
	Logger_d_m95C0BD29B60442754276E97D1FB7ED05E4AC31CE,
	Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB,
	Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8,
	Logger_describe_m6CF7621BCB43B5A4F721ECB71FE509F1EAFD0EB1,
	Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C,
	Logger__ctor_mEC78EBB35D3E40205D9EFF7CEE10E40FBE187CAF,
	Logger__cctor_m42BF51365CFF03BA353B071F75D18DE0C7F277FD,
	Misc_BuffersAreIdentical_mC0F680186480CD64115596AD689421C6E0C38387,
	Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965,
	NULL,
	NULL,
	Misc_IsApiException_m303491E5CA3BFA339C9A5EBFFE55F7C0C3BB9F99,
	PlayGamesHelperObject_CreateObject_m35E4ACCC42D970BFD0BA33D133F35B51A2137601,
	PlayGamesHelperObject_Awake_m38AAE71B9B1F9F4D3F98E863119E2235A3B8251F,
	PlayGamesHelperObject_OnDisable_m395CECEDE06F5F33F23B85418FCFB7FDBFE8B82C,
	PlayGamesHelperObject_RunCoroutine_mCDC6CBF287B6D8EBE869A970C5770808A95DC194,
	PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE,
	PlayGamesHelperObject_Update_m803AA51E52E865F91C722354EE7156072705F5EE,
	PlayGamesHelperObject_OnApplicationFocus_m9A19C80383E682429043888202B65FD0BD998994,
	PlayGamesHelperObject_OnApplicationPause_mADFF6C814E67628969732FC37BCC560B296D4AB6,
	PlayGamesHelperObject_AddFocusCallback_m6E1E5A60B199070EC622D2D47B50D8C20319B82E,
	PlayGamesHelperObject_RemoveFocusCallback_mEA5550B390C0301AB6AE9CFB43379E80E9326979,
	PlayGamesHelperObject_AddPauseCallback_m97D958B110BFCA92A31FA362904CEF6AA1FCD00A,
	PlayGamesHelperObject_RemovePauseCallback_m941669D42C479896AE60A61F2800C97DABE2EA5F,
	PlayGamesHelperObject__ctor_m6F2894860B6C363E51DFB53E692ADCDCCB10D9B1,
	PlayGamesHelperObject__cctor_mED148D40A157EBA3FC978F09D87A3225C30ADF32,
	CommonTypesUtil_StatusIsSuccess_mD31DC8C3A99B06F08FE225CCE67855296D362060,
	CommonTypesUtil__ctor_mF73564494B79C6E17AD874B199D6086DEBA24ADC,
	SignInHelper_ToSignInStatus_m859368773DCD252BDA21574CEFAE8B1D55DF08E0,
	SignInHelper_SetPromptUiSignIn_mFF3C68D64C52D79EB92293B4929FD6D4FF96AE43,
	SignInHelper_ShouldPromptUiSignIn_m057F98856138682BB850BAFCB4E5FC918527563B,
	SignInHelper__ctor_mD4FBF450A975966ABA22A41BFD0787001FAF72BA,
	SignInHelper__cctor_m362E7EFFA0C88CB9F48C1072547815CE968EB927,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VideoCapabilities__ctor_m93DB6D1B1295CF5B2E31AD7480AE282A62386CAD,
	VideoCapabilities_get_IsCameraSupported_mF707F3BB960508F74817A00941D220C1E2B9ABCA,
	VideoCapabilities_get_IsMicSupported_m75E44B8B14E41C6CA5FCD57F3D9773A40ADAF75C,
	VideoCapabilities_get_IsWriteStorageSupported_m25F2CA6BB587E105C189E6A8E6D11505EEEA233D,
	VideoCapabilities_SupportsCaptureMode_m014CA89403FB9C0A684A10ACF384763EB1C4DBFE,
	VideoCapabilities_SupportsQualityLevel_mD6477AF89B8A9C2456B93EF605CD58BC26BDF402,
	VideoCapabilities_ToString_m794CEF4D0F7C3177434876703967E3C20343CF7B,
	VideoCaptureState__ctor_m9FB8FE52A8A8758BB5ABCF1EAEAE522FD93378E5,
	VideoCaptureState_get_IsCapturing_m7C38813203EB92BE4D9B82285039FE14D22C81C0,
	VideoCaptureState_get_CaptureMode_m7D36F460E825166FBE22C385EB811B9A723BEBD9,
	VideoCaptureState_get_QualityLevel_m1965EE6230E442EC1A852D5D620272D6F2F472E9,
	VideoCaptureState_get_IsOverlayVisible_mA7A531329DBDBD0DCA777A5984455850011F5A6C,
	VideoCaptureState_get_IsPaused_m67EE8C8DFA66ED88D594F52CC483295CE334ED89,
	VideoCaptureState_ToString_m31D72AE6DCABEE2EB09502165175970030712606,
	ConflictCallback__ctor_m7AA035B001255FC625B211DE6C1BBD2E85B66F8F,
	ConflictCallback_Invoke_mF267F1669328BC164AF8E5D305A52D0EABAEBDD2,
	ConflictCallback_BeginInvoke_mB3EF38DC068E36F9ADB2D538E09FE2FCAFBF4763,
	ConflictCallback_EndInvoke_mD934D9D128B5ECA394A7E59A8A7A9069EE454015,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SavedGameMetadataUpdate__ctor_m5B508D8C6829985292C4AFC6D5A99F3A9E9AE259_AdjustorThunk,
	SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1CBC7A1976D25DC56DA9D85A358BDF4CED9A26C8_AdjustorThunk,
	SavedGameMetadataUpdate_get_UpdatedDescription_m8B5AD589A240684AE03B31448BA9808E4FAF399C_AdjustorThunk,
	SavedGameMetadataUpdate_get_IsCoverImageUpdated_m8D0478AA3953E8790FC4AF2B8EDEFDD9AB58B7CB_AdjustorThunk,
	SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2E4D77C771ABD4A2E4EDD225A6F59FD05E4D32F1_AdjustorThunk,
	SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F_AdjustorThunk,
	SavedGameMetadataUpdate_get_UpdatedPlayedTime_m610F974D3E0EAA3F1476BF985C47ED6726012A00_AdjustorThunk,
	AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB_AdjustorThunk,
	AdvertisingResult_get_Succeeded_mB18393EF2668F7055A946844069C3FFD5C114427_AdjustorThunk,
	AdvertisingResult_get_Status_m9D257F1C520D8D7B3D75709985B2C6D59061BF89_AdjustorThunk,
	AdvertisingResult_get_LocalEndpointName_mAED984971DC4130D6A7A2716C262B124F313DA3D_AdjustorThunk,
	ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43_AdjustorThunk,
	ConnectionRequest_get_RemoteEndpoint_mFB71214059AC49D631EEB34A4073B05BB9F098D9_AdjustorThunk,
	ConnectionRequest_get_Payload_m5FF54BC4CB9C49835FE68F6EFE6A853A824A37F0_AdjustorThunk,
	ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20_AdjustorThunk,
	ConnectionResponse_get_LocalClientId_mB134B291B50B17DD632719CC808C47F1B5128E94_AdjustorThunk,
	ConnectionResponse_get_RemoteEndpointId_m767C97E62BC21CE88908266C7FA8947CE98BFC18_AdjustorThunk,
	ConnectionResponse_get_ResponseStatus_m592CB9981EE99165FDE5009096E61D37D9D0DF04_AdjustorThunk,
	ConnectionResponse_get_Payload_mACC8A0806D0DD6E10E21F1977B42F9BCB26D1C7F_AdjustorThunk,
	ConnectionResponse_Rejected_mA6985CBE633FF1E10C02C1E89B8752520816F4CB,
	ConnectionResponse_NetworkNotConnected_m1F2A5E374AB38812517E2908D8423C5444AC8D7A,
	ConnectionResponse_InternalError_mDF78FF322A6E590BD4D524645B41A9A1DC800DD5,
	ConnectionResponse_EndpointNotConnected_m77D7E68783C9BAF4ADEE6C7675AF7185DF452D18,
	ConnectionResponse_Accepted_mAEB28D825CEE048F714DE6DA3E571938B32E527F,
	ConnectionResponse_AlreadyConnected_mB3BBBB13B00069C8B7691638AA5F542D2B325A5B,
	ConnectionResponse__cctor_m98A4E442ECE05DE88094B57CE48D5647EE345B4D,
	EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F_AdjustorThunk,
	EndpointDetails_get_EndpointId_m560A17AA21D05B3256DA8F0D07ED1FC55FD4A3BD_AdjustorThunk,
	EndpointDetails_get_Name_m502483C84E998E62F76F450450F4D3D805E69E07_AdjustorThunk,
	EndpointDetails_get_ServiceId_mDB50FBA4F7F40740C49B63E76D92CE37F9090C63_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C_AdjustorThunk,
	NearbyConnectionConfiguration_get_LocalClientId_m70F6E0E0867D4BE7FCDB0A157AC0DDA2F61B0D7D_AdjustorThunk,
	NearbyConnectionConfiguration_get_InitializationCallback_m0E59187AC709946F6C28F84A0C80A0B36539990B_AdjustorThunk,
	Event__ctor_mB8BAC967B425C74E21ED3FA177061AEC433B669E,
	Event_get_Id_m1B22AD589B9AB7AD75E6E1C9BE5ADC64436220CD,
	Event_get_Name_mB8A76B3994A62C91CAFED8552A2407FB2E11B8CE,
	Event_get_Description_m8FA22DDBB79A4F7E4948B3E4CAA7F624A493F054,
	Event_get_ImageUrl_m3C8358B358E199DE35AB1F80B5EED76274C54C93,
	Event_get_CurrentCount_m3F7CD36B22DC6ADE3347D76D5684180D9B9F364A,
	Event_get_Visibility_m883BC24B32C07A45EFF52FA0F26E467637071F0C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass14_0__ctor_m036E1E18C115482EC760CB2A2851B38E75EAA653,
	U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__0_m0E51F1E824212F9985EDD569F5D0E2947979B370,
	U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__1_m2FAFDE5F0F9B6B2AEDFE777D0850E452BFB896B6,
	U3CU3Ec__cctor_m18EE7693094C336A370250B287B562DF94875253,
	U3CU3Ec__ctor_m7D4447F7EDCE4B591674E4A6048A309B5758BA08,
	U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_0_m7CB32875FC16DF9BC460CA91228508118DC534AD,
	U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_1_m7A93743A0A6D77967CFF416B59225ED7EDAF2E93,
	U3CU3Ec__cctor_mEB73EA82F54AD404E29A52A9FED6395C77CF9BAF,
	U3CU3Ec__ctor_mC540C513C869C7F6E8137A2F2BF6AD336599DDD1,
	U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_0_m361BD96B14EE16FCC87B6958136DB824453515A5,
	U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_1_m0F1243EEA002F490F1766EF845A67A40C7E060F7,
	U3CU3Ec__DisplayClass8_0__ctor_m8C4C8977B9B5F82E13A6EFAE0F54ECE849EC214B,
	U3CU3Ec__DisplayClass8_0_U3CdU3Eb__0_m48020CDD6BCC8C1325960C7B8ACAF21C9C46590A,
	U3CU3Ec__DisplayClass9_0__ctor_m7FE9968DBCD1D95BA7D9CE20A30FDF7976C1DDE1,
	U3CU3Ec__DisplayClass9_0_U3CwU3Eb__0_m357B91D2A7DFEF1DA7865CAF4E998B09A0A120E7,
	U3CU3Ec__DisplayClass10_0__ctor_mA3755E25A067B9D324C45DF55C4088DD1480E0C6,
	U3CU3Ec__DisplayClass10_0_U3CeU3Eb__0_m0B9AE2FE606892EF2909B280223B84E4646E8F73,
	U3CU3Ec__cctor_mE9D701C70C5874BD30D40926128A74E673F87ED1,
	U3CU3Ec__ctor_mAF62846782BCBABBC4CE9AD9D5A1EC351B71CE33,
	U3CU3Ec_U3CToLogMessageU3Eb__12_0_m1B59C28B2C712520830759AEE37ECA1FFE239DF0,
	U3CU3Ec__DisplayClass10_0__ctor_mC187ED07653BC33D8A6BA353FC0F6A74C1628F91,
	U3CU3Ec__DisplayClass10_0_U3CRunCoroutineU3Eb__0_m4C5D12A9EFD2B686E793203491194E13C6C5A46D,
	U3CU3Ec__cctor_m570ADC6E83A04B439A89A6F2CA68567764DC2A40,
	U3CU3Ec__ctor_m4BD30BD945191F668A932130240743B6CCFA046E,
	U3CU3Ec_U3CToStringU3Eb__14_0_m75FE11CA93BBBB7EE560F5115E951A2DE484A04B,
	U3CU3Ec_U3CToStringU3Eb__14_1_m7F792E66E4DB839ECE1667C2895A77226FBE14BF,
	Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B_AdjustorThunk,
	Builder_WithUpdatedPngCoverImage_mEF27FDD278DFFEF63F224B254933CB2127BBE223_AdjustorThunk,
	Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB_AdjustorThunk,
	Builder_Build_m0E1D8B5A4CEDDBEBA294C8FC744DE04DA810D803_AdjustorThunk,
};
static const int32_t s_InvokerIndices[241] = 
{
	31,
	27,
	23,
	31,
	904,
	42,
	273,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	124,
	124,
	23,
	23,
	58,
	27,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	102,
	31,
	14,
	26,
	23,
	26,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	26,
	14,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	49,
	763,
	49,
	763,
	111,
	111,
	111,
	0,
	2,
	23,
	3,
	99,
	151,
	-1,
	-1,
	109,
	3,
	23,
	23,
	111,
	111,
	23,
	31,
	31,
	111,
	109,
	111,
	109,
	23,
	3,
	46,
	23,
	21,
	763,
	49,
	23,
	3,
	32,
	26,
	23,
	26,
	62,
	102,
	26,
	23,
	1376,
	102,
	102,
	102,
	30,
	30,
	14,
	1377,
	102,
	10,
	10,
	102,
	102,
	14,
	163,
	731,
	1378,
	26,
	1379,
	1380,
	27,
	1381,
	1382,
	62,
	26,
	26,
	1383,
	102,
	14,
	14,
	14,
	268,
	266,
	1384,
	102,
	14,
	102,
	14,
	102,
	1385,
	62,
	102,
	10,
	14,
	366,
	1389,
	14,
	1390,
	130,
	14,
	10,
	14,
	1391,
	1391,
	1391,
	1391,
	1392,
	1391,
	3,
	156,
	14,
	14,
	14,
	98,
	26,
	1393,
	26,
	129,
	130,
	14,
	1394,
	14,
	14,
	14,
	14,
	130,
	10,
	14,
	14,
	14,
	14,
	130,
	10,
	23,
	32,
	26,
	3,
	23,
	26,
	26,
	3,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	3,
	23,
	300,
	300,
	1386,
	1386,
	1387,
	1388,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000061, { 0, 1 } },
	{ 0x06000062, { 1, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)2, 12416 },
	{ (Il2CppRGCTXDataType)2, 12417 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	241,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
};
