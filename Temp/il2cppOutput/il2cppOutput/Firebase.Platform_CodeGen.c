﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Collections.Generic.List`1<System.Exception> Firebase.ExceptionAggregator::get_Exceptions()
extern void ExceptionAggregator_get_Exceptions_mB8771E2A0D9AFC2BF5A29214B58FAE0621306BE2 (void);
// 0x00000002 System.Exception Firebase.ExceptionAggregator::GetAndClearPendingExceptions()
extern void ExceptionAggregator_GetAndClearPendingExceptions_m0428C791FBC34F687E53E93C8283BF1BDAD52705 (void);
// 0x00000003 System.Void Firebase.ExceptionAggregator::ThrowAndClearPendingExceptions()
extern void ExceptionAggregator_ThrowAndClearPendingExceptions_m43EDFA771E277D927E98305CF105F598C2A19D96 (void);
// 0x00000004 System.Exception Firebase.ExceptionAggregator::LogException(System.Exception)
extern void ExceptionAggregator_LogException_m2D22E7AA0907E7185DDF0F7F5C2EE702265CB12C (void);
// 0x00000005 System.Void Firebase.ExceptionAggregator::Wrap(System.Action)
extern void ExceptionAggregator_Wrap_m93A9BD0DC888F1D02E77860288095CB49D4CFFC2 (void);
// 0x00000006 System.Void Firebase.Dispatcher::.ctor()
extern void Dispatcher__ctor_m524290773AE51A0B146046705A07E78B61C27268 (void);
// 0x00000007 TResult Firebase.Dispatcher::Run(System.Func`1<TResult>)
// 0x00000008 System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsync(System.Func`1<TResult>)
// 0x00000009 System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsyncNow(System.Func`1<TResult>)
// 0x0000000A System.Boolean Firebase.Dispatcher::ManagesThisThread()
extern void Dispatcher_ManagesThisThread_mCC706F89C23B9407225833D81E5B359F6BDA2A3D (void);
// 0x0000000B System.Void Firebase.Dispatcher::PollJobs()
extern void Dispatcher_PollJobs_m27F0E072ED9B5297E072A0AAFD37D3D76B748875 (void);
// 0x0000000C TResult Firebase.Dispatcher_CallbackStorage`1::get_Result()
// 0x0000000D System.Void Firebase.Dispatcher_CallbackStorage`1::set_Result(TResult)
// 0x0000000E System.Exception Firebase.Dispatcher_CallbackStorage`1::get_Exception()
// 0x0000000F System.Void Firebase.Dispatcher_CallbackStorage`1::set_Exception(System.Exception)
// 0x00000010 System.Void Firebase.Dispatcher_CallbackStorage`1::.ctor()
// 0x00000011 System.Void Firebase.Dispatcher_<>c__DisplayClass4_0`1::.ctor()
// 0x00000012 System.Void Firebase.Dispatcher_<>c__DisplayClass4_0`1::<Run>b__0()
// 0x00000013 System.Void Firebase.Dispatcher_<>c__DisplayClass5_0`1::.ctor()
// 0x00000014 System.Void Firebase.Dispatcher_<>c__DisplayClass5_0`1::<RunAsync>b__0()
// 0x00000015 Firebase.Unity.UnityLoggingService Firebase.Unity.UnityLoggingService::get_Instance()
extern void UnityLoggingService_get_Instance_m021810065C28BAD66B00CA2EC2DC55AD3EE85A4E (void);
// 0x00000016 System.Void Firebase.Unity.UnityLoggingService::.ctor()
extern void UnityLoggingService__ctor_m3BCC6353BEADE287DF1A0C9781B6187576523441 (void);
// 0x00000017 System.Void Firebase.Unity.UnityLoggingService::.cctor()
extern void UnityLoggingService__cctor_m07430A774E2E27DE95B135565549505BC99BFAF3 (void);
// 0x00000018 System.Void Firebase.Unity.UnityPlatformServices::SetupServices()
extern void UnityPlatformServices_SetupServices_mC37344AC6AB871E0DCEC44A26FBC98BB05776CE4 (void);
// 0x00000019 System.Void Firebase.Unity.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern void UnitySynchronizationContext__ctor_m47697B161718D309E5BDEE021A7652F2610B3811 (void);
// 0x0000001A System.Void Firebase.Unity.UnitySynchronizationContext::Create(UnityEngine.GameObject)
extern void UnitySynchronizationContext_Create_m457A7DF239D69F664A4C5A71A51831D10F8C5C9B (void);
// 0x0000001B System.Void Firebase.Unity.UnitySynchronizationContext::Destroy()
extern void UnitySynchronizationContext_Destroy_m173DAA6D418F0749046FC98BE3244ECF0CD37188 (void);
// 0x0000001C System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext::GetThreadEvent()
extern void UnitySynchronizationContext_GetThreadEvent_mD3A2B8BC1310D4C5F17918280F14EF089DD8164A (void);
// 0x0000001D System.Void Firebase.Unity.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Post_m4450F9D1EEF52CB7EED4C27ACEFC4F5324C6BFBF (void);
// 0x0000001E System.Void Firebase.Unity.UnitySynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Send_m537EE81F72568A13624BBD4CB4D8796946722A6D (void);
// 0x0000001F System.Void Firebase.Unity.UnitySynchronizationContext::.cctor()
extern void UnitySynchronizationContext__cctor_m3B5D4F04B0CEEF803ED5E341A4AFCD813F706402 (void);
// 0x00000020 System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir::get_CallbackQueue()
extern void SynchronizationContextBehavoir_get_CallbackQueue_mE2EE07CB1868612C976725049F507F5D1B41D346 (void);
// 0x00000021 System.Collections.IEnumerator Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir::Start()
extern void SynchronizationContextBehavoir_Start_m64A7A9B67267950EB78C38059D7A32ACBEA81BDD (void);
// 0x00000022 System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir::.ctor()
extern void SynchronizationContextBehavoir__ctor_m4DA714845D3AB1121B0A4EBE7065F59A49611B89 (void);
// 0x00000023 System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>d__3::.ctor(System.Int32)
extern void U3CStartU3Ed__3__ctor_m3D0E0A82678CE37450D832E22A7BBC6852F6AA96 (void);
// 0x00000024 System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>d__3::System.IDisposable.Dispose()
extern void U3CStartU3Ed__3_System_IDisposable_Dispose_mA50F28932FAE01D71C9A0696526785E33C9D4EF3 (void);
// 0x00000025 System.Boolean Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>d__3::MoveNext()
extern void U3CStartU3Ed__3_MoveNext_m79C10F1648EBB6CC99AEF6B297A3BFD71A256219 (void);
// 0x00000026 System.Object Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4DD00214D87B7ABD0DE391DF2F606326CC7C0D6 (void);
// 0x00000027 System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>d__3::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m9C1B3776E22571E8A613D4027BCFE280A316974D (void);
// 0x00000028 System.Object Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_m4CFE7303EAE1FEED1548CB9F871A2BF65F41BA21 (void);
// 0x00000029 System.Void Firebase.Unity.UnitySynchronizationContext_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mF7270F33C89460C1A50F6FA043A19C7BD6548C27 (void);
// 0x0000002A System.Void Firebase.Unity.UnitySynchronizationContext_<>c__DisplayClass14_1::.ctor()
extern void U3CU3Ec__DisplayClass14_1__ctor_m135F9AD4B63B980A41F20F957C8F813525EC6277 (void);
// 0x0000002B System.Void Firebase.Unity.UnitySynchronizationContext_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m5671BD5D5B0313391A699DA9A44E6E8932AEE7DE (void);
// 0x0000002C System.Void Firebase.Unity.UnitySynchronizationContext_<>c__DisplayClass16_1::.ctor()
extern void U3CU3Ec__DisplayClass16_1__ctor_m1A3792B3251F76A773610B32716D446844960515 (void);
// 0x0000002D System.Void Firebase.Unity.UnitySynchronizationContext_<>c__DisplayClass16_1::<Send>b__0(System.Object)
extern void U3CU3Ec__DisplayClass16_1_U3CSendU3Eb__0_m910615A384C9AC48E4EE6F51BC745E139781896A (void);
// 0x0000002E Firebase.Platform.DebugLogger Firebase.Platform.DebugLogger::get_Instance()
extern void DebugLogger_get_Instance_m06E28364924B44D279EA5EEB003DBA373D3FA2B7 (void);
// 0x0000002F System.Void Firebase.Platform.DebugLogger::.ctor()
extern void DebugLogger__ctor_m6A03E93E7B5D3F77F97E425B65396F7542998E26 (void);
// 0x00000030 System.Void Firebase.Platform.DebugLogger::.cctor()
extern void DebugLogger__cctor_mD5064826923495415C2852B63AA70F8D8FA535B6 (void);
// 0x00000031 System.Void Firebase.Platform.Services::.cctor()
extern void Services__cctor_m0A79A1B50E5F85FB2245D3586954AFB657ADE6C0 (void);
// 0x00000032 System.Void Firebase.Platform.Services::set_AppConfig(Firebase.Platform.IAppConfigExtensions)
extern void Services_set_AppConfig_m8AC0B6FE015A987AF3EAA030436F69CAA74A2142 (void);
// 0x00000033 System.Void Firebase.Platform.Services::set_Clock(Firebase.Platform.IClockService)
extern void Services_set_Clock_mF9C55620425C2005E94108FD36420181CFBAE37C (void);
// 0x00000034 System.Void Firebase.Platform.Services::set_Logging(Firebase.Platform.ILoggingService)
extern void Services_set_Logging_mDAC80751B95ECC1750B80FAD942541095763D5EE (void);
// 0x00000035 System.Void Firebase.Platform.IFirebaseAppUtils::TranslateDllNotFoundException(System.Action)
// 0x00000036 System.Void Firebase.Platform.IFirebaseAppUtils::PollCallbacks()
// 0x00000037 Firebase.Platform.PlatformLogLevel Firebase.Platform.IFirebaseAppUtils::GetLogLevel()
// 0x00000038 Firebase.Platform.FirebaseAppUtilsStub Firebase.Platform.FirebaseAppUtilsStub::get_Instance()
extern void FirebaseAppUtilsStub_get_Instance_m257406CEB647CE6E7419C13FF14C99E617CECC1B (void);
// 0x00000039 System.Void Firebase.Platform.FirebaseAppUtilsStub::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtilsStub_TranslateDllNotFoundException_mF272BF92C2F0E8162A0D39A255FD16A3AB7127F0 (void);
// 0x0000003A System.Void Firebase.Platform.FirebaseAppUtilsStub::PollCallbacks()
extern void FirebaseAppUtilsStub_PollCallbacks_m72BEEDCA8201C0F3F812207092B655865C78DD0E (void);
// 0x0000003B Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtilsStub::GetLogLevel()
extern void FirebaseAppUtilsStub_GetLogLevel_mB2B6B0AFBACB4724630BD1292849369B904D8C87 (void);
// 0x0000003C System.Void Firebase.Platform.FirebaseAppUtilsStub::.ctor()
extern void FirebaseAppUtilsStub__ctor_mEB40FEB244E4D3938E937B53B2641FEACB13A0DB (void);
// 0x0000003D System.Void Firebase.Platform.FirebaseAppUtilsStub::.cctor()
extern void FirebaseAppUtilsStub__cctor_mE24FC5370D04DAFFEF7CA4C8FF4CC87EFD1F081C (void);
// 0x0000003E System.Void Firebase.Platform.MainThreadProperty`1::.ctor(System.Func`1<T>)
// 0x0000003F T Firebase.Platform.MainThreadProperty`1::get_Value()
// 0x00000040 T Firebase.Platform.MainThreadProperty`1::<get_Value>b__5_0()
// 0x00000041 Firebase.Platform.IFirebaseAppUtils Firebase.Platform.FirebaseHandler::get_AppUtils()
extern void FirebaseHandler_get_AppUtils_m4B1DD0CA9C44AB6E9A12EED231FA8EF265948A7C (void);
// 0x00000042 System.Void Firebase.Platform.FirebaseHandler::set_AppUtils(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_set_AppUtils_m91387365FFE85468A59A0A2433DA483F08602DBF (void);
// 0x00000043 System.Int32 Firebase.Platform.FirebaseHandler::get_TickCount()
extern void FirebaseHandler_get_TickCount_m9A88A2C0949942CDFAC05C061004305B148E8019 (void);
// 0x00000044 Firebase.Dispatcher Firebase.Platform.FirebaseHandler::get_ThreadDispatcher()
extern void FirebaseHandler_get_ThreadDispatcher_m576CC2F52B6A331E1D5D7ABDC493937253D51189 (void);
// 0x00000045 System.Void Firebase.Platform.FirebaseHandler::set_ThreadDispatcher(Firebase.Dispatcher)
extern void FirebaseHandler_set_ThreadDispatcher_m7C1647AB2F233DD9D808C3AEA9B0469F19F653F2 (void);
// 0x00000046 System.Boolean Firebase.Platform.FirebaseHandler::get_IsPlayMode()
extern void FirebaseHandler_get_IsPlayMode_m2784DFEDC9F23AE0072B174583BDEBB1D9F8B7FD (void);
// 0x00000047 System.Void Firebase.Platform.FirebaseHandler::set_IsPlayMode(System.Boolean)
extern void FirebaseHandler_set_IsPlayMode_mDFC0BA0F8ADCB56155A7278823E9669758BE38B5 (void);
// 0x00000048 System.Void Firebase.Platform.FirebaseHandler::.cctor()
extern void FirebaseHandler__cctor_mCC22861A54093E75FF8C063CA2B0AC1975F832B5 (void);
// 0x00000049 System.Void Firebase.Platform.FirebaseHandler::.ctor()
extern void FirebaseHandler__ctor_mF69C13C09805BAF05E457BA75976181A117E091A (void);
// 0x0000004A System.Void Firebase.Platform.FirebaseHandler::StartMonoBehaviour()
extern void FirebaseHandler_StartMonoBehaviour_m6728F8C35C8A08C2933B871CA4716CD6C95314DA (void);
// 0x0000004B System.Void Firebase.Platform.FirebaseHandler::StopMonoBehaviour()
extern void FirebaseHandler_StopMonoBehaviour_m741EC378D41A3166E8DE6532F03A195E88C033C2 (void);
// 0x0000004C TResult Firebase.Platform.FirebaseHandler::RunOnMainThread(System.Func`1<TResult>)
// 0x0000004D System.Threading.Tasks.Task`1<TResult> Firebase.Platform.FirebaseHandler::RunOnMainThreadAsync(System.Func`1<TResult>)
// 0x0000004E Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseHandler::get_DefaultInstance()
extern void FirebaseHandler_get_DefaultInstance_mDE47F7545F78ED4AAA09566895610B8DD8A6FE02 (void);
// 0x0000004F System.Void Firebase.Platform.FirebaseHandler::CreatePartialOnMainThread(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_CreatePartialOnMainThread_m96D4E360CF58711B4865E546DCE75CACCFA34C78 (void);
// 0x00000050 System.Void Firebase.Platform.FirebaseHandler::Create(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_Create_m89DB93F791D7C61D45C79484E9207C4967374BEA (void);
// 0x00000051 System.Void Firebase.Platform.FirebaseHandler::Update()
extern void FirebaseHandler_Update_m4A49185D21631806389823523179738DB897EB33 (void);
// 0x00000052 System.Void Firebase.Platform.FirebaseHandler::OnApplicationFocus(System.Boolean)
extern void FirebaseHandler_OnApplicationFocus_m04ED5D86AF9BA18E761EACFEA9455E1829008B8E (void);
// 0x00000053 System.Void Firebase.Platform.FirebaseHandler::OnMonoBehaviourDestroyed(Firebase.Platform.FirebaseMonoBehaviour)
extern void FirebaseHandler_OnMonoBehaviourDestroyed_m93E0EE038A3E5F19681DAFEE5445ABE6065AA086 (void);
// 0x00000054 System.Void Firebase.Platform.FirebaseHandler::<Update>b__36_0()
extern void FirebaseHandler_U3CUpdateU3Eb__36_0_m304D906B78D7FD31C9FF97B3C5F479C4D216639B (void);
// 0x00000055 System.Void Firebase.Platform.FirebaseHandler_ApplicationFocusChangedEventArgs::set_HasFocus(System.Boolean)
extern void ApplicationFocusChangedEventArgs_set_HasFocus_mEA4178D6C578686CE84F1E088EA370AB7225A330 (void);
// 0x00000056 System.Void Firebase.Platform.FirebaseHandler_ApplicationFocusChangedEventArgs::.ctor()
extern void ApplicationFocusChangedEventArgs__ctor_mB1717C3BC9E32E3336D360134B6073C8EE5E4EBA (void);
// 0x00000057 System.Void Firebase.Platform.FirebaseHandler_<>c::.cctor()
extern void U3CU3Ec__cctor_mF3AF1EAFE6004193EE0E549CF5C71537737411DF (void);
// 0x00000058 System.Void Firebase.Platform.FirebaseHandler_<>c::.ctor()
extern void U3CU3Ec__ctor_mC283100F5611A126DFF3180C82B86539C3BAEC64 (void);
// 0x00000059 System.Boolean Firebase.Platform.FirebaseHandler_<>c::<StopMonoBehaviour>b__19_0()
extern void U3CU3Ec_U3CStopMonoBehaviourU3Eb__19_0_m70CF1FE9DB7301ACF8FCC862BFB607C80DF4C738 (void);
// 0x0000005A System.Void Firebase.Platform.FirebaseHandler_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m9F91616850CAEAE4862128387004B3FCFA50D267 (void);
// 0x0000005B System.Void Firebase.Platform.FirebaseHandler_<>c__DisplayClass34_0::<CreatePartialOnMainThread>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CCreatePartialOnMainThreadU3Eb__0_m948577B4C13F948F0223C787D5DB3F68401978B6 (void);
// 0x0000005C System.Boolean Firebase.Platform.PlatformInformation::get_IsAndroid()
extern void PlatformInformation_get_IsAndroid_m61250012539009D27371075104AFBDE2549BB77E (void);
// 0x0000005D System.Boolean Firebase.Platform.PlatformInformation::get_IsIOS()
extern void PlatformInformation_get_IsIOS_mEC53C8E1719192F4A5E5A5C919752D9772706573 (void);
// 0x0000005E System.String Firebase.Platform.PlatformInformation::get_DefaultConfigLocation()
extern void PlatformInformation_get_DefaultConfigLocation_m7B32A608B4B90F6749B02AC1C0E87E4C904E9608 (void);
// 0x0000005F System.Single Firebase.Platform.PlatformInformation::get_RealtimeSinceStartup()
extern void PlatformInformation_get_RealtimeSinceStartup_mA5552638B79722FD7378E430054A9EE21A7F4742 (void);
// 0x00000060 System.Void Firebase.Platform.PlatformInformation::set_RealtimeSinceStartupSafe(System.Single)
extern void PlatformInformation_set_RealtimeSinceStartupSafe_mB37999FFE3AF262FB2234E0308CB8155D7827C47 (void);
// 0x00000061 System.String Firebase.Platform.PlatformInformation::get_RuntimeName()
extern void PlatformInformation_get_RuntimeName_mCD019D8389883E820ED30D97642D63F0BA844EAD (void);
// 0x00000062 System.String Firebase.Platform.PlatformInformation::get_RuntimeVersion()
extern void PlatformInformation_get_RuntimeVersion_m947E74AAC2129616DA7A9330BCE4B79EA8A422F0 (void);
// 0x00000063 System.Void Firebase.Platform.PlatformInformation_<>c::.cctor()
extern void U3CU3Ec__cctor_m312460940A8A4A0E131AEAAB2AD9FBB554B0EA2B (void);
// 0x00000064 System.Void Firebase.Platform.PlatformInformation_<>c::.ctor()
extern void U3CU3Ec__ctor_m73B47E5186EACE64F0CB7D3C7C02A30AC916A16F (void);
// 0x00000065 System.String Firebase.Platform.PlatformInformation_<>c::<get_DefaultConfigLocation>b__6_0()
extern void U3CU3Ec_U3Cget_DefaultConfigLocationU3Eb__6_0_m12BEE7D53BD26E2C616CB063AB8A740BEEDFBF5C (void);
// 0x00000066 System.String Firebase.Platform.PlatformInformation_<>c::<get_RuntimeVersion>b__18_0()
extern void U3CU3Ec_U3Cget_RuntimeVersionU3Eb__18_0_mD84D0D17753CA961AD39B4B6B435E6CB2CBC349D (void);
// 0x00000067 System.Boolean Firebase.Platform.FirebaseLogger::IsStackTraceLogTypeIncompatibleWithNativeLogs(UnityEngine.StackTraceLogType)
extern void FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m32A916D1E8992541EB447E499B85BFF15CEF559F (void);
// 0x00000068 System.Boolean Firebase.Platform.FirebaseLogger::CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs()
extern void FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_m51B2392AEFCF6E04C363994806DCE5EAE27EDA2B (void);
// 0x00000069 System.Boolean Firebase.Platform.FirebaseLogger::get_CanRedirectNativeLogs()
extern void FirebaseLogger_get_CanRedirectNativeLogs_m29C5B1ACE567CEA890B6DA19D91AF37D1EDEEBC8 (void);
// 0x0000006A System.Void Firebase.Platform.FirebaseLogger::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
extern void FirebaseLogger_LogMessage_mEF2033A0588D651D162FFE29C9FA77A39E51233E (void);
// 0x0000006B System.Void Firebase.Platform.FirebaseLogger::.cctor()
extern void FirebaseLogger__cctor_mA0565673CF29882DF40267833252A8B96AC85B77 (void);
// 0x0000006C Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseMonoBehaviour::GetFirebaseHandlerOrDestroyGameObject()
extern void FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_m77DA35BC03EF563A295FDA20901881CFC161A89B (void);
// 0x0000006D System.Void Firebase.Platform.FirebaseMonoBehaviour::OnEnable()
extern void FirebaseMonoBehaviour_OnEnable_mB3D6B00AA0A0A4FFD25FDA48E28AFCE52E359B80 (void);
// 0x0000006E System.Void Firebase.Platform.FirebaseMonoBehaviour::Update()
extern void FirebaseMonoBehaviour_Update_mC3DFEB73F14CD64F3C26006141EF71839E463A65 (void);
// 0x0000006F System.Void Firebase.Platform.FirebaseMonoBehaviour::OnApplicationFocus(System.Boolean)
extern void FirebaseMonoBehaviour_OnApplicationFocus_m84A811D74316C5E254EF57A6253755EBBD8585E9 (void);
// 0x00000070 System.Void Firebase.Platform.FirebaseMonoBehaviour::OnDestroy()
extern void FirebaseMonoBehaviour_OnDestroy_mDCEE8FA7C5820AA947E110083347A7ED4BE71FD5 (void);
// 0x00000071 System.Void Firebase.Platform.FirebaseMonoBehaviour::.ctor()
extern void FirebaseMonoBehaviour__ctor_m137AF46B744D8805EC43DC70A0DCE49AF4F2B326 (void);
// 0x00000072 System.Type Firebase.Platform.FirebaseEditorDispatcher::get_EditorApplicationType()
extern void FirebaseEditorDispatcher_get_EditorApplicationType_m1F7FD788E0579F30AC63063A4AEBAD065A2ACE7E (void);
// 0x00000073 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlaying()
extern void FirebaseEditorDispatcher_get_EditorIsPlaying_m8E80FCF745D38EF53E550955ED6634498B663E2B (void);
// 0x00000074 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlayingOrWillChangePlaymode()
extern void FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_mC656FF2ABDE09D856D738444067F34B9D6F6E741 (void);
// 0x00000075 System.Void Firebase.Platform.FirebaseEditorDispatcher::StartEditorUpdate()
extern void FirebaseEditorDispatcher_StartEditorUpdate_m9014FC7984B5CE57EBC0A23B1309C9B05997FD7B (void);
// 0x00000076 System.Void Firebase.Platform.FirebaseEditorDispatcher::StopEditorUpdate()
extern void FirebaseEditorDispatcher_StopEditorUpdate_mED528078F895F70293A8DF07275180DF2F7C6E24 (void);
// 0x00000077 System.Void Firebase.Platform.FirebaseEditorDispatcher::Update()
extern void FirebaseEditorDispatcher_Update_m1CE1E7BDDB5204EE82EC28C6C7B5F83AB64C5D63 (void);
// 0x00000078 System.Void Firebase.Platform.FirebaseEditorDispatcher::ListenToPlayState(System.Boolean)
extern void FirebaseEditorDispatcher_ListenToPlayState_mFADB143214E7B22867C4D26CD6C1B384C5BA00FD (void);
// 0x00000079 System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChanged()
extern void FirebaseEditorDispatcher_PlayModeStateChanged_mA77B4BC297642FC52BB8AB01FBB79E98B1C47288 (void);
// 0x0000007A System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChangedWithArg(T)
// 0x0000007B System.Void Firebase.Platform.FirebaseEditorDispatcher::AddRemoveCallbackToField(System.Reflection.FieldInfo,System.Action,System.Object,System.Boolean,System.String)
extern void FirebaseEditorDispatcher_AddRemoveCallbackToField_m285003B251B4D5067546A49017F6B5F5C8405FA7 (void);
// 0x0000007C Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.AppConfigExtensions::get_Instance()
extern void AppConfigExtensions_get_Instance_m9E23D31FAA5845CC6A8AB3C6A676DCCE0880C966 (void);
// 0x0000007D System.Void Firebase.Platform.Default.AppConfigExtensions::.ctor()
extern void AppConfigExtensions__ctor_mC5065C76DA54B0C1822FC58959AC5FED94F64328 (void);
// 0x0000007E System.Void Firebase.Platform.Default.AppConfigExtensions::.cctor()
extern void AppConfigExtensions__cctor_m6F6AA804ECFF8AFE261311924D919CAE0C075096 (void);
// 0x0000007F System.Void Firebase.Platform.Default.SystemClock::.ctor()
extern void SystemClock__ctor_mF0290527299F60106E6FCAB61B3697D77C856DDF (void);
// 0x00000080 System.Void Firebase.Platform.Default.SystemClock::.cctor()
extern void SystemClock__cctor_m78C791FBA8496CA05264591D55F49610FFD4A33E (void);
// 0x00000081 Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.UnityConfigExtensions::get_DefaultInstance()
extern void UnityConfigExtensions_get_DefaultInstance_m963AB0EF6217BB9E9569CF4D23E2A0F2753A7B35 (void);
// 0x00000082 System.Void Firebase.Platform.Default.UnityConfigExtensions::.ctor()
extern void UnityConfigExtensions__ctor_m4A9B319E06B0F2778FBADFE4C06F5EF95C9BBD68 (void);
// 0x00000083 System.Void Firebase.Platform.Default.UnityConfigExtensions::.cctor()
extern void UnityConfigExtensions__cctor_m3AE99E171092FB6DBB559F1ECF4E0A4B28497058 (void);
static Il2CppMethodPointer s_methodPointers[131] = 
{
	ExceptionAggregator_get_Exceptions_mB8771E2A0D9AFC2BF5A29214B58FAE0621306BE2,
	ExceptionAggregator_GetAndClearPendingExceptions_m0428C791FBC34F687E53E93C8283BF1BDAD52705,
	ExceptionAggregator_ThrowAndClearPendingExceptions_m43EDFA771E277D927E98305CF105F598C2A19D96,
	ExceptionAggregator_LogException_m2D22E7AA0907E7185DDF0F7F5C2EE702265CB12C,
	ExceptionAggregator_Wrap_m93A9BD0DC888F1D02E77860288095CB49D4CFFC2,
	Dispatcher__ctor_m524290773AE51A0B146046705A07E78B61C27268,
	NULL,
	NULL,
	NULL,
	Dispatcher_ManagesThisThread_mCC706F89C23B9407225833D81E5B359F6BDA2A3D,
	Dispatcher_PollJobs_m27F0E072ED9B5297E072A0AAFD37D3D76B748875,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityLoggingService_get_Instance_m021810065C28BAD66B00CA2EC2DC55AD3EE85A4E,
	UnityLoggingService__ctor_m3BCC6353BEADE287DF1A0C9781B6187576523441,
	UnityLoggingService__cctor_m07430A774E2E27DE95B135565549505BC99BFAF3,
	UnityPlatformServices_SetupServices_mC37344AC6AB871E0DCEC44A26FBC98BB05776CE4,
	UnitySynchronizationContext__ctor_m47697B161718D309E5BDEE021A7652F2610B3811,
	UnitySynchronizationContext_Create_m457A7DF239D69F664A4C5A71A51831D10F8C5C9B,
	UnitySynchronizationContext_Destroy_m173DAA6D418F0749046FC98BE3244ECF0CD37188,
	UnitySynchronizationContext_GetThreadEvent_mD3A2B8BC1310D4C5F17918280F14EF089DD8164A,
	UnitySynchronizationContext_Post_m4450F9D1EEF52CB7EED4C27ACEFC4F5324C6BFBF,
	UnitySynchronizationContext_Send_m537EE81F72568A13624BBD4CB4D8796946722A6D,
	UnitySynchronizationContext__cctor_m3B5D4F04B0CEEF803ED5E341A4AFCD813F706402,
	SynchronizationContextBehavoir_get_CallbackQueue_mE2EE07CB1868612C976725049F507F5D1B41D346,
	SynchronizationContextBehavoir_Start_m64A7A9B67267950EB78C38059D7A32ACBEA81BDD,
	SynchronizationContextBehavoir__ctor_m4DA714845D3AB1121B0A4EBE7065F59A49611B89,
	U3CStartU3Ed__3__ctor_m3D0E0A82678CE37450D832E22A7BBC6852F6AA96,
	U3CStartU3Ed__3_System_IDisposable_Dispose_mA50F28932FAE01D71C9A0696526785E33C9D4EF3,
	U3CStartU3Ed__3_MoveNext_m79C10F1648EBB6CC99AEF6B297A3BFD71A256219,
	U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4DD00214D87B7ABD0DE391DF2F606326CC7C0D6,
	U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m9C1B3776E22571E8A613D4027BCFE280A316974D,
	U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_m4CFE7303EAE1FEED1548CB9F871A2BF65F41BA21,
	U3CU3Ec__DisplayClass14_0__ctor_mF7270F33C89460C1A50F6FA043A19C7BD6548C27,
	U3CU3Ec__DisplayClass14_1__ctor_m135F9AD4B63B980A41F20F957C8F813525EC6277,
	U3CU3Ec__DisplayClass16_0__ctor_m5671BD5D5B0313391A699DA9A44E6E8932AEE7DE,
	U3CU3Ec__DisplayClass16_1__ctor_m1A3792B3251F76A773610B32716D446844960515,
	U3CU3Ec__DisplayClass16_1_U3CSendU3Eb__0_m910615A384C9AC48E4EE6F51BC745E139781896A,
	DebugLogger_get_Instance_m06E28364924B44D279EA5EEB003DBA373D3FA2B7,
	DebugLogger__ctor_m6A03E93E7B5D3F77F97E425B65396F7542998E26,
	DebugLogger__cctor_mD5064826923495415C2852B63AA70F8D8FA535B6,
	Services__cctor_m0A79A1B50E5F85FB2245D3586954AFB657ADE6C0,
	Services_set_AppConfig_m8AC0B6FE015A987AF3EAA030436F69CAA74A2142,
	Services_set_Clock_mF9C55620425C2005E94108FD36420181CFBAE37C,
	Services_set_Logging_mDAC80751B95ECC1750B80FAD942541095763D5EE,
	NULL,
	NULL,
	NULL,
	FirebaseAppUtilsStub_get_Instance_m257406CEB647CE6E7419C13FF14C99E617CECC1B,
	FirebaseAppUtilsStub_TranslateDllNotFoundException_mF272BF92C2F0E8162A0D39A255FD16A3AB7127F0,
	FirebaseAppUtilsStub_PollCallbacks_m72BEEDCA8201C0F3F812207092B655865C78DD0E,
	FirebaseAppUtilsStub_GetLogLevel_mB2B6B0AFBACB4724630BD1292849369B904D8C87,
	FirebaseAppUtilsStub__ctor_mEB40FEB244E4D3938E937B53B2641FEACB13A0DB,
	FirebaseAppUtilsStub__cctor_mE24FC5370D04DAFFEF7CA4C8FF4CC87EFD1F081C,
	NULL,
	NULL,
	NULL,
	FirebaseHandler_get_AppUtils_m4B1DD0CA9C44AB6E9A12EED231FA8EF265948A7C,
	FirebaseHandler_set_AppUtils_m91387365FFE85468A59A0A2433DA483F08602DBF,
	FirebaseHandler_get_TickCount_m9A88A2C0949942CDFAC05C061004305B148E8019,
	FirebaseHandler_get_ThreadDispatcher_m576CC2F52B6A331E1D5D7ABDC493937253D51189,
	FirebaseHandler_set_ThreadDispatcher_m7C1647AB2F233DD9D808C3AEA9B0469F19F653F2,
	FirebaseHandler_get_IsPlayMode_m2784DFEDC9F23AE0072B174583BDEBB1D9F8B7FD,
	FirebaseHandler_set_IsPlayMode_mDFC0BA0F8ADCB56155A7278823E9669758BE38B5,
	FirebaseHandler__cctor_mCC22861A54093E75FF8C063CA2B0AC1975F832B5,
	FirebaseHandler__ctor_mF69C13C09805BAF05E457BA75976181A117E091A,
	FirebaseHandler_StartMonoBehaviour_m6728F8C35C8A08C2933B871CA4716CD6C95314DA,
	FirebaseHandler_StopMonoBehaviour_m741EC378D41A3166E8DE6532F03A195E88C033C2,
	NULL,
	NULL,
	FirebaseHandler_get_DefaultInstance_mDE47F7545F78ED4AAA09566895610B8DD8A6FE02,
	FirebaseHandler_CreatePartialOnMainThread_m96D4E360CF58711B4865E546DCE75CACCFA34C78,
	FirebaseHandler_Create_m89DB93F791D7C61D45C79484E9207C4967374BEA,
	FirebaseHandler_Update_m4A49185D21631806389823523179738DB897EB33,
	FirebaseHandler_OnApplicationFocus_m04ED5D86AF9BA18E761EACFEA9455E1829008B8E,
	FirebaseHandler_OnMonoBehaviourDestroyed_m93E0EE038A3E5F19681DAFEE5445ABE6065AA086,
	FirebaseHandler_U3CUpdateU3Eb__36_0_m304D906B78D7FD31C9FF97B3C5F479C4D216639B,
	ApplicationFocusChangedEventArgs_set_HasFocus_mEA4178D6C578686CE84F1E088EA370AB7225A330,
	ApplicationFocusChangedEventArgs__ctor_mB1717C3BC9E32E3336D360134B6073C8EE5E4EBA,
	U3CU3Ec__cctor_mF3AF1EAFE6004193EE0E549CF5C71537737411DF,
	U3CU3Ec__ctor_mC283100F5611A126DFF3180C82B86539C3BAEC64,
	U3CU3Ec_U3CStopMonoBehaviourU3Eb__19_0_m70CF1FE9DB7301ACF8FCC862BFB607C80DF4C738,
	U3CU3Ec__DisplayClass34_0__ctor_m9F91616850CAEAE4862128387004B3FCFA50D267,
	U3CU3Ec__DisplayClass34_0_U3CCreatePartialOnMainThreadU3Eb__0_m948577B4C13F948F0223C787D5DB3F68401978B6,
	PlatformInformation_get_IsAndroid_m61250012539009D27371075104AFBDE2549BB77E,
	PlatformInformation_get_IsIOS_mEC53C8E1719192F4A5E5A5C919752D9772706573,
	PlatformInformation_get_DefaultConfigLocation_m7B32A608B4B90F6749B02AC1C0E87E4C904E9608,
	PlatformInformation_get_RealtimeSinceStartup_mA5552638B79722FD7378E430054A9EE21A7F4742,
	PlatformInformation_set_RealtimeSinceStartupSafe_mB37999FFE3AF262FB2234E0308CB8155D7827C47,
	PlatformInformation_get_RuntimeName_mCD019D8389883E820ED30D97642D63F0BA844EAD,
	PlatformInformation_get_RuntimeVersion_m947E74AAC2129616DA7A9330BCE4B79EA8A422F0,
	U3CU3Ec__cctor_m312460940A8A4A0E131AEAAB2AD9FBB554B0EA2B,
	U3CU3Ec__ctor_m73B47E5186EACE64F0CB7D3C7C02A30AC916A16F,
	U3CU3Ec_U3Cget_DefaultConfigLocationU3Eb__6_0_m12BEE7D53BD26E2C616CB063AB8A740BEEDFBF5C,
	U3CU3Ec_U3Cget_RuntimeVersionU3Eb__18_0_mD84D0D17753CA961AD39B4B6B435E6CB2CBC349D,
	FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m32A916D1E8992541EB447E499B85BFF15CEF559F,
	FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_m51B2392AEFCF6E04C363994806DCE5EAE27EDA2B,
	FirebaseLogger_get_CanRedirectNativeLogs_m29C5B1ACE567CEA890B6DA19D91AF37D1EDEEBC8,
	FirebaseLogger_LogMessage_mEF2033A0588D651D162FFE29C9FA77A39E51233E,
	FirebaseLogger__cctor_mA0565673CF29882DF40267833252A8B96AC85B77,
	FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_m77DA35BC03EF563A295FDA20901881CFC161A89B,
	FirebaseMonoBehaviour_OnEnable_mB3D6B00AA0A0A4FFD25FDA48E28AFCE52E359B80,
	FirebaseMonoBehaviour_Update_mC3DFEB73F14CD64F3C26006141EF71839E463A65,
	FirebaseMonoBehaviour_OnApplicationFocus_m84A811D74316C5E254EF57A6253755EBBD8585E9,
	FirebaseMonoBehaviour_OnDestroy_mDCEE8FA7C5820AA947E110083347A7ED4BE71FD5,
	FirebaseMonoBehaviour__ctor_m137AF46B744D8805EC43DC70A0DCE49AF4F2B326,
	FirebaseEditorDispatcher_get_EditorApplicationType_m1F7FD788E0579F30AC63063A4AEBAD065A2ACE7E,
	FirebaseEditorDispatcher_get_EditorIsPlaying_m8E80FCF745D38EF53E550955ED6634498B663E2B,
	FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_mC656FF2ABDE09D856D738444067F34B9D6F6E741,
	FirebaseEditorDispatcher_StartEditorUpdate_m9014FC7984B5CE57EBC0A23B1309C9B05997FD7B,
	FirebaseEditorDispatcher_StopEditorUpdate_mED528078F895F70293A8DF07275180DF2F7C6E24,
	FirebaseEditorDispatcher_Update_m1CE1E7BDDB5204EE82EC28C6C7B5F83AB64C5D63,
	FirebaseEditorDispatcher_ListenToPlayState_mFADB143214E7B22867C4D26CD6C1B384C5BA00FD,
	FirebaseEditorDispatcher_PlayModeStateChanged_mA77B4BC297642FC52BB8AB01FBB79E98B1C47288,
	NULL,
	FirebaseEditorDispatcher_AddRemoveCallbackToField_m285003B251B4D5067546A49017F6B5F5C8405FA7,
	AppConfigExtensions_get_Instance_m9E23D31FAA5845CC6A8AB3C6A676DCCE0880C966,
	AppConfigExtensions__ctor_mC5065C76DA54B0C1822FC58959AC5FED94F64328,
	AppConfigExtensions__cctor_m6F6AA804ECFF8AFE261311924D919CAE0C075096,
	SystemClock__ctor_mF0290527299F60106E6FCAB61B3697D77C856DDF,
	SystemClock__cctor_m78C791FBA8496CA05264591D55F49610FFD4A33E,
	UnityConfigExtensions_get_DefaultInstance_m963AB0EF6217BB9E9569CF4D23E2A0F2753A7B35,
	UnityConfigExtensions__ctor_m4A9B319E06B0F2778FBADFE4C06F5EF95C9BBD68,
	UnityConfigExtensions__cctor_m3AE99E171092FB6DBB559F1ECF4E0A4B28497058,
};
static const int32_t s_InvokerIndices[131] = 
{
	4,
	4,
	3,
	0,
	111,
	23,
	-1,
	-1,
	-1,
	102,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	23,
	3,
	3,
	26,
	111,
	3,
	14,
	27,
	27,
	3,
	14,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	4,
	23,
	3,
	3,
	111,
	111,
	111,
	26,
	23,
	10,
	4,
	26,
	23,
	10,
	23,
	3,
	-1,
	-1,
	-1,
	4,
	111,
	509,
	4,
	111,
	102,
	31,
	3,
	23,
	23,
	23,
	-1,
	-1,
	4,
	111,
	111,
	23,
	31,
	111,
	23,
	31,
	23,
	3,
	23,
	102,
	23,
	23,
	49,
	49,
	4,
	988,
	1249,
	4,
	4,
	3,
	23,
	14,
	14,
	46,
	49,
	49,
	506,
	3,
	14,
	23,
	23,
	31,
	23,
	23,
	4,
	49,
	49,
	3,
	3,
	3,
	763,
	3,
	-1,
	1250,
	4,
	23,
	3,
	23,
	3,
	4,
	23,
	3,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000005, { 21, 3 } },
	{ 0x02000006, { 24, 3 } },
	{ 0x02000019, { 27, 6 } },
	{ 0x06000007, { 0, 8 } },
	{ 0x06000008, { 8, 7 } },
	{ 0x06000009, { 15, 6 } },
	{ 0x0600004C, { 33, 2 } },
	{ 0x0600004D, { 35, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[37] = 
{
	{ (Il2CppRGCTXDataType)2, 12709 },
	{ (Il2CppRGCTXDataType)3, 6614 },
	{ (Il2CppRGCTXDataType)3, 6615 },
	{ (Il2CppRGCTXDataType)2, 12710 },
	{ (Il2CppRGCTXDataType)3, 6616 },
	{ (Il2CppRGCTXDataType)3, 6617 },
	{ (Il2CppRGCTXDataType)3, 6618 },
	{ (Il2CppRGCTXDataType)3, 6619 },
	{ (Il2CppRGCTXDataType)2, 12711 },
	{ (Il2CppRGCTXDataType)3, 6620 },
	{ (Il2CppRGCTXDataType)3, 6621 },
	{ (Il2CppRGCTXDataType)2, 12712 },
	{ (Il2CppRGCTXDataType)3, 6622 },
	{ (Il2CppRGCTXDataType)3, 6623 },
	{ (Il2CppRGCTXDataType)3, 6624 },
	{ (Il2CppRGCTXDataType)3, 6625 },
	{ (Il2CppRGCTXDataType)3, 6626 },
	{ (Il2CppRGCTXDataType)2, 12713 },
	{ (Il2CppRGCTXDataType)3, 6627 },
	{ (Il2CppRGCTXDataType)3, 6628 },
	{ (Il2CppRGCTXDataType)3, 6629 },
	{ (Il2CppRGCTXDataType)3, 6630 },
	{ (Il2CppRGCTXDataType)3, 6631 },
	{ (Il2CppRGCTXDataType)3, 6632 },
	{ (Il2CppRGCTXDataType)3, 6633 },
	{ (Il2CppRGCTXDataType)3, 6634 },
	{ (Il2CppRGCTXDataType)3, 6635 },
	{ (Il2CppRGCTXDataType)3, 6636 },
	{ (Il2CppRGCTXDataType)2, 11368 },
	{ (Il2CppRGCTXDataType)3, 6637 },
	{ (Il2CppRGCTXDataType)3, 6638 },
	{ (Il2CppRGCTXDataType)3, 6639 },
	{ (Il2CppRGCTXDataType)3, 6640 },
	{ (Il2CppRGCTXDataType)3, 6641 },
	{ (Il2CppRGCTXDataType)3, 6642 },
	{ (Il2CppRGCTXDataType)3, 6643 },
	{ (Il2CppRGCTXDataType)3, 6644 },
};
extern const Il2CppCodeGenModule g_Firebase_PlatformCodeGenModule;
const Il2CppCodeGenModule g_Firebase_PlatformCodeGenModule = 
{
	"Firebase.Platform.dll",
	131,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	37,
	s_rgctxValues,
	NULL,
};
