﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericVirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericVirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct InterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericInterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};

// AppleAuth.AppleAuthManager
struct AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C;
// AppleAuth.IAppleAuthManager
struct IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287;
// AppleAuth.Interfaces.IAppleError
struct IAppleError_t7BFB5142BCDD43FA9230E884B829D317FFC7445E;
// AppleAuth.Interfaces.ICredential
struct ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44;
// AppleAuth.Interfaces.IPayloadDeserializer
struct IPayloadDeserializer_tE27C18AF93B5B81F79A412F73640C4238FDA5C93;
// AppleAuth.Interfaces.IPersonName
struct IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A;
// AppleAuth.Native.PayloadDeserializer
struct PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E;
// Authentication.SignedInUserFirebaseInfo
struct SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3;
// Authentication.SignedInUserProviderInfo
struct SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1;
// Authentication.WebelinxAnonymousAuthController
struct WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E;
// Authentication.WebelinxFirebaseAuthManager
struct WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52;
// Authentication.WebelinxFirebaseAuthManager/<>c
struct U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE;
// Authentication.WebelinxFirebaseAuthView
struct WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A;
// Firebase.Auth.Credential
struct Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630;
// Firebase.Auth.FirebaseAuth
struct FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D;
// Firebase.Auth.FirebaseUser
struct FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD;
// Firebase.Auth.UserProfile
struct UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31;
// Firebase.FirebaseApp
struct FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986;
// GameMenuHandler
struct GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106;
// GooglePlayGames.BasicApi.CommonTypesUtil
struct CommonTypesUtil_t2873297E6294F5B79AF6B4087BCF1092C7B069E4;
// GooglePlayGames.BasicApi.Events.Event
struct Event_t72642C3269994E99511B3C85054C40A5610B170D;
// GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893;
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct ConflictCallback_t73445AE2938707A22A01739D23A603DC17AB4962;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_tCDA7B052975BA9969B51410E402631047E2C4033;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t9B43B395D8E5868FFD12EA1217063523B5E1CADE;
// GooglePlayGames.BasicApi.SignInHelper
struct SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58;
// GooglePlayGames.BasicApi.Video.VideoCapabilities
struct VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4;
// GooglePlayGames.BasicApi.Video.VideoCapabilities/<>c
struct U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B;
// GooglePlayGames.BasicApi.Video.VideoCaptureState
struct VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F;
// GooglePlayGames.OurUtils.Logger
struct Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B;
// GooglePlayGames.OurUtils.Logger/<>c
struct U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5;
// GooglePlayGames.OurUtils.Logger/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF;
// GooglePlayGames.OurUtils.Logger/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43;
// GooglePlayGames.OurUtils.Logger/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679;
// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F;
// GooglePlayGames.OurUtils.PlayGamesHelperObject/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2;
// GooglePlayGames.PluginVersion
struct PluginVersion_tB0E519E7845CD35129B206E909BC0EF6A24D6BE9;
// LoginMenuHandler
struct LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65;
// MainMenu
struct MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105;
// MainMenu/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action[]
struct ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B;
// System.Action`1<AppleAuth.Enums.CredentialState>
struct Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88;
// System.Action`1<AppleAuth.Interfaces.IAppleError>
struct Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3;
// System.Action`1<AppleAuth.Interfaces.ICredential>
struct Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.Boolean>[]
struct Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC;
// System.Action`1<System.Int32Enum>
struct Action_1_tABA1E3BFA092E3309A0ECC53722E4F9826DCE983;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`1<System.Threading.Tasks.Task>
struct Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827;
// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>
struct Action_1_t5D2D131226D740682F598DD09436558292112781;
// System.Action`1<System.Threading.Tasks.Task`1<System.Object>>
struct Action_1_tF41DEFE08D91E5A3638655E075175E27AA82D1DC;
// System.AggregateException
struct AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.Int32,GooglePlayGames.BasicApi.SignInStatus>[]
struct EntryU5BU5D_tA0390730ED24ACEABD2B5D9C48594C85FB9EB4FC;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,GooglePlayGames.BasicApi.SignInStatus>
struct KeyCollection_tB40C9D0D51F1C5C14B18C8FE5F0BCB35C00E7A32;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,GooglePlayGames.BasicApi.SignInStatus>
struct ValueCollection_tFD6F9B1AE8411C40E0410C958B7C5921BE318C1B;
// System.Collections.Generic.Dictionary`2<System.Int32,GooglePlayGames.BasicApi.SignInStatus>
struct Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32Enum>
struct Dictionary_2_t5B44A7142DEF210BD03CD8381D8FD7A00197C25B;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F;
// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth>
struct Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D;
// System.Collections.Generic.IEnumerable`1<Firebase.Auth.IUserInfo>
struct IEnumerable_1_t62E378E9873F9801909C3B38FB5B032D7A9929EA;
// System.Collections.Generic.IEnumerable`1<System.Action>
struct IEnumerable_1_t0E4A1DFF4A3C826ACB64DE6002EA388F0AA78388;
// System.Collections.Generic.IEnumerable`1<System.Boolean>
struct IEnumerable_1_tC6EB38083371E9C00CC441AE4ADB4A3100BEB721;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2F75FCBEC68AFE08982DA43985F9D04056E2BE73;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t7B82AA0F8B96BAAA21E36DDF7A1FE4348BDDBE95;
// System.Collections.Generic.List`1<System.Action>
struct List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46;
// System.Collections.Generic.List`1<System.Action`1<System.Boolean>>
struct List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t053DAB6E2110E276A0339D73497193F464BC1F82;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Exception>
struct ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.EventArgs
struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F;
// System.Func`2<System.Boolean,System.Object>
struct Func_2_tB9D83096EC4D7C11E734B451F3F094E92E1D287D;
// System.Func`2<System.Boolean,System.String>
struct Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>
struct Func_2_t385E06614170490B1EFD01D106616F0668133752;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<System.Object>
struct Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.DecoderFallback
struct DecoderFallback_t128445EB7676870485230893338EF044F6B72F60;
// System.Text.EncoderFallback
struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.ContextCallback
struct ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676;
// System.Threading.Tasks.StackGuard
struct StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155;
// System.Threading.Tasks.TaskFactory`1<Firebase.Auth.FirebaseUser>
struct TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114;
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Uri/UriInfo
struct UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E;
// System.UriParser
struct UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// WebelinxAppleAuthController
struct WebelinxAppleAuthController_t8256E6D120C43A6DCBD58B2252E235C187CD0280;
// WebelinxAppleAuthController/<>c
struct U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t5D2D131226D740682F598DD09436558292112781_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FacebookAuthProvider_t05151B6B548EB3C91D029FABAB200D7EF11500B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_t62E378E9873F9801909C3B38FB5B032D7A9929EA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t15B010A6F214BD2814994B7E16217E4DADF346CA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPasswordCredential_tC5ACB72704F73E0A1229C3B9B8792031098F1D2E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IUserInfo_t7265E03A4665719EDE78D7F016A2B46DD9C8FE45_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlayGamesAuthProvider_t68590058994858EAA3037ECD3E0CB2264C369324_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RealUserStatus_t3439EB7A9F2D88C6F5FF8D3F571A4B8C92EF596D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VideoCaptureMode_tBF0E331D46B351F8E6097418294116AB4D363609_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VideoQualityLevel_t9E3687DDDE2ABCAE7A1E98DF796AA4CE6535BA52_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral025A63FFA169F26B605C16CA69D0B1E95EC4FB19;
IL2CPP_EXTERN_C String_t* _stringLiteral072C51017526DB281D00402E87CF677C58A7D757;
IL2CPP_EXTERN_C String_t* _stringLiteral0B99CEBE565822C64AC5D84AECB00FE40E59CBD3;
IL2CPP_EXTERN_C String_t* _stringLiteral0BA55F1443103280F88491FDEF25033AA09A9BDE;
IL2CPP_EXTERN_C String_t* _stringLiteral102DA49B7841C2BD193FAB2C18BD222433515F4B;
IL2CPP_EXTERN_C String_t* _stringLiteral135EFDF4B044C63D9D43D56C023F20057C9C5538;
IL2CPP_EXTERN_C String_t* _stringLiteral1415C8E5B39B4B7B13DDF8C2BEBEA3BBEAF5BBAC;
IL2CPP_EXTERN_C String_t* _stringLiteral14CA22B9873E0A288F1FEDAA44E420F347889689;
IL2CPP_EXTERN_C String_t* _stringLiteral16BA435E490B03725CE5809E156F7C12C7ED4493;
IL2CPP_EXTERN_C String_t* _stringLiteral1988A1C5E0F5737FC0FD47973E5DD7EE29CE933B;
IL2CPP_EXTERN_C String_t* _stringLiteral19EDC1210777BA4D45049C29280D9CC5E1064C25;
IL2CPP_EXTERN_C String_t* _stringLiteral1D290DEA00FCBEC55F83A1B68E9C20A643CF6956;
IL2CPP_EXTERN_C String_t* _stringLiteral237EA81755C751F30D0CF842D4B244CF71A0A5CB;
IL2CPP_EXTERN_C String_t* _stringLiteral2660CB30675D112248C692B6C4EDB12FCBBE23DF;
IL2CPP_EXTERN_C String_t* _stringLiteral29D93BE9A1B69EE006EA76D23E4115200F42F51B;
IL2CPP_EXTERN_C String_t* _stringLiteral2D67D11CCBBE0B198201732C9307DA04B494B79F;
IL2CPP_EXTERN_C String_t* _stringLiteral316CC475D64EC17BB3A8AF209DF6E312A67D13A5;
IL2CPP_EXTERN_C String_t* _stringLiteral34A3A1806BCBD2EC0D7FABD0FF470B637146B453;
IL2CPP_EXTERN_C String_t* _stringLiteral34EB4C4EF005207E8B8F916B9F1FFFACCCD6945E;
IL2CPP_EXTERN_C String_t* _stringLiteral359BBFFBCF9FF96C06E2E9D27055844D66667163;
IL2CPP_EXTERN_C String_t* _stringLiteral36AD9F350774B31288103E40432E5EAF745BB520;
IL2CPP_EXTERN_C String_t* _stringLiteral36C3EAA0E1E290F41E2810BAE8D9502C785E92D9;
IL2CPP_EXTERN_C String_t* _stringLiteral394E68F963EACC7FF630DAEC06B6F196322850F0;
IL2CPP_EXTERN_C String_t* _stringLiteral3B0F99F6AB7B8C32DCDA4C7DA28B7D760CE8AC05;
IL2CPP_EXTERN_C String_t* _stringLiteral3D54973F528B01019A58A52D34D518405A01B891;
IL2CPP_EXTERN_C String_t* _stringLiteral3F67E8F4EECF241B91F4CC8C976A487ADE34D09D;
IL2CPP_EXTERN_C String_t* _stringLiteral3F769C6CABA7165B6F0B273132D26B39ADF0B841;
IL2CPP_EXTERN_C String_t* _stringLiteral416B26AAF7E7D413C1FF74BC712BA0CFA4EF92EB;
IL2CPP_EXTERN_C String_t* _stringLiteral4278CC782CD0D8EE6104E01420569B266CD0F43C;
IL2CPP_EXTERN_C String_t* _stringLiteral4411580845F5E7331D1E9CFE4B38E5A0461D487D;
IL2CPP_EXTERN_C String_t* _stringLiteral448D433E9E85775BD6DF632B67AB13A453FF43B1;
IL2CPP_EXTERN_C String_t* _stringLiteral48CD9ADAAB6410711E011ABA2FC738DC0F0708AC;
IL2CPP_EXTERN_C String_t* _stringLiteral4A32DE264ABAE3B7609ED468471FB34F2331402B;
IL2CPP_EXTERN_C String_t* _stringLiteral4A354D2B71364BACE1F77A0943FFC64AE95AC84A;
IL2CPP_EXTERN_C String_t* _stringLiteral4C82CBE32B969CCCC3691112EA5346CFC90C437B;
IL2CPP_EXTERN_C String_t* _stringLiteral4E120F1F20428E6C068E9B1346D9C2E8269221F6;
IL2CPP_EXTERN_C String_t* _stringLiteral4F05CBFCA4DFE76B99B142F609CDCF00D44FA247;
IL2CPP_EXTERN_C String_t* _stringLiteral4FF447B8EF42CA51FA6FB287BED8D40F49BE58F1;
IL2CPP_EXTERN_C String_t* _stringLiteral52049EFA43D99F2C3B47EFCD7FCF0510C8A323E0;
IL2CPP_EXTERN_C String_t* _stringLiteral53A610E925BBC0A175E365D31241AE75AEEAD651;
IL2CPP_EXTERN_C String_t* _stringLiteral564F937562204C6FF6E136F54872B28939A61D64;
IL2CPP_EXTERN_C String_t* _stringLiteral58FAF6730CD731832DEB85A8C0E16ACC72A8A179;
IL2CPP_EXTERN_C String_t* _stringLiteral5C014B442FE475C5DC58BDBB40E688F065E5B859;
IL2CPP_EXTERN_C String_t* _stringLiteral5C10B5B2CD673A0616D529AA5234B12EE7153808;
IL2CPP_EXTERN_C String_t* _stringLiteral5D3FA45438095F8732636CDFE60AFA58C78C4CC3;
IL2CPP_EXTERN_C String_t* _stringLiteral5FE917EE4ED152B3D78CFF96AE9517097B3B1693;
IL2CPP_EXTERN_C String_t* _stringLiteral615D43B81CED22F65E6ECF062ABD62E3F0D89EDC;
IL2CPP_EXTERN_C String_t* _stringLiteral63F467851E6EECFC10C66EB94087ADEA75EA3645;
IL2CPP_EXTERN_C String_t* _stringLiteral645F7AD1F8C46BE73BB67AD11D5F90C8B8077111;
IL2CPP_EXTERN_C String_t* _stringLiteral689B3D123355CBA2415D4FE457847D53ED8ADB52;
IL2CPP_EXTERN_C String_t* _stringLiteral68DF780818C857A0F6F7C26AB235DB1CBA45960E;
IL2CPP_EXTERN_C String_t* _stringLiteral6BDC2D6BA4C498FC8F065844D55008C84BC8B7E9;
IL2CPP_EXTERN_C String_t* _stringLiteral6EAE3A5B062C6D0D79F070C26E6D62486B40CB46;
IL2CPP_EXTERN_C String_t* _stringLiteral72A9C4912E926376C2941C84EBE9D72FA67F2A9C;
IL2CPP_EXTERN_C String_t* _stringLiteral7DD3751EA33F73AB8A2B9F9A41CB7DBDD7A4E3ED;
IL2CPP_EXTERN_C String_t* _stringLiteral8BEAF8B48CD239C5B9FD4F82FBBD3B06F143113D;
IL2CPP_EXTERN_C String_t* _stringLiteral8E135F653FD909BB5456F6E468ABB5D09C7579D0;
IL2CPP_EXTERN_C String_t* _stringLiteral90FD22C37BF4C83D185F8535C863E072DFB4ED39;
IL2CPP_EXTERN_C String_t* _stringLiteral9A7B006D203B362C8CEF6DA001685678FC1D463A;
IL2CPP_EXTERN_C String_t* _stringLiteral9BF54B3696FB70DB7DC021E3F63F039E59C9C412;
IL2CPP_EXTERN_C String_t* _stringLiteral9DF2980BA224D990316F675063181489A7731F76;
IL2CPP_EXTERN_C String_t* _stringLiteral9F7FEF0CB5306BE5E44617752BF0F2B4C36BDDFE;
IL2CPP_EXTERN_C String_t* _stringLiteralA06BF1793036ECE1883508A055CDB1132BDF2164;
IL2CPP_EXTERN_C String_t* _stringLiteralA1A8C523C1AE6DB7A1C78DB548B44DAFD0886747;
IL2CPP_EXTERN_C String_t* _stringLiteralA1EBE8CFEDDD9906386BE64D944F2E6663CC9C4B;
IL2CPP_EXTERN_C String_t* _stringLiteralAB05430164D31C596A61F882DA43E4452DDAABEC;
IL2CPP_EXTERN_C String_t* _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC;
IL2CPP_EXTERN_C String_t* _stringLiteralB45C7AA548BE5CFE94BE16743D453E6201708610;
IL2CPP_EXTERN_C String_t* _stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6;
IL2CPP_EXTERN_C String_t* _stringLiteralB92F72B9FD3D0FA7FF9E7A63992988FBDF5675DD;
IL2CPP_EXTERN_C String_t* _stringLiteralBE197378B78317428D4793F71D084FD494BBA3D9;
IL2CPP_EXTERN_C String_t* _stringLiteralC3A1FE3B518D4A8B1B4B10B5CCDC5BC1E9406A69;
IL2CPP_EXTERN_C String_t* _stringLiteralC906E19FA4044BF51882DA58C9D614C90A29617A;
IL2CPP_EXTERN_C String_t* _stringLiteralCC1B4E6F77870BDBE599FBA4423DC2A71FD628BD;
IL2CPP_EXTERN_C String_t* _stringLiteralCEB04F9890A96997A98081B3F484478976B1F1B1;
IL2CPP_EXTERN_C String_t* _stringLiteralD1AF1318D2CA2E5BA5E5F111775A8F1645093512;
IL2CPP_EXTERN_C String_t* _stringLiteralD24DA40C8EF2074010E98D0B9F4624FB353DC060;
IL2CPP_EXTERN_C String_t* _stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46;
IL2CPP_EXTERN_C String_t* _stringLiteralD5C6A5F48C3935F1D696BB81558492909CB4F13C;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDA9F24755DE561B483F9D678415574B58F334213;
IL2CPP_EXTERN_C String_t* _stringLiteralDCE627299BC3B788CDE5457E1ED6E88FC06DA055;
IL2CPP_EXTERN_C String_t* _stringLiteralDEAE2C9F6284B3727A6AF84C2BF44F20A87C0A0D;
IL2CPP_EXTERN_C String_t* _stringLiteralDEDB5B33E3227FE4457EF07E6806353303A1FE2F;
IL2CPP_EXTERN_C String_t* _stringLiteralE2C84573C370B2CF156317B86B0B80D053A092D3;
IL2CPP_EXTERN_C String_t* _stringLiteralE338843047716F5CA7F106D3A7B0C89672BD92FF;
IL2CPP_EXTERN_C String_t* _stringLiteralE6DA9EE605CA4B2469C8D51E07E3BA0C0CD094C3;
IL2CPP_EXTERN_C String_t* _stringLiteralE8697BF486B289F063DC8C65D77D61852BE0C0A5;
IL2CPP_EXTERN_C String_t* _stringLiteralEBBD5CD2F3EABC5C6D7DF0BB6ED351BCD4DDAC05;
IL2CPP_EXTERN_C String_t* _stringLiteralEC5AC77052DAC34F0AD8EFF9491B42E3510F12C2;
IL2CPP_EXTERN_C String_t* _stringLiteralF04BD4D89F152A5646CCEB0E33CECEC5FE132963;
IL2CPP_EXTERN_C String_t* _stringLiteralF3B0336F4480D618DC414AF075986C7F133F0BF8;
IL2CPP_EXTERN_C String_t* _stringLiteralFA7C7E6B20F20C131CD40822EBEEFDFDA03EA52F;
IL2CPP_EXTERN_C String_t* _stringLiteralFA98C1FD2CA6FC89B5ED010FD16AA461F50AFB3E;
IL2CPP_EXTERN_C String_t* _stringLiteralFD3EDC641024A335A508FDACEFB5F51DED5905CC;
IL2CPP_EXTERN_C String_t* _stringLiteralFDE49AA00F4966A6ABC348B5B3B985032EABF411;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m068FBAE923EF2CD287CD903FDE55A5AF890E8623_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mC614E47849BEBE0AF8A23568BBA0E74B035427AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_Call_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m775AB90594C5F27D6099ED61119EF3608FD1001D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m75C23C174DE8C46B92343E6304A8A7DB305B407B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m9E5157EB15E8B864F3CC6D184BA6371E2A601EFA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m9082FAC2380F0EE9C2D576C66BF0B016931027CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisString_t_mA66BDED9E6FDCFE3BB7C27E0C69135B0EB24C4CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisString_t_m34F0EB317BEC1783F5D3E46852744966D246040C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m0F7AA651E81830E11A33BA5B2CACFFBE25F610A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m08F3FE51A64369D57EC067889997CBA5F81DCC73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mEDF66107B1E3BECA631A37771397FAC62B4BB21B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_mBAD3E8F66408158F3CBC5309309B4545614D38F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisPlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_m89B34D1AD3E162ECA397870BC80FD50E6A99DA3E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_m8AFF779F458D803CEE3FD8A0A2CCD5B5C18B2ED8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mBF59C2344C951987CF2A02BFE8F7F1BF795434FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m851B12AA396220AB3F093C8B3D84B0E815E8A38B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m1087577496C9953C2B9FE83421C010D93E1E39DE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m1A6BEB03A9E4B76F009EACE9E2DC8202C1B00B0D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m201C000881D6CDC3645867A7B25986ADF9614086_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m2806B6CAA6E2D1E105FDA5CCB083EFDD8591339E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MainMenu_U3CAttemptQuickLoginU3Eb__15_0_mE971ABB7EF757755E9980F507531CAB5D16A2549_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MainMenu_U3CAttemptQuickLoginU3Eb__15_1_m3FFF75DA7F2BA4DB719F829F8C4623FFC639B467_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MainMenu_U3CInitializeLoginMenuU3Eb__7_0_mC33C351ABC83249E18FA1ED7E2C2010992174DFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MainMenu_U3CSignInWithAppleU3Eb__16_0_mE59B75DF412205BE9CF77711BDBB75A56461D83F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MainMenu_U3CSignInWithAppleU3Eb__16_1_m2E75C508FE4FBF8F3789A238CD92E5CC8516620B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Misc_CheckNotNull_TisAction_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9_m240E10DDF67AD2B8678A5504DA835C7ACDA4FA2A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Misc_CheckNotNull_TisByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_m6BE81B7330BF6B723B4069514F3C7ACFDAAA5A13_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m99DC0DFF0CCF30E4DFFED5F8F7ABE8721B73B56D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_m619F23350E60DA452BA9F320E9EA38E1D933663D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_0_m7CB32875FC16DF9BC460CA91228508118DC534AD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_1_m7A93743A0A6D77967CFF416B59225ED7EDAF2E93_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_0_m361BD96B14EE16FCC87B6958136DB824453515A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_1_m0F1243EEA002F490F1766EF845A67A40C7E060F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CToLogMessageU3Eb__12_0_m1B59C28B2C712520830759AEE37ECA1FFE239DF0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CToStringU3Eb__14_0_m75FE11CA93BBBB7EE560F5115E951A2DE484A04B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CToStringU3Eb__14_1_m7F792E66E4DB839ECE1667C2895A77226FBE14BF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass10_0_U3CRunCoroutineU3Eb__0_m4C5D12A9EFD2B686E793203491194E13C6C5A46D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass10_0_U3CeU3Eb__0_m0B9AE2FE606892EF2909B280223B84E4646E8F73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__0_m0E51F1E824212F9985EDD569F5D0E2947979B370_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__1_m2FAFDE5F0F9B6B2AEDFE777D0850E452BFB896B6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass8_0_U3CdU3Eb__0_m48020CDD6BCC8C1325960C7B8ACAF21C9C46590A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass9_0_U3CwU3Eb__0_m357B91D2A7DFEF1DA7865CAF4E998B09A0A120E7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WebelinxFirebaseAuthManager_U3CLinkAccountWithAnotherProviderU3Eb__10_0_m531EBED0E89D1B164F16D95AC18B0F7198D57ED7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WebelinxFirebaseAuthManager_U3CSignInAnonymouslyU3Eb__11_0_m42C7B73B3A78BCB3B5D7BF9C5599F6B8256D5B21_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WebelinxFirebaseAuthManager_U3CSignInThroughProviderU3Eb__9_0_m30F8227F6F0FEE4CECB0C0B592AA3E9203669A0C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WebelinxFirebaseAuthView_OnUserSignedOut_m496B5B915654C58C956EEAF9D5271A62620EE580_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_com_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse_AlreadyConnected_mB3BBBB13B00069C8B7691638AA5F542D2B325A5B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse_EndpointNotConnected_m77D7E68783C9BAF4ADEE6C7675AF7185DF452D18_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse_InternalError_mDF78FF322A6E590BD4D524645B41A9A1DC800DD5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse_NetworkNotConnected_m1F2A5E374AB38812517E2908D8423C5444AC8D7A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse_Rejected_mA6985CBE633FF1E10C02C1E89B8752520816F4CB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse__cctor_m98A4E442ECE05DE88094B57CE48D5647EE345B4D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_com_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameMenuHandler_SetupAppleData_m5878285CA5056522A0FD6BDC58739DCEC59E6BC2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger__cctor_m42BF51365CFF03BA353B071F75D18DE0C7F277FD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_d_m95C0BD29B60442754276E97D1FB7ED05E4AC31CE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_describe_m6CF7621BCB43B5A4F721ECB71FE509F1EAFD0EB1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_get_DebugLogEnabled_m3953BFF21DD3B54D640C75656E846C07EF02FB20_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_get_WarningLogEnabled_m2E33359F4E5A78915FD65DD1C51BDCA7AB54F833_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_set_DebugLogEnabled_m5787BDC789E038FC7142BC2EF160318D0D1A918E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_set_WarningLogEnabled_mB416AC86C073873489EA2EFB51C2A70A93E148BF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_AttemptQuickLogin_m1C4F040B20F22A88A09469378F69540EFEAFB883_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_CheckCredentialStatusForUserId_m33A8D3E7479359851349A1532CC5031D03AFE3B2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_InitializeLoginMenu_m6DF9DFCD30D84C572C0FAE153A293E972409D333_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_SetupLoginMenuForAppleSignIn_m314FDA3B2F46F68904F84841840033848ADB585D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_SetupLoginMenuForCheckingCredentials_m16AEE25404D91E530C121EFFFF3257C074EEBF25_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_SetupLoginMenuForQuickLoginAttempt_m351D9B48275D2D2587C3FFD3F5F4D6BF002C948F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_SetupLoginMenuForUnsupportedPlatform_m80C029D8C208B629AF2849559B8D653AD4FCF374_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_SignInWithApple_m54E769319FF0899CCD150E6BB1602E3E5ECFBE94_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_U3CAttemptQuickLoginU3Eb__15_0_mE971ABB7EF757755E9980F507531CAB5D16A2549_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_U3CAttemptQuickLoginU3Eb__15_1_m3FFF75DA7F2BA4DB719F829F8C4623FFC639B467_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_U3CInitializeLoginMenuU3Eb__7_0_mC33C351ABC83249E18FA1ED7E2C2010992174DFE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_U3CSignInWithAppleU3Eb__16_0_mE59B75DF412205BE9CF77711BDBB75A56461D83F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_U3CSignInWithAppleU3Eb__16_1_m2E75C508FE4FBF8F3789A238CD92E5CC8516620B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Misc_IsApiException_m303491E5CA3BFA339C9A5EBFFE55F7C0C3BB9F99_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_com_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_AddFocusCallback_m6E1E5A60B199070EC622D2D47B50D8C20319B82E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_AddPauseCallback_m97D958B110BFCA92A31FA362904CEF6AA1FCD00A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_Awake_m38AAE71B9B1F9F4D3F98E863119E2235A3B8251F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_CreateObject_m35E4ACCC42D970BFD0BA33D133F35B51A2137601_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_OnApplicationFocus_m9A19C80383E682429043888202B65FD0BD998994_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_OnApplicationPause_mADFF6C814E67628969732FC37BCC560B296D4AB6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_OnDisable_m395CECEDE06F5F33F23B85418FCFB7FDBFE8B82C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_RemoveFocusCallback_mEA5550B390C0301AB6AE9CFB43379E80E9326979_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_RemovePauseCallback_m941669D42C479896AE60A61F2800C97DABE2EA5F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_RunCoroutine_mCDC6CBF287B6D8EBE869A970C5770808A95DC194_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject_Update_m803AA51E52E865F91C722354EE7156072705F5EE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject__cctor_mED148D40A157EBA3FC978F09D87A3225C30ADF32_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesHelperObject__ctor_m6F2894860B6C363E51DFB53E692ADCDCCB10D9B1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SignInHelper_SetPromptUiSignIn_mFF3C68D64C52D79EB92293B4929FD6D4FF96AE43_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SignInHelper_ShouldPromptUiSignIn_m057F98856138682BB850BAFCB4E5FC918527563B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SignInHelper_ToSignInStatus_m859368773DCD252BDA21574CEFAE8B1D55DF08E0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SignInHelper__cctor_m362E7EFFA0C88CB9F48C1072547815CE968EB927_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SignedInUserFirebaseInfo_ToString_mDA7E55088D45FA5DDD17F6C5EA660A1B45EE3983_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SignedInUserProviderInfo_SetData_mD617F76287BBA2E8C70996A7B1100C8855D01F2D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SignedInUserProviderInfo_ToString_m0CB989A3CA76D9F627FE4DA65A3884C2D44742E2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_0_m7CB32875FC16DF9BC460CA91228508118DC534AD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_0_m361BD96B14EE16FCC87B6958136DB824453515A5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_1_m0F1243EEA002F490F1766EF845A67A40C7E060F7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CToLogMessageU3Eb__12_0_m1B59C28B2C712520830759AEE37ECA1FFE239DF0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass10_0_U3CRunCoroutineU3Eb__0_m4C5D12A9EFD2B686E793203491194E13C6C5A46D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass10_0_U3CeU3Eb__0_m0B9AE2FE606892EF2909B280223B84E4646E8F73_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__0_m0E51F1E824212F9985EDD569F5D0E2947979B370_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__1_m2FAFDE5F0F9B6B2AEDFE777D0850E452BFB896B6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass8_0_U3CdU3Eb__0_m48020CDD6BCC8C1325960C7B8ACAF21C9C46590A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass9_0_U3CwU3Eb__0_m357B91D2A7DFEF1DA7865CAF4E998B09A0A120E7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m18EE7693094C336A370250B287B562DF94875253_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m570ADC6E83A04B439A89A6F2CA68567764DC2A40_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_mE9D701C70C5874BD30D40926128A74E673F87ED1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_mEB73EA82F54AD404E29A52A9FED6395C77CF9BAF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VideoCapabilities_SupportsCaptureMode_m014CA89403FB9C0A684A10ACF384763EB1C4DBFE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VideoCapabilities_SupportsQualityLevel_mD6477AF89B8A9C2456B93EF605CD58BC26BDF402_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VideoCapabilities_ToString_m794CEF4D0F7C3177434876703967E3C20343CF7B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VideoCaptureState_ToString_m31D72AE6DCABEE2EB09502165175970030712606_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxAnonymousAuthController_Awake_mDC1A3C9F3989E599312109DF59CA727B807DFE96_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxAnonymousAuthController_LoginAnonymously_mB1FDA14714B88436EF79AB2C95EDC6293EE3C602_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxAppleAuthController_AppleAuthHandler_m9C86C49F406E5269A87D21CDF0E43AEF6280BA07_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxAppleAuthController_Start_mC5EE45F17610B28F7978FD18AFC8504770675FA2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxAppleAuthController_Update_m6DCB8BF987E305E3FE6BDFFF0857C5AB7349C25D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_Awake_m23637A0CEA31F6BD70DBECA038DE4D90573A89A2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_InitializeFirebase_mC06C2FF001E3CE03236086DF841C3AA2B729802F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_LinkAccountWithAnotherProvider_m520CC13E2A4771D5DF7167E773A41EA0B619E2AB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_OnDestroy_mBC21EF9D772490890B413F03E5224062C68B12AD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_SetUserInformations_mEF76DEF1EEA9516F497E6F12D673AC56F68AC85B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_SignInAnonymously_m871FBA002D82959565F2E81BD8E28CB7E4D4D2F7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_SignInThroughProvider_m053BD42E82A8F70CD2BF5E4487A977E37B18623C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_SignOut_mB3FA9243EEFD54BCC0B77A337DD18027954406A5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_SyncFirebaseUserDataWithProviderData_m4F5FAF541ED3A1D4F9F72E0B2247C3B4EE92BA36_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_U3CLinkAccountWithAnotherProviderU3Eb__10_0_m531EBED0E89D1B164F16D95AC18B0F7198D57ED7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_U3CSignInAnonymouslyU3Eb__11_0_m42C7B73B3A78BCB3B5D7BF9C5599F6B8256D5B21_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthManager_U3CSignInThroughProviderU3Eb__9_0_m30F8227F6F0FEE4CECB0C0B592AA3E9203669A0C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthView_Awake_m4D3CA124DE180FF292DAA935E31F5CFD616F5A95_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthView_DisplayUserProfilePicture_mCFB8F1344ACD38DB02A6C872D137A27AC2C5618E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthView_OnUserSignedOut_m496B5B915654C58C956EEAF9D5271A62620EE580_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthView_Start_m82368ADFCEF22D8B4E7779B2C09A2C10BC48E4D4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893;;
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com;
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com;;
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke;
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke;;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// System.Object


// AppleAuth.AppleAuthManager
struct  AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C  : public RuntimeObject
{
public:
	// AppleAuth.Interfaces.IPayloadDeserializer AppleAuth.AppleAuthManager::_payloadDeserializer
	RuntimeObject* ____payloadDeserializer_0;
	// System.Action`1<System.String> AppleAuth.AppleAuthManager::_credentialsRevokedCallback
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ____credentialsRevokedCallback_1;

public:
	inline static int32_t get_offset_of__payloadDeserializer_0() { return static_cast<int32_t>(offsetof(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C, ____payloadDeserializer_0)); }
	inline RuntimeObject* get__payloadDeserializer_0() const { return ____payloadDeserializer_0; }
	inline RuntimeObject** get_address_of__payloadDeserializer_0() { return &____payloadDeserializer_0; }
	inline void set__payloadDeserializer_0(RuntimeObject* value)
	{
		____payloadDeserializer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____payloadDeserializer_0), (void*)value);
	}

	inline static int32_t get_offset_of__credentialsRevokedCallback_1() { return static_cast<int32_t>(offsetof(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C, ____credentialsRevokedCallback_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get__credentialsRevokedCallback_1() const { return ____credentialsRevokedCallback_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of__credentialsRevokedCallback_1() { return &____credentialsRevokedCallback_1; }
	inline void set__credentialsRevokedCallback_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		____credentialsRevokedCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____credentialsRevokedCallback_1), (void*)value);
	}
};


// AppleAuth.Native.PayloadDeserializer
struct  PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E  : public RuntimeObject
{
public:

public:
};


// Authentication.SignedInUserFirebaseInfo
struct  SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3  : public RuntimeObject
{
public:
	// System.String Authentication.SignedInUserFirebaseInfo::<UserUUID>k__BackingField
	String_t* ___U3CUserUUIDU3Ek__BackingField_0;
	// System.String Authentication.SignedInUserFirebaseInfo::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_1;
	// System.String Authentication.SignedInUserFirebaseInfo::<Email>k__BackingField
	String_t* ___U3CEmailU3Ek__BackingField_2;
	// System.Boolean Authentication.SignedInUserFirebaseInfo::<IsAnonymous>k__BackingField
	bool ___U3CIsAnonymousU3Ek__BackingField_3;
	// System.Uri Authentication.SignedInUserFirebaseInfo::<PhotoUri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CPhotoUriU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CUserUUIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3, ___U3CUserUUIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CUserUUIDU3Ek__BackingField_0() const { return ___U3CUserUUIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUserUUIDU3Ek__BackingField_0() { return &___U3CUserUUIDU3Ek__BackingField_0; }
	inline void set_U3CUserUUIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CUserUUIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUserUUIDU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3, ___U3CDisplayNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_1() const { return ___U3CDisplayNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_1() { return &___U3CDisplayNameU3Ek__BackingField_1; }
	inline void set_U3CDisplayNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDisplayNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEmailU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3, ___U3CEmailU3Ek__BackingField_2)); }
	inline String_t* get_U3CEmailU3Ek__BackingField_2() const { return ___U3CEmailU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CEmailU3Ek__BackingField_2() { return &___U3CEmailU3Ek__BackingField_2; }
	inline void set_U3CEmailU3Ek__BackingField_2(String_t* value)
	{
		___U3CEmailU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEmailU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsAnonymousU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3, ___U3CIsAnonymousU3Ek__BackingField_3)); }
	inline bool get_U3CIsAnonymousU3Ek__BackingField_3() const { return ___U3CIsAnonymousU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsAnonymousU3Ek__BackingField_3() { return &___U3CIsAnonymousU3Ek__BackingField_3; }
	inline void set_U3CIsAnonymousU3Ek__BackingField_3(bool value)
	{
		___U3CIsAnonymousU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CPhotoUriU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3, ___U3CPhotoUriU3Ek__BackingField_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CPhotoUriU3Ek__BackingField_4() const { return ___U3CPhotoUriU3Ek__BackingField_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CPhotoUriU3Ek__BackingField_4() { return &___U3CPhotoUriU3Ek__BackingField_4; }
	inline void set_U3CPhotoUriU3Ek__BackingField_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CPhotoUriU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPhotoUriU3Ek__BackingField_4), (void*)value);
	}
};


// Authentication.SignedInUserProviderInfo
struct  SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1  : public RuntimeObject
{
public:
	// System.String Authentication.SignedInUserProviderInfo::<ProviderNameID>k__BackingField
	String_t* ___U3CProviderNameIDU3Ek__BackingField_0;
	// System.String Authentication.SignedInUserProviderInfo::<UserProviderID>k__BackingField
	String_t* ___U3CUserProviderIDU3Ek__BackingField_1;
	// System.String Authentication.SignedInUserProviderInfo::<Email>k__BackingField
	String_t* ___U3CEmailU3Ek__BackingField_2;
	// System.String Authentication.SignedInUserProviderInfo::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_3;
	// System.Uri Authentication.SignedInUserProviderInfo::<PhotoUri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CPhotoUriU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CProviderNameIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1, ___U3CProviderNameIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CProviderNameIDU3Ek__BackingField_0() const { return ___U3CProviderNameIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CProviderNameIDU3Ek__BackingField_0() { return &___U3CProviderNameIDU3Ek__BackingField_0; }
	inline void set_U3CProviderNameIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CProviderNameIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CProviderNameIDU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUserProviderIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1, ___U3CUserProviderIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CUserProviderIDU3Ek__BackingField_1() const { return ___U3CUserProviderIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUserProviderIDU3Ek__BackingField_1() { return &___U3CUserProviderIDU3Ek__BackingField_1; }
	inline void set_U3CUserProviderIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CUserProviderIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUserProviderIDU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEmailU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1, ___U3CEmailU3Ek__BackingField_2)); }
	inline String_t* get_U3CEmailU3Ek__BackingField_2() const { return ___U3CEmailU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CEmailU3Ek__BackingField_2() { return &___U3CEmailU3Ek__BackingField_2; }
	inline void set_U3CEmailU3Ek__BackingField_2(String_t* value)
	{
		___U3CEmailU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEmailU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1, ___U3CDisplayNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_3() const { return ___U3CDisplayNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_3() { return &___U3CDisplayNameU3Ek__BackingField_3; }
	inline void set_U3CDisplayNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDisplayNameU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPhotoUriU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1, ___U3CPhotoUriU3Ek__BackingField_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CPhotoUriU3Ek__BackingField_4() const { return ___U3CPhotoUriU3Ek__BackingField_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CPhotoUriU3Ek__BackingField_4() { return &___U3CPhotoUriU3Ek__BackingField_4; }
	inline void set_U3CPhotoUriU3Ek__BackingField_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CPhotoUriU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPhotoUriU3Ek__BackingField_4), (void*)value);
	}
};


// Authentication.WebelinxFirebaseAuthManager_<>c
struct  U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields
{
public:
	// Authentication.WebelinxFirebaseAuthManager_<>c Authentication.WebelinxFirebaseAuthManager_<>c::<>9
	U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * ___U3CU3E9_0;
	// System.Action`1<System.Threading.Tasks.Task> Authentication.WebelinxFirebaseAuthManager_<>c::<>9__16_0
	Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * ___U3CU3E9__16_0_1;
	// System.Action`1<System.Threading.Tasks.Task> Authentication.WebelinxFirebaseAuthManager_<>c::<>9__16_1
	Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * ___U3CU3E9__16_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields, ___U3CU3E9__16_0_1)); }
	inline Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields, ___U3CU3E9__16_1_2)); }
	inline Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * get_U3CU3E9__16_1_2() const { return ___U3CU3E9__16_1_2; }
	inline Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 ** get_address_of_U3CU3E9__16_1_2() { return &___U3CU3E9__16_1_2; }
	inline void set_U3CU3E9__16_1_2(Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * value)
	{
		___U3CU3E9__16_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_1_2), (void*)value);
	}
};


// GPGSIds
struct  GPGSIds_t53FDC85AE5CFBB008A259019EA1F6C0679FF1298  : public RuntimeObject
{
public:

public:
};


// GameMenuHandler
struct  GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameMenuHandler::Parent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Parent_0;
	// UnityEngine.UI.Text GameMenuHandler::AppleUserIdLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___AppleUserIdLabel_1;
	// UnityEngine.UI.Text GameMenuHandler::AppleUserCredentialLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___AppleUserCredentialLabel_2;

public:
	inline static int32_t get_offset_of_Parent_0() { return static_cast<int32_t>(offsetof(GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106, ___Parent_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Parent_0() const { return ___Parent_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Parent_0() { return &___Parent_0; }
	inline void set_Parent_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Parent_0), (void*)value);
	}

	inline static int32_t get_offset_of_AppleUserIdLabel_1() { return static_cast<int32_t>(offsetof(GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106, ___AppleUserIdLabel_1)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_AppleUserIdLabel_1() const { return ___AppleUserIdLabel_1; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_AppleUserIdLabel_1() { return &___AppleUserIdLabel_1; }
	inline void set_AppleUserIdLabel_1(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___AppleUserIdLabel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppleUserIdLabel_1), (void*)value);
	}

	inline static int32_t get_offset_of_AppleUserCredentialLabel_2() { return static_cast<int32_t>(offsetof(GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106, ___AppleUserCredentialLabel_2)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_AppleUserCredentialLabel_2() const { return ___AppleUserCredentialLabel_2; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_AppleUserCredentialLabel_2() { return &___AppleUserCredentialLabel_2; }
	inline void set_AppleUserCredentialLabel_2(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___AppleUserCredentialLabel_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppleUserCredentialLabel_2), (void*)value);
	}
};


// GooglePlayGames.BasicApi.CommonTypesUtil
struct  CommonTypesUtil_t2873297E6294F5B79AF6B4087BCF1092C7B069E4  : public RuntimeObject
{
public:

public:
};


// GooglePlayGames.BasicApi.SignInHelper
struct  SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58  : public RuntimeObject
{
public:

public:
};

struct SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SignInHelper::True
	int32_t ___True_0;
	// System.Int32 GooglePlayGames.BasicApi.SignInHelper::False
	int32_t ___False_1;

public:
	inline static int32_t get_offset_of_True_0() { return static_cast<int32_t>(offsetof(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields, ___True_0)); }
	inline int32_t get_True_0() const { return ___True_0; }
	inline int32_t* get_address_of_True_0() { return &___True_0; }
	inline void set_True_0(int32_t value)
	{
		___True_0 = value;
	}

	inline static int32_t get_offset_of_False_1() { return static_cast<int32_t>(offsetof(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields, ___False_1)); }
	inline int32_t get_False_1() const { return ___False_1; }
	inline int32_t* get_address_of_False_1() { return &___False_1; }
	inline void set_False_1(int32_t value)
	{
		___False_1 = value;
	}
};


// GooglePlayGames.BasicApi.Video.VideoCapabilities
struct  VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4  : public RuntimeObject
{
public:
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsCameraSupported
	bool ___mIsCameraSupported_0;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsMicSupported
	bool ___mIsMicSupported_1;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::mIsWriteStorageSupported
	bool ___mIsWriteStorageSupported_2;
	// System.Boolean[] GooglePlayGames.BasicApi.Video.VideoCapabilities::mCaptureModesSupported
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___mCaptureModesSupported_3;
	// System.Boolean[] GooglePlayGames.BasicApi.Video.VideoCapabilities::mQualityLevelsSupported
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___mQualityLevelsSupported_4;

public:
	inline static int32_t get_offset_of_mIsCameraSupported_0() { return static_cast<int32_t>(offsetof(VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4, ___mIsCameraSupported_0)); }
	inline bool get_mIsCameraSupported_0() const { return ___mIsCameraSupported_0; }
	inline bool* get_address_of_mIsCameraSupported_0() { return &___mIsCameraSupported_0; }
	inline void set_mIsCameraSupported_0(bool value)
	{
		___mIsCameraSupported_0 = value;
	}

	inline static int32_t get_offset_of_mIsMicSupported_1() { return static_cast<int32_t>(offsetof(VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4, ___mIsMicSupported_1)); }
	inline bool get_mIsMicSupported_1() const { return ___mIsMicSupported_1; }
	inline bool* get_address_of_mIsMicSupported_1() { return &___mIsMicSupported_1; }
	inline void set_mIsMicSupported_1(bool value)
	{
		___mIsMicSupported_1 = value;
	}

	inline static int32_t get_offset_of_mIsWriteStorageSupported_2() { return static_cast<int32_t>(offsetof(VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4, ___mIsWriteStorageSupported_2)); }
	inline bool get_mIsWriteStorageSupported_2() const { return ___mIsWriteStorageSupported_2; }
	inline bool* get_address_of_mIsWriteStorageSupported_2() { return &___mIsWriteStorageSupported_2; }
	inline void set_mIsWriteStorageSupported_2(bool value)
	{
		___mIsWriteStorageSupported_2 = value;
	}

	inline static int32_t get_offset_of_mCaptureModesSupported_3() { return static_cast<int32_t>(offsetof(VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4, ___mCaptureModesSupported_3)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_mCaptureModesSupported_3() const { return ___mCaptureModesSupported_3; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_mCaptureModesSupported_3() { return &___mCaptureModesSupported_3; }
	inline void set_mCaptureModesSupported_3(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___mCaptureModesSupported_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCaptureModesSupported_3), (void*)value);
	}

	inline static int32_t get_offset_of_mQualityLevelsSupported_4() { return static_cast<int32_t>(offsetof(VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4, ___mQualityLevelsSupported_4)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_mQualityLevelsSupported_4() const { return ___mQualityLevelsSupported_4; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_mQualityLevelsSupported_4() { return &___mQualityLevelsSupported_4; }
	inline void set_mQualityLevelsSupported_4(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___mQualityLevelsSupported_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mQualityLevelsSupported_4), (void*)value);
	}
};


// GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c
struct  U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields
{
public:
	// GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::<>9
	U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * ___U3CU3E9_0;
	// System.Func`2<System.Boolean,System.String> GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::<>9__14_0
	Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * ___U3CU3E9__14_0_1;
	// System.Func`2<System.Boolean,System.String> GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::<>9__14_1
	Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * ___U3CU3E9__14_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields, ___U3CU3E9__14_0_1)); }
	inline Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * get_U3CU3E9__14_0_1() const { return ___U3CU3E9__14_0_1; }
	inline Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 ** get_address_of_U3CU3E9__14_0_1() { return &___U3CU3E9__14_0_1; }
	inline void set_U3CU3E9__14_0_1(Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * value)
	{
		___U3CU3E9__14_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields, ___U3CU3E9__14_1_2)); }
	inline Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * get_U3CU3E9__14_1_2() const { return ___U3CU3E9__14_1_2; }
	inline Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 ** get_address_of_U3CU3E9__14_1_2() { return &___U3CU3E9__14_1_2; }
	inline void set_U3CU3E9__14_1_2(Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * value)
	{
		___U3CU3E9__14_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_1_2), (void*)value);
	}
};


// GooglePlayGames.OurUtils.Logger
struct  Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B  : public RuntimeObject
{
public:

public:
};

struct Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields
{
public:
	// System.Boolean GooglePlayGames.OurUtils.Logger::debugLogEnabled
	bool ___debugLogEnabled_0;
	// System.Boolean GooglePlayGames.OurUtils.Logger::warningLogEnabled
	bool ___warningLogEnabled_1;

public:
	inline static int32_t get_offset_of_debugLogEnabled_0() { return static_cast<int32_t>(offsetof(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields, ___debugLogEnabled_0)); }
	inline bool get_debugLogEnabled_0() const { return ___debugLogEnabled_0; }
	inline bool* get_address_of_debugLogEnabled_0() { return &___debugLogEnabled_0; }
	inline void set_debugLogEnabled_0(bool value)
	{
		___debugLogEnabled_0 = value;
	}

	inline static int32_t get_offset_of_warningLogEnabled_1() { return static_cast<int32_t>(offsetof(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields, ___warningLogEnabled_1)); }
	inline bool get_warningLogEnabled_1() const { return ___warningLogEnabled_1; }
	inline bool* get_address_of_warningLogEnabled_1() { return &___warningLogEnabled_1; }
	inline void set_warningLogEnabled_1(bool value)
	{
		___warningLogEnabled_1 = value;
	}
};


// GooglePlayGames.OurUtils.Logger_<>c
struct  U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_StaticFields
{
public:
	// GooglePlayGames.OurUtils.Logger_<>c GooglePlayGames.OurUtils.Logger_<>c::<>9
	U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * ___U3CU3E9_0;
	// System.Action GooglePlayGames.OurUtils.Logger_<>c::<>9__12_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__12_0_1), (void*)value);
	}
};


// GooglePlayGames.OurUtils.Logger_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger_<>c__DisplayClass10_0::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___msg_0), (void*)value);
	}
};


// GooglePlayGames.OurUtils.Logger_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger_<>c__DisplayClass8_0::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___msg_0), (void*)value);
	}
};


// GooglePlayGames.OurUtils.Logger_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger_<>c__DisplayClass9_0::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___msg_0), (void*)value);
	}
};


// GooglePlayGames.OurUtils.Misc
struct  Misc_t8380629A73C5A3858F7D44831402FCE50030AE39  : public RuntimeObject
{
public:

public:
};


// GooglePlayGames.OurUtils.PlayGamesHelperObject_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator GooglePlayGames.OurUtils.PlayGamesHelperObject_<>c__DisplayClass10_0::action
	RuntimeObject* ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2, ___action_0)); }
	inline RuntimeObject* get_action_0() const { return ___action_0; }
	inline RuntimeObject** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(RuntimeObject* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___action_0), (void*)value);
	}
};


// GooglePlayGames.PluginVersion
struct  PluginVersion_tB0E519E7845CD35129B206E909BC0EF6A24D6BE9  : public RuntimeObject
{
public:

public:
};


// LoginMenuHandler
struct  LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65  : public RuntimeObject
{
public:
	// UnityEngine.GameObject LoginMenuHandler::Parent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Parent_0;
	// UnityEngine.GameObject LoginMenuHandler::SignInWithAppleParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SignInWithAppleParent_1;
	// UnityEngine.UI.Button LoginMenuHandler::SignInWithAppleButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___SignInWithAppleButton_2;
	// UnityEngine.GameObject LoginMenuHandler::LoadingMessageParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___LoadingMessageParent_3;
	// UnityEngine.Transform LoginMenuHandler::LoadingIconTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___LoadingIconTransform_4;
	// UnityEngine.UI.Text LoginMenuHandler::LoadingMessageLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___LoadingMessageLabel_5;

public:
	inline static int32_t get_offset_of_Parent_0() { return static_cast<int32_t>(offsetof(LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65, ___Parent_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Parent_0() const { return ___Parent_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Parent_0() { return &___Parent_0; }
	inline void set_Parent_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Parent_0), (void*)value);
	}

	inline static int32_t get_offset_of_SignInWithAppleParent_1() { return static_cast<int32_t>(offsetof(LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65, ___SignInWithAppleParent_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SignInWithAppleParent_1() const { return ___SignInWithAppleParent_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SignInWithAppleParent_1() { return &___SignInWithAppleParent_1; }
	inline void set_SignInWithAppleParent_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SignInWithAppleParent_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SignInWithAppleParent_1), (void*)value);
	}

	inline static int32_t get_offset_of_SignInWithAppleButton_2() { return static_cast<int32_t>(offsetof(LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65, ___SignInWithAppleButton_2)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_SignInWithAppleButton_2() const { return ___SignInWithAppleButton_2; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_SignInWithAppleButton_2() { return &___SignInWithAppleButton_2; }
	inline void set_SignInWithAppleButton_2(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___SignInWithAppleButton_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SignInWithAppleButton_2), (void*)value);
	}

	inline static int32_t get_offset_of_LoadingMessageParent_3() { return static_cast<int32_t>(offsetof(LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65, ___LoadingMessageParent_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_LoadingMessageParent_3() const { return ___LoadingMessageParent_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_LoadingMessageParent_3() { return &___LoadingMessageParent_3; }
	inline void set_LoadingMessageParent_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___LoadingMessageParent_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoadingMessageParent_3), (void*)value);
	}

	inline static int32_t get_offset_of_LoadingIconTransform_4() { return static_cast<int32_t>(offsetof(LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65, ___LoadingIconTransform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_LoadingIconTransform_4() const { return ___LoadingIconTransform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_LoadingIconTransform_4() { return &___LoadingIconTransform_4; }
	inline void set_LoadingIconTransform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___LoadingIconTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoadingIconTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_LoadingMessageLabel_5() { return static_cast<int32_t>(offsetof(LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65, ___LoadingMessageLabel_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_LoadingMessageLabel_5() const { return ___LoadingMessageLabel_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_LoadingMessageLabel_5() { return &___LoadingMessageLabel_5; }
	inline void set_LoadingMessageLabel_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___LoadingMessageLabel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoadingMessageLabel_5), (void*)value);
	}
};


// MainMenu_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6  : public RuntimeObject
{
public:
	// MainMenu MainMenu_<>c__DisplayClass14_0::<>4__this
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * ___U3CU3E4__this_0;
	// System.String MainMenu_<>c__DisplayClass14_0::appleUserId
	String_t* ___appleUserId_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6, ___U3CU3E4__this_0)); }
	inline MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_appleUserId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6, ___appleUserId_1)); }
	inline String_t* get_appleUserId_1() const { return ___appleUserId_1; }
	inline String_t** get_address_of_appleUserId_1() { return &___appleUserId_1; }
	inline void set_appleUserId_1(String_t* value)
	{
		___appleUserId_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appleUserId_1), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.Int32,GooglePlayGames.BasicApi.SignInStatus>
struct  Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tA0390730ED24ACEABD2B5D9C48594C85FB9EB4FC* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tB40C9D0D51F1C5C14B18C8FE5F0BCB35C00E7A32 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tFD6F9B1AE8411C40E0410C958B7C5921BE318C1B * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___entries_1)); }
	inline EntryU5BU5D_tA0390730ED24ACEABD2B5D9C48594C85FB9EB4FC* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tA0390730ED24ACEABD2B5D9C48594C85FB9EB4FC** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tA0390730ED24ACEABD2B5D9C48594C85FB9EB4FC* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___keys_7)); }
	inline KeyCollection_tB40C9D0D51F1C5C14B18C8FE5F0BCB35C00E7A32 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tB40C9D0D51F1C5C14B18C8FE5F0BCB35C00E7A32 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tB40C9D0D51F1C5C14B18C8FE5F0BCB35C00E7A32 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ___values_8)); }
	inline ValueCollection_tFD6F9B1AE8411C40E0410C958B7C5921BE318C1B * get_values_8() const { return ___values_8; }
	inline ValueCollection_tFD6F9B1AE8411C40E0410C958B7C5921BE318C1B ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tFD6F9B1AE8411C40E0410C958B7C5921BE318C1B * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Action>
struct  List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____items_1)); }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* get__items_1() const { return ____items_1; }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_StaticFields, ____emptyArray_5)); }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Action`1<System.Boolean>>
struct  List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89, ____items_1)); }
	inline Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC* get__items_1() const { return ____items_1; }
	inline Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89_StaticFields, ____emptyArray_5)); }
	inline Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Action_1U5BU5D_t3034A3AD7E26360C4625A301DB6299EF6B61C0BC* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.EmptyArray`1<System.Object>
struct  EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.Encoding
struct  Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___dataItem_10)); }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___encoderFallback_13)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_13), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___decoderFallback_14)); }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_14), (void*)value);
	}
};

struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___encodings_8)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_15), (void*)value);
	}
};


// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.AndroidJavaObject
struct  AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D  : public RuntimeObject
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jobject
	GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * ___m_jobject_1;
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jclass
	GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * ___m_jclass_2;

public:
	inline static int32_t get_offset_of_m_jobject_1() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D, ___m_jobject_1)); }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * get_m_jobject_1() const { return ___m_jobject_1; }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 ** get_address_of_m_jobject_1() { return &___m_jobject_1; }
	inline void set_m_jobject_1(GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * value)
	{
		___m_jobject_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jobject_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_jclass_2() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D, ___m_jclass_2)); }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * get_m_jclass_2() const { return ___m_jclass_2; }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 ** get_address_of_m_jclass_2() { return &___m_jclass_2; }
	inline void set_m_jclass_2(GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * value)
	{
		___m_jclass_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jclass_2), (void*)value);
	}
};

struct AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_StaticFields
{
public:
	// System.Boolean UnityEngine.AndroidJavaObject::enableDebugPrints
	bool ___enableDebugPrints_0;

public:
	inline static int32_t get_offset_of_enableDebugPrints_0() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_StaticFields, ___enableDebugPrints_0)); }
	inline bool get_enableDebugPrints_0() const { return ___enableDebugPrints_0; }
	inline bool* get_address_of_enableDebugPrints_0() { return &___enableDebugPrints_0; }
	inline void set_enableDebugPrints_0(bool value)
	{
		___enableDebugPrints_0 = value;
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// WebelinxAppleAuthController_<>c
struct  U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields
{
public:
	// WebelinxAppleAuthController_<>c WebelinxAppleAuthController_<>c::<>9
	U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * ___U3CU3E9_0;
	// System.Action`1<AppleAuth.Interfaces.ICredential> WebelinxAppleAuthController_<>c::<>9__3_0
	Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___U3CU3E9__3_0_1;
	// System.Action`1<AppleAuth.Interfaces.IAppleError> WebelinxAppleAuthController_<>c::<>9__3_1
	Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___U3CU3E9__3_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields, ___U3CU3E9__3_1_2)); }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * get_U3CU3E9__3_1_2() const { return ___U3CU3E9__3_1_2; }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 ** get_address_of_U3CU3E9__3_1_2() { return &___U3CU3E9__3_1_2; }
	inline void set_U3CU3E9__3_1_2(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * value)
	{
		___U3CU3E9__3_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_1_2), (void*)value);
	}
};


// AppleAuth.AppleAuthQuickLoginArgs
struct  AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A 
{
public:
	// System.String AppleAuth.AppleAuthQuickLoginArgs::Nonce
	String_t* ___Nonce_0;
	// System.String AppleAuth.AppleAuthQuickLoginArgs::State
	String_t* ___State_1;

public:
	inline static int32_t get_offset_of_Nonce_0() { return static_cast<int32_t>(offsetof(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A, ___Nonce_0)); }
	inline String_t* get_Nonce_0() const { return ___Nonce_0; }
	inline String_t** get_address_of_Nonce_0() { return &___Nonce_0; }
	inline void set_Nonce_0(String_t* value)
	{
		___Nonce_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Nonce_0), (void*)value);
	}

	inline static int32_t get_offset_of_State_1() { return static_cast<int32_t>(offsetof(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A, ___State_1)); }
	inline String_t* get_State_1() const { return ___State_1; }
	inline String_t** get_address_of_State_1() { return &___State_1; }
	inline void set_State_1(String_t* value)
	{
		___State_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___State_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of AppleAuth.AppleAuthQuickLoginArgs
struct AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_pinvoke
{
	char* ___Nonce_0;
	char* ___State_1;
};
// Native definition for COM marshalling of AppleAuth.AppleAuthQuickLoginArgs
struct AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_com
{
	Il2CppChar* ___Nonce_0;
	Il2CppChar* ___State_1;
};

// GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct  EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 
{
public:
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mEndpointId
	String_t* ___mEndpointId_0;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mName
	String_t* ___mName_1;
	// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::mServiceId
	String_t* ___mServiceId_2;

public:
	inline static int32_t get_offset_of_mEndpointId_0() { return static_cast<int32_t>(offsetof(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893, ___mEndpointId_0)); }
	inline String_t* get_mEndpointId_0() const { return ___mEndpointId_0; }
	inline String_t** get_address_of_mEndpointId_0() { return &___mEndpointId_0; }
	inline void set_mEndpointId_0(String_t* value)
	{
		___mEndpointId_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mEndpointId_0), (void*)value);
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893, ___mName_1)); }
	inline String_t* get_mName_1() const { return ___mName_1; }
	inline String_t** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(String_t* value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mName_1), (void*)value);
	}

	inline static int32_t get_offset_of_mServiceId_2() { return static_cast<int32_t>(offsetof(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893, ___mServiceId_2)); }
	inline String_t* get_mServiceId_2() const { return ___mServiceId_2; }
	inline String_t** get_address_of_mServiceId_2() { return &___mServiceId_2; }
	inline void set_mServiceId_2(String_t* value)
	{
		___mServiceId_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mServiceId_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke
{
	char* ___mEndpointId_0;
	char* ___mName_1;
	char* ___mServiceId_2;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com
{
	Il2CppChar* ___mEndpointId_0;
	Il2CppChar* ___mName_1;
	Il2CppChar* ___mServiceId_2;
};

// GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct  NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 
{
public:
	// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::mInitializationCallback
	Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * ___mInitializationCallback_2;
	// System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::mLocalClientId
	int64_t ___mLocalClientId_3;

public:
	inline static int32_t get_offset_of_mInitializationCallback_2() { return static_cast<int32_t>(offsetof(NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2, ___mInitializationCallback_2)); }
	inline Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * get_mInitializationCallback_2() const { return ___mInitializationCallback_2; }
	inline Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 ** get_address_of_mInitializationCallback_2() { return &___mInitializationCallback_2; }
	inline void set_mInitializationCallback_2(Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * value)
	{
		___mInitializationCallback_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mInitializationCallback_2), (void*)value);
	}

	inline static int32_t get_offset_of_mLocalClientId_3() { return static_cast<int32_t>(offsetof(NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2, ___mLocalClientId_3)); }
	inline int64_t get_mLocalClientId_3() const { return ___mLocalClientId_3; }
	inline int64_t* get_address_of_mLocalClientId_3() { return &___mLocalClientId_3; }
	inline void set_mLocalClientId_3(int64_t value)
	{
		___mLocalClientId_3 = value;
	}
};

// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_pinvoke
{
	Il2CppMethodPointer ___mInitializationCallback_2;
	int64_t ___mLocalClientId_3;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_com
{
	Il2CppMethodPointer ___mInitializationCallback_2;
	int64_t ___mLocalClientId_3;
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Action`1<System.Boolean>>
struct  Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE, ___list_0)); }
	inline List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * get_list_0() const { return ___list_0; }
	inline List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE, ___current_3)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_current_3() const { return ___current_3; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt64
struct  UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_SelectedSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_3)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// AppleAuth.Enums.AuthorizationErrorCode
struct  AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F 
{
public:
	// System.Int32 AppleAuth.Enums.AuthorizationErrorCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.CredentialState
struct  CredentialState_t67035148011C9DB2CE800949AA1DA32AEB22D089 
{
public:
	// System.Int32 AppleAuth.Enums.CredentialState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CredentialState_t67035148011C9DB2CE800949AA1DA32AEB22D089, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.LoginOptions
struct  LoginOptions_t0143F76C42AF6F27BB47BC6AC903FAED0C403519 
{
public:
	// System.Int32 AppleAuth.Enums.LoginOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoginOptions_t0143F76C42AF6F27BB47BC6AC903FAED0C403519, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.PersonNameFormatterStyle
struct  PersonNameFormatterStyle_tF5E845067A91D379772C2926040E34D24C90669A 
{
public:
	// System.Int32 AppleAuth.Enums.PersonNameFormatterStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PersonNameFormatterStyle_tF5E845067A91D379772C2926040E34D24C90669A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.RealUserStatus
struct  RealUserStatus_t3439EB7A9F2D88C6F5FF8D3F571A4B8C92EF596D 
{
public:
	// System.Int32 AppleAuth.Enums.RealUserStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RealUserStatus_t3439EB7A9F2D88C6F5FF8D3F571A4B8C92EF596D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Authentication.Providers
struct  Providers_t31B38E7893EDC2FE49BF5A04F6CF0F8924594DE7 
{
public:
	// System.Int32 Authentication.Providers::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Providers_t31B38E7893EDC2FE49BF5A04F6CF0F8924594DE7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.DataSource
struct  DataSource_t565A716190CF2CF72CFD529400A596509227E7F6 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.DataSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataSource_t565A716190CF2CF72CFD529400A596509227E7F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.Events.EventVisibility
struct  EventVisibility_tB913F90F62A7CD503C219C15E7B41D01D6EC4E9C 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Events.EventVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventVisibility_tB913F90F62A7CD503C219C15E7B41D01D6EC4E9C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.FriendsListVisibilityStatus
struct  FriendsListVisibilityStatus_t5259EE60CE6419D7AFBAB42B3FDE4FD4F5F752A2 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.FriendsListVisibilityStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FriendsListVisibilityStatus_t5259EE60CE6419D7AFBAB42B3FDE4FD4F5F752A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.Gravity
struct  Gravity_t5B09DD369ACCBCB8EFA1AB54065492B428798919 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Gravity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Gravity_t5B09DD369ACCBCB8EFA1AB54065492B428798919, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.LeaderboardCollection
struct  LeaderboardCollection_t43C070D1665D942B07A4E83BFE9645B86A5F2AAB 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardCollection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeaderboardCollection_t43C070D1665D942B07A4E83BFE9645B86A5F2AAB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.LeaderboardStart
struct  LeaderboardStart_tD56A26A86AB36A4660EEB90F8850432C6755634C 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardStart::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeaderboardStart_tD56A26A86AB36A4660EEB90F8850432C6755634C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.LeaderboardTimeSpan
struct  LeaderboardTimeSpan_tA79BB65E567BB62CB9D06FF295C83142115F57E0 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LeaderboardTimeSpan::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeaderboardTimeSpan_tA79BB65E567BB62CB9D06FF295C83142115F57E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.LoadFriendsStatus
struct  LoadFriendsStatus_tCBC3C93C39FFB453AFC3A2ADE4A91C71E194A014 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.LoadFriendsStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadFriendsStatus_tCBC3C93C39FFB453AFC3A2ADE4A91C71E194A014, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct  ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 
{
public:
	// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mRemoteEndpoint
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  ___mRemoteEndpoint_0;
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mPayload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPayload_1;

public:
	inline static int32_t get_offset_of_mRemoteEndpoint_0() { return static_cast<int32_t>(offsetof(ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01, ___mRemoteEndpoint_0)); }
	inline EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  get_mRemoteEndpoint_0() const { return ___mRemoteEndpoint_0; }
	inline EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * get_address_of_mRemoteEndpoint_0() { return &___mRemoteEndpoint_0; }
	inline void set_mRemoteEndpoint_0(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  value)
	{
		___mRemoteEndpoint_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mRemoteEndpoint_0))->___mEndpointId_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___mRemoteEndpoint_0))->___mName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___mRemoteEndpoint_0))->___mServiceId_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_mPayload_1() { return static_cast<int32_t>(offsetof(ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01, ___mPayload_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPayload_1() const { return ___mPayload_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPayload_1() { return &___mPayload_1; }
	inline void set_mPayload_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPayload_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPayload_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_pinvoke
{
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke ___mRemoteEndpoint_0;
	Il2CppSafeArray/*NONE*/* ___mPayload_1;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_com
{
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com ___mRemoteEndpoint_0;
	Il2CppSafeArray/*NONE*/* ___mPayload_1;
};

// GooglePlayGames.BasicApi.Nearby.ConnectionResponse_Status
struct  Status_t8399F037E9CF29EEF79648A135A048230226EE5E 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Nearby.ConnectionResponse_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t8399F037E9CF29EEF79648A135A048230226EE5E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.Nearby.InitializationStatus
struct  InitializationStatus_t411F31DA43CBCAA517F3E4FE4A28765E514D673D 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.Nearby.InitializationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializationStatus_t411F31DA43CBCAA517F3E4FE4A28765E514D673D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.ResponseStatus
struct  ResponseStatus_t3F5F0C363FB0B0BF385D682390EEB710870DF87E 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.ResponseStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResponseStatus_t3F5F0C363FB0B0BF385D682390EEB710870DF87E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
struct  ConflictResolutionStrategy_tE92E5FD2EB99ECD7F72DB5A4730416A43D39E063 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConflictResolutionStrategy_tE92E5FD2EB99ECD7F72DB5A4730416A43D39E063, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
struct  SavedGameRequestStatus_t8BA37D52D86E66BF39DA5BD1DC6A27DF63F86A94 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SavedGameRequestStatus_t8BA37D52D86E66BF39DA5BD1DC6A27DF63F86A94, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
struct  SelectUIStatus_t147E25E95AF46FF7485383E714FDFC1284062852 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SavedGame.SelectUIStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectUIStatus_t147E25E95AF46FF7485383E714FDFC1284062852, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.SignInInteractivity
struct  SignInInteractivity_t9907062051168DED2AE55AEFCE60E483E641F8FA 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SignInInteractivity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SignInInteractivity_t9907062051168DED2AE55AEFCE60E483E641F8FA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.SignInStatus
struct  SignInStatus_tEA744FB3BB94DF5F7D2F58C0D935F89B1BF26234 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.SignInStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SignInStatus_tEA744FB3BB94DF5F7D2F58C0D935F89B1BF26234, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.UIStatus
struct  UIStatus_t97ED34FE68AEF0453BCF9E26334F010E72F21682 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.UIStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UIStatus_t97ED34FE68AEF0453BCF9E26334F010E72F21682, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.VideoCaptureMode
struct  VideoCaptureMode_tBF0E331D46B351F8E6097418294116AB4D363609 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoCaptureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoCaptureMode_tBF0E331D46B351F8E6097418294116AB4D363609, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.VideoCaptureOverlayState
struct  VideoCaptureOverlayState_tEB79370174C6BE180328E30298167F0B7ED7DDC9 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoCaptureOverlayState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoCaptureOverlayState_tEB79370174C6BE180328E30298167F0B7ED7DDC9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GooglePlayGames.BasicApi.VideoQualityLevel
struct  VideoQualityLevel_t9E3687DDDE2ABCAE7A1E98DF796AA4CE6535BA52 
{
public:
	// System.Int32 GooglePlayGames.BasicApi.VideoQualityLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoQualityLevel_t9E3687DDDE2ABCAE7A1E98DF796AA4CE6535BA52, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Int32Enum
struct  Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t876E76124F400D12395BF61D562162AB6822204A 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::m_wrapper
	RuntimeObject * ___m_wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::m_handle
	intptr_t ___m_handle_1;

public:
	inline static int32_t get_offset_of_m_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t876E76124F400D12395BF61D562162AB6822204A, ___m_wrapper_0)); }
	inline RuntimeObject * get_m_wrapper_0() const { return ___m_wrapper_0; }
	inline RuntimeObject ** get_address_of_m_wrapper_0() { return &___m_wrapper_0; }
	inline void set_m_wrapper_0(RuntimeObject * value)
	{
		___m_wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_wrapper_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t876E76124F400D12395BF61D562162AB6822204A, ___m_handle_1)); }
	inline intptr_t get_m_handle_1() const { return ___m_handle_1; }
	inline intptr_t* get_address_of_m_handle_1() { return &___m_handle_1; }
	inline void set_m_handle_1(intptr_t value)
	{
		___m_handle_1 = value;
	}
};


// System.Threading.Tasks.Task
struct  Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_10;
	// System.Threading.Tasks.Task_ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 * ___m_contingentProperties_15;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_taskScheduler_7)); }
	inline TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_parent_8)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_10() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_continuationObject_10)); }
	inline RuntimeObject * get_m_continuationObject_10() const { return ___m_continuationObject_10; }
	inline RuntimeObject ** get_address_of_m_continuationObject_10() { return &___m_continuationObject_10; }
	inline void set_m_continuationObject_10(RuntimeObject * value)
	{
		___m_continuationObject_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_15() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_contingentProperties_15)); }
	inline ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 * get_m_contingentProperties_15() const { return ___m_contingentProperties_15; }
	inline ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 ** get_address_of_m_contingentProperties_15() { return &___m_contingentProperties_15; }
	inline void set_m_contingentProperties_15(ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 * value)
	{
		___m_contingentProperties_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_15), (void*)value);
	}
};

struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_11;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F * ___s_currentActiveTasks_13;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_14;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___s_taskCancelCallback_16;
	// System.Func`1<System.Threading.Tasks.Task_ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F * ___s_createContingentProperties_17;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___s_completedTask_18;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 * ___s_IsExceptionObservedByParentPredicate_19;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 * ___s_ecCallback_20;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___s_IsTaskContinuationNullPredicate_21;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_factory_3)); }
	inline TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_11() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_taskCompletionSentinel_11)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_11() const { return ___s_taskCompletionSentinel_11; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_11() { return &___s_taskCompletionSentinel_11; }
	inline void set_s_taskCompletionSentinel_11(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_12() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_asyncDebuggingEnabled_12)); }
	inline bool get_s_asyncDebuggingEnabled_12() const { return ___s_asyncDebuggingEnabled_12; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_12() { return &___s_asyncDebuggingEnabled_12; }
	inline void set_s_asyncDebuggingEnabled_12(bool value)
	{
		___s_asyncDebuggingEnabled_12 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_13() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_currentActiveTasks_13)); }
	inline Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F * get_s_currentActiveTasks_13() const { return ___s_currentActiveTasks_13; }
	inline Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F ** get_address_of_s_currentActiveTasks_13() { return &___s_currentActiveTasks_13; }
	inline void set_s_currentActiveTasks_13(Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F * value)
	{
		___s_currentActiveTasks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_14() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_activeTasksLock_14)); }
	inline RuntimeObject * get_s_activeTasksLock_14() const { return ___s_activeTasksLock_14; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_14() { return &___s_activeTasksLock_14; }
	inline void set_s_activeTasksLock_14(RuntimeObject * value)
	{
		___s_activeTasksLock_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_16() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_taskCancelCallback_16)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_s_taskCancelCallback_16() const { return ___s_taskCancelCallback_16; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_s_taskCancelCallback_16() { return &___s_taskCancelCallback_16; }
	inline void set_s_taskCancelCallback_16(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___s_taskCancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_17() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_createContingentProperties_17)); }
	inline Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F * get_s_createContingentProperties_17() const { return ___s_createContingentProperties_17; }
	inline Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F ** get_address_of_s_createContingentProperties_17() { return &___s_createContingentProperties_17; }
	inline void set_s_createContingentProperties_17(Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F * value)
	{
		___s_createContingentProperties_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_18() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_completedTask_18)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_s_completedTask_18() const { return ___s_completedTask_18; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_s_completedTask_18() { return &___s_completedTask_18; }
	inline void set_s_completedTask_18(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___s_completedTask_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_19() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_IsExceptionObservedByParentPredicate_19)); }
	inline Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 * get_s_IsExceptionObservedByParentPredicate_19() const { return ___s_IsExceptionObservedByParentPredicate_19; }
	inline Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 ** get_address_of_s_IsExceptionObservedByParentPredicate_19() { return &___s_IsExceptionObservedByParentPredicate_19; }
	inline void set_s_IsExceptionObservedByParentPredicate_19(Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 * value)
	{
		___s_IsExceptionObservedByParentPredicate_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_20() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_ecCallback_20)); }
	inline ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 * get_s_ecCallback_20() const { return ___s_ecCallback_20; }
	inline ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 ** get_address_of_s_ecCallback_20() { return &___s_ecCallback_20; }
	inline void set_s_ecCallback_20(ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 * value)
	{
		___s_ecCallback_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_21() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_IsTaskContinuationNullPredicate_21)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_s_IsTaskContinuationNullPredicate_21() const { return ___s_IsTaskContinuationNullPredicate_21; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_s_IsTaskContinuationNullPredicate_21() { return &___s_IsTaskContinuationNullPredicate_21; }
	inline void set_s_IsTaskContinuationNullPredicate_21(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___s_IsTaskContinuationNullPredicate_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_21), (void*)value);
	}
};

struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};


// System.Uri_Flags
struct  Flags_tEBE7CABEBD13F16920D6950B384EB8F988250A2A 
{
public:
	// System.UInt64 System.Uri_Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tEBE7CABEBD13F16920D6950B384EB8F988250A2A, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};


// System.UriFormat
struct  UriFormat_t4355763D39FF6F0FAA2B43E3A209BA8500730992 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t4355763D39FF6F0FAA2B43E3A209BA8500730992, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriIdnScope
struct  UriIdnScope_tE1574B39C7492C761EFE2FC12DDE82DE013AC9D1 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_tE1574B39C7492C761EFE2FC12DDE82DE013AC9D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriKind
struct  UriKind_t26D0760DDF148ADC939FECD934C0B9FF5C71EA08 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_t26D0760DDF148ADC939FECD934C0B9FF5C71EA08, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_SelectedColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};


// UnityEngine.UI.Image_FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image_FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image_Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.AppleAuthLoginArgs
struct  AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 
{
public:
	// AppleAuth.Enums.LoginOptions AppleAuth.AppleAuthLoginArgs::Options
	int32_t ___Options_0;
	// System.String AppleAuth.AppleAuthLoginArgs::Nonce
	String_t* ___Nonce_1;
	// System.String AppleAuth.AppleAuthLoginArgs::State
	String_t* ___State_2;

public:
	inline static int32_t get_offset_of_Options_0() { return static_cast<int32_t>(offsetof(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22, ___Options_0)); }
	inline int32_t get_Options_0() const { return ___Options_0; }
	inline int32_t* get_address_of_Options_0() { return &___Options_0; }
	inline void set_Options_0(int32_t value)
	{
		___Options_0 = value;
	}

	inline static int32_t get_offset_of_Nonce_1() { return static_cast<int32_t>(offsetof(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22, ___Nonce_1)); }
	inline String_t* get_Nonce_1() const { return ___Nonce_1; }
	inline String_t** get_address_of_Nonce_1() { return &___Nonce_1; }
	inline void set_Nonce_1(String_t* value)
	{
		___Nonce_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Nonce_1), (void*)value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22, ___State_2)); }
	inline String_t* get_State_2() const { return ___State_2; }
	inline String_t** get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(String_t* value)
	{
		___State_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___State_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of AppleAuth.AppleAuthLoginArgs
struct AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_pinvoke
{
	int32_t ___Options_0;
	char* ___Nonce_1;
	char* ___State_2;
};
// Native definition for COM marshalling of AppleAuth.AppleAuthLoginArgs
struct AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_com
{
	int32_t ___Options_0;
	Il2CppChar* ___Nonce_1;
	Il2CppChar* ___State_2;
};

// Firebase.Auth.Credential
struct  Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.Credential::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.Credential::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// Firebase.Auth.FirebaseAuth
struct  FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseAuth::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.FirebaseAuth::swigCMemOwn
	bool ___swigCMemOwn_1;
	// Firebase.FirebaseApp Firebase.Auth.FirebaseAuth::appProxy
	FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___appProxy_2;
	// System.IntPtr Firebase.Auth.FirebaseAuth::appCPtr
	intptr_t ___appCPtr_3;
	// System.IntPtr Firebase.Auth.FirebaseAuth::authStateListener
	intptr_t ___authStateListener_4;
	// System.IntPtr Firebase.Auth.FirebaseAuth::idTokenListener
	intptr_t ___idTokenListener_5;
	// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::currentUser
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___currentUser_6;
	// System.EventHandler Firebase.Auth.FirebaseAuth::stateChangedImpl
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___stateChangedImpl_7;
	// System.EventHandler Firebase.Auth.FirebaseAuth::idTokenChangedImpl
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___idTokenChangedImpl_8;
	// System.Boolean Firebase.Auth.FirebaseAuth::persistentLoaded
	bool ___persistentLoaded_9;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_appProxy_2() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___appProxy_2)); }
	inline FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * get_appProxy_2() const { return ___appProxy_2; }
	inline FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 ** get_address_of_appProxy_2() { return &___appProxy_2; }
	inline void set_appProxy_2(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * value)
	{
		___appProxy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appProxy_2), (void*)value);
	}

	inline static int32_t get_offset_of_appCPtr_3() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___appCPtr_3)); }
	inline intptr_t get_appCPtr_3() const { return ___appCPtr_3; }
	inline intptr_t* get_address_of_appCPtr_3() { return &___appCPtr_3; }
	inline void set_appCPtr_3(intptr_t value)
	{
		___appCPtr_3 = value;
	}

	inline static int32_t get_offset_of_authStateListener_4() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___authStateListener_4)); }
	inline intptr_t get_authStateListener_4() const { return ___authStateListener_4; }
	inline intptr_t* get_address_of_authStateListener_4() { return &___authStateListener_4; }
	inline void set_authStateListener_4(intptr_t value)
	{
		___authStateListener_4 = value;
	}

	inline static int32_t get_offset_of_idTokenListener_5() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___idTokenListener_5)); }
	inline intptr_t get_idTokenListener_5() const { return ___idTokenListener_5; }
	inline intptr_t* get_address_of_idTokenListener_5() { return &___idTokenListener_5; }
	inline void set_idTokenListener_5(intptr_t value)
	{
		___idTokenListener_5 = value;
	}

	inline static int32_t get_offset_of_currentUser_6() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___currentUser_6)); }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * get_currentUser_6() const { return ___currentUser_6; }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD ** get_address_of_currentUser_6() { return &___currentUser_6; }
	inline void set_currentUser_6(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * value)
	{
		___currentUser_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentUser_6), (void*)value);
	}

	inline static int32_t get_offset_of_stateChangedImpl_7() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___stateChangedImpl_7)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_stateChangedImpl_7() const { return ___stateChangedImpl_7; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_stateChangedImpl_7() { return &___stateChangedImpl_7; }
	inline void set_stateChangedImpl_7(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___stateChangedImpl_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateChangedImpl_7), (void*)value);
	}

	inline static int32_t get_offset_of_idTokenChangedImpl_8() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___idTokenChangedImpl_8)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_idTokenChangedImpl_8() const { return ___idTokenChangedImpl_8; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_idTokenChangedImpl_8() { return &___idTokenChangedImpl_8; }
	inline void set_idTokenChangedImpl_8(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___idTokenChangedImpl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idTokenChangedImpl_8), (void*)value);
	}

	inline static int32_t get_offset_of_persistentLoaded_9() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___persistentLoaded_9)); }
	inline bool get_persistentLoaded_9() const { return ___persistentLoaded_9; }
	inline bool* get_address_of_persistentLoaded_9() { return &___persistentLoaded_9; }
	inline void set_persistentLoaded_9(bool value)
	{
		___persistentLoaded_9 = value;
	}
};

struct FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth> Firebase.Auth.FirebaseAuth::appCPtrToAuth
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * ___appCPtrToAuth_10;

public:
	inline static int32_t get_offset_of_appCPtrToAuth_10() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields, ___appCPtrToAuth_10)); }
	inline Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * get_appCPtrToAuth_10() const { return ___appCPtrToAuth_10; }
	inline Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D ** get_address_of_appCPtrToAuth_10() { return &___appCPtrToAuth_10; }
	inline void set_appCPtrToAuth_10(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * value)
	{
		___appCPtrToAuth_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appCPtrToAuth_10), (void*)value);
	}
};


// Firebase.Auth.UserInfoInterface
struct  UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserInfoInterface::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.UserInfoInterface::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// Firebase.Auth.UserProfile
struct  UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserProfile::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.UserProfile::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// GooglePlayGames.BasicApi.Events.Event
struct  Event_t72642C3269994E99511B3C85054C40A5610B170D  : public RuntimeObject
{
public:
	// System.String GooglePlayGames.BasicApi.Events.Event::mId
	String_t* ___mId_0;
	// System.String GooglePlayGames.BasicApi.Events.Event::mName
	String_t* ___mName_1;
	// System.String GooglePlayGames.BasicApi.Events.Event::mDescription
	String_t* ___mDescription_2;
	// System.String GooglePlayGames.BasicApi.Events.Event::mImageUrl
	String_t* ___mImageUrl_3;
	// System.UInt64 GooglePlayGames.BasicApi.Events.Event::mCurrentCount
	uint64_t ___mCurrentCount_4;
	// GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.BasicApi.Events.Event::mVisibility
	int32_t ___mVisibility_5;

public:
	inline static int32_t get_offset_of_mId_0() { return static_cast<int32_t>(offsetof(Event_t72642C3269994E99511B3C85054C40A5610B170D, ___mId_0)); }
	inline String_t* get_mId_0() const { return ___mId_0; }
	inline String_t** get_address_of_mId_0() { return &___mId_0; }
	inline void set_mId_0(String_t* value)
	{
		___mId_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mId_0), (void*)value);
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(Event_t72642C3269994E99511B3C85054C40A5610B170D, ___mName_1)); }
	inline String_t* get_mName_1() const { return ___mName_1; }
	inline String_t** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(String_t* value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mName_1), (void*)value);
	}

	inline static int32_t get_offset_of_mDescription_2() { return static_cast<int32_t>(offsetof(Event_t72642C3269994E99511B3C85054C40A5610B170D, ___mDescription_2)); }
	inline String_t* get_mDescription_2() const { return ___mDescription_2; }
	inline String_t** get_address_of_mDescription_2() { return &___mDescription_2; }
	inline void set_mDescription_2(String_t* value)
	{
		___mDescription_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mDescription_2), (void*)value);
	}

	inline static int32_t get_offset_of_mImageUrl_3() { return static_cast<int32_t>(offsetof(Event_t72642C3269994E99511B3C85054C40A5610B170D, ___mImageUrl_3)); }
	inline String_t* get_mImageUrl_3() const { return ___mImageUrl_3; }
	inline String_t** get_address_of_mImageUrl_3() { return &___mImageUrl_3; }
	inline void set_mImageUrl_3(String_t* value)
	{
		___mImageUrl_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mImageUrl_3), (void*)value);
	}

	inline static int32_t get_offset_of_mCurrentCount_4() { return static_cast<int32_t>(offsetof(Event_t72642C3269994E99511B3C85054C40A5610B170D, ___mCurrentCount_4)); }
	inline uint64_t get_mCurrentCount_4() const { return ___mCurrentCount_4; }
	inline uint64_t* get_address_of_mCurrentCount_4() { return &___mCurrentCount_4; }
	inline void set_mCurrentCount_4(uint64_t value)
	{
		___mCurrentCount_4 = value;
	}

	inline static int32_t get_offset_of_mVisibility_5() { return static_cast<int32_t>(offsetof(Event_t72642C3269994E99511B3C85054C40A5610B170D, ___mVisibility_5)); }
	inline int32_t get_mVisibility_5() const { return ___mVisibility_5; }
	inline int32_t* get_address_of_mVisibility_5() { return &___mVisibility_5; }
	inline void set_mVisibility_5(int32_t value)
	{
		___mVisibility_5 = value;
	}
};


// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct  AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 
{
public:
	// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mStatus
	int32_t ___mStatus_0;
	// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mLocalEndpointName
	String_t* ___mLocalEndpointName_1;

public:
	inline static int32_t get_offset_of_mStatus_0() { return static_cast<int32_t>(offsetof(AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0, ___mStatus_0)); }
	inline int32_t get_mStatus_0() const { return ___mStatus_0; }
	inline int32_t* get_address_of_mStatus_0() { return &___mStatus_0; }
	inline void set_mStatus_0(int32_t value)
	{
		___mStatus_0 = value;
	}

	inline static int32_t get_offset_of_mLocalEndpointName_1() { return static_cast<int32_t>(offsetof(AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0, ___mLocalEndpointName_1)); }
	inline String_t* get_mLocalEndpointName_1() const { return ___mLocalEndpointName_1; }
	inline String_t** get_address_of_mLocalEndpointName_1() { return &___mLocalEndpointName_1; }
	inline void set_mLocalEndpointName_1(String_t* value)
	{
		___mLocalEndpointName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mLocalEndpointName_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_pinvoke
{
	int32_t ___mStatus_0;
	char* ___mLocalEndpointName_1;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_com
{
	int32_t ___mStatus_0;
	Il2CppChar* ___mLocalEndpointName_1;
};

// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct  ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 
{
public:
	// System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mLocalClientId
	int64_t ___mLocalClientId_1;
	// System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mRemoteEndpointId
	String_t* ___mRemoteEndpointId_2;
	// GooglePlayGames.BasicApi.Nearby.ConnectionResponse_Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mResponseStatus
	int32_t ___mResponseStatus_3;
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::mPayload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPayload_4;

public:
	inline static int32_t get_offset_of_mLocalClientId_1() { return static_cast<int32_t>(offsetof(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70, ___mLocalClientId_1)); }
	inline int64_t get_mLocalClientId_1() const { return ___mLocalClientId_1; }
	inline int64_t* get_address_of_mLocalClientId_1() { return &___mLocalClientId_1; }
	inline void set_mLocalClientId_1(int64_t value)
	{
		___mLocalClientId_1 = value;
	}

	inline static int32_t get_offset_of_mRemoteEndpointId_2() { return static_cast<int32_t>(offsetof(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70, ___mRemoteEndpointId_2)); }
	inline String_t* get_mRemoteEndpointId_2() const { return ___mRemoteEndpointId_2; }
	inline String_t** get_address_of_mRemoteEndpointId_2() { return &___mRemoteEndpointId_2; }
	inline void set_mRemoteEndpointId_2(String_t* value)
	{
		___mRemoteEndpointId_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mRemoteEndpointId_2), (void*)value);
	}

	inline static int32_t get_offset_of_mResponseStatus_3() { return static_cast<int32_t>(offsetof(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70, ___mResponseStatus_3)); }
	inline int32_t get_mResponseStatus_3() const { return ___mResponseStatus_3; }
	inline int32_t* get_address_of_mResponseStatus_3() { return &___mResponseStatus_3; }
	inline void set_mResponseStatus_3(int32_t value)
	{
		___mResponseStatus_3 = value;
	}

	inline static int32_t get_offset_of_mPayload_4() { return static_cast<int32_t>(offsetof(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70, ___mPayload_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPayload_4() const { return ___mPayload_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPayload_4() { return &___mPayload_4; }
	inline void set_mPayload_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPayload_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPayload_4), (void*)value);
	}
};

struct ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields
{
public:
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::EmptyPayload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyPayload_0;

public:
	inline static int32_t get_offset_of_EmptyPayload_0() { return static_cast<int32_t>(offsetof(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields, ___EmptyPayload_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyPayload_0() const { return ___EmptyPayload_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyPayload_0() { return &___EmptyPayload_0; }
	inline void set_EmptyPayload_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyPayload_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyPayload_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_pinvoke
{
	int64_t ___mLocalClientId_1;
	char* ___mRemoteEndpointId_2;
	int32_t ___mResponseStatus_3;
	Il2CppSafeArray/*NONE*/* ___mPayload_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.Nearby.ConnectionResponse
struct ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_com
{
	int64_t ___mLocalClientId_1;
	Il2CppChar* ___mRemoteEndpointId_2;
	int32_t ___mResponseStatus_3;
	Il2CppSafeArray/*NONE*/* ___mPayload_4;
};

// GooglePlayGames.BasicApi.Video.VideoCaptureState
struct  VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F  : public RuntimeObject
{
public:
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsCapturing
	bool ___mIsCapturing_0;
	// GooglePlayGames.BasicApi.VideoCaptureMode GooglePlayGames.BasicApi.Video.VideoCaptureState::mCaptureMode
	int32_t ___mCaptureMode_1;
	// GooglePlayGames.BasicApi.VideoQualityLevel GooglePlayGames.BasicApi.Video.VideoCaptureState::mQualityLevel
	int32_t ___mQualityLevel_2;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsOverlayVisible
	bool ___mIsOverlayVisible_3;
	// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::mIsPaused
	bool ___mIsPaused_4;

public:
	inline static int32_t get_offset_of_mIsCapturing_0() { return static_cast<int32_t>(offsetof(VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F, ___mIsCapturing_0)); }
	inline bool get_mIsCapturing_0() const { return ___mIsCapturing_0; }
	inline bool* get_address_of_mIsCapturing_0() { return &___mIsCapturing_0; }
	inline void set_mIsCapturing_0(bool value)
	{
		___mIsCapturing_0 = value;
	}

	inline static int32_t get_offset_of_mCaptureMode_1() { return static_cast<int32_t>(offsetof(VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F, ___mCaptureMode_1)); }
	inline int32_t get_mCaptureMode_1() const { return ___mCaptureMode_1; }
	inline int32_t* get_address_of_mCaptureMode_1() { return &___mCaptureMode_1; }
	inline void set_mCaptureMode_1(int32_t value)
	{
		___mCaptureMode_1 = value;
	}

	inline static int32_t get_offset_of_mQualityLevel_2() { return static_cast<int32_t>(offsetof(VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F, ___mQualityLevel_2)); }
	inline int32_t get_mQualityLevel_2() const { return ___mQualityLevel_2; }
	inline int32_t* get_address_of_mQualityLevel_2() { return &___mQualityLevel_2; }
	inline void set_mQualityLevel_2(int32_t value)
	{
		___mQualityLevel_2 = value;
	}

	inline static int32_t get_offset_of_mIsOverlayVisible_3() { return static_cast<int32_t>(offsetof(VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F, ___mIsOverlayVisible_3)); }
	inline bool get_mIsOverlayVisible_3() const { return ___mIsOverlayVisible_3; }
	inline bool* get_address_of_mIsOverlayVisible_3() { return &___mIsOverlayVisible_3; }
	inline void set_mIsOverlayVisible_3(bool value)
	{
		___mIsOverlayVisible_3 = value;
	}

	inline static int32_t get_offset_of_mIsPaused_4() { return static_cast<int32_t>(offsetof(VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F, ___mIsPaused_4)); }
	inline bool get_mIsPaused_4() const { return ___mIsPaused_4; }
	inline bool* get_address_of_mIsPaused_4() { return &___mIsPaused_4; }
	inline void set_mIsPaused_4(bool value)
	{
		___mIsPaused_4 = value;
	}
};


// System.AggregateException
struct  AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E  : public Exception_t
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Exception> System.AggregateException::m_innerExceptions
	ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 * ___m_innerExceptions_17;

public:
	inline static int32_t get_offset_of_m_innerExceptions_17() { return static_cast<int32_t>(offsetof(AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E, ___m_innerExceptions_17)); }
	inline ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 * get_m_innerExceptions_17() const { return ___m_innerExceptions_17; }
	inline ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 ** get_address_of_m_innerExceptions_17() { return &___m_innerExceptions_17; }
	inline void set_m_innerExceptions_17(ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 * value)
	{
		___m_innerExceptions_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_innerExceptions_17), (void*)value);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 
{
public:
	// T System.Nullable`1::value
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80, ___value_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_value_0() const { return ___value_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct  Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7  : public Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___m_result_22;

public:
	inline static int32_t get_offset_of_m_result_22() { return static_cast<int32_t>(offsetof(Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7, ___m_result_22)); }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * get_m_result_22() const { return ___m_result_22; }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD ** get_address_of_m_result_22() { return &___m_result_22; }
	inline void set_m_result_22(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * value)
	{
		___m_result_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_22), (void*)value);
	}
};

struct Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 * ___s_Factory_23;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t385E06614170490B1EFD01D106616F0668133752 * ___TaskWhenAnyCast_24;

public:
	inline static int32_t get_offset_of_s_Factory_23() { return static_cast<int32_t>(offsetof(Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7_StaticFields, ___s_Factory_23)); }
	inline TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 * get_s_Factory_23() const { return ___s_Factory_23; }
	inline TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 ** get_address_of_s_Factory_23() { return &___s_Factory_23; }
	inline void set_s_Factory_23(TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 * value)
	{
		___s_Factory_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_23), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_24() { return static_cast<int32_t>(offsetof(Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7_StaticFields, ___TaskWhenAnyCast_24)); }
	inline Func_2_t385E06614170490B1EFD01D106616F0668133752 * get_TaskWhenAnyCast_24() const { return ___TaskWhenAnyCast_24; }
	inline Func_2_t385E06614170490B1EFD01D106616F0668133752 ** get_address_of_TaskWhenAnyCast_24() { return &___TaskWhenAnyCast_24; }
	inline void set_TaskWhenAnyCast_24(Func_2_t385E06614170490B1EFD01D106616F0668133752 * value)
	{
		___TaskWhenAnyCast_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_24), (void*)value);
	}
};


// System.Uri
struct  Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC * ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri_Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri_UriInfo System.Uri::m_Info
	UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E * ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;

public:
	inline static int32_t get_offset_of_m_String_16() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_String_16)); }
	inline String_t* get_m_String_16() const { return ___m_String_16; }
	inline String_t** get_address_of_m_String_16() { return &___m_String_16; }
	inline void set_m_String_16(String_t* value)
	{
		___m_String_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_17() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_originalUnicodeString_17)); }
	inline String_t* get_m_originalUnicodeString_17() const { return ___m_originalUnicodeString_17; }
	inline String_t** get_address_of_m_originalUnicodeString_17() { return &___m_originalUnicodeString_17; }
	inline void set_m_originalUnicodeString_17(String_t* value)
	{
		___m_originalUnicodeString_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_originalUnicodeString_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_Syntax_18() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_Syntax_18)); }
	inline UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC * get_m_Syntax_18() const { return ___m_Syntax_18; }
	inline UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC ** get_address_of_m_Syntax_18() { return &___m_Syntax_18; }
	inline void set_m_Syntax_18(UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC * value)
	{
		___m_Syntax_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Syntax_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_19() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_DnsSafeHost_19)); }
	inline String_t* get_m_DnsSafeHost_19() const { return ___m_DnsSafeHost_19; }
	inline String_t** get_address_of_m_DnsSafeHost_19() { return &___m_DnsSafeHost_19; }
	inline void set_m_DnsSafeHost_19(String_t* value)
	{
		___m_DnsSafeHost_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DnsSafeHost_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_20() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_Flags_20)); }
	inline uint64_t get_m_Flags_20() const { return ___m_Flags_20; }
	inline uint64_t* get_address_of_m_Flags_20() { return &___m_Flags_20; }
	inline void set_m_Flags_20(uint64_t value)
	{
		___m_Flags_20 = value;
	}

	inline static int32_t get_offset_of_m_Info_21() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_Info_21)); }
	inline UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E * get_m_Info_21() const { return ___m_Info_21; }
	inline UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E ** get_address_of_m_Info_21() { return &___m_Info_21; }
	inline void set_m_Info_21(UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E * value)
	{
		___m_Info_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Info_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_iriParsing_22() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_iriParsing_22)); }
	inline bool get_m_iriParsing_22() const { return ___m_iriParsing_22; }
	inline bool* get_address_of_m_iriParsing_22() { return &___m_iriParsing_22; }
	inline void set_m_iriParsing_22(bool value)
	{
		___m_iriParsing_22 = value;
	}
};

struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____WSchars_35;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFile_0), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFtp_1), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeGopher_2), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttp_3), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttps_4), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWs_5), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWss_6), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeMailto_7), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNews_8), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNntp_9), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetTcp_10), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetPipe_11), (void*)value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SchemeDelimiter_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_23() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_ConfigInitialized_23)); }
	inline bool get_s_ConfigInitialized_23() const { return ___s_ConfigInitialized_23; }
	inline bool* get_address_of_s_ConfigInitialized_23() { return &___s_ConfigInitialized_23; }
	inline void set_s_ConfigInitialized_23(bool value)
	{
		___s_ConfigInitialized_23 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_24() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_ConfigInitializing_24)); }
	inline bool get_s_ConfigInitializing_24() const { return ___s_ConfigInitializing_24; }
	inline bool* get_address_of_s_ConfigInitializing_24() { return &___s_ConfigInitializing_24; }
	inline void set_s_ConfigInitializing_24(bool value)
	{
		___s_ConfigInitializing_24 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_25() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_IdnScope_25)); }
	inline int32_t get_s_IdnScope_25() const { return ___s_IdnScope_25; }
	inline int32_t* get_address_of_s_IdnScope_25() { return &___s_IdnScope_25; }
	inline void set_s_IdnScope_25(int32_t value)
	{
		___s_IdnScope_25 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_26() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_IriParsing_26)); }
	inline bool get_s_IriParsing_26() const { return ___s_IriParsing_26; }
	inline bool* get_address_of_s_IriParsing_26() { return &___s_IriParsing_26; }
	inline void set_s_IriParsing_26(bool value)
	{
		___s_IriParsing_26 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_27() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___useDotNetRelativeOrAbsolute_27)); }
	inline bool get_useDotNetRelativeOrAbsolute_27() const { return ___useDotNetRelativeOrAbsolute_27; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_27() { return &___useDotNetRelativeOrAbsolute_27; }
	inline void set_useDotNetRelativeOrAbsolute_27(bool value)
	{
		___useDotNetRelativeOrAbsolute_27 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_29() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___IsWindowsFileSystem_29)); }
	inline bool get_IsWindowsFileSystem_29() const { return ___IsWindowsFileSystem_29; }
	inline bool* get_address_of_IsWindowsFileSystem_29() { return &___IsWindowsFileSystem_29; }
	inline void set_IsWindowsFileSystem_29(bool value)
	{
		___IsWindowsFileSystem_29 = value;
	}

	inline static int32_t get_offset_of_s_initLock_30() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_initLock_30)); }
	inline RuntimeObject * get_s_initLock_30() const { return ___s_initLock_30; }
	inline RuntimeObject ** get_address_of_s_initLock_30() { return &___s_initLock_30; }
	inline void set_s_initLock_30(RuntimeObject * value)
	{
		___s_initLock_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_initLock_30), (void*)value);
	}

	inline static int32_t get_offset_of_HexLowerChars_34() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___HexLowerChars_34)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_HexLowerChars_34() const { return ___HexLowerChars_34; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_HexLowerChars_34() { return &___HexLowerChars_34; }
	inline void set_HexLowerChars_34(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___HexLowerChars_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HexLowerChars_34), (void*)value);
	}

	inline static int32_t get_offset_of__WSchars_35() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ____WSchars_35)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__WSchars_35() const { return ____WSchars_35; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__WSchars_35() { return &____WSchars_35; }
	inline void set__WSchars_35(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____WSchars_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WSchars_35), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Sprite
struct  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};

// Firebase.Auth.FirebaseUser
struct  FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD  : public UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseUser::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_2;
	// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseUser::authProxy
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___authProxy_3;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD, ___swigCPtr_2)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_2))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_authProxy_3() { return static_cast<int32_t>(offsetof(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD, ___authProxy_3)); }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * get_authProxy_3() const { return ___authProxy_3; }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** get_address_of_authProxy_3() { return &___authProxy_3; }
	inline void set_authProxy_3(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * value)
	{
		___authProxy_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authProxy_3), (void*)value);
	}
};


// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct  ConflictCallback_t73445AE2938707A22A01739D23A603DC17AB4962  : public MulticastDelegate_t
{
public:

public:
};


// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct  SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A 
{
public:
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mDescriptionUpdated
	bool ___mDescriptionUpdated_0;
	// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewDescription
	String_t* ___mNewDescription_1;
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mCoverImageUpdated
	bool ___mCoverImageUpdated_2;
	// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewPngCoverImage
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mNewPngCoverImage_3;
	// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::mNewPlayedTime
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___mNewPlayedTime_4;

public:
	inline static int32_t get_offset_of_mDescriptionUpdated_0() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A, ___mDescriptionUpdated_0)); }
	inline bool get_mDescriptionUpdated_0() const { return ___mDescriptionUpdated_0; }
	inline bool* get_address_of_mDescriptionUpdated_0() { return &___mDescriptionUpdated_0; }
	inline void set_mDescriptionUpdated_0(bool value)
	{
		___mDescriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of_mNewDescription_1() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A, ___mNewDescription_1)); }
	inline String_t* get_mNewDescription_1() const { return ___mNewDescription_1; }
	inline String_t** get_address_of_mNewDescription_1() { return &___mNewDescription_1; }
	inline void set_mNewDescription_1(String_t* value)
	{
		___mNewDescription_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mNewDescription_1), (void*)value);
	}

	inline static int32_t get_offset_of_mCoverImageUpdated_2() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A, ___mCoverImageUpdated_2)); }
	inline bool get_mCoverImageUpdated_2() const { return ___mCoverImageUpdated_2; }
	inline bool* get_address_of_mCoverImageUpdated_2() { return &___mCoverImageUpdated_2; }
	inline void set_mCoverImageUpdated_2(bool value)
	{
		___mCoverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of_mNewPngCoverImage_3() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A, ___mNewPngCoverImage_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mNewPngCoverImage_3() const { return ___mNewPngCoverImage_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mNewPngCoverImage_3() { return &___mNewPngCoverImage_3; }
	inline void set_mNewPngCoverImage_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mNewPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mNewPngCoverImage_3), (void*)value);
	}

	inline static int32_t get_offset_of_mNewPlayedTime_4() { return static_cast<int32_t>(offsetof(SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A, ___mNewPlayedTime_4)); }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  get_mNewPlayedTime_4() const { return ___mNewPlayedTime_4; }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * get_address_of_mNewPlayedTime_4() { return &___mNewPlayedTime_4; }
	inline void set_mNewPlayedTime_4(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  value)
	{
		___mNewPlayedTime_4 = value;
	}
};

// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_pinvoke
{
	int32_t ___mDescriptionUpdated_0;
	char* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	Il2CppSafeArray/*NONE*/* ___mNewPngCoverImage_3;
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___mNewPlayedTime_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_com
{
	int32_t ___mDescriptionUpdated_0;
	Il2CppChar* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	Il2CppSafeArray/*NONE*/* ___mNewPngCoverImage_3;
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___mNewPlayedTime_4;
};

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder
struct  Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 
{
public:
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::mDescriptionUpdated
	bool ___mDescriptionUpdated_0;
	// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::mNewDescription
	String_t* ___mNewDescription_1;
	// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::mCoverImageUpdated
	bool ___mCoverImageUpdated_2;
	// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::mNewPngCoverImage
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mNewPngCoverImage_3;
	// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::mNewPlayedTime
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___mNewPlayedTime_4;

public:
	inline static int32_t get_offset_of_mDescriptionUpdated_0() { return static_cast<int32_t>(offsetof(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40, ___mDescriptionUpdated_0)); }
	inline bool get_mDescriptionUpdated_0() const { return ___mDescriptionUpdated_0; }
	inline bool* get_address_of_mDescriptionUpdated_0() { return &___mDescriptionUpdated_0; }
	inline void set_mDescriptionUpdated_0(bool value)
	{
		___mDescriptionUpdated_0 = value;
	}

	inline static int32_t get_offset_of_mNewDescription_1() { return static_cast<int32_t>(offsetof(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40, ___mNewDescription_1)); }
	inline String_t* get_mNewDescription_1() const { return ___mNewDescription_1; }
	inline String_t** get_address_of_mNewDescription_1() { return &___mNewDescription_1; }
	inline void set_mNewDescription_1(String_t* value)
	{
		___mNewDescription_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mNewDescription_1), (void*)value);
	}

	inline static int32_t get_offset_of_mCoverImageUpdated_2() { return static_cast<int32_t>(offsetof(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40, ___mCoverImageUpdated_2)); }
	inline bool get_mCoverImageUpdated_2() const { return ___mCoverImageUpdated_2; }
	inline bool* get_address_of_mCoverImageUpdated_2() { return &___mCoverImageUpdated_2; }
	inline void set_mCoverImageUpdated_2(bool value)
	{
		___mCoverImageUpdated_2 = value;
	}

	inline static int32_t get_offset_of_mNewPngCoverImage_3() { return static_cast<int32_t>(offsetof(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40, ___mNewPngCoverImage_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mNewPngCoverImage_3() const { return ___mNewPngCoverImage_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mNewPngCoverImage_3() { return &___mNewPngCoverImage_3; }
	inline void set_mNewPngCoverImage_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mNewPngCoverImage_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mNewPngCoverImage_3), (void*)value);
	}

	inline static int32_t get_offset_of_mNewPlayedTime_4() { return static_cast<int32_t>(offsetof(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40, ___mNewPlayedTime_4)); }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  get_mNewPlayedTime_4() const { return ___mNewPlayedTime_4; }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * get_address_of_mNewPlayedTime_4() { return &___mNewPlayedTime_4; }
	inline void set_mNewPlayedTime_4(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  value)
	{
		___mNewPlayedTime_4 = value;
	}
};

// Native definition for P/Invoke marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_pinvoke
{
	int32_t ___mDescriptionUpdated_0;
	char* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	Il2CppSafeArray/*NONE*/* ___mNewPngCoverImage_3;
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___mNewPlayedTime_4;
};
// Native definition for COM marshalling of GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_com
{
	int32_t ___mDescriptionUpdated_0;
	Il2CppChar* ___mNewDescription_1;
	int32_t ___mCoverImageUpdated_2;
	Il2CppSafeArray/*NONE*/* ___mNewPngCoverImage_3;
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___mNewPlayedTime_4;
};

// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<AppleAuth.Enums.CredentialState>
struct  Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<AppleAuth.Interfaces.IAppleError>
struct  Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<AppleAuth.Interfaces.ICredential>
struct  Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct  Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.Boolean>
struct  Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.String>
struct  Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.Threading.Tasks.Task>
struct  Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>
struct  Action_1_t5D2D131226D740682F598DD09436558292112781  : public MulticastDelegate_t
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler
struct  EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Boolean,System.String>
struct  Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130  : public MulticastDelegate_t
{
public:

public:
};


// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Events.UnityAction
struct  UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Authentication.WebelinxAnonymousAuthController
struct  WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E_StaticFields
{
public:
	// Authentication.WebelinxAnonymousAuthController Authentication.WebelinxAnonymousAuthController::Instance
	WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E_StaticFields, ___Instance_4)); }
	inline WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E * get_Instance_4() const { return ___Instance_4; }
	inline WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// Authentication.WebelinxFirebaseAuthManager
struct  WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Events.UnityAction Authentication.WebelinxFirebaseAuthManager::OnSignOut
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___OnSignOut_5;
	// System.Boolean Authentication.WebelinxFirebaseAuthManager::isLinkingAccounts
	bool ___isLinkingAccounts_6;
	// Firebase.Auth.FirebaseAuth Authentication.WebelinxFirebaseAuthManager::auth
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth_7;
	// Firebase.Auth.FirebaseUser Authentication.WebelinxFirebaseAuthManager::user
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user_8;
	// Authentication.SignedInUserFirebaseInfo Authentication.WebelinxFirebaseAuthManager::userFirebaseInfo
	SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * ___userFirebaseInfo_9;
	// Authentication.SignedInUserProviderInfo Authentication.WebelinxFirebaseAuthManager::userProviderInfo
	SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * ___userProviderInfo_10;

public:
	inline static int32_t get_offset_of_OnSignOut_5() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52, ___OnSignOut_5)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_OnSignOut_5() const { return ___OnSignOut_5; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_OnSignOut_5() { return &___OnSignOut_5; }
	inline void set_OnSignOut_5(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___OnSignOut_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSignOut_5), (void*)value);
	}

	inline static int32_t get_offset_of_isLinkingAccounts_6() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52, ___isLinkingAccounts_6)); }
	inline bool get_isLinkingAccounts_6() const { return ___isLinkingAccounts_6; }
	inline bool* get_address_of_isLinkingAccounts_6() { return &___isLinkingAccounts_6; }
	inline void set_isLinkingAccounts_6(bool value)
	{
		___isLinkingAccounts_6 = value;
	}

	inline static int32_t get_offset_of_auth_7() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52, ___auth_7)); }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * get_auth_7() const { return ___auth_7; }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** get_address_of_auth_7() { return &___auth_7; }
	inline void set_auth_7(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * value)
	{
		___auth_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___auth_7), (void*)value);
	}

	inline static int32_t get_offset_of_user_8() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52, ___user_8)); }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * get_user_8() const { return ___user_8; }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD ** get_address_of_user_8() { return &___user_8; }
	inline void set_user_8(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * value)
	{
		___user_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___user_8), (void*)value);
	}

	inline static int32_t get_offset_of_userFirebaseInfo_9() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52, ___userFirebaseInfo_9)); }
	inline SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * get_userFirebaseInfo_9() const { return ___userFirebaseInfo_9; }
	inline SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 ** get_address_of_userFirebaseInfo_9() { return &___userFirebaseInfo_9; }
	inline void set_userFirebaseInfo_9(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * value)
	{
		___userFirebaseInfo_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___userFirebaseInfo_9), (void*)value);
	}

	inline static int32_t get_offset_of_userProviderInfo_10() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52, ___userProviderInfo_10)); }
	inline SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * get_userProviderInfo_10() const { return ___userProviderInfo_10; }
	inline SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 ** get_address_of_userProviderInfo_10() { return &___userProviderInfo_10; }
	inline void set_userProviderInfo_10(SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * value)
	{
		___userProviderInfo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___userProviderInfo_10), (void*)value);
	}
};

struct WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields
{
public:
	// Authentication.WebelinxFirebaseAuthManager Authentication.WebelinxFirebaseAuthManager::Instance
	WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields, ___Instance_4)); }
	inline WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * get_Instance_4() const { return ___Instance_4; }
	inline WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// Authentication.WebelinxFirebaseAuthView
struct  WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image Authentication.WebelinxFirebaseAuthView::userImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___userImage_5;
	// UnityEngine.UI.Text Authentication.WebelinxFirebaseAuthView::statusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___statusText_6;
	// UnityEngine.UI.Text Authentication.WebelinxFirebaseAuthView::linkingStatusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___linkingStatusText_7;
	// UnityEngine.UI.Text Authentication.WebelinxFirebaseAuthView::firebaseUserInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___firebaseUserInfo_8;
	// UnityEngine.UI.Text Authentication.WebelinxFirebaseAuthView::providerUserInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___providerUserInfo_9;

public:
	inline static int32_t get_offset_of_userImage_5() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A, ___userImage_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_userImage_5() const { return ___userImage_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_userImage_5() { return &___userImage_5; }
	inline void set_userImage_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___userImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___userImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_statusText_6() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A, ___statusText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_statusText_6() const { return ___statusText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_statusText_6() { return &___statusText_6; }
	inline void set_statusText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___statusText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___statusText_6), (void*)value);
	}

	inline static int32_t get_offset_of_linkingStatusText_7() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A, ___linkingStatusText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_linkingStatusText_7() const { return ___linkingStatusText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_linkingStatusText_7() { return &___linkingStatusText_7; }
	inline void set_linkingStatusText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___linkingStatusText_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___linkingStatusText_7), (void*)value);
	}

	inline static int32_t get_offset_of_firebaseUserInfo_8() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A, ___firebaseUserInfo_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_firebaseUserInfo_8() const { return ___firebaseUserInfo_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_firebaseUserInfo_8() { return &___firebaseUserInfo_8; }
	inline void set_firebaseUserInfo_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___firebaseUserInfo_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firebaseUserInfo_8), (void*)value);
	}

	inline static int32_t get_offset_of_providerUserInfo_9() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A, ___providerUserInfo_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_providerUserInfo_9() const { return ___providerUserInfo_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_providerUserInfo_9() { return &___providerUserInfo_9; }
	inline void set_providerUserInfo_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___providerUserInfo_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___providerUserInfo_9), (void*)value);
	}
};

struct WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields
{
public:
	// Authentication.WebelinxFirebaseAuthView Authentication.WebelinxFirebaseAuthView::Instance
	WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields, ___Instance_4)); }
	inline WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * get_Instance_4() const { return ___Instance_4; }
	inline WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct  PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::localQueue
	List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * ___localQueue_7;

public:
	inline static int32_t get_offset_of_localQueue_7() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F, ___localQueue_7)); }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * get_localQueue_7() const { return ___localQueue_7; }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 ** get_address_of_localQueue_7() { return &___localQueue_7; }
	inline void set_localQueue_7(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * value)
	{
		___localQueue_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___localQueue_7), (void*)value);
	}
};

struct PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields
{
public:
	// GooglePlayGames.OurUtils.PlayGamesHelperObject GooglePlayGames.OurUtils.PlayGamesHelperObject::instance
	PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * ___instance_4;
	// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::sIsDummy
	bool ___sIsDummy_5;
	// System.Collections.Generic.List`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueue
	List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * ___sQueue_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueueEmpty
	bool ___sQueueEmpty_8;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> GooglePlayGames.OurUtils.PlayGamesHelperObject::sPauseCallbackList
	List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * ___sPauseCallbackList_9;
	// System.Collections.Generic.List`1<System.Action`1<System.Boolean>> GooglePlayGames.OurUtils.PlayGamesHelperObject::sFocusCallbackList
	List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * ___sFocusCallbackList_10;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields, ___instance_4)); }
	inline PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * get_instance_4() const { return ___instance_4; }
	inline PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}

	inline static int32_t get_offset_of_sIsDummy_5() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields, ___sIsDummy_5)); }
	inline bool get_sIsDummy_5() const { return ___sIsDummy_5; }
	inline bool* get_address_of_sIsDummy_5() { return &___sIsDummy_5; }
	inline void set_sIsDummy_5(bool value)
	{
		___sIsDummy_5 = value;
	}

	inline static int32_t get_offset_of_sQueue_6() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields, ___sQueue_6)); }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * get_sQueue_6() const { return ___sQueue_6; }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 ** get_address_of_sQueue_6() { return &___sQueue_6; }
	inline void set_sQueue_6(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * value)
	{
		___sQueue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sQueue_6), (void*)value);
	}

	inline static int32_t get_offset_of_sQueueEmpty_8() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields, ___sQueueEmpty_8)); }
	inline bool get_sQueueEmpty_8() const { return ___sQueueEmpty_8; }
	inline bool* get_address_of_sQueueEmpty_8() { return &___sQueueEmpty_8; }
	inline void set_sQueueEmpty_8(bool value)
	{
		___sQueueEmpty_8 = value;
	}

	inline static int32_t get_offset_of_sPauseCallbackList_9() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields, ___sPauseCallbackList_9)); }
	inline List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * get_sPauseCallbackList_9() const { return ___sPauseCallbackList_9; }
	inline List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 ** get_address_of_sPauseCallbackList_9() { return &___sPauseCallbackList_9; }
	inline void set_sPauseCallbackList_9(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * value)
	{
		___sPauseCallbackList_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sPauseCallbackList_9), (void*)value);
	}

	inline static int32_t get_offset_of_sFocusCallbackList_10() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields, ___sFocusCallbackList_10)); }
	inline List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * get_sFocusCallbackList_10() const { return ___sFocusCallbackList_10; }
	inline List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 ** get_address_of_sFocusCallbackList_10() { return &___sFocusCallbackList_10; }
	inline void set_sFocusCallbackList_10(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * value)
	{
		___sFocusCallbackList_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sFocusCallbackList_10), (void*)value);
	}
};


// MainMenu
struct  MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// AppleAuth.IAppleAuthManager MainMenu::_appleAuthManager
	RuntimeObject* ____appleAuthManager_5;
	// LoginMenuHandler MainMenu::LoginMenu
	LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * ___LoginMenu_6;
	// GameMenuHandler MainMenu::GameMenu
	GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * ___GameMenu_7;

public:
	inline static int32_t get_offset_of__appleAuthManager_5() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ____appleAuthManager_5)); }
	inline RuntimeObject* get__appleAuthManager_5() const { return ____appleAuthManager_5; }
	inline RuntimeObject** get_address_of__appleAuthManager_5() { return &____appleAuthManager_5; }
	inline void set__appleAuthManager_5(RuntimeObject* value)
	{
		____appleAuthManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____appleAuthManager_5), (void*)value);
	}

	inline static int32_t get_offset_of_LoginMenu_6() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___LoginMenu_6)); }
	inline LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * get_LoginMenu_6() const { return ___LoginMenu_6; }
	inline LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 ** get_address_of_LoginMenu_6() { return &___LoginMenu_6; }
	inline void set_LoginMenu_6(LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * value)
	{
		___LoginMenu_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoginMenu_6), (void*)value);
	}

	inline static int32_t get_offset_of_GameMenu_7() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___GameMenu_7)); }
	inline GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * get_GameMenu_7() const { return ___GameMenu_7; }
	inline GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 ** get_address_of_GameMenu_7() { return &___GameMenu_7; }
	inline void set_GameMenu_7(GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * value)
	{
		___GameMenu_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GameMenu_7), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// WebelinxAppleAuthController
struct  WebelinxAppleAuthController_t8256E6D120C43A6DCBD58B2252E235C187CD0280  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// AppleAuth.IAppleAuthManager WebelinxAppleAuthController::appleAuthManager
	RuntimeObject* ___appleAuthManager_4;

public:
	inline static int32_t get_offset_of_appleAuthManager_4() { return static_cast<int32_t>(offsetof(WebelinxAppleAuthController_t8256E6D120C43A6DCBD58B2252E235C187CD0280, ___appleAuthManager_4)); }
	inline RuntimeObject* get_appleAuthManager_4() const { return ___appleAuthManager_4; }
	inline RuntimeObject** get_address_of_appleAuthManager_4() { return &___appleAuthManager_4; }
	inline void set_appleAuthManager_4(RuntimeObject* value)
	{
		___appleAuthManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appleAuthManager_4), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_7;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_7)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_4), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_9)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_10)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_13)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_19)); }
	inline List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t053DAB6E2110E276A0339D73497193F464BC1F82 * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Button_ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B, ___m_OnClick_20)); }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_30;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_31;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_32;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_33;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_34;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IsMaskingGraphic_29)); }
	inline bool get_m_IsMaskingGraphic_29() const { return ___m_IsMaskingGraphic_29; }
	inline bool* get_address_of_m_IsMaskingGraphic_29() { return &___m_IsMaskingGraphic_29; }
	inline void set_m_IsMaskingGraphic_29(bool value)
	{
		___m_IsMaskingGraphic_29 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_30)); }
	inline bool get_m_IncludeForMasking_30() const { return ___m_IncludeForMasking_30; }
	inline bool* get_address_of_m_IncludeForMasking_30() { return &___m_IncludeForMasking_30; }
	inline void set_m_IncludeForMasking_30(bool value)
	{
		___m_IncludeForMasking_30 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_31)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_31() const { return ___m_OnCullStateChanged_31; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_31() { return &___m_OnCullStateChanged_31; }
	inline void set_m_OnCullStateChanged_31(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_32)); }
	inline bool get_m_ShouldRecalculate_32() const { return ___m_ShouldRecalculate_32; }
	inline bool* get_address_of_m_ShouldRecalculate_32() { return &___m_ShouldRecalculate_32; }
	inline void set_m_ShouldRecalculate_32(bool value)
	{
		___m_ShouldRecalculate_32 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_33)); }
	inline int32_t get_m_StencilValue_33() const { return ___m_StencilValue_33; }
	inline int32_t* get_address_of_m_StencilValue_33() { return &___m_StencilValue_33; }
	inline void set_m_StencilValue_33(int32_t value)
	{
		___m_StencilValue_33 = value;
	}

	inline static int32_t get_offset_of_m_Corners_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_34)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_34() const { return ___m_Corners_34; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_34() { return &___m_Corners_34; }
	inline void set_m_Corners_34(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_34), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_Sprite_36;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_OverrideSprite_37;
	// UnityEngine.UI.Image_Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_38;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_39;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_40;
	// UnityEngine.UI.Image_FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_41;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_42;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_43;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_44;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_45;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_46;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_47;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_48;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_49;

public:
	inline static int32_t get_offset_of_m_Sprite_36() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Sprite_36)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_Sprite_36() const { return ___m_Sprite_36; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_Sprite_36() { return &___m_Sprite_36; }
	inline void set_m_Sprite_36(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_Sprite_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_37() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_OverrideSprite_37)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_OverrideSprite_37() const { return ___m_OverrideSprite_37; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_OverrideSprite_37() { return &___m_OverrideSprite_37; }
	inline void set_m_OverrideSprite_37(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_OverrideSprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_38() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Type_38)); }
	inline int32_t get_m_Type_38() const { return ___m_Type_38; }
	inline int32_t* get_address_of_m_Type_38() { return &___m_Type_38; }
	inline void set_m_Type_38(int32_t value)
	{
		___m_Type_38 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_39() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PreserveAspect_39)); }
	inline bool get_m_PreserveAspect_39() const { return ___m_PreserveAspect_39; }
	inline bool* get_address_of_m_PreserveAspect_39() { return &___m_PreserveAspect_39; }
	inline void set_m_PreserveAspect_39(bool value)
	{
		___m_PreserveAspect_39 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_40() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillCenter_40)); }
	inline bool get_m_FillCenter_40() const { return ___m_FillCenter_40; }
	inline bool* get_address_of_m_FillCenter_40() { return &___m_FillCenter_40; }
	inline void set_m_FillCenter_40(bool value)
	{
		___m_FillCenter_40 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_41() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillMethod_41)); }
	inline int32_t get_m_FillMethod_41() const { return ___m_FillMethod_41; }
	inline int32_t* get_address_of_m_FillMethod_41() { return &___m_FillMethod_41; }
	inline void set_m_FillMethod_41(int32_t value)
	{
		___m_FillMethod_41 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_42() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillAmount_42)); }
	inline float get_m_FillAmount_42() const { return ___m_FillAmount_42; }
	inline float* get_address_of_m_FillAmount_42() { return &___m_FillAmount_42; }
	inline void set_m_FillAmount_42(float value)
	{
		___m_FillAmount_42 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_43() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillClockwise_43)); }
	inline bool get_m_FillClockwise_43() const { return ___m_FillClockwise_43; }
	inline bool* get_address_of_m_FillClockwise_43() { return &___m_FillClockwise_43; }
	inline void set_m_FillClockwise_43(bool value)
	{
		___m_FillClockwise_43 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_44() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillOrigin_44)); }
	inline int32_t get_m_FillOrigin_44() const { return ___m_FillOrigin_44; }
	inline int32_t* get_address_of_m_FillOrigin_44() { return &___m_FillOrigin_44; }
	inline void set_m_FillOrigin_44(int32_t value)
	{
		___m_FillOrigin_44 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_45() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_AlphaHitTestMinimumThreshold_45)); }
	inline float get_m_AlphaHitTestMinimumThreshold_45() const { return ___m_AlphaHitTestMinimumThreshold_45; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_45() { return &___m_AlphaHitTestMinimumThreshold_45; }
	inline void set_m_AlphaHitTestMinimumThreshold_45(float value)
	{
		___m_AlphaHitTestMinimumThreshold_45 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_46() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Tracked_46)); }
	inline bool get_m_Tracked_46() const { return ___m_Tracked_46; }
	inline bool* get_address_of_m_Tracked_46() { return &___m_Tracked_46; }
	inline void set_m_Tracked_46(bool value)
	{
		___m_Tracked_46 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_47() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_UseSpriteMesh_47)); }
	inline bool get_m_UseSpriteMesh_47() const { return ___m_UseSpriteMesh_47; }
	inline bool* get_address_of_m_UseSpriteMesh_47() { return &___m_UseSpriteMesh_47; }
	inline void set_m_UseSpriteMesh_47(bool value)
	{
		___m_UseSpriteMesh_47 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_48() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PixelsPerUnitMultiplier_48)); }
	inline float get_m_PixelsPerUnitMultiplier_48() const { return ___m_PixelsPerUnitMultiplier_48; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_48() { return &___m_PixelsPerUnitMultiplier_48; }
	inline void set_m_PixelsPerUnitMultiplier_48(float value)
	{
		___m_PixelsPerUnitMultiplier_48 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_49() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_CachedReferencePixelsPerUnit_49)); }
	inline float get_m_CachedReferencePixelsPerUnit_49() const { return ___m_CachedReferencePixelsPerUnit_49; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_49() { return &___m_CachedReferencePixelsPerUnit_49; }
	inline void set_m_CachedReferencePixelsPerUnit_49(float value)
	{
		___m_CachedReferencePixelsPerUnit_49 = value;
	}
};

struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_ETC1DefaultUI_35;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_VertScratch_50;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_UVScratch_51;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Xy_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Uv_53;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * ___m_TrackedTexturelessImages_54;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_55;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_35() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_ETC1DefaultUI_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_ETC1DefaultUI_35() const { return ___s_ETC1DefaultUI_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_ETC1DefaultUI_35() { return &___s_ETC1DefaultUI_35; }
	inline void set_s_ETC1DefaultUI_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_ETC1DefaultUI_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_35), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_50() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_VertScratch_50)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_VertScratch_50() const { return ___s_VertScratch_50; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_VertScratch_50() { return &___s_VertScratch_50; }
	inline void set_s_VertScratch_50(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_VertScratch_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_50), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_51() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_UVScratch_51)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_UVScratch_51() const { return ___s_UVScratch_51; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_UVScratch_51() { return &___s_UVScratch_51; }
	inline void set_s_UVScratch_51(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_UVScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_52() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Xy_52)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Xy_52() const { return ___s_Xy_52; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Xy_52() { return &___s_Xy_52; }
	inline void set_s_Xy_52(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Xy_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_53() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Uv_53)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Uv_53() const { return ___s_Uv_53; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Uv_53() { return &___s_Uv_53; }
	inline void set_s_Uv_53(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Uv_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_53), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_54() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___m_TrackedTexturelessImages_54)); }
	inline List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * get_m_TrackedTexturelessImages_54() const { return ___m_TrackedTexturelessImages_54; }
	inline List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA ** get_address_of_m_TrackedTexturelessImages_54() { return &___m_TrackedTexturelessImages_54; }
	inline void set_m_TrackedTexturelessImages_54(List_1_tB46CB6C7789F514A8BE08EB5A4F3D9D26AE5E4DA * value)
	{
		___m_TrackedTexturelessImages_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_54), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_55() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Initialized_55)); }
	inline bool get_s_Initialized_55() const { return ___s_Initialized_55; }
	inline bool* get_address_of_s_Initialized_55() { return &___s_Initialized_55; }
	inline void set_s_Initialized_55(bool value)
	{
		___s_Initialized_55 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_35;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_36;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_38;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_40;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_41;

public:
	inline static int32_t get_offset_of_m_FontData_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_35)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_35() const { return ___m_FontData_35; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_35() { return &___m_FontData_35; }
	inline void set_m_FontData_35(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_36)); }
	inline String_t* get_m_Text_36() const { return ___m_Text_36; }
	inline String_t** get_address_of_m_Text_36() { return &___m_Text_36; }
	inline void set_m_Text_36(String_t* value)
	{
		___m_Text_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_37() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_37)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_37() const { return ___m_TextCache_37; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_37() { return &___m_TextCache_37; }
	inline void set_m_TextCache_37(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_38() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_38)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_38() const { return ___m_TextCacheForLayout_38; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_38() { return &___m_TextCacheForLayout_38; }
	inline void set_m_TextCacheForLayout_38(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_40() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_40)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_40() const { return ___m_DisableFontTextureRebuiltCallback_40; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_40() { return &___m_DisableFontTextureRebuiltCallback_40; }
	inline void set_m_DisableFontTextureRebuiltCallback_40(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_40 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_41() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_41)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_41() const { return ___m_TempVerts_41; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_41() { return &___m_TempVerts_41; }
	inline void set_m_TempVerts_41(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_41), (void*)value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_39;

public:
	inline static int32_t get_offset_of_s_DefaultText_39() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_39)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_39() const { return ___s_DefaultText_39; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_39() { return &___s_DefaultText_39; }
	inline void set_s_DefaultText_39(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_39), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};

IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke_back(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke& marshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled);
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke_cleanup(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com& marshaled);
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com_back(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com& marshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled);
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com_cleanup(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com& marshaled);

// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Threading.Tasks.Task System.Threading.Tasks.Task`1<System.Object>::ContinueWith(System.Action`1<System.Threading.Tasks.Task`1<!0>>)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * Task_1_ContinueWith_mCFC2582CE8330039C62FC88152095FF923BAECE8_gshared (Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * __this, Action_1_tF41DEFE08D91E5A3638655E075175E27AA82D1DC * ___continuationAction0, const RuntimeMethod* method);
// !0 System.Threading.Tasks.Task`1<System.Object>::get_Result()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Task_1_get_Result_m828E969718CE9AD38335D3F48B6F9C84AB98DEE9_gshared (Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * __this, const RuntimeMethod* method);
// T GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Misc_CheckNotNull_TisRuntimeObject_m7C4724497CDD686F22A5692CF56E7650F4A7AF6C_gshared (RuntimeObject * ___value0, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m619F23350E60DA452BA9F320E9EA38E1D933663D_gshared_inline (Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m99DC0DFF0CCF30E4DFFED5F8F7ABE8721B73B56D_gshared (Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * __this, TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32Enum>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m8C1A5D907933E6E22CA51A19A6A16416B5918461_gshared (Dictionary_2_t5B44A7142DEF210BD03CD8381D8FD7A00197C25B * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32Enum>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m90AB97A4014AACDB26B56701601AC1056BBB67F8_gshared (Dictionary_2_t5B44A7142DEF210BD03CD8381D8FD7A00197C25B * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32Enum>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m4BD357973DD8DE7D80993CA7B3FB1AF94E820B98_gshared (Dictionary_2_t5B44A7142DEF210BD03CD8381D8FD7A00197C25B * __this, int32_t ___key0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32Enum>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Item_mE66912F123AB912D91C017C169781A70F51A98F9_gshared (Dictionary_2_t5B44A7142DEF210BD03CD8381D8FD7A00197C25B * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mF8B81ACE210571A85FC1059BC3074A9C9FF546A4_gshared (Func_2_tB9D83096EC4D7C11E734B451F3F094E92E1D287D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Boolean,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisRuntimeObject_m1C135590280D9819DEA7999BDC72CD7CFE099A0B_gshared (RuntimeObject* ___source0, Func_2_tB9D83096EC4D7C11E734B451F3F094E92E1D287D * ___selector1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Enumerable_ToArray_TisRuntimeObject_m0343A227B31BEB205D569F05AEAE761094FB1B34_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::Call<System.Object>(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_Call_TisRuntimeObject_m38064E69DD787BA971B0757788FD11E7239A03B7_gshared (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_m629B40CD4286736C328FA496AAFC388F697CF984_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_gshared (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * __this, bool ___obj0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_mE08D561E86879A26245096C572A8593279383FDB_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Int32Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m0EB224785E85251D2EBBDD9E4E13EA4C7C04A4A2_gshared (Action_1_tABA1E3BFA092E3309A0ECC53722E4F9826DCE983 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String Firebase.Auth.FirebaseUser::get_UserId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserFirebaseInfo::set_UserUUID(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_UserUUID_mC1824612BC320BA142FDBE242726D2712BFCD902_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String Firebase.Auth.FirebaseUser::get_DisplayName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserFirebaseInfo::set_DisplayName(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_DisplayName_m4F58543D0F421804556204E391108297C3C51D50_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String Firebase.Auth.FirebaseUser::get_Email()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserFirebaseInfo::set_Email(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_Email_m69A025C44E648B762D68463C85FA74144897CE93_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean Firebase.Auth.FirebaseUser::get_IsAnonymous()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserFirebaseInfo::set_IsAnonymous(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_IsAnonymous_mEEC1E03C2960F04B8A8DCE046775D85100AC522D_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, bool ___value0, const RuntimeMethod* method);
// System.Uri Firebase.Auth.FirebaseUser::get_PhotoUrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * FirebaseUser_get_PhotoUrl_mAC0F8F1390483E1FD30D932527542C89E9C389C6 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserFirebaseInfo::set_PhotoUri(System.Uri)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_PhotoUri_m2A865C067ACE928BE0C30A959AF3EB5727157E6A_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method);
// System.String Authentication.SignedInUserFirebaseInfo::get_UserUUID()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_UserUUID_m1BA830CB5DC3B933B6DA95CF15A6D1883D78629C_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method);
// System.String Authentication.SignedInUserFirebaseInfo::get_DisplayName()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method);
// System.String Authentication.SignedInUserFirebaseInfo::get_Email()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method);
// System.Boolean Authentication.SignedInUserFirebaseInfo::get_IsAnonymous()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool SignedInUserFirebaseInfo_get_IsAnonymous_m064F66405EE6B554D78E364FD555E180BDF2F0DB_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method);
// System.Uri Authentication.SignedInUserFirebaseInfo::get_PhotoUri()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865 (String_t* ___format0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<Firebase.Auth.IUserInfo> Firebase.Auth.FirebaseUser::get_ProviderData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FirebaseUser_get_ProviderData_mE5D70ADC73F94F71D686939C53A8984CCEC682D0 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserProviderInfo::set_UserProviderID(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_UserProviderID_mC93C141881ABA67D15BCF271FE0D0F546D01C165_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserProviderInfo::set_ProviderNameID(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_ProviderNameID_m8422083B4FAAB0D2C1FB9F208CB1A7BB7C559828_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserProviderInfo::set_DisplayName(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_DisplayName_mDB53CCA9AB396016EF447DE6440A54CE0928EB81_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserProviderInfo::set_Email(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_Email_m851CCA9B5D5F346D62D5EF68A9F9E9A2B7DAFE74_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserProviderInfo::set_PhotoUri(System.Uri)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_PhotoUri_mE69209E187C563DC763A3349D67E7DDCA0D60AFC_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method);
// System.String Authentication.SignedInUserProviderInfo::get_ProviderNameID()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_ProviderNameID_mD11C21B821492EF3EDC289324BDE73FB5E427E3E_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method);
// System.String Authentication.SignedInUserProviderInfo::get_UserProviderID()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_UserProviderID_m4D91634A20FC5A4B6D78749E72CC14153B9C962D_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method);
// System.String Authentication.SignedInUserProviderInfo::get_Email()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method);
// System.String Authentication.SignedInUserProviderInfo::get_DisplayName()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method);
// System.Uri Authentication.SignedInUserProviderInfo::get_PhotoUri()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void Authentication.WebelinxFirebaseAuthManager::SignInAnonymously()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SignInAnonymously_m871FBA002D82959565F2E81BD8E28CB7E4D4D2F7 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void Authentication.WebelinxFirebaseAuthManager::InitializeFirebase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_InitializeFirebase_mC06C2FF001E3CE03236086DF841C3AA2B729802F (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method);
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::get_DefaultInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_get_DefaultInstance_mA307D08C178B8C5A8EA7BE0F3222668A544FA228 (const RuntimeMethod* method);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::add_StateChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_add_StateChanged_mFA3F7A1FE096865FF349DBD64644371C0C8CCB34 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// Firebase.Auth.Credential Authentication.WebelinxFirebaseAuthManager::GetCredential(System.String,Authentication.Providers)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, String_t* ___accessToken0, int32_t ___provider1, const RuntimeMethod* method);
// System.Boolean Firebase.Auth.Credential::IsValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInWithCredentialAsync(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInWithCredentialAsync_m0C8097559DE43C62ED1AAC3D6105B155B02C948F (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___credential0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1 (Action_1_t5D2D131226D740682F598DD09436558292112781 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t5D2D131226D740682F598DD09436558292112781 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Threading.Tasks.Task System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::ContinueWith(System.Action`1<System.Threading.Tasks.Task`1<!0>>)
inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * __this, Action_1_t5D2D131226D740682F598DD09436558292112781 * ___continuationAction0, const RuntimeMethod* method)
{
	return ((  Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * (*) (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 *, Action_1_t5D2D131226D740682F598DD09436558292112781 *, const RuntimeMethod*))Task_1_ContinueWith_mCFC2582CE8330039C62FC88152095FF923BAECE8_gshared)(__this, ___continuationAction0, method);
}
// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::get_CurrentUser()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseUser::LinkWithCredentialAsync(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___credential0, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInAnonymouslyAsync()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInAnonymouslyAsync_m6963AC820328CD24A672D8800D1B51BD98FF0995 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerPrefs_HasKey_mD87D3051ACE7EC6F5B54F4FC9E18572C917CA0D1 (String_t* ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_DeleteKey_mE0D76FF284F638715170DB52728B7595B41B6E8C (String_t* ___key0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::SignOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_Invoke_mC9FF5AA1F82FDE635B3B6644CE71C94C31C3E71A (UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * __this, const RuntimeMethod* method);
// Firebase.Auth.Credential Firebase.Auth.FacebookAuthProvider::GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5 (String_t* ___accessToken0, const RuntimeMethod* method);
// Firebase.Auth.Credential Firebase.Auth.PlayGamesAuthProvider::GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33 (String_t* ___serverAuthCode0, const RuntimeMethod* method);
// System.Void Authentication.WebelinxFirebaseAuthView::UpdateUserStatusText(Firebase.Auth.FirebaseUser,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, String_t* ___specialMessage1, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserFirebaseInfo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo__ctor_m3814EC958E68B7047E0E3157273D4F46C17934CC (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method);
// System.Void Authentication.WebelinxFirebaseAuthManager::SetUserInformations(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SetUserInformations_mEF76DEF1EEA9516F497E6F12D673AC56F68AC85B (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, const RuntimeMethod* method);
// System.Void Authentication.WebelinxFirebaseAuthView::PrintUserInformations()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserProviderInfo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo__ctor_mE5F7CDFB0E15F3FC967D59B05E4995D2269225E6 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserFirebaseInfo::SetData(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_SetData_m3CF4935FE552BD2AA83C83465FE1973147C12FB9 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, const RuntimeMethod* method);
// System.Void Authentication.SignedInUserProviderInfo::SetData(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_SetData_mD617F76287BBA2E8C70996A7B1100C8855D01F2D (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, const RuntimeMethod* method);
// System.Void Authentication.WebelinxFirebaseAuthManager::SyncFirebaseUserDataWithProviderData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SyncFirebaseUserDataWithProviderData_m4F5FAF541ED3A1D4F9F72E0B2247C3B4EE92BA36 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserProfile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserProfile::set_DisplayName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.Uri::op_Inequality(System.Uri,System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Uri_op_Inequality_m07015206F59460E87CDE2A8D303D5712E30A7F6B (Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri10, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri21, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserProfile::set_PhotoUrl(System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_set_PhotoUrl_mCDA8A14D63367925AD4D59808B472B88086D977A (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method);
// System.Threading.Tasks.Task Firebase.Auth.FirebaseUser::UpdateUserProfileAsync(Firebase.Auth.UserProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * FirebaseUser_UpdateUserProfileAsync_mACFB318A8FA0A47B70F317197B0C5884D5772D9D (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * ___profile0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Threading.Tasks.Task>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m068FBAE923EF2CD287CD903FDE55A5AF890E8623 (Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Threading.Tasks.Task System.Threading.Tasks.Task::ContinueWith(System.Action`1<System.Threading.Tasks.Task>)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * Task_ContinueWith_mC71D3CE9DF20879884F2D08D363BA6E47EF87638 (Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * __this, Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * ___continuationAction0, const RuntimeMethod* method);
// System.Threading.Tasks.Task Firebase.Auth.FirebaseUser::UpdateEmailAsync(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * FirebaseUser_UpdateEmailAsync_m8D0B4CA7BE4605CF684B74CEE6C5A2ADA2E1F5A7 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, String_t* ___email0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::remove_StateChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_remove_StateChanged_mF960C837B4BD78DAF5716BC2348EF9702AD2B736 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Boolean System.Threading.Tasks.Task::get_IsCanceled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42 (Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * __this, const RuntimeMethod* method);
// System.Boolean System.Threading.Tasks.Task::get_IsFaulted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48 (Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * __this, const RuntimeMethod* method);
// System.AggregateException System.Threading.Tasks.Task::get_Exception()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95 (Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// !0 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::get_Result()
inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * __this, const RuntimeMethod* method)
{
	return ((  FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * (*) (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 *, const RuntimeMethod*))Task_1_get_Result_m828E969718CE9AD38335D3F48B6F9C84AB98DEE9_gshared)(__this, method);
}
// System.Void Authentication.WebelinxFirebaseAuthManager::AuthStateChanged(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___eventArgs1, const RuntimeMethod* method);
// System.Void Authentication.WebelinxFirebaseAuthManager/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC540C513C869C7F6E8137A2F2BF6AD336599DDD1 (U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_mEFC4B92529CE83DF72501F92E07EC5598C54BDAC (UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957 (const RuntimeMethod* method);
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlayerPrefs_GetString_m3031AD2D5DEAB97677A9EF629618541437F079F1 (String_t* ___key0, const RuntimeMethod* method);
// System.Byte[] System.Convert::FromBase64String(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Convert_FromBase64String_m079F788D000703E8018DA39BE9C05F1CBF60B156 (String_t* ___s0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m0C86A87871AA8075791EF98499D34DA95ACB0E35 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_m94295492E96C38984406A23CC2A3931758ECE86B (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___tex0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data1, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * Sprite_Create_m9ED36DA8DA0637F93BA2753A16405EB0F7B17A3C (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot2, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m77F8D681D4EE6D58F4F235AFF704C3EB165A9646 (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9 (const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC (RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method);
// System.String System.String::Join(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4 (String_t* ___separator0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___value1, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendLine_mB5B3F68726B05CD404C8C8D8F5A3D2A58FF16BB9 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.String AppleAuth.Extensions.PersonNameExtensions::ToLocalizedString(AppleAuth.Interfaces.IPersonName,AppleAuth.Enums.PersonNameFormatterStyle,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A (RuntimeObject* ___personName0, int32_t ___style1, bool ___usePhoneticRepresentation2, const RuntimeMethod* method);
// T GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(T)
inline String_t* Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91 (String_t* ___value0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (String_t*, const RuntimeMethod*))Misc_CheckNotNull_TisRuntimeObject_m7C4724497CDD686F22A5692CF56E7650F4A7AF6C_gshared)(___value0, method);
}
// System.Void GooglePlayGames.BasicApi.Nearby.AdvertisingResult::.ctor(GooglePlayGames.BasicApi.ResponseStatus,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, int32_t ___status0, String_t* ___localEndpointName1, const RuntimeMethod* method);
// System.Boolean GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Succeeded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdvertisingResult_get_Succeeded_mB18393EF2668F7055A946844069C3FFD5C114427 (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, const RuntimeMethod* method);
// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Status()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdvertisingResult_get_Status_m9D257F1C520D8D7B3D75709985B2C6D59061BF89 (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, const RuntimeMethod* method);
// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_LocalEndpointName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AdvertisingResult_get_LocalEndpointName_mAED984971DC4130D6A7A2716C262B124F313DA3D (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.Logger::d(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_d_m95C0BD29B60442754276E97D1FB7ED05E4AC31CE (String_t* ___msg0, const RuntimeMethod* method);
// System.Void GooglePlayGames.BasicApi.Nearby.EndpointDetails::.ctor(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, String_t* ___endpointId0, String_t* ___name1, String_t* ___serviceId2, const RuntimeMethod* method);
// T GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Byte[]>(T)
inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Misc_CheckNotNull_TisByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_m6BE81B7330BF6B723B4069514F3C7ACFDAAA5A13 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value0, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* (*) (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*))Misc_CheckNotNull_TisRuntimeObject_m7C4724497CDD686F22A5692CF56E7650F4A7AF6C_gshared)(___value0, method);
}
// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionRequest::.ctor(System.String,System.String,System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43 (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * __this, String_t* ___remoteEndpointId0, String_t* ___remoteEndpointName1, String_t* ___serviceId2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload3, const RuntimeMethod* method);
// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_RemoteEndpoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  ConnectionRequest_get_RemoteEndpoint_mFB71214059AC49D631EEB34A4073B05BB9F098D9 (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * __this, const RuntimeMethod* method);
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_Payload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ConnectionRequest_get_Payload_m5FF54BC4CB9C49835FE68F6EFE6A853A824A37F0 (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * __this, const RuntimeMethod* method);
// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.ctor(System.Int64,System.String,GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, int64_t ___localClientId0, String_t* ___remoteEndpointId1, int32_t ___code2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload3, const RuntimeMethod* method);
// System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_LocalClientId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t ConnectionResponse_get_LocalClientId_mB134B291B50B17DD632719CC808C47F1B5128E94 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method);
// System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_RemoteEndpointId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ConnectionResponse_get_RemoteEndpointId_m767C97E62BC21CE88908266C7FA8947CE98BFC18 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method);
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_ResponseStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConnectionResponse_get_ResponseStatus_m592CB9981EE99165FDE5009096E61D37D9D0DF04 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method);
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_Payload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ConnectionResponse_get_Payload_mACC8A0806D0DD6E10E21F1977B42F9BCB26D1C7F (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method);
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_EndpointId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EndpointDetails_get_EndpointId_m560A17AA21D05B3256DA8F0D07ED1FC55FD4A3BD (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, const RuntimeMethod* method);
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EndpointDetails_get_Name_m502483C84E998E62F76F450450F4D3D805E69E07 (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, const RuntimeMethod* method);
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_ServiceId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EndpointDetails_get_ServiceId_mDB50FBA4F7F40740C49B63E76D92CE37F9090C63 (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, const RuntimeMethod* method);
// T GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>>(T)
inline Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * Misc_CheckNotNull_TisAction_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9_m240E10DDF67AD2B8678A5504DA835C7ACDA4FA2A (Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * ___value0, const RuntimeMethod* method)
{
	return ((  Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * (*) (Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 *, const RuntimeMethod*))Misc_CheckNotNull_TisRuntimeObject_m7C4724497CDD686F22A5692CF56E7650F4A7AF6C_gshared)(___value0, method);
}
// System.Void GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::.ctor(System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * __this, Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * ___callback0, int64_t ___localClientId1, const RuntimeMethod* method);
// System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_LocalClientId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t NearbyConnectionConfiguration_get_LocalClientId_m70F6E0E0867D4BE7FCDB0A157AC0DDA2F61B0D7D (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * __this, const RuntimeMethod* method);
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_InitializationCallback()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * NearbyConnectionConfiguration_get_InitializationCallback_m0E59187AC709946F6C28F84A0C80A0B36539990B (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * __this, const RuntimeMethod* method);
// System.Void GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::.ctor(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SavedGameMetadataUpdate__ctor_m5B508D8C6829985292C4AFC6D5A99F3A9E9AE259 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  ___builder0, const RuntimeMethod* method);
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsDescriptionUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1CBC7A1976D25DC56DA9D85A358BDF4CED9A26C8 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method);
// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedDescription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SavedGameMetadataUpdate_get_UpdatedDescription_m8B5AD589A240684AE03B31448BA9808E4FAF399C (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method);
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsCoverImageUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SavedGameMetadataUpdate_get_IsCoverImageUpdated_m8D0478AA3953E8790FC4AF2B8EDEFDD9AB58B7CB (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method);
// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPngCoverImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2E4D77C771ABD4A2E4EDD225A6F59FD05E4D32F1 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
inline bool Nullable_1_get_HasValue_m619F23350E60DA452BA9F320E9EA38E1D933663D_inline (Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 *, const RuntimeMethod*))Nullable_1_get_HasValue_m619F23350E60DA452BA9F320E9EA38E1D933663D_gshared_inline)(__this, method);
}
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsPlayedTimeUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method);
// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPlayedTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  SavedGameMetadataUpdate_get_UpdatedPlayedTime_m610F974D3E0EAA3F1476BF985C47ED6726012A00 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method);
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedDescription(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, String_t* ___description0, const RuntimeMethod* method);
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPngCoverImage(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedPngCoverImage_mEF27FDD278DFFEF63F224B254933CB2127BBE223 (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___newPngCoverImage0, const RuntimeMethod* method);
// System.Double System.TimeSpan::get_TotalMilliseconds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double TimeSpan_get_TotalMilliseconds_m48B00B27D485CC556C10A5119BC11E1A1E0FE363 (TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * __this, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706 (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(!0)
inline void Nullable_1__ctor_m99DC0DFF0CCF30E4DFFED5F8F7ABE8721B73B56D (Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * __this, TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 *, TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 , const RuntimeMethod*))Nullable_1__ctor_m99DC0DFF0CCF30E4DFFED5F8F7ABE8721B73B56D_gshared)(__this, ___value0, method);
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPlayedTime(System.TimeSpan)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___newPlayedTime0, const RuntimeMethod* method);
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::Build()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A  Builder_Build_m0E1D8B5A4CEDDBEBA294C8FC744DE04DA810D803 (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,GooglePlayGames.BasicApi.SignInStatus>::.ctor()
inline void Dictionary_2__ctor_m9E5157EB15E8B864F3CC6D184BA6371E2A601EFA (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F *, const RuntimeMethod*))Dictionary_2__ctor_m8C1A5D907933E6E22CA51A19A6A16416B5918461_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,GooglePlayGames.BasicApi.SignInStatus>::Add(!0,!1)
inline void Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F *, int32_t, int32_t, const RuntimeMethod*))Dictionary_2_Add_m90AB97A4014AACDB26B56701601AC1056BBB67F8_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,GooglePlayGames.BasicApi.SignInStatus>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m75C23C174DE8C46B92343E6304A8A7DB305B407B (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m4BD357973DD8DE7D80993CA7B3FB1AF94E820B98_gshared)(__this, ___key0, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.Int32,GooglePlayGames.BasicApi.SignInStatus>::get_Item(!0)
inline int32_t Dictionary_2_get_Item_m9082FAC2380F0EE9C2D576C66BF0B016931027CE (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_mE66912F123AB912D91C017C169781A70F51A98F9_gshared)(__this, ___key0, method);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m1CBBA4989F15BA668EE24950D3C6B56E2ED20BD6 (String_t* ___key0, int32_t ___defaultValue1, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.Logger::w(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB (String_t* ___msg0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Boolean,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mBAD3E8F66408158F3CBC5309309B4545614D38F5 (Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mF8B81ACE210571A85FC1059BC3074A9C9FF546A4_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Boolean,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisString_t_mA66BDED9E6FDCFE3BB7C27E0C69135B0EB24C4CF (RuntimeObject* ___source0, Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 *, const RuntimeMethod*))Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisRuntimeObject_m1C135590280D9819DEA7999BDC72CD7CFE099A0B_gshared)(___source0, ___selector1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* Enumerable_ToArray_TisString_t_m34F0EB317BEC1783F5D3E46852744966D246040C (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m0343A227B31BEB205D569F05AEAE761094FB1B34_gshared)(___source0, method);
}
// System.Void GooglePlayGames.BasicApi.Video.VideoCapabilities/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4BD30BD945191F668A932130240743B6CCFA046E (U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * __this, const RuntimeMethod* method);
// System.String System.Boolean::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301 (bool* __this, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.Logger/<>c__DisplayClass8_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass8_0__ctor_m8C4C8977B9B5F82E13A6EFAE0F54ECE849EC214B (U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunOnGameThread(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.Logger/<>c__DisplayClass9_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0__ctor_m7FE9968DBCD1D95BA7D9CE20A30FDF7976C1DDE1 (U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 * __this, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.Logger/<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_mA3755E25A067B9D324C45DF55C4088DD1480E0C6 (U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF * __this, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2 (const RuntimeMethod* method);
// System.String System.DateTime::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DateTime_ToString_m203C5710CD7AB2F5F1B2D9DA1DFD45BB3774179A (DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * __this, String_t* ___format0, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.Logger/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAF62846782BCBABBC4CE9AD9D5A1EC351B71CE33 (U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.String GooglePlayGames.OurUtils.Logger::ToLogMessage(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C (String_t* ___prefix0, String_t* ___logType1, String_t* ___msg2, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6 (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6 (RuntimeArray * ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline (const RuntimeMethod* method)
{
	return ((  ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline)(method);
}
// !!0 UnityEngine.AndroidJavaObject::Call<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
inline AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * AndroidJavaObject_Call_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m775AB90594C5F27D6099ED61119EF3608FD1001D (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * (*) (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D *, String_t*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*))AndroidJavaObject_Call_TisRuntimeObject_m38064E69DD787BA971B0757788FD11E7239A03B7_gshared)(__this, ___methodName0, ___args1, method);
}
// !!0 UnityEngine.AndroidJavaObject::Call<System.String>(System.String,System.Object[])
inline String_t* AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method)
{
	return ((  String_t* (*) (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D *, String_t*, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, const RuntimeMethod*))AndroidJavaObject_Call_TisRuntimeObject_m38064E69DD787BA971B0757788FD11E7239A03B7_gshared)(__this, ___methodName0, ___args1, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5 (const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___target0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<GooglePlayGames.OurUtils.PlayGamesHelperObject>()
inline PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * GameObject_AddComponent_TisPlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_m89B34D1AD3E162ECA397870BC80FD50E6A99DA3E (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mCB8164FB05F8DCF99E098ADC5E13E96FEF6FC4E9_gshared)(__this, method);
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject__ctor_m6F2894860B6C363E51DFB53E692ADCDCCB10D9B1 (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject/<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_mC187ED07653BC33D8A6BA353FC0F6A74C1628F91 (U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 * __this, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5 (RuntimeObject * ___obj0, bool* ___lockTaken1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::Add(!0)
inline void List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::Clear()
inline void List_1_Clear_m851B12AA396220AB3F093C8B3D84B0E815E8A38B (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_m8AFF779F458D803CEE3FD8A0A2CCD5B5C18B2ED8 (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m629B40CD4286736C328FA496AAFC388F697CF984_gshared)(__this, ___collection0, method);
}
// !0 System.Collections.Generic.List`1<System.Action>::get_Item(System.Int32)
inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_inline (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count()
inline int32_t List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_inline (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Action`1<System.Boolean>>::GetEnumerator()
inline Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE  List_1_GetEnumerator_m1A6BEB03A9E4B76F009EACE9E2DC8202C1B00B0D (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE  (*) (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Boolean>>::get_Current()
inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * Enumerator_get_Current_mEDF66107B1E3BECA631A37771397FAC62B4BB21B_inline (Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE * __this, const RuntimeMethod* method)
{
	return ((  Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * (*) (Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
inline void Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * __this, bool ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *, bool, const RuntimeMethod*))Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_gshared)(__this, ___obj0, method);
}
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Void GooglePlayGames.OurUtils.Logger::e(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8 (String_t* ___msg0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Boolean>>::MoveNext()
inline bool Enumerator_MoveNext_m08F3FE51A64369D57EC067889997CBA5F81DCC73 (Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Boolean>>::Dispose()
inline void Enumerator_Dispose_m0F7AA651E81830E11A33BA5B2CACFFBE25F610A9 (Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1<System.Action`1<System.Boolean>>::Contains(!0)
inline bool List_1_Contains_m1087577496C9953C2B9FE83421C010D93E1E39DE (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * __this, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 *, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *, const RuntimeMethod*))List_1_Contains_mE08D561E86879A26245096C572A8593279383FDB_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<System.Action`1<System.Boolean>>::Add(!0)
inline void List_1_Add_mBF59C2344C951987CF2A02BFE8F7F1BF795434FB (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * __this, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 *, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<System.Action`1<System.Boolean>>::Remove(!0)
inline bool List_1_Remove_m201C000881D6CDC3645867A7B25986ADF9614086 (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * __this, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 *, Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *, const RuntimeMethod*))List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
inline void List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968 (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Action`1<System.Boolean>>::.ctor()
inline void List_1__ctor_m2806B6CAA6E2D1E105FDA5CCB083EFDD8591339E (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method);
// System.Boolean AppleAuth.AppleAuthManager::get_IsCurrentPlatformSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AppleAuthManager_get_IsCurrentPlatformSupported_m4B3FB905E225F53F9FB03208A10FA86EB8C6CBB4 (const RuntimeMethod* method);
// System.Void AppleAuth.Native.PayloadDeserializer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PayloadDeserializer__ctor_m3DD225AE33F56326A8DF099FAEDDD75F65469D31 (PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * __this, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager::.ctor(AppleAuth.Interfaces.IPayloadDeserializer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager__ctor_m4597D08EC5F35812DB3BAF881BF569B77C15EC87 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, RuntimeObject* ___payloadDeserializer0, const RuntimeMethod* method);
// System.Void MainMenu::InitializeLoginMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_InitializeLoginMenu_m6DF9DFCD30D84C572C0FAE153A293E972409D333 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void LoginMenuHandler::UpdateLoadingMessage(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_UpdateLoadingMessage_m9F88D8943762FB49A8DCB56CDEC6BC63F35E7083 (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, float ___deltaTime0, const RuntimeMethod* method);
// System.Void MainMenu::SetupLoginMenuForAppleSignIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForAppleSignIn_m314FDA3B2F46F68904F84841840033848ADB585D (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Void MainMenu::SignInWithApple()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SignInWithApple_m54E769319FF0899CCD150E6BB1602E3E5ECFBE94 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Void LoginMenuHandler::SetVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069 (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, bool ___visible0, const RuntimeMethod* method);
// System.Void GameMenuHandler::SetVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C (GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * __this, bool ___visible0, const RuntimeMethod* method);
// System.Void MainMenu::SetupLoginMenuForUnsupportedPlatform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForUnsupportedPlatform_m80C029D8C208B629AF2849559B8D653AD4FCF374 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void MainMenu::SetupLoginMenuForCheckingCredentials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForCheckingCredentials_m16AEE25404D91E530C121EFFFF3257C074EEBF25 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Void MainMenu::CheckCredentialStatusForUserId(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_CheckCredentialStatusForUserId_m33A8D3E7479359851349A1532CC5031D03AFE3B2 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, String_t* ___appleUserId0, const RuntimeMethod* method);
// System.Void MainMenu::SetupLoginMenuForQuickLoginAttempt()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForQuickLoginAttempt_m351D9B48275D2D2587C3FFD3F5F4D6BF002C948F (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Void MainMenu::AttemptQuickLogin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_AttemptQuickLogin_m1C4F040B20F22A88A09469378F69540EFEAFB883 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Void LoginMenuHandler::SetSignInWithAppleButton(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608 (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, bool ___visible0, bool ___enabled1, const RuntimeMethod* method);
// System.Void LoginMenuHandler::SetLoadingMessage(System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, bool ___visible0, String_t* ___message1, const RuntimeMethod* method);
// System.Void GameMenuHandler::SetupAppleData(System.String,AppleAuth.Interfaces.ICredential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuHandler_SetupAppleData_m5878285CA5056522A0FD6BDC58739DCEC59E6BC2 (GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * __this, String_t* ___appleUserId0, RuntimeObject* ___receivedCredential1, const RuntimeMethod* method);
// System.Void MainMenu/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_m036E1E18C115482EC760CB2A2851B38E75EAA653 (U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<AppleAuth.Enums.CredentialState>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mC614E47849BEBE0AF8A23568BBA0E74B035427AC (Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m0EB224785E85251D2EBBDD9E4E13EA4C7C04A4A2_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`1<AppleAuth.Interfaces.IAppleError>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055 (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`1<AppleAuth.Interfaces.ICredential>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4 (Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void AppleAuth.AppleAuthLoginArgs::.ctor(AppleAuth.Enums.LoginOptions,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54 (AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 * __this, int32_t ___options0, String_t* ___nonce1, String_t* ___state2, const RuntimeMethod* method);
// System.Void MainMenu::SetupLoginMenuForSignInWithApple()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetString_m7AC4E332A5DCA04E0AD91544AF836744BA8C2583 (String_t* ___key0, String_t* ___value1, const RuntimeMethod* method);
// System.Void MainMenu::SetupGameMenu(System.String,AppleAuth.Interfaces.ICredential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupGameMenu_m9141BCE4B7637E206CF1718C18189AC0D6DDCFF9 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, String_t* ___appleUserId0, RuntimeObject* ___credential1, const RuntimeMethod* method);
// AppleAuth.Enums.AuthorizationErrorCode AppleAuth.Extensions.AppleErrorExtensions::GetAuthorizationErrorCode(AppleAuth.Interfaces.IAppleError)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B (RuntimeObject* ___error0, const RuntimeMethod* method);
// System.Void WebelinxAppleAuthController/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7D4447F7EDCE4B591674E4A6048A309B5758BA08 (U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Authentication.SignedInUserFirebaseInfo::get_UserUUID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_UserUUID_m1BA830CB5DC3B933B6DA95CF15A6D1883D78629C (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public string UserUUID { get; set; }
		String_t* L_0 = __this->get_U3CUserUUIDU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserFirebaseInfo::set_UserUUID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_UserUUID_mC1824612BC320BA142FDBE242726D2712BFCD902 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string UserUUID { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CUserUUIDU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Authentication.SignedInUserFirebaseInfo::get_DisplayName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserFirebaseInfo::set_DisplayName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_DisplayName_m4F58543D0F421804556204E391108297C3C51D50 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String Authentication.SignedInUserFirebaseInfo::get_Email()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserFirebaseInfo::set_Email(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_Email_m69A025C44E648B762D68463C85FA74144897CE93 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean Authentication.SignedInUserFirebaseInfo::get_IsAnonymous()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SignedInUserFirebaseInfo_get_IsAnonymous_m064F66405EE6B554D78E364FD555E180BDF2F0DB (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public bool IsAnonymous { get; set; }
		bool L_0 = __this->get_U3CIsAnonymousU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserFirebaseInfo::set_IsAnonymous(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_IsAnonymous_mEEC1E03C2960F04B8A8DCE046775D85100AC522D (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsAnonymous { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CIsAnonymousU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Uri Authentication.SignedInUserFirebaseInfo::get_PhotoUri()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = __this->get_U3CPhotoUriU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserFirebaseInfo::set_PhotoUri(System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_PhotoUri_m2A865C067ACE928BE0C30A959AF3EB5727157E6A (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = ___value0;
		__this->set_U3CPhotoUriU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Authentication.SignedInUserFirebaseInfo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo__ctor_m3814EC958E68B7047E0E3157273D4F46C17934CC (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public SignedInUserFirebaseInfo() { }
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// public SignedInUserFirebaseInfo() { }
		return;
	}
}
// System.Void Authentication.SignedInUserFirebaseInfo::SetData(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_SetData_m3CF4935FE552BD2AA83C83465FE1973147C12FB9 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, const RuntimeMethod* method)
{
	{
		// UserUUID = user.UserId;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_0 = ___user0;
		NullCheck(L_0);
		String_t* L_1 = FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD(L_0, /*hidden argument*/NULL);
		SignedInUserFirebaseInfo_set_UserUUID_mC1824612BC320BA142FDBE242726D2712BFCD902_inline(__this, L_1, /*hidden argument*/NULL);
		// DisplayName = user.DisplayName;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_2 = ___user0;
		NullCheck(L_2);
		String_t* L_3 = FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB(L_2, /*hidden argument*/NULL);
		SignedInUserFirebaseInfo_set_DisplayName_m4F58543D0F421804556204E391108297C3C51D50_inline(__this, L_3, /*hidden argument*/NULL);
		// Email = user.Email;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_4 = ___user0;
		NullCheck(L_4);
		String_t* L_5 = FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06(L_4, /*hidden argument*/NULL);
		SignedInUserFirebaseInfo_set_Email_m69A025C44E648B762D68463C85FA74144897CE93_inline(__this, L_5, /*hidden argument*/NULL);
		// IsAnonymous = user.IsAnonymous;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_6 = ___user0;
		NullCheck(L_6);
		bool L_7 = FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902(L_6, /*hidden argument*/NULL);
		SignedInUserFirebaseInfo_set_IsAnonymous_mEEC1E03C2960F04B8A8DCE046775D85100AC522D_inline(__this, L_7, /*hidden argument*/NULL);
		// PhotoUri = user.PhotoUrl;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_8 = ___user0;
		NullCheck(L_8);
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_9 = FirebaseUser_get_PhotoUrl_mAC0F8F1390483E1FD30D932527542C89E9C389C6(L_8, /*hidden argument*/NULL);
		SignedInUserFirebaseInfo_set_PhotoUri_m2A865C067ACE928BE0C30A959AF3EB5727157E6A_inline(__this, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String Authentication.SignedInUserFirebaseInfo::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_ToString_mDA7E55088D45FA5DDD17F6C5EA660A1B45EE3983 (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignedInUserFirebaseInfo_ToString_mDA7E55088D45FA5DDD17F6C5EA660A1B45EE3983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return $"Firebase UUID: {UserUUID}\nDisplay Name: {DisplayName}\n Email: {Email}\n Is Anonymous? : {IsAnonymous}\n Photo URI : {PhotoUri}";
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		String_t* L_2 = SignedInUserFirebaseInfo_get_UserUUID_m1BA830CB5DC3B933B6DA95CF15A6D1883D78629C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_1;
		String_t* L_4 = SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_3;
		String_t* L_6 = SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_5;
		bool L_8 = SignedInUserFirebaseInfo_get_IsAnonymous_m064F66405EE6B554D78E364FD555E180BDF2F0DB_inline(__this, /*hidden argument*/NULL);
		bool L_9 = L_8;
		RuntimeObject * L_10 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_7;
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_12 = SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_12);
		String_t* L_13 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral1D290DEA00FCBEC55F83A1B68E9C20A643CF6956, L_11, /*hidden argument*/NULL);
		V_0 = L_13;
		goto IL_0046;
	}

IL_0046:
	{
		// }
		String_t* L_14 = V_0;
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Authentication.SignedInUserProviderInfo::get_ProviderNameID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_ProviderNameID_mD11C21B821492EF3EDC289324BDE73FB5E427E3E (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string ProviderNameID { get; set; }
		String_t* L_0 = __this->get_U3CProviderNameIDU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserProviderInfo::set_ProviderNameID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_ProviderNameID_m8422083B4FAAB0D2C1FB9F208CB1A7BB7C559828 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string ProviderNameID { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CProviderNameIDU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Authentication.SignedInUserProviderInfo::get_UserProviderID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_UserProviderID_m4D91634A20FC5A4B6D78749E72CC14153B9C962D (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string UserProviderID { get; set; }
		String_t* L_0 = __this->get_U3CUserProviderIDU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserProviderInfo::set_UserProviderID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_UserProviderID_mC93C141881ABA67D15BCF271FE0D0F546D01C165 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string UserProviderID { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CUserProviderIDU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String Authentication.SignedInUserProviderInfo::get_Email()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserProviderInfo::set_Email(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_Email_m851CCA9B5D5F346D62D5EF68A9F9E9A2B7DAFE74 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String Authentication.SignedInUserProviderInfo::get_DisplayName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserProviderInfo::set_DisplayName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_DisplayName_mDB53CCA9AB396016EF447DE6440A54CE0928EB81 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Uri Authentication.SignedInUserProviderInfo::get_PhotoUri()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = __this->get_U3CPhotoUriU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Authentication.SignedInUserProviderInfo::set_PhotoUri(System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_PhotoUri_mE69209E187C563DC763A3349D67E7DDCA0D60AFC (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = ___value0;
		__this->set_U3CPhotoUriU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Authentication.SignedInUserProviderInfo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo__ctor_mE5F7CDFB0E15F3FC967D59B05E4995D2269225E6 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public SignedInUserProviderInfo() { }
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// public SignedInUserProviderInfo() { }
		return;
	}
}
// System.Void Authentication.SignedInUserProviderInfo::SetData(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_SetData_mD617F76287BBA2E8C70996A7B1100C8855D01F2D (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignedInUserProviderInfo_SetData_mD617F76287BBA2E8C70996A7B1100C8855D01F2D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// foreach (IUserInfo profile in user.ProviderData)
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_0 = ___user0;
		NullCheck(L_0);
		RuntimeObject* L_1 = FirebaseUser_get_ProviderData_mE5D70ADC73F94F71D686939C53A8984CCEC682D0(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Firebase.Auth.IUserInfo>::GetEnumerator() */, IEnumerable_1_t62E378E9873F9801909C3B38FB5B032D7A9929EA_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005a;
		}

IL_0010:
		{
			// foreach (IUserInfo profile in user.ProviderData)
			RuntimeObject* L_3 = V_0;
			NullCheck(L_3);
			RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Firebase.Auth.IUserInfo>::get_Current() */, IEnumerator_1_t15B010A6F214BD2814994B7E16217E4DADF346CA_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			// UserProviderID = profile.UserId;
			RuntimeObject* L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String Firebase.Auth.IUserInfo::get_UserId() */, IUserInfo_t7265E03A4665719EDE78D7F016A2B46DD9C8FE45_il2cpp_TypeInfo_var, L_5);
			SignedInUserProviderInfo_set_UserProviderID_mC93C141881ABA67D15BCF271FE0D0F546D01C165_inline(__this, L_6, /*hidden argument*/NULL);
			// ProviderNameID = profile.ProviderId;
			RuntimeObject* L_7 = V_1;
			NullCheck(L_7);
			String_t* L_8 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String Firebase.Auth.IUserInfo::get_ProviderId() */, IUserInfo_t7265E03A4665719EDE78D7F016A2B46DD9C8FE45_il2cpp_TypeInfo_var, L_7);
			SignedInUserProviderInfo_set_ProviderNameID_m8422083B4FAAB0D2C1FB9F208CB1A7BB7C559828_inline(__this, L_8, /*hidden argument*/NULL);
			// DisplayName = profile.DisplayName;
			RuntimeObject* L_9 = V_1;
			NullCheck(L_9);
			String_t* L_10 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Firebase.Auth.IUserInfo::get_DisplayName() */, IUserInfo_t7265E03A4665719EDE78D7F016A2B46DD9C8FE45_il2cpp_TypeInfo_var, L_9);
			SignedInUserProviderInfo_set_DisplayName_mDB53CCA9AB396016EF447DE6440A54CE0928EB81_inline(__this, L_10, /*hidden argument*/NULL);
			// Email = profile.Email;
			RuntimeObject* L_11 = V_1;
			NullCheck(L_11);
			String_t* L_12 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String Firebase.Auth.IUserInfo::get_Email() */, IUserInfo_t7265E03A4665719EDE78D7F016A2B46DD9C8FE45_il2cpp_TypeInfo_var, L_11);
			SignedInUserProviderInfo_set_Email_m851CCA9B5D5F346D62D5EF68A9F9E9A2B7DAFE74_inline(__this, L_12, /*hidden argument*/NULL);
			// PhotoUri = profile.PhotoUrl;
			RuntimeObject* L_13 = V_1;
			NullCheck(L_13);
			Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_14 = InterfaceFuncInvoker0< Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * >::Invoke(2 /* System.Uri Firebase.Auth.IUserInfo::get_PhotoUrl() */, IUserInfo_t7265E03A4665719EDE78D7F016A2B46DD9C8FE45_il2cpp_TypeInfo_var, L_13);
			SignedInUserProviderInfo_set_PhotoUri_mE69209E187C563DC763A3349D67E7DDCA0D60AFC_inline(__this, L_14, /*hidden argument*/NULL);
		}

IL_005a:
		{
			// foreach (IUserInfo profile in user.ProviderData)
			RuntimeObject* L_15 = V_0;
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0010;
			}
		}

IL_0062:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_17 = V_0;
			if (!L_17)
			{
				goto IL_006e;
			}
		}

IL_0067:
		{
			RuntimeObject* L_18 = V_0;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_18);
		}

IL_006e:
		{
			IL2CPP_END_FINALLY(100)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
	}

IL_006f:
	{
		// }
		return;
	}
}
// System.String Authentication.SignedInUserProviderInfo::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_ToString_m0CB989A3CA76D9F627FE4DA65A3884C2D44742E2 (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignedInUserProviderInfo_ToString_m0CB989A3CA76D9F627FE4DA65A3884C2D44742E2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return $"Provider name: {ProviderNameID}\n User's provider ID: {UserProviderID}\n Email: {Email}\n Display Name: {DisplayName}\n Photo URI : {PhotoUri}";
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		String_t* L_2 = SignedInUserProviderInfo_get_ProviderNameID_mD11C21B821492EF3EDC289324BDE73FB5E427E3E_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_1;
		String_t* L_4 = SignedInUserProviderInfo_get_UserProviderID_m4D91634A20FC5A4B6D78749E72CC14153B9C962D_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_3;
		String_t* L_6 = SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_5;
		String_t* L_8 = SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_7;
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_10 = SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_10);
		String_t* L_11 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral645F7AD1F8C46BE73BB67AD11D5F90C8B8077111, L_9, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0041;
	}

IL_0041:
	{
		// }
		String_t* L_12 = V_0;
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Authentication.WebelinxAnonymousAuthController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxAnonymousAuthController_Awake_mDC1A3C9F3989E599312109DF59CA727B807DFE96 (WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxAnonymousAuthController_Awake_mDC1A3C9F3989E599312109DF59CA727B807DFE96_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (Instance == null)
		WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E * L_0 = ((WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		// Instance = this;
		((WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E_il2cpp_TypeInfo_var))->set_Instance_4(__this);
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxAnonymousAuthController::LoginAnonymously()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxAnonymousAuthController_LoginAnonymously_mB1FDA14714B88436EF79AB2C95EDC6293EE3C602 (WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxAnonymousAuthController_LoginAnonymously_mB1FDA14714B88436EF79AB2C95EDC6293EE3C602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// WebelinxFirebaseAuthManager.Instance.SignInAnonymously();
		WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * L_0 = ((WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_0);
		WebelinxFirebaseAuthManager_SignInAnonymously_m871FBA002D82959565F2E81BD8E28CB7E4D4D2F7(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Authentication.WebelinxAnonymousAuthController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxAnonymousAuthController__ctor_m19A3027562B3E2BE1D838024499B58647B3A4F81 (WebelinxAnonymousAuthController_t92DC055B3D262BD8EA2BA2C3FF26B161CE625D0E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Authentication.WebelinxFirebaseAuthManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_Awake_m23637A0CEA31F6BD70DBECA038DE4D90573A89A2 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_Awake_m23637A0CEA31F6BD70DBECA038DE4D90573A89A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// isLinkingAccounts = true;
		__this->set_isLinkingAccounts_6((bool)1);
		// if (Instance == null) Instance = this;
		WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * L_0 = ((WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		// if (Instance == null) Instance = this;
		((WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_il2cpp_TypeInfo_var))->set_Instance_4(__this);
	}

IL_001d:
	{
		// isLinkingAccounts = false;
		__this->set_isLinkingAccounts_6((bool)0);
		// InitializeFirebase();
		WebelinxFirebaseAuthManager_InitializeFirebase_mC06C2FF001E3CE03236086DF841C3AA2B729802F(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::InitializeFirebase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_InitializeFirebase_mC06C2FF001E3CE03236086DF841C3AA2B729802F (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_InitializeFirebase_mC06C2FF001E3CE03236086DF841C3AA2B729802F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// auth = FirebaseAuth.DefaultInstance;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = FirebaseAuth_get_DefaultInstance_mA307D08C178B8C5A8EA7BE0F3222668A544FA228(/*hidden argument*/NULL);
		__this->set_auth_7(L_0);
		// user = null;
		__this->set_user_8((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL);
		// auth.StateChanged += AuthStateChanged;
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_1 = __this->get_auth_7();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_2, __this, (intptr_t)((intptr_t)WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		FirebaseAuth_add_StateChanged_mFA3F7A1FE096865FF349DBD64644371C0C8CCB34(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::SignInThroughProvider(System.String,Authentication.Providers)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SignInThroughProvider_m053BD42E82A8F70CD2BF5E4487A977E37B18623C (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, String_t* ___accessToken0, int32_t ___provider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_SignInThroughProvider_m053BD42E82A8F70CD2BF5E4487A977E37B18623C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	{
		// Credential credential = GetCredential(accessToken, provider);
		String_t* L_0 = ___accessToken0;
		int32_t L_1 = ___provider1;
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_2 = WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// if(credential == null || !credential.IsValid())
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039(L_4, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
	}

IL_0019:
	{
		V_1 = (bool)G_B3_0;
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		// Debug.LogError("Invalid Credential");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral16BA435E490B03725CE5809E156F7C12C7ED4493, /*hidden argument*/NULL);
		// return;
		goto IL_0049;
	}

IL_002b:
	{
		// auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
		// {
		//     if (task.IsCanceled)
		//     {
		//         WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Login canceled");
		// 
		//         Debug.LogError("You canceled your Login.");
		//         return;
		//     }
		// 
		//     if (task.IsFaulted)
		//     {
		//         WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Login error");
		// 
		//         Debug.LogError("Login error " + task.Exception);
		//         return;
		//     }
		// 
		//     user = task.Result;
		// });
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_7 = __this->get_auth_7();
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_8 = V_0;
		NullCheck(L_7);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_9 = FirebaseAuth_SignInWithCredentialAsync_m0C8097559DE43C62ED1AAC3D6105B155B02C948F(L_7, L_8, /*hidden argument*/NULL);
		Action_1_t5D2D131226D740682F598DD09436558292112781 * L_10 = (Action_1_t5D2D131226D740682F598DD09436558292112781 *)il2cpp_codegen_object_new(Action_1_t5D2D131226D740682F598DD09436558292112781_il2cpp_TypeInfo_var);
		Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1(L_10, __this, (intptr_t)((intptr_t)WebelinxFirebaseAuthManager_U3CSignInThroughProviderU3Eb__9_0_m30F8227F6F0FEE4CECB0C0B592AA3E9203669A0C_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1_RuntimeMethod_var);
		NullCheck(L_9);
		Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E(L_9, L_10, /*hidden argument*/Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E_RuntimeMethod_var);
	}

IL_0049:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::LinkAccountWithAnotherProvider(System.String,Authentication.Providers)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_LinkAccountWithAnotherProvider_m520CC13E2A4771D5DF7167E773A41EA0B619E2AB (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, String_t* ___accessToken0, int32_t ___provider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_LinkAccountWithAnotherProvider_m520CC13E2A4771D5DF7167E773A41EA0B619E2AB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	{
		// Credential credential = GetCredential(accessToken, provider);
		String_t* L_0 = ___accessToken0;
		int32_t L_1 = ___provider1;
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_2 = WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (credential == null || !credential.IsValid())
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039(L_4, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
	}

IL_0019:
	{
		V_1 = (bool)G_B3_0;
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		// Debug.LogError("Invalid Credential");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral16BA435E490B03725CE5809E156F7C12C7ED4493, /*hidden argument*/NULL);
		// return;
		goto IL_004e;
	}

IL_002b:
	{
		// auth.CurrentUser.LinkWithCredentialAsync(credential).ContinueWith(task =>
		// {
		//     if (task.IsCanceled)
		//     {
		//         Debug.LogError("LinkWithCredentialAsync was canceled.");
		//         return;
		//     }
		//     if (task.IsFaulted)
		//     {
		//         Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
		//         return;
		//     }
		// 
		//     
		//     AuthStateChanged(this, null);
		// });
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_7 = __this->get_auth_7();
		NullCheck(L_7);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_8 = FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF(L_7, /*hidden argument*/NULL);
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_9 = V_0;
		NullCheck(L_8);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_10 = FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627(L_8, L_9, /*hidden argument*/NULL);
		Action_1_t5D2D131226D740682F598DD09436558292112781 * L_11 = (Action_1_t5D2D131226D740682F598DD09436558292112781 *)il2cpp_codegen_object_new(Action_1_t5D2D131226D740682F598DD09436558292112781_il2cpp_TypeInfo_var);
		Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1(L_11, __this, (intptr_t)((intptr_t)WebelinxFirebaseAuthManager_U3CLinkAccountWithAnotherProviderU3Eb__10_0_m531EBED0E89D1B164F16D95AC18B0F7198D57ED7_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1_RuntimeMethod_var);
		NullCheck(L_10);
		Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E(L_10, L_11, /*hidden argument*/Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E_RuntimeMethod_var);
	}

IL_004e:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::SignInAnonymously()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SignInAnonymously_m871FBA002D82959565F2E81BD8E28CB7E4D4D2F7 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_SignInAnonymously_m871FBA002D82959565F2E81BD8E28CB7E4D4D2F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// auth.SignInAnonymouslyAsync().ContinueWith(task => {
		//     if (task.IsCanceled)
		//     {
		//         Debug.LogError("SignInAnonymouslyAsync was canceled.");
		//         return;
		//     }
		//     if (task.IsFaulted)
		//     {
		//         Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
		//         return;
		//     }
		// 
		//     user = task.Result;
		// });
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = __this->get_auth_7();
		NullCheck(L_0);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_1 = FirebaseAuth_SignInAnonymouslyAsync_m6963AC820328CD24A672D8800D1B51BD98FF0995(L_0, /*hidden argument*/NULL);
		Action_1_t5D2D131226D740682F598DD09436558292112781 * L_2 = (Action_1_t5D2D131226D740682F598DD09436558292112781 *)il2cpp_codegen_object_new(Action_1_t5D2D131226D740682F598DD09436558292112781_il2cpp_TypeInfo_var);
		Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1(L_2, __this, (intptr_t)((intptr_t)WebelinxFirebaseAuthManager_U3CSignInAnonymouslyU3Eb__11_0_m42C7B73B3A78BCB3B5D7BF9C5599F6B8256D5B21_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1_RuntimeMethod_var);
		NullCheck(L_1);
		Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E(L_1, L_2, /*hidden argument*/Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::SignOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SignOut_mB3FA9243EEFD54BCC0B77A337DD18027954406A5 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_SignOut_mB3FA9243EEFD54BCC0B77A337DD18027954406A5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	{
		// if (auth.CurrentUser != null && user != null)
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = __this->get_auth_7();
		NullCheck(L_0);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_1 = FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_2 = __this->get_user_8();
		G_B3_0 = ((!(((RuntimeObject*)(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)L_2) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		V_0 = (bool)G_B3_0;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0058;
		}
	}
	{
		// if (PlayerPrefs.HasKey("ProfilePicture"))
		bool L_4 = PlayerPrefs_HasKey_mD87D3051ACE7EC6F5B54F4FC9E18572C917CA0D1(_stringLiteral72A9C4912E926376C2941C84EBE9D72FA67F2A9C, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		// PlayerPrefs.DeleteKey("ProfilePicture");
		PlayerPrefs_DeleteKey_mE0D76FF284F638715170DB52728B7595B41B6E8C(_stringLiteral72A9C4912E926376C2941C84EBE9D72FA67F2A9C, /*hidden argument*/NULL);
	}

IL_0038:
	{
		// user = null;
		__this->set_user_8((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL);
		// auth.SignOut();
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_6 = __this->get_auth_7();
		NullCheck(L_6);
		FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D(L_6, /*hidden argument*/NULL);
		// OnSignOut.Invoke();
		UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * L_7 = __this->get_OnSignOut_5();
		NullCheck(L_7);
		UnityAction_Invoke_mC9FF5AA1F82FDE635B3B6644CE71C94C31C3E71A(L_7, /*hidden argument*/NULL);
	}

IL_0058:
	{
		// }
		return;
	}
}
// Firebase.Auth.Credential Authentication.WebelinxFirebaseAuthManager::GetCredential(System.String,Authentication.Providers)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, String_t* ___accessToken0, int32_t ___provider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_GetCredential_m7E954E487C1659ED527331A7C4556E890C094BC3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_0 = NULL;
	int32_t V_1 = 0;
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_2 = NULL;
	{
		// Credential credential = null;
		V_0 = (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 *)NULL;
		// switch (provider)
		int32_t L_0 = ___provider1;
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0022;
	}

IL_0010:
	{
		// credential = FacebookAuthProvider.GetCredential(accessToken);
		String_t* L_3 = ___accessToken0;
		IL2CPP_RUNTIME_CLASS_INIT(FacebookAuthProvider_t05151B6B548EB3C91D029FABAB200D7EF11500B2_il2cpp_TypeInfo_var);
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_4 = FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// break;
		goto IL_0024;
	}

IL_0019:
	{
		// credential = PlayGamesAuthProvider.GetCredential(accessToken);
		String_t* L_5 = ___accessToken0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesAuthProvider_t68590058994858EAA3037ECD3E0CB2264C369324_il2cpp_TypeInfo_var);
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_6 = PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// break;
		goto IL_0024;
	}

IL_0022:
	{
		// break;
		goto IL_0024;
	}

IL_0024:
	{
		// return credential;
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_7 = V_0;
		V_2 = L_7;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_8 = V_2;
		return L_8;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::AuthStateChanged(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___eventArgs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	RuntimeObject * G_B7_0 = NULL;
	WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * G_B7_1 = NULL;
	RuntimeObject * G_B6_0 = NULL;
	WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * G_B6_1 = NULL;
	String_t* G_B8_0 = NULL;
	RuntimeObject * G_B8_1 = NULL;
	WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * G_B8_2 = NULL;
	{
		// if (auth.CurrentUser == null)
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = __this->get_auth_7();
		NullCheck(L_0);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_1 = FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((RuntimeObject*)(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		// user = null;
		__this->set_user_8((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL);
		// WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "You are not signed in");
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_3 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_3);
		WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A(L_3, (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL, _stringLiteral4E120F1F20428E6C068E9B1346D9C2E8269221F6, /*hidden argument*/NULL);
		// return;
		goto IL_00e4;
	}

IL_0031:
	{
		// if(isLinkingAccounts)
		bool L_4 = __this->get_isLinkingAccounts_6();
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0077;
		}
	}
	{
		// isLinkingAccounts = false;
		__this->set_isLinkingAccounts_6((bool)0);
		// WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Linking accounts done. Sign in again");
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_6 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_6);
		WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A(L_6, (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL, _stringLiteralCEB04F9890A96997A98081B3F484478976B1F1B1, /*hidden argument*/NULL);
		// user = null;
		__this->set_user_8((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL);
		// userFirebaseInfo = null;
		__this->set_userFirebaseInfo_9((SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 *)NULL);
		// userProviderInfo = null;
		__this->set_userProviderInfo_10((SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 *)NULL);
		// auth.SignOut();
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_7 = __this->get_auth_7();
		NullCheck(L_7);
		FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D(L_7, /*hidden argument*/NULL);
		// return;
		goto IL_00e4;
	}

IL_0077:
	{
		// if(auth.CurrentUser != null)
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_8 = __this->get_auth_7();
		NullCheck(L_8);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_9 = FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF(L_8, /*hidden argument*/NULL);
		V_2 = (bool)((!(((RuntimeObject*)(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)L_9) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_10 = V_2;
		if (!L_10)
		{
			goto IL_00e4;
		}
	}
	{
		// user = auth.CurrentUser;
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_11 = __this->get_auth_7();
		NullCheck(L_11);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_12 = FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF(L_11, /*hidden argument*/NULL);
		__this->set_user_8(L_12);
		// WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, user.IsAnonymous ? "You signed in anonymously!" : "You signed in through provider");
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_13 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_14 = __this->get_user_8();
		NullCheck(L_14);
		bool L_15 = FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902(L_14, /*hidden argument*/NULL);
		G_B6_0 = NULL;
		G_B6_1 = L_13;
		if (L_15)
		{
			G_B7_0 = NULL;
			G_B7_1 = L_13;
			goto IL_00b5;
		}
	}
	{
		G_B8_0 = _stringLiteral3F769C6CABA7165B6F0B273132D26B39ADF0B841;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_00ba;
	}

IL_00b5:
	{
		G_B8_0 = _stringLiteral2D67D11CCBBE0B198201732C9307DA04B494B79F;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_00ba:
	{
		NullCheck(G_B8_2);
		WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A(G_B8_2, (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)G_B8_1, G_B8_0, /*hidden argument*/NULL);
		// userFirebaseInfo = new SignedInUserFirebaseInfo();
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_16 = (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 *)il2cpp_codegen_object_new(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3_il2cpp_TypeInfo_var);
		SignedInUserFirebaseInfo__ctor_m3814EC958E68B7047E0E3157273D4F46C17934CC(L_16, /*hidden argument*/NULL);
		__this->set_userFirebaseInfo_9(L_16);
		// SetUserInformations(user);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_17 = __this->get_user_8();
		WebelinxFirebaseAuthManager_SetUserInformations_mEF76DEF1EEA9516F497E6F12D673AC56F68AC85B(__this, L_17, /*hidden argument*/NULL);
		// WebelinxFirebaseAuthView.Instance.PrintUserInformations();
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_18 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_18);
		WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C(L_18, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::SetUserInformations(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SetUserInformations_mEF76DEF1EEA9516F497E6F12D673AC56F68AC85B (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_SetUserInformations_mEF76DEF1EEA9516F497E6F12D673AC56F68AC85B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// userFirebaseInfo = new SignedInUserFirebaseInfo();
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_0 = (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 *)il2cpp_codegen_object_new(SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3_il2cpp_TypeInfo_var);
		SignedInUserFirebaseInfo__ctor_m3814EC958E68B7047E0E3157273D4F46C17934CC(L_0, /*hidden argument*/NULL);
		__this->set_userFirebaseInfo_9(L_0);
		// userProviderInfo = new SignedInUserProviderInfo();
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_1 = (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 *)il2cpp_codegen_object_new(SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1_il2cpp_TypeInfo_var);
		SignedInUserProviderInfo__ctor_mE5F7CDFB0E15F3FC967D59B05E4995D2269225E6(L_1, /*hidden argument*/NULL);
		__this->set_userProviderInfo_10(L_1);
		// userFirebaseInfo.SetData(user);
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_2 = __this->get_userFirebaseInfo_9();
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_3 = ___user0;
		NullCheck(L_2);
		SignedInUserFirebaseInfo_SetData_m3CF4935FE552BD2AA83C83465FE1973147C12FB9(L_2, L_3, /*hidden argument*/NULL);
		// if(!user.IsAnonymous)
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_4 = ___user0;
		NullCheck(L_4);
		bool L_5 = FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902(L_4, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		// userProviderInfo.SetData(user);
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_7 = __this->get_userProviderInfo_10();
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_8 = ___user0;
		NullCheck(L_7);
		SignedInUserProviderInfo_SetData_mD617F76287BBA2E8C70996A7B1100C8855D01F2D(L_7, L_8, /*hidden argument*/NULL);
		// SyncFirebaseUserDataWithProviderData();
		WebelinxFirebaseAuthManager_SyncFirebaseUserDataWithProviderData_m4F5FAF541ED3A1D4F9F72E0B2247C3B4EE92BA36(__this, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::SyncFirebaseUserDataWithProviderData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_SyncFirebaseUserDataWithProviderData_m4F5FAF541ED3A1D4F9F72E0B2247C3B4EE92BA36 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_SyncFirebaseUserDataWithProviderData_m4F5FAF541ED3A1D4F9F72E0B2247C3B4EE92BA36_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * G_B9_0 = NULL;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * G_B9_1 = NULL;
	Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * G_B8_0 = NULL;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * G_B8_1 = NULL;
	Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * G_B13_0 = NULL;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * G_B13_1 = NULL;
	Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * G_B12_0 = NULL;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * G_B12_1 = NULL;
	{
		// Firebase.Auth.UserProfile profile = new UserProfile();
		UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * L_0 = (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 *)il2cpp_codegen_object_new(UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31_il2cpp_TypeInfo_var);
		UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// bool isAnythingChanged = false;
		V_1 = (bool)0;
		// bool isEmailChanged = false;
		V_2 = (bool)0;
		// if (userFirebaseInfo.DisplayName != userProviderInfo.DisplayName)
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_1 = __this->get_userFirebaseInfo_9();
		NullCheck(L_1);
		String_t* L_2 = SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46_inline(L_1, /*hidden argument*/NULL);
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_3 = __this->get_userProviderInfo_10();
		NullCheck(L_3);
		String_t* L_4 = SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B_inline(L_3, /*hidden argument*/NULL);
		bool L_5 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_2, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		bool L_6 = V_3;
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		// userFirebaseInfo.DisplayName = userProviderInfo.DisplayName;
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_7 = __this->get_userFirebaseInfo_9();
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_8 = __this->get_userProviderInfo_10();
		NullCheck(L_8);
		String_t* L_9 = SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B_inline(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		SignedInUserFirebaseInfo_set_DisplayName_m4F58543D0F421804556204E391108297C3C51D50_inline(L_7, L_9, /*hidden argument*/NULL);
		// profile.DisplayName = userFirebaseInfo.DisplayName;
		UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * L_10 = V_0;
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_11 = __this->get_userFirebaseInfo_9();
		NullCheck(L_11);
		String_t* L_12 = SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46_inline(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557(L_10, L_12, /*hidden argument*/NULL);
		// isAnythingChanged = true;
		V_1 = (bool)1;
	}

IL_0057:
	{
		// if (userFirebaseInfo.PhotoUri != userProviderInfo.PhotoUri)
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_13 = __this->get_userFirebaseInfo_9();
		NullCheck(L_13);
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_14 = SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8_inline(L_13, /*hidden argument*/NULL);
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_15 = __this->get_userProviderInfo_10();
		NullCheck(L_15);
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_16 = SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355_inline(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_il2cpp_TypeInfo_var);
		bool L_17 = Uri_op_Inequality_m07015206F59460E87CDE2A8D303D5712E30A7F6B(L_14, L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		bool L_18 = V_4;
		if (!L_18)
		{
			goto IL_00a5;
		}
	}
	{
		// userFirebaseInfo.PhotoUri = userProviderInfo.PhotoUri;
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_19 = __this->get_userFirebaseInfo_9();
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_20 = __this->get_userProviderInfo_10();
		NullCheck(L_20);
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_21 = SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355_inline(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		SignedInUserFirebaseInfo_set_PhotoUri_m2A865C067ACE928BE0C30A959AF3EB5727157E6A_inline(L_19, L_21, /*hidden argument*/NULL);
		// profile.PhotoUrl = userFirebaseInfo.PhotoUri;
		UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * L_22 = V_0;
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_23 = __this->get_userFirebaseInfo_9();
		NullCheck(L_23);
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_24 = SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8_inline(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		UserProfile_set_PhotoUrl_mCDA8A14D63367925AD4D59808B472B88086D977A(L_22, L_24, /*hidden argument*/NULL);
		// isAnythingChanged = true;
		V_1 = (bool)1;
	}

IL_00a5:
	{
		// if (userFirebaseInfo.Email != userProviderInfo.Email)
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_25 = __this->get_userFirebaseInfo_9();
		NullCheck(L_25);
		String_t* L_26 = SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF_inline(L_25, /*hidden argument*/NULL);
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_27 = __this->get_userProviderInfo_10();
		NullCheck(L_27);
		String_t* L_28 = SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D_inline(L_27, /*hidden argument*/NULL);
		bool L_29 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_26, L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		bool L_30 = V_5;
		if (!L_30)
		{
			goto IL_00e1;
		}
	}
	{
		// userFirebaseInfo.Email = userProviderInfo.Email;
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_31 = __this->get_userFirebaseInfo_9();
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_32 = __this->get_userProviderInfo_10();
		NullCheck(L_32);
		String_t* L_33 = SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D_inline(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		SignedInUserFirebaseInfo_set_Email_m69A025C44E648B762D68463C85FA74144897CE93_inline(L_31, L_33, /*hidden argument*/NULL);
		// isEmailChanged = true;
		V_2 = (bool)1;
	}

IL_00e1:
	{
		// if(isAnythingChanged)
		bool L_34 = V_1;
		V_6 = L_34;
		bool L_35 = V_6;
		if (!L_35)
		{
			goto IL_011b;
		}
	}
	{
		// user.UpdateUserProfileAsync(profile).ContinueWith(task =>
		// {
		//     if (task.IsCanceled)
		//     {
		//         Debug.LogError("Profile update canceled");
		//         return;
		//     }
		//     if (task.IsFaulted)
		//     {
		//         Debug.LogError("Error update-ing profile : "+task.Exception);
		//         return;
		//     }
		// 
		//     
		//     WebelinxFirebaseAuthView.Instance.PrintUserInformations();
		// });
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_36 = __this->get_user_8();
		UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * L_37 = V_0;
		NullCheck(L_36);
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_38 = FirebaseUser_UpdateUserProfileAsync_mACFB318A8FA0A47B70F317197B0C5884D5772D9D(L_36, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var);
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_39 = ((U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var))->get_U3CU3E9__16_0_1();
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_40 = L_39;
		G_B8_0 = L_40;
		G_B8_1 = L_38;
		if (L_40)
		{
			G_B9_0 = L_40;
			G_B9_1 = L_38;
			goto IL_0114;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var);
		U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * L_41 = ((U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_42 = (Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 *)il2cpp_codegen_object_new(Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827_il2cpp_TypeInfo_var);
		Action_1__ctor_m068FBAE923EF2CD287CD903FDE55A5AF890E8623(L_42, L_41, (intptr_t)((intptr_t)U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_0_m361BD96B14EE16FCC87B6958136DB824453515A5_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m068FBAE923EF2CD287CD903FDE55A5AF890E8623_RuntimeMethod_var);
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_43 = L_42;
		((U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var))->set_U3CU3E9__16_0_1(L_43);
		G_B9_0 = L_43;
		G_B9_1 = G_B8_1;
	}

IL_0114:
	{
		NullCheck(G_B9_1);
		Task_ContinueWith_mC71D3CE9DF20879884F2D08D363BA6E47EF87638(G_B9_1, G_B9_0, /*hidden argument*/NULL);
	}

IL_011b:
	{
		// if(isEmailChanged)
		bool L_44 = V_2;
		V_7 = L_44;
		bool L_45 = V_7;
		if (!L_45)
		{
			goto IL_015f;
		}
	}
	{
		// user.UpdateEmailAsync(userFirebaseInfo.Email).ContinueWith(task =>
		// {
		//     if (task.IsCanceled)
		//     {
		//         Debug.LogError("UpdateEmailAsync was canceled.");
		//         return;
		//     }
		//     if (task.IsFaulted)
		//     {
		//         Debug.LogError("UpdateEmailAsync encountered an error: " + task.Exception);
		//         return;
		//     }
		// 
		//     
		//     WebelinxFirebaseAuthView.Instance.PrintUserInformations();
		// });
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_46 = __this->get_user_8();
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_47 = __this->get_userFirebaseInfo_9();
		NullCheck(L_47);
		String_t* L_48 = SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF_inline(L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_49 = FirebaseUser_UpdateEmailAsync_m8D0B4CA7BE4605CF684B74CEE6C5A2ADA2E1F5A7(L_46, L_48, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var);
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_50 = ((U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var))->get_U3CU3E9__16_1_2();
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_51 = L_50;
		G_B12_0 = L_51;
		G_B12_1 = L_49;
		if (L_51)
		{
			G_B13_0 = L_51;
			G_B13_1 = L_49;
			goto IL_0158;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var);
		U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * L_52 = ((U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_53 = (Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 *)il2cpp_codegen_object_new(Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827_il2cpp_TypeInfo_var);
		Action_1__ctor_m068FBAE923EF2CD287CD903FDE55A5AF890E8623(L_53, L_52, (intptr_t)((intptr_t)U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_1_m0F1243EEA002F490F1766EF845A67A40C7E060F7_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m068FBAE923EF2CD287CD903FDE55A5AF890E8623_RuntimeMethod_var);
		Action_1_t965212EDC10FB8052CC3E9BF3FBDF913BEFD4827 * L_54 = L_53;
		((U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var))->set_U3CU3E9__16_1_2(L_54);
		G_B13_0 = L_54;
		G_B13_1 = G_B12_1;
	}

IL_0158:
	{
		NullCheck(G_B13_1);
		Task_ContinueWith_mC71D3CE9DF20879884F2D08D363BA6E47EF87638(G_B13_1, G_B13_0, /*hidden argument*/NULL);
	}

IL_015f:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_OnDestroy_mBC21EF9D772490890B413F03E5224062C68B12AD (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_OnDestroy_mBC21EF9D772490890B413F03E5224062C68B12AD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// auth.StateChanged -= AuthStateChanged;
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = __this->get_auth_7();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_1, __this, (intptr_t)((intptr_t)WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		FirebaseAuth_remove_StateChanged_mF960C837B4BD78DAF5716BC2348EF9702AD2B736(L_0, L_1, /*hidden argument*/NULL);
		// auth = null;
		__this->set_auth_7((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *)NULL);
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager__ctor_m527265DC58D5B4B7EF12D3DB821C4CB5CFAF8549 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::<SignInThroughProvider>b__9_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_U3CSignInThroughProviderU3Eb__9_0_m30F8227F6F0FEE4CECB0C0B592AA3E9203669A0C (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___task0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_U3CSignInThroughProviderU3Eb__9_0_m30F8227F6F0FEE4CECB0C0B592AA3E9203669A0C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (task.IsCanceled)
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_0 = ___task0;
		NullCheck(L_0);
		bool L_1 = Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		// WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Login canceled");
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_3 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_3);
		WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A(L_3, (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL, _stringLiteral564F937562204C6FF6E136F54872B28939A61D64, /*hidden argument*/NULL);
		// Debug.LogError("You canceled your Login.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral316CC475D64EC17BB3A8AF209DF6E312A67D13A5, /*hidden argument*/NULL);
		// return;
		goto IL_006a;
	}

IL_002a:
	{
		// if (task.IsFaulted)
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_4 = ___task0;
		NullCheck(L_4);
		bool L_5 = Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		// WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Login error");
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_7 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_7);
		WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A(L_7, (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL, _stringLiteralAB05430164D31C596A61F882DA43E4452DDAABEC, /*hidden argument*/NULL);
		// Debug.LogError("Login error " + task.Exception);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_8 = ___task0;
		NullCheck(L_8);
		AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * L_9 = Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95(L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralDCE627299BC3B788CDE5457E1ED6E88FC06DA055, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_10, /*hidden argument*/NULL);
		// return;
		goto IL_006a;
	}

IL_005e:
	{
		// user = task.Result;
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_11 = ___task0;
		NullCheck(L_11);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_12 = Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D(L_11, /*hidden argument*/Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D_RuntimeMethod_var);
		__this->set_user_8(L_12);
	}

IL_006a:
	{
		// });
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::<LinkAccountWithAnotherProvider>b__10_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_U3CLinkAccountWithAnotherProviderU3Eb__10_0_m531EBED0E89D1B164F16D95AC18B0F7198D57ED7 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___task0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_U3CLinkAccountWithAnotherProviderU3Eb__10_0_m531EBED0E89D1B164F16D95AC18B0F7198D57ED7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (task.IsCanceled)
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_0 = ___task0;
		NullCheck(L_0);
		bool L_1 = Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// Debug.LogError("LinkWithCredentialAsync was canceled.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral4A32DE264ABAE3B7609ED468471FB34F2331402B, /*hidden argument*/NULL);
		// return;
		goto IL_0045;
	}

IL_0019:
	{
		// if (task.IsFaulted)
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_3 = ___task0;
		NullCheck(L_3);
		bool L_4 = Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		// Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_6 = ___task0;
		NullCheck(L_6);
		AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * L_7 = Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95(L_6, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralFA7C7E6B20F20C131CD40822EBEEFDFDA03EA52F, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_8, /*hidden argument*/NULL);
		// return;
		goto IL_0045;
	}

IL_003c:
	{
		// AuthStateChanged(this, null);
		WebelinxFirebaseAuthManager_AuthStateChanged_m6806B30F8792A9A1EC874FC770A5821B51CD1C4A(__this, __this, (EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E *)NULL, /*hidden argument*/NULL);
	}

IL_0045:
	{
		// });
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager::<SignInAnonymously>b__11_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthManager_U3CSignInAnonymouslyU3Eb__11_0_m42C7B73B3A78BCB3B5D7BF9C5599F6B8256D5B21 (WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * __this, Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___task0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthManager_U3CSignInAnonymouslyU3Eb__11_0_m42C7B73B3A78BCB3B5D7BF9C5599F6B8256D5B21_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (task.IsCanceled)
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_0 = ___task0;
		NullCheck(L_0);
		bool L_1 = Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// Debug.LogError("SignInAnonymouslyAsync was canceled.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral448D433E9E85775BD6DF632B67AB13A453FF43B1, /*hidden argument*/NULL);
		// return;
		goto IL_0048;
	}

IL_0019:
	{
		// if (task.IsFaulted)
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_3 = ___task0;
		NullCheck(L_3);
		bool L_4 = Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		// Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_6 = ___task0;
		NullCheck(L_6);
		AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * L_7 = Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95(L_6, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral63F467851E6EECFC10C66EB94087ADEA75EA3645, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_8, /*hidden argument*/NULL);
		// return;
		goto IL_0048;
	}

IL_003c:
	{
		// user = task.Result;
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_9 = ___task0;
		NullCheck(L_9);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_10 = Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D(L_9, /*hidden argument*/Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D_RuntimeMethod_var);
		__this->set_user_8(L_10);
	}

IL_0048:
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Authentication.WebelinxFirebaseAuthManager_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mEB73EA82F54AD404E29A52A9FED6395C77CF9BAF (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mEB73EA82F54AD404E29A52A9FED6395C77CF9BAF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * L_0 = (U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE *)il2cpp_codegen_object_new(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mC540C513C869C7F6E8137A2F2BF6AD336599DDD1(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC540C513C869C7F6E8137A2F2BF6AD336599DDD1 (U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager_<>c::<SyncFirebaseUserDataWithProviderData>b__16_0(System.Threading.Tasks.Task)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_0_m361BD96B14EE16FCC87B6958136DB824453515A5 (U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * __this, Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___task0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_0_m361BD96B14EE16FCC87B6958136DB824453515A5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (task.IsCanceled)
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_0 = ___task0;
		NullCheck(L_0);
		bool L_1 = Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// Debug.LogError("Profile update canceled");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteralD24DA40C8EF2074010E98D0B9F4624FB353DC060, /*hidden argument*/NULL);
		// return;
		goto IL_0047;
	}

IL_0019:
	{
		// if (task.IsFaulted)
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_3 = ___task0;
		NullCheck(L_3);
		bool L_4 = Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		// Debug.LogError("Error update-ing profile : "+task.Exception);
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_6 = ___task0;
		NullCheck(L_6);
		AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * L_7 = Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95(L_6, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralA06BF1793036ECE1883508A055CDB1132BDF2164, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_8, /*hidden argument*/NULL);
		// return;
		goto IL_0047;
	}

IL_003c:
	{
		// WebelinxFirebaseAuthView.Instance.PrintUserInformations();
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_9 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_9);
		WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C(L_9, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// });
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthManager_<>c::<SyncFirebaseUserDataWithProviderData>b__16_1(System.Threading.Tasks.Task)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_1_m0F1243EEA002F490F1766EF845A67A40C7E060F7 (U3CU3Ec_tC4A1EDCB60589E5AE85B3BA55863418B6DFE7BDE * __this, Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___task0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CSyncFirebaseUserDataWithProviderDataU3Eb__16_1_m0F1243EEA002F490F1766EF845A67A40C7E060F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (task.IsCanceled)
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_0 = ___task0;
		NullCheck(L_0);
		bool L_1 = Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// Debug.LogError("UpdateEmailAsync was canceled.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral8E135F653FD909BB5456F6E468ABB5D09C7579D0, /*hidden argument*/NULL);
		// return;
		goto IL_0047;
	}

IL_0019:
	{
		// if (task.IsFaulted)
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_3 = ___task0;
		NullCheck(L_3);
		bool L_4 = Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		// Debug.LogError("UpdateEmailAsync encountered an error: " + task.Exception);
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_6 = ___task0;
		NullCheck(L_6);
		AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * L_7 = Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95(L_6, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral2660CB30675D112248C692B6C4EDB12FCBBE23DF, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_8, /*hidden argument*/NULL);
		// return;
		goto IL_0047;
	}

IL_003c:
	{
		// WebelinxFirebaseAuthView.Instance.PrintUserInformations();
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_9 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_9);
		WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C(L_9, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Authentication.WebelinxFirebaseAuthView::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_Awake_m4D3CA124DE180FF292DAA935E31F5CFD616F5A95 (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthView_Awake_m4D3CA124DE180FF292DAA935E31F5CFD616F5A95_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (Instance == null)
		WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * L_0 = ((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		// Instance = this;
		((WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A_il2cpp_TypeInfo_var))->set_Instance_4(__this);
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthView::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_Start_m82368ADFCEF22D8B4E7779B2C09A2C10BC48E4D4 (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthView_Start_m82368ADFCEF22D8B4E7779B2C09A2C10BC48E4D4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// WebelinxFirebaseAuthManager.Instance.OnSignOut += OnUserSignedOut;
		WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * L_0 = ((WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_il2cpp_TypeInfo_var))->get_Instance_4();
		WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * L_1 = L_0;
		NullCheck(L_1);
		UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * L_2 = L_1->get_OnSignOut_5();
		UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * L_3 = (UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 *)il2cpp_codegen_object_new(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4_il2cpp_TypeInfo_var);
		UnityAction__ctor_mEFC4B92529CE83DF72501F92E07EC5598C54BDAC(L_3, __this, (intptr_t)((intptr_t)WebelinxFirebaseAuthView_OnUserSignedOut_m496B5B915654C58C956EEAF9D5271A62620EE580_RuntimeMethod_var), /*hidden argument*/NULL);
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_OnSignOut_5(((UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 *)CastclassSealed((RuntimeObject*)L_4, UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4_il2cpp_TypeInfo_var)));
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthView::UpdateUserStatusText(Firebase.Auth.FirebaseUser,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___user0, String_t* ___specialMessage1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthView_UpdateUserStatusText_m5A6D6DBCAD957CB179FBEFCFCDA0DF9E013A007A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (specialMessage != "")
		String_t* L_0 = ___specialMessage1;
		bool L_1 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_0, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		// statusText.enabled = false;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_3 = __this->get_statusText_6();
		NullCheck(L_3);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_3, (bool)0, /*hidden argument*/NULL);
		// statusText.text = specialMessage;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_4 = __this->get_statusText_6();
		String_t* L_5 = ___specialMessage1;
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		// statusText.color = Color.red;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_6 = __this->get_statusText_6();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_7 = Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957(/*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		// statusText.enabled = true;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_8 = __this->get_statusText_6();
		NullCheck(L_8);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_8, (bool)1, /*hidden argument*/NULL);
		// return;
		goto IL_004b;
	}

IL_004b:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthView::PrintUserInformations()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthView_PrintUserInformations_m29560FAADDF0AEA490ADDC72FA1329F2194AD06C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// firebaseUserInfo.text = WebelinxFirebaseAuthManager.Instance.userFirebaseInfo.ToString();
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_0 = __this->get_firebaseUserInfo_8();
		WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * L_1 = ((WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_1);
		SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * L_2 = L_1->get_userFirebaseInfo_9();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_3);
		// providerUserInfo.text = WebelinxFirebaseAuthManager.Instance.userProviderInfo.ToString();
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_4 = __this->get_providerUserInfo_9();
		WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52 * L_5 = ((WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_StaticFields*)il2cpp_codegen_static_fields_for(WebelinxFirebaseAuthManager_tC0E690D4089E6FB4074CA967BFA28D94865F2E52_il2cpp_TypeInfo_var))->get_Instance_4();
		NullCheck(L_5);
		SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * L_6 = L_5->get_userProviderInfo_10();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_7);
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthView::DisplayUserProfilePicture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_DisplayUserProfilePicture_mCFB8F1344ACD38DB02A6C872D137A27AC2C5618E (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthView_DisplayUserProfilePicture_mCFB8F1344ACD38DB02A6C872D137A27AC2C5618E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * V_2 = NULL;
	bool V_3 = false;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// if (!PlayerPrefs.HasKey("ProfilePicture")) return;
		bool L_0 = PlayerPrefs_HasKey_mD87D3051ACE7EC6F5B54F4FC9E18572C917CA0D1(_stringLiteral72A9C4912E926376C2941C84EBE9D72FA67F2A9C, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_3;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// if (!PlayerPrefs.HasKey("ProfilePicture")) return;
		goto IL_0077;
	}

IL_0014:
	{
		// string texString = PlayerPrefs.GetString("ProfilePicture");
		String_t* L_2 = PlayerPrefs_GetString_m3031AD2D5DEAB97677A9EF629618541437F079F1(_stringLiteral72A9C4912E926376C2941C84EBE9D72FA67F2A9C, /*hidden argument*/NULL);
		V_0 = L_2;
		// byte[] texBytes = Convert.FromBase64String(texString);
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = Convert_FromBase64String_m079F788D000703E8018DA39BE9C05F1CBF60B156(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// Texture2D tex = new Texture2D(128, 128);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_5 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m0C86A87871AA8075791EF98499D34DA95ACB0E35(L_5, ((int32_t)128), ((int32_t)128), /*hidden argument*/NULL);
		V_2 = L_5;
		// tex.LoadImage(texBytes);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_6 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_1;
		ImageConversion_LoadImage_m94295492E96C38984406A23CC2A3931758ECE86B(L_6, L_7, /*hidden argument*/NULL);
		// userImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_8 = __this->get_userImage_5();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_9 = V_2;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_10 = V_2;
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_10);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_12);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_14), (0.0f), (0.0f), (((float)((float)L_11))), (((float)((float)L_13))), /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_4), sizeof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_15 = V_4;
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_16 = Sprite_Create_m9ED36DA8DA0637F93BA2753A16405EB0F7B17A3C(L_9, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_8);
		Image_set_sprite_m77F8D681D4EE6D58F4F235AFF704C3EB165A9646(L_8, L_16, /*hidden argument*/NULL);
	}

IL_0077:
	{
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthView::OnUserSignedOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView_OnUserSignedOut_m496B5B915654C58C956EEAF9D5271A62620EE580 (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxFirebaseAuthView_OnUserSignedOut_m496B5B915654C58C956EEAF9D5271A62620EE580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// firebaseUserInfo.text = "";
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_0 = __this->get_firebaseUserInfo_8();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// providerUserInfo.text = "";
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_1 = __this->get_providerUserInfo_9();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// statusText.text = "";
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_2 = __this->get_statusText_6();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// }
		return;
	}
}
// System.Void Authentication.WebelinxFirebaseAuthView::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxFirebaseAuthView__ctor_mB431C330881E3F1F0EAB7F346E3518DB21BDA144 (WebelinxFirebaseAuthView_t319D1F1E304D8E62159DDA27E59E9CAA22F2BE8A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameMenuHandler::SetVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C (GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * __this, bool ___visible0, const RuntimeMethod* method)
{
	{
		// this.Parent.SetActive(visible);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_Parent_0();
		bool L_1 = ___visible0;
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameMenuHandler::SetupAppleData(System.String,AppleAuth.Interfaces.ICredential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuHandler_SetupAppleData_m5878285CA5056522A0FD6BDC58739DCEC59E6BC2 (GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * __this, String_t* ___appleUserId0, RuntimeObject* ___receivedCredential1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameMenuHandler_SetupAppleData_m5878285CA5056522A0FD6BDC58739DCEC59E6BC2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	StringBuilder_t * V_4 = NULL;
	int32_t V_5 = 0;
	bool V_6 = false;
	bool V_7 = false;
	String_t* V_8 = NULL;
	bool V_9 = false;
	String_t* V_10 = NULL;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	RuntimeObject* V_14 = NULL;
	bool V_15 = false;
	RuntimeObject* V_16 = NULL;
	bool V_17 = false;
	StringBuilder_t * V_18 = NULL;
	{
		// this.AppleUserIdLabel.text = "Apple User ID: " + appleUserId;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_0 = __this->get_AppleUserIdLabel_1();
		String_t* L_1 = ___appleUserId0;
		String_t* L_2 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral9DF2980BA224D990316F675063181489A7731F76, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		// if (receivedCredential == null)
		RuntimeObject* L_3 = ___receivedCredential1;
		V_2 = (bool)((((RuntimeObject*)(RuntimeObject*)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_4 = V_2;
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		// this.AppleUserCredentialLabel.text = "NO CREDENTIALS RECEIVED\nProbably credential status for " + appleUserId + "was Authorized";
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_5 = __this->get_AppleUserCredentialLabel_2();
		String_t* L_6 = ___appleUserId0;
		String_t* L_7 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral5FE917EE4ED152B3D78CFF96AE9517097B3B1693, L_6, _stringLiteralD1AF1318D2CA2E5BA5E5F111775A8F1645093512, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_7);
		// return;
		goto IL_0438;
	}

IL_0042:
	{
		// var appleIdCredential = receivedCredential as IAppleIDCredential;
		RuntimeObject* L_8 = ___receivedCredential1;
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_8, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var));
		// var passwordCredential = receivedCredential as IPasswordCredential;
		RuntimeObject* L_9 = ___receivedCredential1;
		V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_9, IPasswordCredential_tC5ACB72704F73E0A1229C3B9B8792031098F1D2E_il2cpp_TypeInfo_var));
		// if (appleIdCredential != null)
		RuntimeObject* L_10 = V_0;
		V_3 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_10) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_11 = V_3;
		if (!L_11)
		{
			goto IL_03b5;
		}
	}
	{
		// var stringBuilder = new StringBuilder();
		StringBuilder_t * L_12 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_12, /*hidden argument*/NULL);
		V_4 = L_12;
		// stringBuilder.AppendLine("RECEIVED APPLE ID CREDENTIAL.\nYOU CAN LOGIN/CREATE A USER WITH THIS");
		StringBuilder_t * L_13 = V_4;
		NullCheck(L_13);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_13, _stringLiteralE6DA9EE605CA4B2469C8D51E07E3BA0C0CD094C3, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Username:</b> " + appleIdCredential.User);
		StringBuilder_t * L_14 = V_4;
		RuntimeObject* L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_15);
		String_t* L_17 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral102DA49B7841C2BD193FAB2C18BD222433515F4B, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_14, L_17, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Real user status:</b> " + appleIdCredential.RealUserStatus.ToString());
		StringBuilder_t * L_18 = V_4;
		RuntimeObject* L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = InterfaceFuncInvoker0< int32_t >::Invoke(6 /* AppleAuth.Enums.RealUserStatus AppleAuth.Interfaces.IAppleIDCredential::get_RealUserStatus() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_19);
		V_5 = L_20;
		RuntimeObject * L_21 = Box(RealUserStatus_t3439EB7A9F2D88C6F5FF8D3F571A4B8C92EF596D_il2cpp_TypeInfo_var, (&V_5));
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		V_5 = *(int32_t*)UnBox(L_21);
		String_t* L_23 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralF04BD4D89F152A5646CCEB0E33CECEC5FE132963, L_22, /*hidden argument*/NULL);
		NullCheck(L_18);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_18, L_23, /*hidden argument*/NULL);
		// if (appleIdCredential.State != null)
		RuntimeObject* L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String AppleAuth.Interfaces.IAppleIDCredential::get_State() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_24);
		V_6 = (bool)((!(((RuntimeObject*)(String_t*)L_25) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_26 = V_6;
		if (!L_26)
		{
			goto IL_00d6;
		}
	}
	{
		// stringBuilder.AppendLine("<b>State:</b> " + appleIdCredential.State);
		StringBuilder_t * L_27 = V_4;
		RuntimeObject* L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String AppleAuth.Interfaces.IAppleIDCredential::get_State() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_28);
		String_t* L_30 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral68DF780818C857A0F6F7C26AB235DB1CBA45960E, L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_27, L_30, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		// if (appleIdCredential.IdentityToken != null)
		RuntimeObject* L_31 = V_0;
		NullCheck(L_31);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_32 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(0 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_IdentityToken() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_31);
		V_7 = (bool)((!(((RuntimeObject*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)L_32) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_33 = V_7;
		if (!L_33)
		{
			goto IL_0142;
		}
	}
	{
		// var identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken, 0, appleIdCredential.IdentityToken.Length);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_34 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		RuntimeObject* L_35 = V_0;
		NullCheck(L_35);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_36 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(0 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_IdentityToken() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_35);
		RuntimeObject* L_37 = V_0;
		NullCheck(L_37);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_38 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(0 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_IdentityToken() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_37);
		NullCheck(L_38);
		NullCheck(L_34);
		String_t* L_39 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(31 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_34, L_36, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_38)->max_length)))));
		V_8 = L_39;
		// stringBuilder.AppendLine("<b>Identity token (" + appleIdCredential.IdentityToken.Length + " bytes)</b>");
		StringBuilder_t * L_40 = V_4;
		RuntimeObject* L_41 = V_0;
		NullCheck(L_41);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_42 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(0 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_IdentityToken() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_41);
		NullCheck(L_42);
		int32_t L_43 = (((int32_t)((int32_t)(((RuntimeArray*)L_42)->max_length))));
		RuntimeObject * L_44 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_43);
		String_t* L_45 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteralF3B0336F4480D618DC414AF075986C7F133F0BF8, L_44, _stringLiteral0BA55F1443103280F88491FDEF25033AA09A9BDE, /*hidden argument*/NULL);
		NullCheck(L_40);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_40, L_45, /*hidden argument*/NULL);
		// stringBuilder.AppendLine(identityToken.Substring(0, 45) + "...");
		StringBuilder_t * L_46 = V_4;
		String_t* L_47 = V_8;
		NullCheck(L_47);
		String_t* L_48 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_47, 0, ((int32_t)45), /*hidden argument*/NULL);
		String_t* L_49 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_48, _stringLiteral6EAE3A5B062C6D0D79F070C26E6D62486B40CB46, /*hidden argument*/NULL);
		NullCheck(L_46);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_46, L_49, /*hidden argument*/NULL);
	}

IL_0142:
	{
		// if (appleIdCredential.AuthorizationCode != null)
		RuntimeObject* L_50 = V_0;
		NullCheck(L_50);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_51 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(1 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizationCode() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_50);
		V_9 = (bool)((!(((RuntimeObject*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)L_51) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_52 = V_9;
		if (!L_52)
		{
			goto IL_01ae;
		}
	}
	{
		// var authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode, 0, appleIdCredential.AuthorizationCode.Length);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_53 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		RuntimeObject* L_54 = V_0;
		NullCheck(L_54);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_55 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(1 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizationCode() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_54);
		RuntimeObject* L_56 = V_0;
		NullCheck(L_56);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_57 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(1 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizationCode() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_56);
		NullCheck(L_57);
		NullCheck(L_53);
		String_t* L_58 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(31 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_53, L_55, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_57)->max_length)))));
		V_10 = L_58;
		// stringBuilder.AppendLine("<b>Authorization Code (" + appleIdCredential.AuthorizationCode.Length + " bytes)</b>");
		StringBuilder_t * L_59 = V_4;
		RuntimeObject* L_60 = V_0;
		NullCheck(L_60);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_61 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(1 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizationCode() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_60);
		NullCheck(L_61);
		int32_t L_62 = (((int32_t)((int32_t)(((RuntimeArray*)L_61)->max_length))));
		RuntimeObject * L_63 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_62);
		String_t* L_64 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteralEC5AC77052DAC34F0AD8EFF9491B42E3510F12C2, L_63, _stringLiteral0BA55F1443103280F88491FDEF25033AA09A9BDE, /*hidden argument*/NULL);
		NullCheck(L_59);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_59, L_64, /*hidden argument*/NULL);
		// stringBuilder.AppendLine(authorizationCode.Substring(0, 45) + "...");
		StringBuilder_t * L_65 = V_4;
		String_t* L_66 = V_10;
		NullCheck(L_66);
		String_t* L_67 = String_Substring_mB593C0A320C683E6E47EFFC0A12B7A465E5E43BB(L_66, 0, ((int32_t)45), /*hidden argument*/NULL);
		String_t* L_68 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_67, _stringLiteral6EAE3A5B062C6D0D79F070C26E6D62486B40CB46, /*hidden argument*/NULL);
		NullCheck(L_65);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_65, L_68, /*hidden argument*/NULL);
	}

IL_01ae:
	{
		// if (appleIdCredential.AuthorizedScopes != null)
		RuntimeObject* L_69 = V_0;
		NullCheck(L_69);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_70 = InterfaceFuncInvoker0< StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(3 /* System.String[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizedScopes() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_69);
		V_11 = (bool)((!(((RuntimeObject*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)L_70) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_71 = V_11;
		if (!L_71)
		{
			goto IL_01df;
		}
	}
	{
		// stringBuilder.AppendLine("<b>Authorized Scopes:</b> " + string.Join(", ", appleIdCredential.AuthorizedScopes));
		StringBuilder_t * L_72 = V_4;
		RuntimeObject* L_73 = V_0;
		NullCheck(L_73);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_74 = InterfaceFuncInvoker0< StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* >::Invoke(3 /* System.String[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizedScopes() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_73);
		String_t* L_75 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(_stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46, L_74, /*hidden argument*/NULL);
		String_t* L_76 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralDA9F24755DE561B483F9D678415574B58F334213, L_75, /*hidden argument*/NULL);
		NullCheck(L_72);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_72, L_76, /*hidden argument*/NULL);
	}

IL_01df:
	{
		// if (appleIdCredential.Email != null)
		RuntimeObject* L_77 = V_0;
		NullCheck(L_77);
		String_t* L_78 = InterfaceFuncInvoker0< String_t* >::Invoke(5 /* System.String AppleAuth.Interfaces.IAppleIDCredential::get_Email() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_77);
		V_12 = (bool)((!(((RuntimeObject*)(String_t*)L_78) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_79 = V_12;
		if (!L_79)
		{
			goto IL_022a;
		}
	}
	{
		// stringBuilder.AppendLine();
		StringBuilder_t * L_80 = V_4;
		NullCheck(L_80);
		StringBuilder_AppendLine_mB5B3F68726B05CD404C8C8D8F5A3D2A58FF16BB9(L_80, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>EMAIL RECEIVED: YOU WILL ONLY SEE THIS ONCE PER SIGN UP. SEND THIS INFORMATION TO YOUR BACKEND!</b>");
		StringBuilder_t * L_81 = V_4;
		NullCheck(L_81);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_81, _stringLiteralE2C84573C370B2CF156317B86B0B80D053A092D3, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>You can test this again by revoking credentials in Settings</b>");
		StringBuilder_t * L_82 = V_4;
		NullCheck(L_82);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_82, _stringLiteral4411580845F5E7331D1E9CFE4B38E5A0461D487D, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Email:</b> " + appleIdCredential.Email);
		StringBuilder_t * L_83 = V_4;
		RuntimeObject* L_84 = V_0;
		NullCheck(L_84);
		String_t* L_85 = InterfaceFuncInvoker0< String_t* >::Invoke(5 /* System.String AppleAuth.Interfaces.IAppleIDCredential::get_Email() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_84);
		String_t* L_86 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral48CD9ADAAB6410711E011ABA2FC738DC0F0708AC, L_85, /*hidden argument*/NULL);
		NullCheck(L_83);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_83, L_86, /*hidden argument*/NULL);
	}

IL_022a:
	{
		// if (appleIdCredential.FullName != null)
		RuntimeObject* L_87 = V_0;
		NullCheck(L_87);
		RuntimeObject* L_88 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(4 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IAppleIDCredential::get_FullName() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_87);
		V_13 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_88) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_89 = V_13;
		if (!L_89)
		{
			goto IL_039c;
		}
	}
	{
		// var fullName = appleIdCredential.FullName;
		RuntimeObject* L_90 = V_0;
		NullCheck(L_90);
		RuntimeObject* L_91 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(4 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IAppleIDCredential::get_FullName() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_90);
		V_14 = L_91;
		// stringBuilder.AppendLine();
		StringBuilder_t * L_92 = V_4;
		NullCheck(L_92);
		StringBuilder_AppendLine_mB5B3F68726B05CD404C8C8D8F5A3D2A58FF16BB9(L_92, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>NAME RECEIVED: YOU WILL ONLY SEE THIS ONCE PER SIGN UP. SEND THIS INFORMATION TO YOUR BACKEND!</b>");
		StringBuilder_t * L_93 = V_4;
		NullCheck(L_93);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_93, _stringLiteral9F7FEF0CB5306BE5E44617752BF0F2B4C36BDDFE, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>You can test this again by revoking credentials in Settings</b>");
		StringBuilder_t * L_94 = V_4;
		NullCheck(L_94);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_94, _stringLiteral4411580845F5E7331D1E9CFE4B38E5A0461D487D, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Name:</b> " + fullName.ToLocalizedString());
		StringBuilder_t * L_95 = V_4;
		RuntimeObject* L_96 = V_14;
		String_t* L_97 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_96, 0, (bool)0, /*hidden argument*/NULL);
		String_t* L_98 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralE338843047716F5CA7F106D3A7B0C89672BD92FF, L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_95, L_98, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Name (Short):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Short));
		StringBuilder_t * L_99 = V_4;
		RuntimeObject* L_100 = V_14;
		String_t* L_101 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_100, 1, (bool)0, /*hidden argument*/NULL);
		String_t* L_102 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral34A3A1806BCBD2EC0D7FABD0FF470B637146B453, L_101, /*hidden argument*/NULL);
		NullCheck(L_99);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_99, L_102, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Name (Medium):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Medium));
		StringBuilder_t * L_103 = V_4;
		RuntimeObject* L_104 = V_14;
		String_t* L_105 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_104, 2, (bool)0, /*hidden argument*/NULL);
		String_t* L_106 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral29D93BE9A1B69EE006EA76D23E4115200F42F51B, L_105, /*hidden argument*/NULL);
		NullCheck(L_103);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_103, L_106, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Name (Long):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Long));
		StringBuilder_t * L_107 = V_4;
		RuntimeObject* L_108 = V_14;
		String_t* L_109 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_108, 3, (bool)0, /*hidden argument*/NULL);
		String_t* L_110 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral4C82CBE32B969CCCC3691112EA5346CFC90C437B, L_109, /*hidden argument*/NULL);
		NullCheck(L_107);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_107, L_110, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Name (Abbreviated):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Abbreviated));
		StringBuilder_t * L_111 = V_4;
		RuntimeObject* L_112 = V_14;
		String_t* L_113 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_112, 4, (bool)0, /*hidden argument*/NULL);
		String_t* L_114 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral7DD3751EA33F73AB8A2B9F9A41CB7DBDD7A4E3ED, L_113, /*hidden argument*/NULL);
		NullCheck(L_111);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_111, L_114, /*hidden argument*/NULL);
		// if (appleIdCredential.FullName.PhoneticRepresentation != null)
		RuntimeObject* L_115 = V_0;
		NullCheck(L_115);
		RuntimeObject* L_116 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(4 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IAppleIDCredential::get_FullName() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_115);
		NullCheck(L_116);
		RuntimeObject* L_117 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(6 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IPersonName::get_PhoneticRepresentation() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_116);
		V_15 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_117) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_118 = V_15;
		if (!L_118)
		{
			goto IL_039b;
		}
	}
	{
		// var phoneticName = appleIdCredential.FullName.PhoneticRepresentation;
		RuntimeObject* L_119 = V_0;
		NullCheck(L_119);
		RuntimeObject* L_120 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(4 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IAppleIDCredential::get_FullName() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_119);
		NullCheck(L_120);
		RuntimeObject* L_121 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(6 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IPersonName::get_PhoneticRepresentation() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_120);
		V_16 = L_121;
		// stringBuilder.AppendLine("<b>Phonetic name:</b> " + phoneticName.ToLocalizedString());
		StringBuilder_t * L_122 = V_4;
		RuntimeObject* L_123 = V_16;
		String_t* L_124 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_123, 0, (bool)0, /*hidden argument*/NULL);
		String_t* L_125 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral5D3FA45438095F8732636CDFE60AFA58C78C4CC3, L_124, /*hidden argument*/NULL);
		NullCheck(L_122);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_122, L_125, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Phonetic name (Short):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Short));
		StringBuilder_t * L_126 = V_4;
		RuntimeObject* L_127 = V_16;
		String_t* L_128 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_127, 1, (bool)0, /*hidden argument*/NULL);
		String_t* L_129 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral5C014B442FE475C5DC58BDBB40E688F065E5B859, L_128, /*hidden argument*/NULL);
		NullCheck(L_126);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_126, L_129, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Phonetic name (Medium):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Medium));
		StringBuilder_t * L_130 = V_4;
		RuntimeObject* L_131 = V_16;
		String_t* L_132 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_131, 2, (bool)0, /*hidden argument*/NULL);
		String_t* L_133 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralC3A1FE3B518D4A8B1B4B10B5CCDC5BC1E9406A69, L_132, /*hidden argument*/NULL);
		NullCheck(L_130);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_130, L_133, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Phonetic name (Long):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Long));
		StringBuilder_t * L_134 = V_4;
		RuntimeObject* L_135 = V_16;
		String_t* L_136 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_135, 3, (bool)0, /*hidden argument*/NULL);
		String_t* L_137 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralA1A8C523C1AE6DB7A1C78DB548B44DAFD0886747, L_136, /*hidden argument*/NULL);
		NullCheck(L_134);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_134, L_137, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Phonetic name (Abbreviated):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Abbreviated));
		StringBuilder_t * L_138 = V_4;
		RuntimeObject* L_139 = V_16;
		String_t* L_140 = PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A(L_139, 4, (bool)0, /*hidden argument*/NULL);
		String_t* L_141 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral36AD9F350774B31288103E40432E5EAF745BB520, L_140, /*hidden argument*/NULL);
		NullCheck(L_138);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_138, L_141, /*hidden argument*/NULL);
	}

IL_039b:
	{
	}

IL_039c:
	{
		// this.AppleUserCredentialLabel.text = stringBuilder.ToString();
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_142 = __this->get_AppleUserCredentialLabel_2();
		StringBuilder_t * L_143 = V_4;
		NullCheck(L_143);
		String_t* L_144 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_143);
		NullCheck(L_142);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_142, L_144);
		goto IL_0438;
	}

IL_03b5:
	{
		// else if (passwordCredential != null)
		RuntimeObject* L_145 = V_1;
		V_17 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_145) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_146 = V_17;
		if (!L_146)
		{
			goto IL_041a;
		}
	}
	{
		// var stringBuilder = new StringBuilder();
		StringBuilder_t * L_147 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_147, /*hidden argument*/NULL);
		V_18 = L_147;
		// stringBuilder.AppendLine("USERNAME/PASSWORD RECEIVED (iCloud?)");
		StringBuilder_t * L_148 = V_18;
		NullCheck(L_148);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_148, _stringLiteralBE197378B78317428D4793F71D084FD494BBA3D9, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Username:</b> " + passwordCredential.User);
		StringBuilder_t * L_149 = V_18;
		RuntimeObject* L_150 = V_1;
		NullCheck(L_150);
		String_t* L_151 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_150);
		String_t* L_152 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral102DA49B7841C2BD193FAB2C18BD222433515F4B, L_151, /*hidden argument*/NULL);
		NullCheck(L_149);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_149, L_152, /*hidden argument*/NULL);
		// stringBuilder.AppendLine("<b>Password:</b> " + passwordCredential.Password);
		StringBuilder_t * L_153 = V_18;
		RuntimeObject* L_154 = V_1;
		NullCheck(L_154);
		String_t* L_155 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.IPasswordCredential::get_Password() */, IPasswordCredential_tC5ACB72704F73E0A1229C3B9B8792031098F1D2E_il2cpp_TypeInfo_var, L_154);
		String_t* L_156 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralCC1B4E6F77870BDBE599FBA4423DC2A71FD628BD, L_155, /*hidden argument*/NULL);
		NullCheck(L_153);
		StringBuilder_AppendLine_mA2F79A5F2CAA91B9F7917C0EB2B381357A395609(L_153, L_156, /*hidden argument*/NULL);
		// this.AppleUserCredentialLabel.text = stringBuilder.ToString();
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_157 = __this->get_AppleUserCredentialLabel_2();
		StringBuilder_t * L_158 = V_18;
		NullCheck(L_158);
		String_t* L_159 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_158);
		NullCheck(L_157);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_157, L_159);
		goto IL_0438;
	}

IL_041a:
	{
		// this.AppleUserCredentialLabel.text = "Unknown credentials for user " + receivedCredential.User;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_160 = __this->get_AppleUserCredentialLabel_2();
		RuntimeObject* L_161 = ___receivedCredential1;
		NullCheck(L_161);
		String_t* L_162 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_161);
		String_t* L_163 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral416B26AAF7E7D413C1FF74BC712BA0CFA4EF92EB, L_162, /*hidden argument*/NULL);
		NullCheck(L_160);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_160, L_163);
	}

IL_0438:
	{
		// }
		return;
	}
}
// System.Void GameMenuHandler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuHandler__ctor_m4A869F8A31ADAE4B56BE371C18A75C926184DA1A (GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean GooglePlayGames.BasicApi.CommonTypesUtil::StatusIsSuccess(GooglePlayGames.BasicApi.ResponseStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CommonTypesUtil_StatusIsSuccess_mD31DC8C3A99B06F08FE225CCE67855296D362060 (int32_t ___status0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return ((int) status) > 0;
		int32_t L_0 = ___status0;
		V_0 = (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
		goto IL_0008;
	}

IL_0008:
	{
		// }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void GooglePlayGames.BasicApi.CommonTypesUtil::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommonTypesUtil__ctor_mF73564494B79C6E17AD874B199D6086DEBA24ADC (CommonTypesUtil_t2873297E6294F5B79AF6B4087BCF1092C7B069E4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.BasicApi.Events.Event::.ctor(System.String,System.String,System.String,System.String,System.UInt64,GooglePlayGames.BasicApi.Events.EventVisibility)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event__ctor_mB8BAC967B425C74E21ED3FA177061AEC433B669E (Event_t72642C3269994E99511B3C85054C40A5610B170D * __this, String_t* ___id0, String_t* ___name1, String_t* ___description2, String_t* ___imageUrl3, uint64_t ___currentCount4, int32_t ___visibility5, const RuntimeMethod* method)
{
	{
		// internal Event(string id, string name, string description, string imageUrl,
		//     ulong currentCount, EventVisibility visibility)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// mId = id;
		String_t* L_0 = ___id0;
		__this->set_mId_0(L_0);
		// mName = name;
		String_t* L_1 = ___name1;
		__this->set_mName_1(L_1);
		// mDescription = description;
		String_t* L_2 = ___description2;
		__this->set_mDescription_2(L_2);
		// mImageUrl = imageUrl;
		String_t* L_3 = ___imageUrl3;
		__this->set_mImageUrl_3(L_3);
		// mCurrentCount = currentCount;
		uint64_t L_4 = ___currentCount4;
		__this->set_mCurrentCount_4(L_4);
		// mVisibility = visibility;
		int32_t L_5 = ___visibility5;
		__this->set_mVisibility_5(L_5);
		// }
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Events.Event::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_Id_m1B22AD589B9AB7AD75E6E1C9BE5ADC64436220CD (Event_t72642C3269994E99511B3C85054C40A5610B170D * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mId; }
		String_t* L_0 = __this->get_mId_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mId; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String GooglePlayGames.BasicApi.Events.Event::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_Name_mB8A76B3994A62C91CAFED8552A2407FB2E11B8CE (Event_t72642C3269994E99511B3C85054C40A5610B170D * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mName; }
		String_t* L_0 = __this->get_mName_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mName; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String GooglePlayGames.BasicApi.Events.Event::get_Description()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_Description_m8FA22DDBB79A4F7E4948B3E4CAA7F624A493F054 (Event_t72642C3269994E99511B3C85054C40A5610B170D * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mDescription; }
		String_t* L_0 = __this->get_mDescription_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mDescription; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String GooglePlayGames.BasicApi.Events.Event::get_ImageUrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_ImageUrl_m3C8358B358E199DE35AB1F80B5EED76274C54C93 (Event_t72642C3269994E99511B3C85054C40A5610B170D * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mImageUrl; }
		String_t* L_0 = __this->get_mImageUrl_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mImageUrl; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.UInt64 GooglePlayGames.BasicApi.Events.Event::get_CurrentCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint64_t Event_get_CurrentCount_m3F7CD36B22DC6ADE3347D76D5684180D9B9F364A (Event_t72642C3269994E99511B3C85054C40A5610B170D * __this, const RuntimeMethod* method)
{
	uint64_t V_0 = 0;
	{
		// get { return mCurrentCount; }
		uint64_t L_0 = __this->get_mCurrentCount_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mCurrentCount; }
		uint64_t L_1 = V_0;
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.BasicApi.Events.Event::get_Visibility()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Event_get_Visibility_m883BC24B32C07A45EFF52FA0F26E467637071F0C (Event_t72642C3269994E99511B3C85054C40A5610B170D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return mVisibility; }
		int32_t L_0 = __this->get_mVisibility_5();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mVisibility; }
		int32_t L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.AdvertisingResult
IL2CPP_EXTERN_C void AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshal_pinvoke(const AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0& unmarshaled, AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_pinvoke& marshaled)
{
	marshaled.___mStatus_0 = unmarshaled.get_mStatus_0();
	marshaled.___mLocalEndpointName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_mLocalEndpointName_1());
}
IL2CPP_EXTERN_C void AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshal_pinvoke_back(const AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_pinvoke& marshaled, AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0& unmarshaled)
{
	int32_t unmarshaled_mStatus_temp_0 = 0;
	unmarshaled_mStatus_temp_0 = marshaled.___mStatus_0;
	unmarshaled.set_mStatus_0(unmarshaled_mStatus_temp_0);
	unmarshaled.set_mLocalEndpointName_1(il2cpp_codegen_marshal_string_result(marshaled.___mLocalEndpointName_1));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.AdvertisingResult
IL2CPP_EXTERN_C void AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshal_pinvoke_cleanup(AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___mLocalEndpointName_1);
	marshaled.___mLocalEndpointName_1 = NULL;
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.AdvertisingResult
IL2CPP_EXTERN_C void AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshal_com(const AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0& unmarshaled, AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_com& marshaled)
{
	marshaled.___mStatus_0 = unmarshaled.get_mStatus_0();
	marshaled.___mLocalEndpointName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_mLocalEndpointName_1());
}
IL2CPP_EXTERN_C void AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshal_com_back(const AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_com& marshaled, AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0& unmarshaled)
{
	int32_t unmarshaled_mStatus_temp_0 = 0;
	unmarshaled_mStatus_temp_0 = marshaled.___mStatus_0;
	unmarshaled.set_mStatus_0(unmarshaled_mStatus_temp_0);
	unmarshaled.set_mLocalEndpointName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___mLocalEndpointName_1));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.AdvertisingResult
IL2CPP_EXTERN_C void AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshal_com_cleanup(AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___mLocalEndpointName_1);
	marshaled.___mLocalEndpointName_1 = NULL;
}
// System.Void GooglePlayGames.BasicApi.Nearby.AdvertisingResult::.ctor(GooglePlayGames.BasicApi.ResponseStatus,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, int32_t ___status0, String_t* ___localEndpointName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.mStatus = status;
		int32_t L_0 = ___status0;
		__this->set_mStatus_0(L_0);
		// this.mLocalEndpointName = Misc.CheckNotNull(localEndpointName);
		String_t* L_1 = ___localEndpointName1;
		String_t* L_2 = Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91(L_1, /*hidden argument*/Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91_RuntimeMethod_var);
		__this->set_mLocalEndpointName_1(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB_AdjustorThunk (RuntimeObject * __this, int32_t ___status0, String_t* ___localEndpointName1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * _thisAdjusted = reinterpret_cast<AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 *>(__this + _offset);
	AdvertisingResult__ctor_mEA1C1F6ED47ACBB44C5A2BBD93E7EF5157430BDB(_thisAdjusted, ___status0, ___localEndpointName1, method);
}
// System.Boolean GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Succeeded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdvertisingResult_get_Succeeded_mB18393EF2668F7055A946844069C3FFD5C114427 (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mStatus == ResponseStatus.Success; }
		int32_t L_0 = __this->get_mStatus_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
		goto IL_000d;
	}

IL_000d:
	{
		// get { return mStatus == ResponseStatus.Success; }
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool AdvertisingResult_get_Succeeded_mB18393EF2668F7055A946844069C3FFD5C114427_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * _thisAdjusted = reinterpret_cast<AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 *>(__this + _offset);
	return AdvertisingResult_get_Succeeded_mB18393EF2668F7055A946844069C3FFD5C114427(_thisAdjusted, method);
}
// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Status()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdvertisingResult_get_Status_m9D257F1C520D8D7B3D75709985B2C6D59061BF89 (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return mStatus; }
		int32_t L_0 = __this->get_mStatus_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mStatus; }
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t AdvertisingResult_get_Status_m9D257F1C520D8D7B3D75709985B2C6D59061BF89_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * _thisAdjusted = reinterpret_cast<AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 *>(__this + _offset);
	return AdvertisingResult_get_Status_m9D257F1C520D8D7B3D75709985B2C6D59061BF89(_thisAdjusted, method);
}
// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_LocalEndpointName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AdvertisingResult_get_LocalEndpointName_mAED984971DC4130D6A7A2716C262B124F313DA3D (AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mLocalEndpointName; }
		String_t* L_0 = __this->get_mLocalEndpointName_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mLocalEndpointName; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  String_t* AdvertisingResult_get_LocalEndpointName_mAED984971DC4130D6A7A2716C262B124F313DA3D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 * _thisAdjusted = reinterpret_cast<AdvertisingResult_t3B4641A99C4968A189344B6FE3A8C04F9870ADA0 *>(__this + _offset);
	return AdvertisingResult_get_LocalEndpointName_mAED984971DC4130D6A7A2716C262B124F313DA3D(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionRequest
IL2CPP_EXTERN_C void ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshal_pinvoke(const ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01& unmarshaled, ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_pinvoke& marshaled)
{
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke(unmarshaled.get_mRemoteEndpoint_0(), marshaled.___mRemoteEndpoint_0);
	marshaled.___mPayload_1 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_mPayload_1());
}
IL2CPP_EXTERN_C void ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshal_pinvoke_back(const ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_pinvoke& marshaled, ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  unmarshaled_mRemoteEndpoint_temp_0;
	memset((&unmarshaled_mRemoteEndpoint_temp_0), 0, sizeof(unmarshaled_mRemoteEndpoint_temp_0));
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke_back(marshaled.___mRemoteEndpoint_0, unmarshaled_mRemoteEndpoint_temp_0);
	unmarshaled.set_mRemoteEndpoint_0(unmarshaled_mRemoteEndpoint_temp_0);
	unmarshaled.set_mPayload_1((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var, marshaled.___mPayload_1));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionRequest
IL2CPP_EXTERN_C void ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshal_pinvoke_cleanup(ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_pinvoke& marshaled)
{
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke_cleanup(marshaled.___mRemoteEndpoint_0);
	il2cpp_codegen_com_destroy_safe_array(marshaled.___mPayload_1);
	marshaled.___mPayload_1 = NULL;
}


// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionRequest
IL2CPP_EXTERN_C void ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshal_com(const ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01& unmarshaled, ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_com& marshaled)
{
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com(unmarshaled.get_mRemoteEndpoint_0(), marshaled.___mRemoteEndpoint_0);
	marshaled.___mPayload_1 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_mPayload_1());
}
IL2CPP_EXTERN_C void ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshal_com_back(const ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_com& marshaled, ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  unmarshaled_mRemoteEndpoint_temp_0;
	memset((&unmarshaled_mRemoteEndpoint_temp_0), 0, sizeof(unmarshaled_mRemoteEndpoint_temp_0));
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com_back(marshaled.___mRemoteEndpoint_0, unmarshaled_mRemoteEndpoint_temp_0);
	unmarshaled.set_mRemoteEndpoint_0(unmarshaled_mRemoteEndpoint_temp_0);
	unmarshaled.set_mPayload_1((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var, marshaled.___mPayload_1));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionRequest
IL2CPP_EXTERN_C void ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshal_com_cleanup(ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01_marshaled_com& marshaled)
{
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com_cleanup(marshaled.___mRemoteEndpoint_0);
	il2cpp_codegen_com_destroy_safe_array(marshaled.___mPayload_1);
	marshaled.___mPayload_1 = NULL;
}
// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionRequest::.ctor(System.String,System.String,System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43 (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * __this, String_t* ___remoteEndpointId0, String_t* ___remoteEndpointName1, String_t* ___serviceId2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Logger.d("Constructing ConnectionRequest");
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		Logger_d_m95C0BD29B60442754276E97D1FB7ED05E4AC31CE(_stringLiteralC906E19FA4044BF51882DA58C9D614C90A29617A, /*hidden argument*/NULL);
		// mRemoteEndpoint = new EndpointDetails(remoteEndpointId, remoteEndpointName, serviceId);
		String_t* L_0 = ___remoteEndpointId0;
		String_t* L_1 = ___remoteEndpointName1;
		String_t* L_2 = ___serviceId2;
		EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  L_3;
		memset((&L_3), 0, sizeof(L_3));
		EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_mRemoteEndpoint_0(L_3);
		// this.mPayload = Misc.CheckNotNull(payload);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___payload3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = Misc_CheckNotNull_TisByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_m6BE81B7330BF6B723B4069514F3C7ACFDAAA5A13(L_4, /*hidden argument*/Misc_CheckNotNull_TisByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_m6BE81B7330BF6B723B4069514F3C7ACFDAAA5A13_RuntimeMethod_var);
		__this->set_mPayload_1(L_5);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43_AdjustorThunk (RuntimeObject * __this, String_t* ___remoteEndpointId0, String_t* ___remoteEndpointName1, String_t* ___serviceId2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * _thisAdjusted = reinterpret_cast<ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 *>(__this + _offset);
	ConnectionRequest__ctor_m78893F39A8FB4F137BDAC81F7BAC3166B9F2CC43(_thisAdjusted, ___remoteEndpointId0, ___remoteEndpointName1, ___serviceId2, ___payload3, method);
}
// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_RemoteEndpoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  ConnectionRequest_get_RemoteEndpoint_mFB71214059AC49D631EEB34A4073B05BB9F098D9 (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * __this, const RuntimeMethod* method)
{
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// get { return mRemoteEndpoint; }
		EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  L_0 = __this->get_mRemoteEndpoint_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mRemoteEndpoint; }
		EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893  ConnectionRequest_get_RemoteEndpoint_mFB71214059AC49D631EEB34A4073B05BB9F098D9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * _thisAdjusted = reinterpret_cast<ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 *>(__this + _offset);
	return ConnectionRequest_get_RemoteEndpoint_mFB71214059AC49D631EEB34A4073B05BB9F098D9(_thisAdjusted, method);
}
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::get_Payload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ConnectionRequest_get_Payload_m5FF54BC4CB9C49835FE68F6EFE6A853A824A37F0 (ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * __this, const RuntimeMethod* method)
{
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		// get { return mPayload; }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_mPayload_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mPayload; }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ConnectionRequest_get_Payload_m5FF54BC4CB9C49835FE68F6EFE6A853A824A37F0_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 * _thisAdjusted = reinterpret_cast<ConnectionRequest_tD637D29E19B812161F39390F4D8290004B35BD01 *>(__this + _offset);
	return ConnectionRequest_get_Payload_m5FF54BC4CB9C49835FE68F6EFE6A853A824A37F0(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionResponse
IL2CPP_EXTERN_C void ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshal_pinvoke(const ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70& unmarshaled, ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_pinvoke& marshaled)
{
	marshaled.___mLocalClientId_1 = unmarshaled.get_mLocalClientId_1();
	marshaled.___mRemoteEndpointId_2 = il2cpp_codegen_marshal_string(unmarshaled.get_mRemoteEndpointId_2());
	marshaled.___mResponseStatus_3 = unmarshaled.get_mResponseStatus_3();
	marshaled.___mPayload_4 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_mPayload_4());
}
IL2CPP_EXTERN_C void ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshal_pinvoke_back(const ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_pinvoke& marshaled, ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t unmarshaled_mLocalClientId_temp_0 = 0;
	unmarshaled_mLocalClientId_temp_0 = marshaled.___mLocalClientId_1;
	unmarshaled.set_mLocalClientId_1(unmarshaled_mLocalClientId_temp_0);
	unmarshaled.set_mRemoteEndpointId_2(il2cpp_codegen_marshal_string_result(marshaled.___mRemoteEndpointId_2));
	int32_t unmarshaled_mResponseStatus_temp_2 = 0;
	unmarshaled_mResponseStatus_temp_2 = marshaled.___mResponseStatus_3;
	unmarshaled.set_mResponseStatus_3(unmarshaled_mResponseStatus_temp_2);
	unmarshaled.set_mPayload_4((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var, marshaled.___mPayload_4));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionResponse
IL2CPP_EXTERN_C void ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshal_pinvoke_cleanup(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___mRemoteEndpointId_2);
	marshaled.___mRemoteEndpointId_2 = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___mPayload_4);
	marshaled.___mPayload_4 = NULL;
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionResponse
IL2CPP_EXTERN_C void ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshal_com(const ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70& unmarshaled, ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_com& marshaled)
{
	marshaled.___mLocalClientId_1 = unmarshaled.get_mLocalClientId_1();
	marshaled.___mRemoteEndpointId_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_mRemoteEndpointId_2());
	marshaled.___mResponseStatus_3 = unmarshaled.get_mResponseStatus_3();
	marshaled.___mPayload_4 = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.get_mPayload_4());
}
IL2CPP_EXTERN_C void ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshal_com_back(const ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_com& marshaled, ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t unmarshaled_mLocalClientId_temp_0 = 0;
	unmarshaled_mLocalClientId_temp_0 = marshaled.___mLocalClientId_1;
	unmarshaled.set_mLocalClientId_1(unmarshaled_mLocalClientId_temp_0);
	unmarshaled.set_mRemoteEndpointId_2(il2cpp_codegen_marshal_bstring_result(marshaled.___mRemoteEndpointId_2));
	int32_t unmarshaled_mResponseStatus_temp_2 = 0;
	unmarshaled_mResponseStatus_temp_2 = marshaled.___mResponseStatus_3;
	unmarshaled.set_mResponseStatus_3(unmarshaled_mResponseStatus_temp_2);
	unmarshaled.set_mPayload_4((ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_il2cpp_TypeInfo_var, marshaled.___mPayload_4));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionResponse
IL2CPP_EXTERN_C void ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshal_com_cleanup(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___mRemoteEndpointId_2);
	marshaled.___mRemoteEndpointId_2 = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___mPayload_4);
	marshaled.___mPayload_4 = NULL;
}
// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.ctor(System.Int64,System.String,GooglePlayGames.BasicApi.Nearby.ConnectionResponse_Status,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, int64_t ___localClientId0, String_t* ___remoteEndpointId1, int32_t ___code2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.mLocalClientId = localClientId;
		int64_t L_0 = ___localClientId0;
		__this->set_mLocalClientId_1(L_0);
		// this.mRemoteEndpointId = Misc.CheckNotNull(remoteEndpointId);
		String_t* L_1 = ___remoteEndpointId1;
		String_t* L_2 = Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91(L_1, /*hidden argument*/Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91_RuntimeMethod_var);
		__this->set_mRemoteEndpointId_2(L_2);
		// this.mResponseStatus = code;
		int32_t L_3 = ___code2;
		__this->set_mResponseStatus_3(L_3);
		// this.mPayload = Misc.CheckNotNull(payload);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___payload3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = Misc_CheckNotNull_TisByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_m6BE81B7330BF6B723B4069514F3C7ACFDAAA5A13(L_4, /*hidden argument*/Misc_CheckNotNull_TisByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_m6BE81B7330BF6B723B4069514F3C7ACFDAAA5A13_RuntimeMethod_var);
		__this->set_mPayload_4(L_5);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20_AdjustorThunk (RuntimeObject * __this, int64_t ___localClientId0, String_t* ___remoteEndpointId1, int32_t ___code2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * _thisAdjusted = reinterpret_cast<ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 *>(__this + _offset);
	ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20(_thisAdjusted, ___localClientId0, ___remoteEndpointId1, ___code2, ___payload3, method);
}
// System.Int64 GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_LocalClientId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t ConnectionResponse_get_LocalClientId_mB134B291B50B17DD632719CC808C47F1B5128E94 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method)
{
	int64_t V_0 = 0;
	{
		// get { return mLocalClientId; }
		int64_t L_0 = __this->get_mLocalClientId_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mLocalClientId; }
		int64_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  int64_t ConnectionResponse_get_LocalClientId_mB134B291B50B17DD632719CC808C47F1B5128E94_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * _thisAdjusted = reinterpret_cast<ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 *>(__this + _offset);
	return ConnectionResponse_get_LocalClientId_mB134B291B50B17DD632719CC808C47F1B5128E94(_thisAdjusted, method);
}
// System.String GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_RemoteEndpointId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ConnectionResponse_get_RemoteEndpointId_m767C97E62BC21CE88908266C7FA8947CE98BFC18 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mRemoteEndpointId; }
		String_t* L_0 = __this->get_mRemoteEndpointId_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mRemoteEndpointId; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  String_t* ConnectionResponse_get_RemoteEndpointId_m767C97E62BC21CE88908266C7FA8947CE98BFC18_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * _thisAdjusted = reinterpret_cast<ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 *>(__this + _offset);
	return ConnectionResponse_get_RemoteEndpointId_m767C97E62BC21CE88908266C7FA8947CE98BFC18(_thisAdjusted, method);
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse_Status GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_ResponseStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConnectionResponse_get_ResponseStatus_m592CB9981EE99165FDE5009096E61D37D9D0DF04 (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return mResponseStatus; }
		int32_t L_0 = __this->get_mResponseStatus_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mResponseStatus; }
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t ConnectionResponse_get_ResponseStatus_m592CB9981EE99165FDE5009096E61D37D9D0DF04_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * _thisAdjusted = reinterpret_cast<ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 *>(__this + _offset);
	return ConnectionResponse_get_ResponseStatus_m592CB9981EE99165FDE5009096E61D37D9D0DF04(_thisAdjusted, method);
}
// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionResponse::get_Payload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ConnectionResponse_get_Payload_mACC8A0806D0DD6E10E21F1977B42F9BCB26D1C7F (ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * __this, const RuntimeMethod* method)
{
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		// get { return mPayload; }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_mPayload_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mPayload; }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ConnectionResponse_get_Payload_mACC8A0806D0DD6E10E21F1977B42F9BCB26D1C7F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 * _thisAdjusted = reinterpret_cast<ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70 *>(__this + _offset);
	return ConnectionResponse_get_Payload_mACC8A0806D0DD6E10E21F1977B42F9BCB26D1C7F(_thisAdjusted, method);
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Rejected(System.Int64,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  ConnectionResponse_Rejected_mA6985CBE633FF1E10C02C1E89B8752520816F4CB (int64_t ___localClientId0, String_t* ___remoteEndpointId1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse_Rejected_mA6985CBE633FF1E10C02C1E89B8752520816F4CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new ConnectionResponse(localClientId, remoteEndpointId, Status.Rejected,
		//     EmptyPayload);
		int64_t L_0 = ___localClientId0;
		String_t* L_1 = ___remoteEndpointId1;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ((ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields*)il2cpp_codegen_static_fields_for(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var))->get_EmptyPayload_0();
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20((&L_3), L_0, L_1, 1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0011;
	}

IL_0011:
	{
		// }
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_4 = V_0;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::NetworkNotConnected(System.Int64,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  ConnectionResponse_NetworkNotConnected_m1F2A5E374AB38812517E2908D8423C5444AC8D7A (int64_t ___localClientId0, String_t* ___remoteEndpointId1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse_NetworkNotConnected_m1F2A5E374AB38812517E2908D8423C5444AC8D7A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new ConnectionResponse(localClientId, remoteEndpointId, Status.ErrorNetworkNotConnected,
		//     EmptyPayload);
		int64_t L_0 = ___localClientId0;
		String_t* L_1 = ___remoteEndpointId1;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ((ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields*)il2cpp_codegen_static_fields_for(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var))->get_EmptyPayload_0();
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20((&L_3), L_0, L_1, 3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0011;
	}

IL_0011:
	{
		// }
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_4 = V_0;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::InternalError(System.Int64,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  ConnectionResponse_InternalError_mDF78FF322A6E590BD4D524645B41A9A1DC800DD5 (int64_t ___localClientId0, String_t* ___remoteEndpointId1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse_InternalError_mDF78FF322A6E590BD4D524645B41A9A1DC800DD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new ConnectionResponse(localClientId, remoteEndpointId, Status.ErrorInternal,
		//     EmptyPayload);
		int64_t L_0 = ___localClientId0;
		String_t* L_1 = ___remoteEndpointId1;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ((ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields*)il2cpp_codegen_static_fields_for(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var))->get_EmptyPayload_0();
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20((&L_3), L_0, L_1, 2, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0011;
	}

IL_0011:
	{
		// }
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_4 = V_0;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::EndpointNotConnected(System.Int64,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  ConnectionResponse_EndpointNotConnected_m77D7E68783C9BAF4ADEE6C7675AF7185DF452D18 (int64_t ___localClientId0, String_t* ___remoteEndpointId1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse_EndpointNotConnected_m77D7E68783C9BAF4ADEE6C7675AF7185DF452D18_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new ConnectionResponse(localClientId, remoteEndpointId, Status.ErrorEndpointNotConnected,
		//     EmptyPayload);
		int64_t L_0 = ___localClientId0;
		String_t* L_1 = ___remoteEndpointId1;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ((ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields*)il2cpp_codegen_static_fields_for(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var))->get_EmptyPayload_0();
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20((&L_3), L_0, L_1, 4, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0011;
	}

IL_0011:
	{
		// }
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_4 = V_0;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::Accepted(System.Int64,System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  ConnectionResponse_Accepted_mAEB28D825CEE048F714DE6DA3E571938B32E527F (int64_t ___localClientId0, String_t* ___remoteEndpointId1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload2, const RuntimeMethod* method)
{
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new ConnectionResponse(localClientId, remoteEndpointId, Status.Accepted,
		//     payload);
		int64_t L_0 = ___localClientId0;
		String_t* L_1 = ___remoteEndpointId1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___payload2;
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20((&L_3), L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000d;
	}

IL_000d:
	{
		// }
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_4 = V_0;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse GooglePlayGames.BasicApi.Nearby.ConnectionResponse::AlreadyConnected(System.Int64,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  ConnectionResponse_AlreadyConnected_mB3BBBB13B00069C8B7691638AA5F542D2B325A5B (int64_t ___localClientId0, String_t* ___remoteEndpointId1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse_AlreadyConnected_mB3BBBB13B00069C8B7691638AA5F542D2B325A5B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new ConnectionResponse(localClientId, remoteEndpointId,
		//     Status.ErrorAlreadyConnected,
		//     EmptyPayload);
		int64_t L_0 = ___localClientId0;
		String_t* L_1 = ___remoteEndpointId1;
		IL2CPP_RUNTIME_CLASS_INIT(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ((ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields*)il2cpp_codegen_static_fields_for(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var))->get_EmptyPayload_0();
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ConnectionResponse__ctor_m192E5F98FE98AEAB5123FFFB3671706EB0C03C20((&L_3), L_0, L_1, 5, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0011;
	}

IL_0011:
	{
		// }
		ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70  L_4 = V_0;
		return L_4;
	}
}
// System.Void GooglePlayGames.BasicApi.Nearby.ConnectionResponse::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConnectionResponse__cctor_m98A4E442ECE05DE88094B57CE48D5647EE345B4D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionResponse__cctor_m98A4E442ECE05DE88094B57CE48D5647EE345B4D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly byte[] EmptyPayload = new byte[0];
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)0);
		((ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_StaticFields*)il2cpp_codegen_static_fields_for(ConnectionResponse_tCD9C36C9A63A80E37458323770D0460008949C70_il2cpp_TypeInfo_var))->set_EmptyPayload_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.EndpointDetails
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke& marshaled)
{
	marshaled.___mEndpointId_0 = il2cpp_codegen_marshal_string(unmarshaled.get_mEndpointId_0());
	marshaled.___mName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_mName_1());
	marshaled.___mServiceId_2 = il2cpp_codegen_marshal_string(unmarshaled.get_mServiceId_2());
}
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke_back(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke& marshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled)
{
	unmarshaled.set_mEndpointId_0(il2cpp_codegen_marshal_string_result(marshaled.___mEndpointId_0));
	unmarshaled.set_mName_1(il2cpp_codegen_marshal_string_result(marshaled.___mName_1));
	unmarshaled.set_mServiceId_2(il2cpp_codegen_marshal_string_result(marshaled.___mServiceId_2));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.EndpointDetails
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_pinvoke_cleanup(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___mEndpointId_0);
	marshaled.___mEndpointId_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___mName_1);
	marshaled.___mName_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___mServiceId_2);
	marshaled.___mServiceId_2 = NULL;
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.EndpointDetails
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com& marshaled)
{
	marshaled.___mEndpointId_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_mEndpointId_0());
	marshaled.___mName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_mName_1());
	marshaled.___mServiceId_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_mServiceId_2());
}
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com_back(const EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com& marshaled, EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893& unmarshaled)
{
	unmarshaled.set_mEndpointId_0(il2cpp_codegen_marshal_bstring_result(marshaled.___mEndpointId_0));
	unmarshaled.set_mName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___mName_1));
	unmarshaled.set_mServiceId_2(il2cpp_codegen_marshal_bstring_result(marshaled.___mServiceId_2));
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.EndpointDetails
IL2CPP_EXTERN_C void EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshal_com_cleanup(EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___mEndpointId_0);
	marshaled.___mEndpointId_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___mName_1);
	marshaled.___mName_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___mServiceId_2);
	marshaled.___mServiceId_2 = NULL;
}
// System.Void GooglePlayGames.BasicApi.Nearby.EndpointDetails::.ctor(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, String_t* ___endpointId0, String_t* ___name1, String_t* ___serviceId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.mEndpointId = Misc.CheckNotNull(endpointId);
		String_t* L_0 = ___endpointId0;
		String_t* L_1 = Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91(L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91_RuntimeMethod_var);
		__this->set_mEndpointId_0(L_1);
		// this.mName = Misc.CheckNotNull(name);
		String_t* L_2 = ___name1;
		String_t* L_3 = Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91(L_2, /*hidden argument*/Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91_RuntimeMethod_var);
		__this->set_mName_1(L_3);
		// this.mServiceId = Misc.CheckNotNull(serviceId);
		String_t* L_4 = ___serviceId2;
		String_t* L_5 = Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91(L_4, /*hidden argument*/Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91_RuntimeMethod_var);
		__this->set_mServiceId_2(L_5);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F_AdjustorThunk (RuntimeObject * __this, String_t* ___endpointId0, String_t* ___name1, String_t* ___serviceId2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * _thisAdjusted = reinterpret_cast<EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 *>(__this + _offset);
	EndpointDetails__ctor_mE802FC1351463D0E1191F3F2E9F5844FA8ADD30F(_thisAdjusted, ___endpointId0, ___name1, ___serviceId2, method);
}
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_EndpointId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EndpointDetails_get_EndpointId_m560A17AA21D05B3256DA8F0D07ED1FC55FD4A3BD (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mEndpointId; }
		String_t* L_0 = __this->get_mEndpointId_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mEndpointId; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  String_t* EndpointDetails_get_EndpointId_m560A17AA21D05B3256DA8F0D07ED1FC55FD4A3BD_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * _thisAdjusted = reinterpret_cast<EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 *>(__this + _offset);
	return EndpointDetails_get_EndpointId_m560A17AA21D05B3256DA8F0D07ED1FC55FD4A3BD(_thisAdjusted, method);
}
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EndpointDetails_get_Name_m502483C84E998E62F76F450450F4D3D805E69E07 (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mName; }
		String_t* L_0 = __this->get_mName_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mName; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  String_t* EndpointDetails_get_Name_m502483C84E998E62F76F450450F4D3D805E69E07_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * _thisAdjusted = reinterpret_cast<EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 *>(__this + _offset);
	return EndpointDetails_get_Name_m502483C84E998E62F76F450450F4D3D805E69E07(_thisAdjusted, method);
}
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_ServiceId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EndpointDetails_get_ServiceId_mDB50FBA4F7F40740C49B63E76D92CE37F9090C63 (EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mServiceId; }
		String_t* L_0 = __this->get_mServiceId_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mServiceId; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  String_t* EndpointDetails_get_ServiceId_mDB50FBA4F7F40740C49B63E76D92CE37F9090C63_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 * _thisAdjusted = reinterpret_cast<EndpointDetails_tF68895B3F6DA77C1DE38BA7663E4BC47AE018893 *>(__this + _offset);
	return EndpointDetails_get_ServiceId_mDB50FBA4F7F40740C49B63E76D92CE37F9090C63(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
IL2CPP_EXTERN_C void NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshal_pinvoke(const NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2& unmarshaled, NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_pinvoke& marshaled)
{
	marshaled.___mInitializationCallback_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(unmarshaled.get_mInitializationCallback_2()));
	marshaled.___mLocalClientId_3 = unmarshaled.get_mLocalClientId_3();
}
IL2CPP_EXTERN_C void NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshal_pinvoke_back(const NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_pinvoke& marshaled, NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_mInitializationCallback_2(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9>(marshaled.___mInitializationCallback_2, Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9_il2cpp_TypeInfo_var));
	int64_t unmarshaled_mLocalClientId_temp_1 = 0;
	unmarshaled_mLocalClientId_temp_1 = marshaled.___mLocalClientId_3;
	unmarshaled.set_mLocalClientId_3(unmarshaled_mLocalClientId_temp_1);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
IL2CPP_EXTERN_C void NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshal_pinvoke_cleanup(NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
IL2CPP_EXTERN_C void NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshal_com(const NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2& unmarshaled, NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_com& marshaled)
{
	marshaled.___mInitializationCallback_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(unmarshaled.get_mInitializationCallback_2()));
	marshaled.___mLocalClientId_3 = unmarshaled.get_mLocalClientId_3();
}
IL2CPP_EXTERN_C void NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshal_com_back(const NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_com& marshaled, NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_mInitializationCallback_2(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9>(marshaled.___mInitializationCallback_2, Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9_il2cpp_TypeInfo_var));
	int64_t unmarshaled_mLocalClientId_temp_1 = 0;
	unmarshaled_mLocalClientId_temp_1 = marshaled.___mLocalClientId_3;
	unmarshaled.set_mLocalClientId_3(unmarshaled_mLocalClientId_temp_1);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
IL2CPP_EXTERN_C void NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshal_com_cleanup(NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2_marshaled_com& marshaled)
{
}
// System.Void GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::.ctor(System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * __this, Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * ___callback0, int64_t ___localClientId1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.mInitializationCallback = Misc.CheckNotNull(callback);
		Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * L_0 = ___callback0;
		Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * L_1 = Misc_CheckNotNull_TisAction_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9_m240E10DDF67AD2B8678A5504DA835C7ACDA4FA2A(L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9_m240E10DDF67AD2B8678A5504DA835C7ACDA4FA2A_RuntimeMethod_var);
		__this->set_mInitializationCallback_2(L_1);
		// this.mLocalClientId = localClientId;
		int64_t L_2 = ___localClientId1;
		__this->set_mLocalClientId_3(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C_AdjustorThunk (RuntimeObject * __this, Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * ___callback0, int64_t ___localClientId1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * _thisAdjusted = reinterpret_cast<NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 *>(__this + _offset);
	NearbyConnectionConfiguration__ctor_mB6D3EB4BA4FAE1630889F81D3DF92AE1C7613B3C(_thisAdjusted, ___callback0, ___localClientId1, method);
}
// System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_LocalClientId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t NearbyConnectionConfiguration_get_LocalClientId_m70F6E0E0867D4BE7FCDB0A157AC0DDA2F61B0D7D (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * __this, const RuntimeMethod* method)
{
	int64_t V_0 = 0;
	{
		// get { return mLocalClientId; }
		int64_t L_0 = __this->get_mLocalClientId_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mLocalClientId; }
		int64_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  int64_t NearbyConnectionConfiguration_get_LocalClientId_m70F6E0E0867D4BE7FCDB0A157AC0DDA2F61B0D7D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * _thisAdjusted = reinterpret_cast<NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 *>(__this + _offset);
	return NearbyConnectionConfiguration_get_LocalClientId_m70F6E0E0867D4BE7FCDB0A157AC0DDA2F61B0D7D(_thisAdjusted, method);
}
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_InitializationCallback()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * NearbyConnectionConfiguration_get_InitializationCallback_m0E59187AC709946F6C28F84A0C80A0B36539990B (NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * __this, const RuntimeMethod* method)
{
	Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * V_0 = NULL;
	{
		// get { return mInitializationCallback; }
		Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * L_0 = __this->get_mInitializationCallback_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mInitializationCallback; }
		Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  Action_1_t2F1A84DD9C8EF1EB3B442A7A00D89B67F8B65BD9 * NearbyConnectionConfiguration_get_InitializationCallback_m0E59187AC709946F6C28F84A0C80A0B36539990B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 * _thisAdjusted = reinterpret_cast<NearbyConnectionConfiguration_t02D447BB3D5D39576ABEB98B8DC624529D49EDB2 *>(__this + _offset);
	return NearbyConnectionConfiguration_get_InitializationCallback_m0E59187AC709946F6C28F84A0C80A0B36539990B(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConflictCallback__ctor_m7AA035B001255FC625B211DE6C1BBD2E85B66F8F (ConflictCallback_t73445AE2938707A22A01739D23A603DC17AB4962 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConflictCallback_Invoke_mF267F1669328BC164AF8E5D305A52D0EABAEBDD2 (ConflictCallback_t73445AE2938707A22A01739D23A603DC17AB4962 * __this, RuntimeObject* ___resolver0, RuntimeObject* ___original1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___originalData2, RuntimeObject* ___unmerged3, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___unmergedData4, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 5)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4, targetMethod);
			}
		}
		else if (___parameterCount != 5)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker4< RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(targetMethod, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
					else
						GenericVirtActionInvoker4< RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(targetMethod, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker4< RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
					else
						VirtActionInvoker4< RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___original1) - 1), ___originalData2, ___unmerged3, ___unmergedData4, targetMethod);
				}
				typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker5< RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(targetMethod, targetThis, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
					else
						GenericVirtActionInvoker5< RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(targetMethod, targetThis, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker5< RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
					else
						VirtActionInvoker5< RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___resolver0) - 1), ___original1, ___originalData2, ___unmerged3, ___unmergedData4, targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___resolver0, ___original1, ___originalData2, ___unmerged3, ___unmergedData4, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult GooglePlayGames.BasicApi.SavedGame.ConflictCallback::BeginInvoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ConflictCallback_BeginInvoke_mB3EF38DC068E36F9ADB2D538E09FE2FCAFBF4763 (ConflictCallback_t73445AE2938707A22A01739D23A603DC17AB4962 * __this, RuntimeObject* ___resolver0, RuntimeObject* ___original1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___originalData2, RuntimeObject* ___unmerged3, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___unmergedData4, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	void *__d_args[6] = {0};
	__d_args[0] = ___resolver0;
	__d_args[1] = ___original1;
	__d_args[2] = ___originalData2;
	__d_args[3] = ___unmerged3;
	__d_args[4] = ___unmergedData4;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConflictCallback_EndInvoke_mD934D9D128B5ECA394A7E59A8A7A9069EE454015 (ConflictCallback_t73445AE2938707A22A01739D23A603DC17AB4962 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
IL2CPP_EXTERN_C void SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshal_pinvoke(const SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A& unmarshaled, SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_pinvoke& marshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'SavedGameMetadataUpdate'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
IL2CPP_EXTERN_C void SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshal_pinvoke_back(const SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_pinvoke& marshaled, SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A& unmarshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'SavedGameMetadataUpdate'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
IL2CPP_EXTERN_C void SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshal_pinvoke_cleanup(SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
IL2CPP_EXTERN_C void SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshal_com(const SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A& unmarshaled, SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_com& marshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'SavedGameMetadataUpdate'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
IL2CPP_EXTERN_C void SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshal_com_back(const SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_com& marshaled, SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A& unmarshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'SavedGameMetadataUpdate'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
IL2CPP_EXTERN_C void SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshal_com_cleanup(SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A_marshaled_com& marshaled)
{
}
// System.Void GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::.ctor(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SavedGameMetadataUpdate__ctor_m5B508D8C6829985292C4AFC6D5A99F3A9E9AE259 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  ___builder0, const RuntimeMethod* method)
{
	{
		// mDescriptionUpdated = builder.mDescriptionUpdated;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_0 = ___builder0;
		bool L_1 = L_0.get_mDescriptionUpdated_0();
		__this->set_mDescriptionUpdated_0(L_1);
		// mNewDescription = builder.mNewDescription;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_2 = ___builder0;
		String_t* L_3 = L_2.get_mNewDescription_1();
		__this->set_mNewDescription_1(L_3);
		// mCoverImageUpdated = builder.mCoverImageUpdated;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_4 = ___builder0;
		bool L_5 = L_4.get_mCoverImageUpdated_2();
		__this->set_mCoverImageUpdated_2(L_5);
		// mNewPngCoverImage = builder.mNewPngCoverImage;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_6 = ___builder0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = L_6.get_mNewPngCoverImage_3();
		__this->set_mNewPngCoverImage_3(L_7);
		// mNewPlayedTime = builder.mNewPlayedTime;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_8 = ___builder0;
		Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  L_9 = L_8.get_mNewPlayedTime_4();
		__this->set_mNewPlayedTime_4(L_9);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SavedGameMetadataUpdate__ctor_m5B508D8C6829985292C4AFC6D5A99F3A9E9AE259_AdjustorThunk (RuntimeObject * __this, Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  ___builder0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * _thisAdjusted = reinterpret_cast<SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A *>(__this + _offset);
	SavedGameMetadataUpdate__ctor_m5B508D8C6829985292C4AFC6D5A99F3A9E9AE259(_thisAdjusted, ___builder0, method);
}
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsDescriptionUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1CBC7A1976D25DC56DA9D85A358BDF4CED9A26C8 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mDescriptionUpdated; }
		bool L_0 = __this->get_mDescriptionUpdated_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mDescriptionUpdated; }
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1CBC7A1976D25DC56DA9D85A358BDF4CED9A26C8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * _thisAdjusted = reinterpret_cast<SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A *>(__this + _offset);
	return SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1CBC7A1976D25DC56DA9D85A358BDF4CED9A26C8(_thisAdjusted, method);
}
// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedDescription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SavedGameMetadataUpdate_get_UpdatedDescription_m8B5AD589A240684AE03B31448BA9808E4FAF399C (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// get { return mNewDescription; }
		String_t* L_0 = __this->get_mNewDescription_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mNewDescription; }
		String_t* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  String_t* SavedGameMetadataUpdate_get_UpdatedDescription_m8B5AD589A240684AE03B31448BA9808E4FAF399C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * _thisAdjusted = reinterpret_cast<SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A *>(__this + _offset);
	return SavedGameMetadataUpdate_get_UpdatedDescription_m8B5AD589A240684AE03B31448BA9808E4FAF399C(_thisAdjusted, method);
}
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsCoverImageUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SavedGameMetadataUpdate_get_IsCoverImageUpdated_m8D0478AA3953E8790FC4AF2B8EDEFDD9AB58B7CB (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mCoverImageUpdated; }
		bool L_0 = __this->get_mCoverImageUpdated_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mCoverImageUpdated; }
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool SavedGameMetadataUpdate_get_IsCoverImageUpdated_m8D0478AA3953E8790FC4AF2B8EDEFDD9AB58B7CB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * _thisAdjusted = reinterpret_cast<SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A *>(__this + _offset);
	return SavedGameMetadataUpdate_get_IsCoverImageUpdated_m8D0478AA3953E8790FC4AF2B8EDEFDD9AB58B7CB(_thisAdjusted, method);
}
// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPngCoverImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2E4D77C771ABD4A2E4EDD225A6F59FD05E4D32F1 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method)
{
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		// get { return mNewPngCoverImage; }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_mNewPngCoverImage_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mNewPngCoverImage; }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2E4D77C771ABD4A2E4EDD225A6F59FD05E4D32F1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * _thisAdjusted = reinterpret_cast<SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A *>(__this + _offset);
	return SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2E4D77C771ABD4A2E4EDD225A6F59FD05E4D32F1(_thisAdjusted, method);
}
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsPlayedTimeUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// get { return mNewPlayedTime.HasValue; }
		Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * L_0 = __this->get_address_of_mNewPlayedTime_4();
		bool L_1 = Nullable_1_get_HasValue_m619F23350E60DA452BA9F320E9EA38E1D933663D_inline((Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 *)L_0, /*hidden argument*/Nullable_1_get_HasValue_m619F23350E60DA452BA9F320E9EA38E1D933663D_RuntimeMethod_var);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// get { return mNewPlayedTime.HasValue; }
		bool L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  bool SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * _thisAdjusted = reinterpret_cast<SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A *>(__this + _offset);
	return SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_mB6780A8E4ACB0D7152A61712A2D0C610EC24416F(_thisAdjusted, method);
}
// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPlayedTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  SavedGameMetadataUpdate_get_UpdatedPlayedTime_m610F974D3E0EAA3F1476BF985C47ED6726012A00 (SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * __this, const RuntimeMethod* method)
{
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// get { return mNewPlayedTime; }
		Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  L_0 = __this->get_mNewPlayedTime_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mNewPlayedTime; }
		Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  SavedGameMetadataUpdate_get_UpdatedPlayedTime_m610F974D3E0EAA3F1476BF985C47ED6726012A00_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A * _thisAdjusted = reinterpret_cast<SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A *>(__this + _offset);
	return SavedGameMetadataUpdate_get_UpdatedPlayedTime_m610F974D3E0EAA3F1476BF985C47ED6726012A00(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
IL2CPP_EXTERN_C void Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshal_pinvoke(const Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40& unmarshaled, Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_pinvoke& marshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'Builder'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
IL2CPP_EXTERN_C void Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshal_pinvoke_back(const Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_pinvoke& marshaled, Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40& unmarshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'Builder'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
IL2CPP_EXTERN_C void Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshal_pinvoke_cleanup(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
IL2CPP_EXTERN_C void Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshal_com(const Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40& unmarshaled, Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_com& marshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'Builder'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
IL2CPP_EXTERN_C void Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshal_com_back(const Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_com& marshaled, Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40& unmarshaled)
{
	Exception_t* ___mNewPlayedTime_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'mNewPlayedTime' of type 'Builder'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___mNewPlayedTime_4Exception, NULL);
}
// Conversion method for clean up from marshalling of: GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
IL2CPP_EXTERN_C void Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshal_com_cleanup(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40_marshaled_com& marshaled)
{
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::WithUpdatedDescription(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, String_t* ___description0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// mNewDescription = Misc.CheckNotNull(description);
		String_t* L_0 = ___description0;
		String_t* L_1 = Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91(L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_mEA112362809ECB8E1AA663393BA1D2E0466ABC91_RuntimeMethod_var);
		__this->set_mNewDescription_1(L_1);
		// mDescriptionUpdated = true;
		__this->set_mDescriptionUpdated_0((bool)1);
		// return this;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_2 = (*(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *)__this);
		V_0 = L_2;
		goto IL_001d;
	}

IL_001d:
	{
		// }
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_3 = V_0;
		return L_3;
	}
}
IL2CPP_EXTERN_C  Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B_AdjustorThunk (RuntimeObject * __this, String_t* ___description0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * _thisAdjusted = reinterpret_cast<Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *>(__this + _offset);
	return Builder_WithUpdatedDescription_m15E5A1DFCF333DE0EADF8E4DD475E75FD96A0A5B(_thisAdjusted, ___description0, method);
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::WithUpdatedPngCoverImage(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedPngCoverImage_mEF27FDD278DFFEF63F224B254933CB2127BBE223 (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___newPngCoverImage0, const RuntimeMethod* method)
{
	Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// mCoverImageUpdated = true;
		__this->set_mCoverImageUpdated_2((bool)1);
		// mNewPngCoverImage = newPngCoverImage;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___newPngCoverImage0;
		__this->set_mNewPngCoverImage_3(L_0);
		// return this;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_1 = (*(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *)__this);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		// }
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedPngCoverImage_mEF27FDD278DFFEF63F224B254933CB2127BBE223_AdjustorThunk (RuntimeObject * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___newPngCoverImage0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * _thisAdjusted = reinterpret_cast<Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *>(__this + _offset);
	return Builder_WithUpdatedPngCoverImage_mEF27FDD278DFFEF63F224B254933CB2127BBE223(_thisAdjusted, ___newPngCoverImage0, method);
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::WithUpdatedPlayedTime(System.TimeSpan)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___newPlayedTime0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// if (newPlayedTime.TotalMilliseconds > ulong.MaxValue)
		double L_0 = TimeSpan_get_TotalMilliseconds_m48B00B27D485CC556C10A5119BC11E1A1E0FE363((TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 *)(&___newPlayedTime0), /*hidden argument*/NULL);
		V_0 = (bool)((((double)L_0) > ((double)(1.8446744073709552E+19)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		// throw new InvalidOperationException("Timespans longer than ulong.MaxValue " +
		//                                     "milliseconds are not allowed");
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_2 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_2, _stringLiteral394E68F963EACC7FF630DAEC06B6F196322850F0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB_RuntimeMethod_var);
	}

IL_0023:
	{
		// mNewPlayedTime = newPlayedTime;
		TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  L_3 = ___newPlayedTime0;
		Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Nullable_1__ctor_m99DC0DFF0CCF30E4DFFED5F8F7ABE8721B73B56D((&L_4), L_3, /*hidden argument*/Nullable_1__ctor_m99DC0DFF0CCF30E4DFFED5F8F7ABE8721B73B56D_RuntimeMethod_var);
		__this->set_mNewPlayedTime_4(L_4);
		// return this;
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_5 = (*(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *)__this);
		V_1 = L_5;
		goto IL_0038;
	}

IL_0038:
	{
		// }
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_6 = V_1;
		return L_6;
	}
}
IL2CPP_EXTERN_C  Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB_AdjustorThunk (RuntimeObject * __this, TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___newPlayedTime0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * _thisAdjusted = reinterpret_cast<Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *>(__this + _offset);
	return Builder_WithUpdatedPlayedTime_m01290CDB042C5EA0555746D5F88178F65CAA42AB(_thisAdjusted, ___newPlayedTime0, method);
}
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate_Builder::Build()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A  Builder_Build_m0E1D8B5A4CEDDBEBA294C8FC744DE04DA810D803 (Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * __this, const RuntimeMethod* method)
{
	SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new SavedGameMetadataUpdate(this);
		Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40  L_0 = (*(Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *)__this);
		SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A  L_1;
		memset((&L_1), 0, sizeof(L_1));
		SavedGameMetadataUpdate__ctor_m5B508D8C6829985292C4AFC6D5A99F3A9E9AE259((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		// }
		SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A  L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  SavedGameMetadataUpdate_tEF922209CC2D28C5B8DE6CD545453EE1B560BA0A  Builder_Build_m0E1D8B5A4CEDDBEBA294C8FC744DE04DA810D803_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 * _thisAdjusted = reinterpret_cast<Builder_t743C495B40C2B5A20789AC3E233502A0A01A1E40 *>(__this + _offset);
	return Builder_Build_m0E1D8B5A4CEDDBEBA294C8FC744DE04DA810D803(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// GooglePlayGames.BasicApi.SignInStatus GooglePlayGames.BasicApi.SignInHelper::ToSignInStatus(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SignInHelper_ToSignInStatus_m859368773DCD252BDA21574CEFAE8B1D55DF08E0 (int32_t ___code0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignInHelper_ToSignInStatus_m859368773DCD252BDA21574CEFAE8B1D55DF08E0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		// Dictionary<int, SignInStatus> dictionary = new Dictionary<int, SignInStatus>()
		// {
		//     {
		//         /* CommonUIStatus.UI_BUSY */ -12, SignInStatus.AlreadyInProgress
		//     },
		//     {
		//         /* CommonStatusCodes.SUCCESS */ 0, SignInStatus.Success
		//     },
		//     {
		//         /* CommonStatusCodes.SIGN_IN_REQUIRED */ 4, SignInStatus.UiSignInRequired
		//     },
		//     {
		//         /* CommonStatusCodes.NETWORK_ERROR */ 7, SignInStatus.NetworkError
		//     },
		//     {
		//         /* CommonStatusCodes.INTERNAL_ERROR */ 8, SignInStatus.InternalError
		//     },
		//     {
		//         /* CommonStatusCodes.DEVELOPER_ERROR */ 10, SignInStatus.DeveloperError
		//     },
		//     {
		//         /* CommonStatusCodes.CANCELED */ 16, SignInStatus.Canceled
		//     },
		//     {
		//         /* CommonStatusCodes.API_NOT_CONNECTED */ 17, SignInStatus.Failed
		//     },
		//     {
		//         /* GoogleSignInStatusCodes.SIGN_IN_FAILED */ 12500, SignInStatus.Failed
		//     },
		//     {
		//         /* GoogleSignInStatusCodes.SIGN_IN_CANCELLED */ 12501, SignInStatus.Canceled
		//     },
		//     {
		//         /* GoogleSignInStatusCodes.SIGN_IN_CURRENTLY_IN_PROGRESS */ 12502, SignInStatus.AlreadyInProgress
		//     },
		// };
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_0 = (Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F *)il2cpp_codegen_object_new(Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m9E5157EB15E8B864F3CC6D184BA6371E2A601EFA(L_0, /*hidden argument*/Dictionary_2__ctor_m9E5157EB15E8B864F3CC6D184BA6371E2A601EFA_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_1 = L_0;
		NullCheck(L_1);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_1, ((int32_t)-12), 6, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_2 = L_1;
		NullCheck(L_2);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_2, 0, 0, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_3 = L_2;
		NullCheck(L_3);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_3, 4, 1, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_4 = L_3;
		NullCheck(L_4);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_4, 7, 3, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_5 = L_4;
		NullCheck(L_5);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_5, 8, 4, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_6 = L_5;
		NullCheck(L_6);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_6, ((int32_t)10), 2, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_7 = L_6;
		NullCheck(L_7);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_7, ((int32_t)16), 5, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_8 = L_7;
		NullCheck(L_8);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_8, ((int32_t)17), 7, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_9 = L_8;
		NullCheck(L_9);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_9, ((int32_t)12500), 7, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_10 = L_9;
		NullCheck(L_10);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_10, ((int32_t)12501), 5, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_11 = L_10;
		NullCheck(L_11);
		Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A(L_11, ((int32_t)12502), 6, /*hidden argument*/Dictionary_2_Add_mD6FFD3590C4BFBD229FF308E101579E069CECE6A_RuntimeMethod_var);
		V_0 = L_11;
		// return dictionary.ContainsKey(code) ? dictionary[code] : SignInStatus.Failed;
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_12 = V_0;
		int32_t L_13 = ___code0;
		NullCheck(L_12);
		bool L_14 = Dictionary_2_ContainsKey_m75C23C174DE8C46B92343E6304A8A7DB305B407B(L_12, L_13, /*hidden argument*/Dictionary_2_ContainsKey_m75C23C174DE8C46B92343E6304A8A7DB305B407B_RuntimeMethod_var);
		if (L_14)
		{
			goto IL_0086;
		}
	}
	{
		G_B3_0 = 7;
		goto IL_008d;
	}

IL_0086:
	{
		Dictionary_2_t96EDDB0108AD92D7F4CF61424D44433CD9FDAA3F * L_15 = V_0;
		int32_t L_16 = ___code0;
		NullCheck(L_15);
		int32_t L_17 = Dictionary_2_get_Item_m9082FAC2380F0EE9C2D576C66BF0B016931027CE(L_15, L_16, /*hidden argument*/Dictionary_2_get_Item_m9082FAC2380F0EE9C2D576C66BF0B016931027CE_RuntimeMethod_var);
		G_B3_0 = ((int32_t)(L_17));
	}

IL_008d:
	{
		V_1 = G_B3_0;
		goto IL_0090;
	}

IL_0090:
	{
		// }
		int32_t L_18 = V_1;
		return L_18;
	}
}
// System.Void GooglePlayGames.BasicApi.SignInHelper::SetPromptUiSignIn(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignInHelper_SetPromptUiSignIn_mFF3C68D64C52D79EB92293B4929FD6D4FF96AE43 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignInHelper_SetPromptUiSignIn_mFF3C68D64C52D79EB92293B4929FD6D4FF96AE43_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	{
		// PlayerPrefs.SetInt(PromptSignInKey, value ? True : False);
		bool L_0 = ___value0;
		G_B1_0 = _stringLiteral6BDC2D6BA4C498FC8F065844D55008C84BC8B7E9;
		if (L_0)
		{
			G_B2_0 = _stringLiteral6BDC2D6BA4C498FC8F065844D55008C84BC8B7E9;
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var);
		int32_t L_1 = ((SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields*)il2cpp_codegen_static_fields_for(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var))->get_False_1();
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0015;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var);
		int32_t L_2 = ((SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields*)il2cpp_codegen_static_fields_for(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var))->get_True_0();
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0015:
	{
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.SignInHelper::ShouldPromptUiSignIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SignInHelper_ShouldPromptUiSignIn_m057F98856138682BB850BAFCB4E5FC918527563B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignInHelper_ShouldPromptUiSignIn_m057F98856138682BB850BAFCB4E5FC918527563B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// return PlayerPrefs.GetInt(PromptSignInKey, True) != False;
		IL2CPP_RUNTIME_CLASS_INIT(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields*)il2cpp_codegen_static_fields_for(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var))->get_True_0();
		int32_t L_1 = PlayerPrefs_GetInt_m1CBBA4989F15BA668EE24950D3C6B56E2ED20BD6(_stringLiteral6BDC2D6BA4C498FC8F065844D55008C84BC8B7E9, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ((SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields*)il2cpp_codegen_static_fields_for(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var))->get_False_1();
		V_0 = (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001d;
	}

IL_001d:
	{
		// }
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void GooglePlayGames.BasicApi.SignInHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignInHelper__ctor_mD4FBF450A975966ABA22A41BFD0787001FAF72BA (SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.SignInHelper::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SignInHelper__cctor_m362E7EFFA0C88CB9F48C1072547815CE968EB927 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SignInHelper__cctor_m362E7EFFA0C88CB9F48C1072547815CE968EB927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static int True = 0;
		((SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields*)il2cpp_codegen_static_fields_for(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var))->set_True_0(0);
		// private static int False = 1;
		((SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_StaticFields*)il2cpp_codegen_static_fields_for(SignInHelper_t3B85C6A6A590592BFC2F489E3DCBA02FE107EC58_il2cpp_TypeInfo_var))->set_False_1(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.BasicApi.Video.VideoCapabilities::.ctor(System.Boolean,System.Boolean,System.Boolean,System.Boolean[],System.Boolean[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoCapabilities__ctor_m93DB6D1B1295CF5B2E31AD7480AE282A62386CAD (VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4 * __this, bool ___isCameraSupported0, bool ___isMicSupported1, bool ___isWriteStorageSupported2, BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___captureModesSupported3, BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___qualityLevelsSupported4, const RuntimeMethod* method)
{
	{
		// internal VideoCapabilities(bool isCameraSupported, bool isMicSupported, bool isWriteStorageSupported,
		//     bool[] captureModesSupported, bool[] qualityLevelsSupported)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// mIsCameraSupported = isCameraSupported;
		bool L_0 = ___isCameraSupported0;
		__this->set_mIsCameraSupported_0(L_0);
		// mIsMicSupported = isMicSupported;
		bool L_1 = ___isMicSupported1;
		__this->set_mIsMicSupported_1(L_1);
		// mIsWriteStorageSupported = isWriteStorageSupported;
		bool L_2 = ___isWriteStorageSupported2;
		__this->set_mIsWriteStorageSupported_2(L_2);
		// mCaptureModesSupported = captureModesSupported;
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_3 = ___captureModesSupported3;
		__this->set_mCaptureModesSupported_3(L_3);
		// mQualityLevelsSupported = qualityLevelsSupported;
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_4 = ___qualityLevelsSupported4;
		__this->set_mQualityLevelsSupported_4(L_4);
		// }
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::get_IsCameraSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCapabilities_get_IsCameraSupported_mF707F3BB960508F74817A00941D220C1E2B9ABCA (VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mIsCameraSupported; }
		bool L_0 = __this->get_mIsCameraSupported_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mIsCameraSupported; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::get_IsMicSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCapabilities_get_IsMicSupported_m75E44B8B14E41C6CA5FCD57F3D9773A40ADAF75C (VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mIsMicSupported; }
		bool L_0 = __this->get_mIsMicSupported_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mIsMicSupported; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::get_IsWriteStorageSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCapabilities_get_IsWriteStorageSupported_m25F2CA6BB587E105C189E6A8E6D11505EEEA233D (VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mIsWriteStorageSupported; }
		bool L_0 = __this->get_mIsWriteStorageSupported_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mIsWriteStorageSupported; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::SupportsCaptureMode(GooglePlayGames.BasicApi.VideoCaptureMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCapabilities_SupportsCaptureMode_m014CA89403FB9C0A684A10ACF384763EB1C4DBFE (VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4 * __this, int32_t ___captureMode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapabilities_SupportsCaptureMode_m014CA89403FB9C0A684A10ACF384763EB1C4DBFE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (captureMode != VideoCaptureMode.Unknown)
		int32_t L_0 = ___captureMode0;
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return mCaptureModesSupported[(int) captureMode];
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_2 = __this->get_mCaptureModesSupported_3();
		int32_t L_3 = ___captureMode0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (uint8_t)(L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = (bool)L_5;
		goto IL_0028;
	}

IL_0018:
	{
		// Logger.w("SupportsCaptureMode called with an unknown captureMode.");
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB(_stringLiteral1415C8E5B39B4B7B13DDF8C2BEBEA3BBEAF5BBAC, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCapabilities::SupportsQualityLevel(GooglePlayGames.BasicApi.VideoQualityLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCapabilities_SupportsQualityLevel_mD6477AF89B8A9C2456B93EF605CD58BC26BDF402 (VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4 * __this, int32_t ___qualityLevel0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapabilities_SupportsQualityLevel_mD6477AF89B8A9C2456B93EF605CD58BC26BDF402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (qualityLevel != VideoQualityLevel.Unknown)
		int32_t L_0 = ___qualityLevel0;
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// return mQualityLevelsSupported[(int) qualityLevel];
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_2 = __this->get_mQualityLevelsSupported_4();
		int32_t L_3 = ___qualityLevel0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (uint8_t)(L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = (bool)L_5;
		goto IL_0028;
	}

IL_0018:
	{
		// Logger.w("SupportsCaptureMode called with an unknown qualityLevel.");
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB(_stringLiteral072C51017526DB281D00402E87CF677C58A7D757, /*hidden argument*/NULL);
		// return false;
		V_1 = (bool)0;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
// System.String GooglePlayGames.BasicApi.Video.VideoCapabilities::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VideoCapabilities_ToString_m794CEF4D0F7C3177434876703967E3C20343CF7B (VideoCapabilities_t97C6BAD0131C353570E253D01E90B56CC57A08B4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCapabilities_ToString_m794CEF4D0F7C3177434876703967E3C20343CF7B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * G_B2_0 = NULL;
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	int32_t G_B2_3 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B2_4 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B2_5 = NULL;
	String_t* G_B2_6 = NULL;
	Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * G_B1_0 = NULL;
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	int32_t G_B1_3 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B1_4 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B1_5 = NULL;
	String_t* G_B1_6 = NULL;
	Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * G_B4_0 = NULL;
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* G_B4_1 = NULL;
	String_t* G_B4_2 = NULL;
	int32_t G_B4_3 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B4_4 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B4_5 = NULL;
	String_t* G_B4_6 = NULL;
	Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * G_B3_0 = NULL;
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	int32_t G_B3_3 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B3_4 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B3_5 = NULL;
	String_t* G_B3_6 = NULL;
	{
		// return string.Format(
		//     "[VideoCapabilities: mIsCameraSupported={0}, mIsMicSupported={1}, mIsWriteStorageSupported={2}, " +
		//     "mCaptureModesSupported={3}, mQualityLevelsSupported={4}]",
		//     mIsCameraSupported,
		//     mIsMicSupported,
		//     mIsWriteStorageSupported,
		//     string.Join(",", mCaptureModesSupported.Select(p => p.ToString()).ToArray()),
		//     string.Join(",", mQualityLevelsSupported.Select(p => p.ToString()).ToArray()));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		bool L_2 = __this->get_mIsCameraSupported_0();
		bool L_3 = L_2;
		RuntimeObject * L_4 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		bool L_6 = __this->get_mIsMicSupported_1();
		bool L_7 = L_6;
		RuntimeObject * L_8 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		bool L_10 = __this->get_mIsWriteStorageSupported_2();
		bool L_11 = L_10;
		RuntimeObject * L_12 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_9;
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_14 = __this->get_mCaptureModesSupported_3();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var);
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_15 = ((U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var))->get_U3CU3E9__14_0_1();
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_16 = L_15;
		G_B1_0 = L_16;
		G_B1_1 = L_14;
		G_B1_2 = _stringLiteral5C10B5B2CD673A0616D529AA5234B12EE7153808;
		G_B1_3 = 3;
		G_B1_4 = L_13;
		G_B1_5 = L_13;
		G_B1_6 = _stringLiteral135EFDF4B044C63D9D43D56C023F20057C9C5538;
		if (L_16)
		{
			G_B2_0 = L_16;
			G_B2_1 = L_14;
			G_B2_2 = _stringLiteral5C10B5B2CD673A0616D529AA5234B12EE7153808;
			G_B2_3 = 3;
			G_B2_4 = L_13;
			G_B2_5 = L_13;
			G_B2_6 = _stringLiteral135EFDF4B044C63D9D43D56C023F20057C9C5538;
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var);
		U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * L_17 = ((U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_18 = (Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 *)il2cpp_codegen_object_new(Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130_il2cpp_TypeInfo_var);
		Func_2__ctor_mBAD3E8F66408158F3CBC5309309B4545614D38F5(L_18, L_17, (intptr_t)((intptr_t)U3CU3Ec_U3CToStringU3Eb__14_0_m75FE11CA93BBBB7EE560F5115E951A2DE484A04B_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mBAD3E8F66408158F3CBC5309309B4545614D38F5_RuntimeMethod_var);
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_19 = L_18;
		((U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var))->set_U3CU3E9__14_0_1(L_19);
		G_B2_0 = L_19;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
		G_B2_4 = G_B1_4;
		G_B2_5 = G_B1_5;
		G_B2_6 = G_B1_6;
	}

IL_0062:
	{
		RuntimeObject* L_20 = Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisString_t_mA66BDED9E6FDCFE3BB7C27E0C69135B0EB24C4CF((RuntimeObject*)(RuntimeObject*)G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisString_t_mA66BDED9E6FDCFE3BB7C27E0C69135B0EB24C4CF_RuntimeMethod_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = Enumerable_ToArray_TisString_t_m34F0EB317BEC1783F5D3E46852744966D246040C(L_20, /*hidden argument*/Enumerable_ToArray_TisString_t_m34F0EB317BEC1783F5D3E46852744966D246040C_RuntimeMethod_var);
		String_t* L_22 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(G_B2_2, L_21, /*hidden argument*/NULL);
		NullCheck(G_B2_4);
		ArrayElementTypeCheck (G_B2_4, L_22);
		(G_B2_4)->SetAt(static_cast<il2cpp_array_size_t>(G_B2_3), (RuntimeObject *)L_22);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = G_B2_5;
		BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* L_24 = __this->get_mQualityLevelsSupported_4();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var);
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_25 = ((U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var))->get_U3CU3E9__14_1_2();
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_26 = L_25;
		G_B3_0 = L_26;
		G_B3_1 = L_24;
		G_B3_2 = _stringLiteral5C10B5B2CD673A0616D529AA5234B12EE7153808;
		G_B3_3 = 4;
		G_B3_4 = L_23;
		G_B3_5 = L_23;
		G_B3_6 = G_B2_6;
		if (L_26)
		{
			G_B4_0 = L_26;
			G_B4_1 = L_24;
			G_B4_2 = _stringLiteral5C10B5B2CD673A0616D529AA5234B12EE7153808;
			G_B4_3 = 4;
			G_B4_4 = L_23;
			G_B4_5 = L_23;
			G_B4_6 = G_B2_6;
			goto IL_009e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var);
		U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * L_27 = ((U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_28 = (Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 *)il2cpp_codegen_object_new(Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130_il2cpp_TypeInfo_var);
		Func_2__ctor_mBAD3E8F66408158F3CBC5309309B4545614D38F5(L_28, L_27, (intptr_t)((intptr_t)U3CU3Ec_U3CToStringU3Eb__14_1_m7F792E66E4DB839ECE1667C2895A77226FBE14BF_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mBAD3E8F66408158F3CBC5309309B4545614D38F5_RuntimeMethod_var);
		Func_2_t78EFB663E88E49A932E63322D3AA52CA56E30130 * L_29 = L_28;
		((U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var))->set_U3CU3E9__14_1_2(L_29);
		G_B4_0 = L_29;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		G_B4_3 = G_B3_3;
		G_B4_4 = G_B3_4;
		G_B4_5 = G_B3_5;
		G_B4_6 = G_B3_6;
	}

IL_009e:
	{
		RuntimeObject* L_30 = Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisString_t_mA66BDED9E6FDCFE3BB7C27E0C69135B0EB24C4CF((RuntimeObject*)(RuntimeObject*)G_B4_1, G_B4_0, /*hidden argument*/Enumerable_Select_TisBoolean_tB53F6830F670160873277339AA58F15CAED4399C_TisString_t_mA66BDED9E6FDCFE3BB7C27E0C69135B0EB24C4CF_RuntimeMethod_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_31 = Enumerable_ToArray_TisString_t_m34F0EB317BEC1783F5D3E46852744966D246040C(L_30, /*hidden argument*/Enumerable_ToArray_TisString_t_m34F0EB317BEC1783F5D3E46852744966D246040C_RuntimeMethod_var);
		String_t* L_32 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(G_B4_2, L_31, /*hidden argument*/NULL);
		NullCheck(G_B4_4);
		ArrayElementTypeCheck (G_B4_4, L_32);
		(G_B4_4)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_3), (RuntimeObject *)L_32);
		String_t* L_33 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(G_B4_6, G_B4_5, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_00b6;
	}

IL_00b6:
	{
		// }
		String_t* L_34 = V_0;
		return L_34;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m570ADC6E83A04B439A89A6F2CA68567764DC2A40 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m570ADC6E83A04B439A89A6F2CA68567764DC2A40_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * L_0 = (U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B *)il2cpp_codegen_object_new(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m4BD30BD945191F668A932130240743B6CCFA046E(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4BD30BD945191F668A932130240743B6CCFA046E (U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::<ToString>b__14_0(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3CToStringU3Eb__14_0_m75FE11CA93BBBB7EE560F5115E951A2DE484A04B (U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * __this, bool ___p0, const RuntimeMethod* method)
{
	{
		// string.Join(",", mCaptureModesSupported.Select(p => p.ToString()).ToArray()),
		String_t* L_0 = Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301((bool*)(&___p0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String GooglePlayGames.BasicApi.Video.VideoCapabilities_<>c::<ToString>b__14_1(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3CToStringU3Eb__14_1_m7F792E66E4DB839ECE1667C2895A77226FBE14BF (U3CU3Ec_tB68F05A6355ADF75A9D3F3E72228E2C023E4D28B * __this, bool ___p0, const RuntimeMethod* method)
{
	{
		// string.Join(",", mQualityLevelsSupported.Select(p => p.ToString()).ToArray()));
		String_t* L_0 = Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301((bool*)(&___p0), /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.BasicApi.Video.VideoCaptureState::.ctor(System.Boolean,GooglePlayGames.BasicApi.VideoCaptureMode,GooglePlayGames.BasicApi.VideoQualityLevel,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoCaptureState__ctor_m9FB8FE52A8A8758BB5ABCF1EAEAE522FD93378E5 (VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F * __this, bool ___isCapturing0, int32_t ___captureMode1, int32_t ___qualityLevel2, bool ___isOverlayVisible3, bool ___isPaused4, const RuntimeMethod* method)
{
	{
		// internal VideoCaptureState(bool isCapturing, VideoCaptureMode captureMode,
		//     VideoQualityLevel qualityLevel, bool isOverlayVisible, bool isPaused)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// mIsCapturing = isCapturing;
		bool L_0 = ___isCapturing0;
		__this->set_mIsCapturing_0(L_0);
		// mCaptureMode = captureMode;
		int32_t L_1 = ___captureMode1;
		__this->set_mCaptureMode_1(L_1);
		// mQualityLevel = qualityLevel;
		int32_t L_2 = ___qualityLevel2;
		__this->set_mQualityLevel_2(L_2);
		// mIsOverlayVisible = isOverlayVisible;
		bool L_3 = ___isOverlayVisible3;
		__this->set_mIsOverlayVisible_3(L_3);
		// mIsPaused = isPaused;
		bool L_4 = ___isPaused4;
		__this->set_mIsPaused_4(L_4);
		// }
		return;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::get_IsCapturing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCaptureState_get_IsCapturing_m7C38813203EB92BE4D9B82285039FE14D22C81C0 (VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mIsCapturing; }
		bool L_0 = __this->get_mIsCapturing_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mIsCapturing; }
		bool L_1 = V_0;
		return L_1;
	}
}
// GooglePlayGames.BasicApi.VideoCaptureMode GooglePlayGames.BasicApi.Video.VideoCaptureState::get_CaptureMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t VideoCaptureState_get_CaptureMode_m7D36F460E825166FBE22C385EB811B9A723BEBD9 (VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return mCaptureMode; }
		int32_t L_0 = __this->get_mCaptureMode_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mCaptureMode; }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// GooglePlayGames.BasicApi.VideoQualityLevel GooglePlayGames.BasicApi.Video.VideoCaptureState::get_QualityLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t VideoCaptureState_get_QualityLevel_m1965EE6230E442EC1A852D5D620272D6F2F472E9 (VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// get { return mQualityLevel; }
		int32_t L_0 = __this->get_mQualityLevel_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mQualityLevel; }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::get_IsOverlayVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCaptureState_get_IsOverlayVisible_mA7A531329DBDBD0DCA777A5984455850011F5A6C (VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mIsOverlayVisible; }
		bool L_0 = __this->get_mIsOverlayVisible_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mIsOverlayVisible; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean GooglePlayGames.BasicApi.Video.VideoCaptureState::get_IsPaused()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VideoCaptureState_get_IsPaused_m67EE8C8DFA66ED88D594F52CC483295CE334ED89 (VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// get { return mIsPaused; }
		bool L_0 = __this->get_mIsPaused_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// get { return mIsPaused; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.String GooglePlayGames.BasicApi.Video.VideoCaptureState::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VideoCaptureState_ToString_m31D72AE6DCABEE2EB09502165175970030712606 (VideoCaptureState_t92CFCE1DF60983CAA41F2FE5F691208C79A0DF4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoCaptureState_ToString_m31D72AE6DCABEE2EB09502165175970030712606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// return string.Format("[VideoCaptureState: mIsCapturing={0}, mCaptureMode={1}, mQualityLevel={2}, " +
		//                      "mIsOverlayVisible={3}, mIsPaused={4}]",
		//     mIsCapturing,
		//     mCaptureMode.ToString(),
		//     mQualityLevel.ToString(),
		//     mIsOverlayVisible,
		//     mIsPaused);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		bool L_2 = __this->get_mIsCapturing_0();
		bool L_3 = L_2;
		RuntimeObject * L_4 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		int32_t* L_6 = __this->get_address_of_mCaptureMode_1();
		RuntimeObject * L_7 = Box(VideoCaptureMode_tBF0E331D46B351F8E6097418294116AB4D363609_il2cpp_TypeInfo_var, L_6);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		*L_6 = *(int32_t*)UnBox(L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		int32_t* L_10 = __this->get_address_of_mQualityLevel_2();
		RuntimeObject * L_11 = Box(VideoQualityLevel_t9E3687DDDE2ABCAE7A1E98DF796AA4CE6535BA52_il2cpp_TypeInfo_var, L_10);
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		*L_10 = *(int32_t*)UnBox(L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_9;
		bool L_14 = __this->get_mIsOverlayVisible_3();
		bool L_15 = L_14;
		RuntimeObject * L_16 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = L_13;
		bool L_18 = __this->get_mIsPaused_4();
		bool L_19 = L_18;
		RuntimeObject * L_20 = Box(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_20);
		String_t* L_21 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteralEBBD5CD2F3EABC5C6D7DF0BB6ED351BCD4DDAC05, L_17, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_0066;
	}

IL_0066:
	{
		// }
		String_t* L_22 = V_0;
		return L_22;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean GooglePlayGames.OurUtils.Logger::get_DebugLogEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Logger_get_DebugLogEnabled_m3953BFF21DD3B54D640C75656E846C07EF02FB20 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_get_DebugLogEnabled_m3953BFF21DD3B54D640C75656E846C07EF02FB20_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// get { return debugLogEnabled; }
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->get_debugLogEnabled_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		// get { return debugLogEnabled; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::set_DebugLogEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_set_DebugLogEnabled_m5787BDC789E038FC7142BC2EF160318D0D1A918E (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_set_DebugLogEnabled_m5787BDC789E038FC7142BC2EF160318D0D1A918E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { debugLogEnabled = value; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->set_debugLogEnabled_0(L_0);
		// set { debugLogEnabled = value; }
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.Logger::get_WarningLogEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Logger_get_WarningLogEnabled_m2E33359F4E5A78915FD65DD1C51BDCA7AB54F833 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_get_WarningLogEnabled_m2E33359F4E5A78915FD65DD1C51BDCA7AB54F833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// get { return warningLogEnabled; }
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->get_warningLogEnabled_1();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		// get { return warningLogEnabled; }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::set_WarningLogEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_set_WarningLogEnabled_mB416AC86C073873489EA2EFB51C2A70A93E148BF (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_set_WarningLogEnabled_mB416AC86C073873489EA2EFB51C2A70A93E148BF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { warningLogEnabled = value; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->set_warningLogEnabled_1(L_0);
		// set { warningLogEnabled = value; }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::d(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_d_m95C0BD29B60442754276E97D1FB7ED05E4AC31CE (String_t* ___msg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_d_m95C0BD29B60442754276E97D1FB7ED05E4AC31CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 * V_0 = NULL;
	bool V_1 = false;
	{
		U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 * L_0 = (U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass8_0__ctor_m8C4C8977B9B5F82E13A6EFAE0F54ECE849EC214B(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 * L_1 = V_0;
		String_t* L_2 = ___msg0;
		NullCheck(L_1);
		L_1->set_msg_0(L_2);
		// if (debugLogEnabled)
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		bool L_3 = ((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->get_debugLogEnabled_0();
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		// PlayGamesHelperObject.RunOnGameThread(() =>
		//     Debug.Log(ToLogMessage(string.Empty, "DEBUG", msg)));
		U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 * L_5 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_6 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass8_0_U3CdU3Eb__0_m48020CDD6BCC8C1325960C7B8ACAF21C9C46590A_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE(L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::w(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB (String_t* ___msg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_w_mC5E0B8747075DB3753444575C6297436A1BDCCBB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 * V_0 = NULL;
	bool V_1 = false;
	{
		U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 * L_0 = (U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass9_0__ctor_m7FE9968DBCD1D95BA7D9CE20A30FDF7976C1DDE1(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 * L_1 = V_0;
		String_t* L_2 = ___msg0;
		NullCheck(L_1);
		L_1->set_msg_0(L_2);
		// if (warningLogEnabled)
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		bool L_3 = ((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->get_warningLogEnabled_1();
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		// PlayGamesHelperObject.RunOnGameThread(() =>
		//     Debug.LogWarning(ToLogMessage("!!!", "WARNING", msg)));
		U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 * L_5 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_6 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass9_0_U3CwU3Eb__0_m357B91D2A7DFEF1DA7865CAF4E998B09A0A120E7_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE(L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::e(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8 (String_t* ___msg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF * V_0 = NULL;
	bool V_1 = false;
	{
		U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF * L_0 = (U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass10_0__ctor_mA3755E25A067B9D324C45DF55C4088DD1480E0C6(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF * L_1 = V_0;
		String_t* L_2 = ___msg0;
		NullCheck(L_1);
		L_1->set_msg_0(L_2);
		// if (warningLogEnabled)
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		bool L_3 = ((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->get_warningLogEnabled_1();
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		// PlayGamesHelperObject.RunOnGameThread(() =>
		//     Debug.LogWarning(ToLogMessage("***", "ERROR", msg)));
		U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF * L_5 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_6 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass10_0_U3CeU3Eb__0_m0B9AE2FE606892EF2909B280223B84E4646E8F73_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE(L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.String GooglePlayGames.OurUtils.Logger::describe(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Logger_describe_m6CF7621BCB43B5A4F721ECB71FE509F1EAFD0EB1 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___b0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_describe_m6CF7621BCB43B5A4F721ECB71FE509F1EAFD0EB1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		// return b == null ? "(null)" : "byte[" + b.Length + "]";
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___b0;
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___b0;
		NullCheck(L_1);
		int32_t L_2 = (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))));
		RuntimeObject * L_3 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteralD5C6A5F48C3935F1D696BB81558492909CB4F13C, L_3, _stringLiteral4FF447B8EF42CA51FA6FB287BED8D40F49BE58F1, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_0022;
	}

IL_001d:
	{
		G_B3_0 = _stringLiteralFDE49AA00F4966A6ABC348B5B3B985032EABF411;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		goto IL_0025;
	}

IL_0025:
	{
		// }
		String_t* L_5 = V_0;
		return L_5;
	}
}
// System.String GooglePlayGames.OurUtils.Logger::ToLogMessage(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C (String_t* ___prefix0, String_t* ___logType1, String_t* ___msg2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  V_1;
	memset((&V_1), 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * G_B4_0 = NULL;
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * G_B3_0 = NULL;
	{
		// string timeString = null;
		V_0 = (String_t*)NULL;
	}

IL_0003:
	try
	{ // begin try (depth: 1)
		// timeString = DateTime.Now.ToString("MM/dd/yy H:mm:ss zzz");
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var);
		DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  L_0 = DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2(/*hidden argument*/NULL);
		V_1 = L_0;
		String_t* L_1 = DateTime_ToString_m203C5710CD7AB2F5F1B2D9DA1DFD45BB3774179A((DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 *)(&V_1), _stringLiteral689B3D123355CBA2415D4FE457847D53ED8ADB52, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_004a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001a;
		throw e;
	}

CATCH_001a:
	{ // begin catch(System.Exception)
		{
			// catch (Exception)
			// PlayGamesHelperObject.RunOnGameThread(() =>
			//     Debug.LogWarning("*** [Play Games Plugin " + PluginVersion.VersionString + "] ERROR: Failed to format DateTime.Now"));
			IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var);
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_2 = ((U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var))->get_U3CU3E9__12_0_1();
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_3 = L_2;
			G_B3_0 = L_3;
			if (L_3)
			{
				G_B4_0 = L_3;
				goto IL_003b;
			}
		}

IL_0024:
		{
			IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var);
			U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * L_4 = ((U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_5 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
			Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec_U3CToLogMessageU3Eb__12_0_m1B59C28B2C712520830759AEE37ECA1FFE239DF0_RuntimeMethod_var), /*hidden argument*/NULL);
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_6 = L_5;
			((U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var))->set_U3CU3E9__12_0_1(L_6);
			G_B4_0 = L_6;
		}

IL_003b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
			PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE(G_B4_0, /*hidden argument*/NULL);
			// timeString = string.Empty;
			String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
			V_0 = L_7;
			goto IL_004a;
		}
	} // end catch (depth: 1)

IL_004a:
	{
		// return string.Format("{0} [Play Games Plugin " + PluginVersion.VersionString+ "] {1} {2}: {3}",
		//     prefix, timeString, logType, msg);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_8 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_8;
		String_t* L_10 = ___prefix0;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_9;
		String_t* L_12 = V_0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_11;
		String_t* L_14 = ___logType1;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_14);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_15 = L_13;
		String_t* L_16 = ___msg2;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteralA1EBE8CFEDDD9906386BE64D944F2E6663CC9C4B, L_15, /*hidden argument*/NULL);
		V_2 = L_17;
		goto IL_006d;
	}

IL_006d:
	{
		// }
		String_t* L_18 = V_2;
		return L_18;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger__ctor_mEC78EBB35D3E40205D9EFF7CEE10E40FBE187CAF (Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Logger__cctor_m42BF51365CFF03BA353B071F75D18DE0C7F277FD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger__cctor_m42BF51365CFF03BA353B071F75D18DE0C7F277FD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static bool debugLogEnabled = false;
		((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->set_debugLogEnabled_0((bool)0);
		// private static bool warningLogEnabled = true;
		((Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_StaticFields*)il2cpp_codegen_static_fields_for(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var))->set_warningLogEnabled_1((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.OurUtils.Logger_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mE9D701C70C5874BD30D40926128A74E673F87ED1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mE9D701C70C5874BD30D40926128A74E673F87ED1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * L_0 = (U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 *)il2cpp_codegen_object_new(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mAF62846782BCBABBC4CE9AD9D5A1EC351B71CE33(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAF62846782BCBABBC4CE9AD9D5A1EC351B71CE33 (U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger_<>c::<ToLogMessage>b__12_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CToLogMessageU3Eb__12_0_m1B59C28B2C712520830759AEE37ECA1FFE239DF0 (U3CU3Ec_tF97C38A8A5EC6DE3F36C9498E8A1B953E8D21FE5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CToLogMessageU3Eb__12_0_m1B59C28B2C712520830759AEE37ECA1FFE239DF0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.LogWarning("*** [Play Games Plugin " + PluginVersion.VersionString + "] ERROR: Failed to format DateTime.Now"));
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(_stringLiteral14CA22B9873E0A288F1FEDAA44E420F347889689, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_mA3755E25A067B9D324C45DF55C4088DD1480E0C6 (U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass10_0::<e>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0_U3CeU3Eb__0_m0B9AE2FE606892EF2909B280223B84E4646E8F73 (U3CU3Ec__DisplayClass10_0_tED23B8EDB2B98BCB9091E69C00034307C3E199FF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass10_0_U3CeU3Eb__0_m0B9AE2FE606892EF2909B280223B84E4646E8F73_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.LogWarning(ToLogMessage("***", "ERROR", msg)));
		String_t* L_0 = __this->get_msg_0();
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		String_t* L_1 = Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C(_stringLiteral36C3EAA0E1E290F41E2810BAE8D9502C785E92D9, _stringLiteral0B99CEBE565822C64AC5D84AECB00FE40E59CBD3, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass8_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass8_0__ctor_m8C4C8977B9B5F82E13A6EFAE0F54ECE849EC214B (U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass8_0::<d>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass8_0_U3CdU3Eb__0_m48020CDD6BCC8C1325960C7B8ACAF21C9C46590A (U3CU3Ec__DisplayClass8_0_tD477209F4BBB4D17DAC902DA46F9B9BB7FA94A43 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass8_0_U3CdU3Eb__0_m48020CDD6BCC8C1325960C7B8ACAF21C9C46590A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log(ToLogMessage(string.Empty, "DEBUG", msg)));
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		String_t* L_1 = __this->get_msg_0();
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		String_t* L_2 = Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C(L_0, _stringLiteral3F67E8F4EECF241B91F4CC8C976A487ADE34D09D, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass9_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0__ctor_m7FE9968DBCD1D95BA7D9CE20A30FDF7976C1DDE1 (U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger_<>c__DisplayClass9_0::<w>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0_U3CwU3Eb__0_m357B91D2A7DFEF1DA7865CAF4E998B09A0A120E7 (U3CU3Ec__DisplayClass9_0_t041FE7E5ADB844116427DCCE7DD85D5115C7A679 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass9_0_U3CwU3Eb__0_m357B91D2A7DFEF1DA7865CAF4E998B09A0A120E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.LogWarning(ToLogMessage("!!!", "WARNING", msg)));
		String_t* L_0 = __this->get_msg_0();
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
		String_t* L_1 = Logger_ToLogMessage_mFECA70F9EBCC32A4C25E9ACE99C8A93E5A181A4C(_stringLiteral9A7B006D203B362C8CEF6DA001685678FC1D463A, _stringLiteralFD3EDC641024A335A508FDACEFB5F51DED5905CC, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean GooglePlayGames.OurUtils.Misc::BuffersAreIdentical(System.Byte[],System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Misc_BuffersAreIdentical_mC0F680186480CD64115596AD689421C6E0C38387 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___a0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___b1, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	int32_t G_B5_0 = 0;
	{
		// if (a == b)
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___a0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___b1;
		V_0 = (bool)((((RuntimeObject*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)L_0) == ((RuntimeObject*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		// return true;
		V_1 = (bool)1;
		goto IL_006b;
	}

IL_000e:
	{
		// if (a == null || b == null)
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___a0;
		if (!L_3)
		{
			goto IL_0017;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___b1;
		G_B5_0 = ((((RuntimeObject*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B5_0 = 1;
	}

IL_0018:
	{
		V_2 = (bool)G_B5_0;
		bool L_5 = V_2;
		if (!L_5)
		{
			goto IL_0021;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_006b;
	}

IL_0021:
	{
		// if (a.Length != b.Length)
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = ___a0;
		NullCheck(L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ___b1;
		NullCheck(L_7);
		V_3 = (bool)((((int32_t)((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_8 = V_3;
		if (!L_8)
		{
			goto IL_0035;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_006b;
	}

IL_0035:
	{
		// for (int i = 0; i < a.Length; i++)
		V_4 = 0;
		goto IL_005a;
	}

IL_003a:
	{
		// if (a[i] != b[i])
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = ___a0;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		uint8_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = ___b1;
		int32_t L_14 = V_4;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_5 = (bool)((((int32_t)((((int32_t)L_12) == ((int32_t)L_16))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_17 = V_5;
		if (!L_17)
		{
			goto IL_0053;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_006b;
	}

IL_0053:
	{
		// for (int i = 0; i < a.Length; i++)
		int32_t L_18 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_005a:
	{
		// for (int i = 0; i < a.Length; i++)
		int32_t L_19 = V_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = ___a0;
		NullCheck(L_20);
		V_6 = (bool)((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))))))? 1 : 0);
		bool L_21 = V_6;
		if (L_21)
		{
			goto IL_003a;
		}
	}
	{
		// return true;
		V_1 = (bool)1;
		goto IL_006b;
	}

IL_006b:
	{
		// }
		bool L_22 = V_1;
		return L_22;
	}
}
// System.Byte[] GooglePlayGames.OurUtils.Misc::GetSubsetBytes(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___array0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_5 = NULL;
	int32_t G_B5_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B15_0 = 0;
	{
		// if (array == null)
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = ___array0;
		V_1 = (bool)((((RuntimeObject*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// throw new ArgumentNullException("array");
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, _stringLiteral19EDC1210777BA4D45049C29280D9CC5E1064C25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965_RuntimeMethod_var);
	}

IL_0015:
	{
		// if (offset < 0 || offset >= array.Length)
		int32_t L_3 = ___offset1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_4 = ___offset1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ___array0;
		NullCheck(L_5);
		G_B5_0 = ((((int32_t)((((int32_t)L_4) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B5_0 = 1;
	}

IL_0025:
	{
		V_2 = (bool)G_B5_0;
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		// throw new ArgumentOutOfRangeException("offset");
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_7 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_7, _stringLiteral53A610E925BBC0A175E365D31241AE75AEEAD651, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965_RuntimeMethod_var);
	}

IL_0035:
	{
		// if (length < 0 || (array.Length - offset) < length)
		int32_t L_8 = ___length2;
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___offset1;
		int32_t L_11 = ___length2;
		G_B10_0 = ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))), (int32_t)L_10))) < ((int32_t)L_11))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B10_0 = 1;
	}

IL_0044:
	{
		V_3 = (bool)G_B10_0;
		bool L_12 = V_3;
		if (!L_12)
		{
			goto IL_0054;
		}
	}
	{
		// throw new ArgumentOutOfRangeException("length");
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_13 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_13, _stringLiteral3D54973F528B01019A58A52D34D518405A01B891, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, Misc_GetSubsetBytes_mFEC986BA8604E6150FBF9A5D2A1DEF7481FF4965_RuntimeMethod_var);
	}

IL_0054:
	{
		// if (offset == 0 && length == array.Length)
		int32_t L_14 = ___offset1;
		if (L_14)
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_15 = ___length2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = ___array0;
		NullCheck(L_16);
		G_B15_0 = ((((int32_t)L_15) == ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length))))))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B15_0 = 0;
	}

IL_0060:
	{
		V_4 = (bool)G_B15_0;
		bool L_17 = V_4;
		if (!L_17)
		{
			goto IL_006c;
		}
	}
	{
		// return array;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = ___array0;
		V_5 = L_18;
		goto IL_0083;
	}

IL_006c:
	{
		// byte[] piece = new byte[length];
		int32_t L_19 = ___length2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_19);
		V_0 = L_20;
		// Array.Copy(array, offset, piece, 0, length);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_21 = ___array0;
		int32_t L_22 = ___offset1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = V_0;
		int32_t L_24 = ___length2;
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_21, L_22, (RuntimeArray *)(RuntimeArray *)L_23, 0, L_24, /*hidden argument*/NULL);
		// return piece;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = V_0;
		V_5 = L_25;
		goto IL_0083;
	}

IL_0083:
	{
		// }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = V_5;
		return L_26;
	}
}
// System.Boolean GooglePlayGames.OurUtils.Misc::IsApiException(UnityEngine.AndroidJavaObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Misc_IsApiException_m303491E5CA3BFA339C9A5EBFFE55F7C0C3BB9F99 (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * ___exception0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Misc_IsApiException_m303491E5CA3BFA339C9A5EBFFE55F7C0C3BB9F99_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	{
		// var exceptionClassName = exception.Call<AndroidJavaObject>("getClass")
		//     .Call<String>("getName");
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_0 = ___exception0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_0);
		AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * L_2 = AndroidJavaObject_Call_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m775AB90594C5F27D6099ED61119EF3608FD1001D(L_0, _stringLiteral4F05CBFCA4DFE76B99B142F609CDCF00D44FA247, L_1, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_m775AB90594C5F27D6099ED61119EF3608FD1001D_RuntimeMethod_var);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_4 = AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA(L_2, _stringLiteralFA98C1FD2CA6FC89B5ED010FD16AA461F50AFB3E, L_3, /*hidden argument*/AndroidJavaObject_Call_TisString_t_m5EAE53C9E2A8893FD8FEA710378D22C162A0FDEA_RuntimeMethod_var);
		V_0 = L_4;
		// return exceptionClassName == "com.google.android.gms.common.api.ApiException";
		String_t* L_5 = V_0;
		bool L_6 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_5, _stringLiteral3B0F99F6AB7B8C32DCDA4C7DA28B7D760CE8AC05, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002f;
	}

IL_002f:
	{
		// }
		bool L_7 = V_1;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::CreateObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_CreateObject_m35E4ACCC42D970BFD0BA33D133F35B51A2137601 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_CreateObject_m35E4ACCC42D970BFD0BA33D133F35B51A2137601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_2 = NULL;
	{
		// if (instance != null)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		goto IL_004f;
	}

IL_0013:
	{
		// if (Application.isPlaying)
		bool L_3 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		// GameObject obj = new GameObject("PlayGames_QueueRunner");
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_5, _stringLiteral8BEAF8B48CD239C5B9FD4F82FBBD3B06F143113D, /*hidden argument*/NULL);
		V_2 = L_5;
		// DontDestroyOnLoad(obj);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207(L_6, /*hidden argument*/NULL);
		// instance = obj.AddComponent<PlayGamesHelperObject>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = V_2;
		NullCheck(L_7);
		PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * L_8 = GameObject_AddComponent_TisPlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_m89B34D1AD3E162ECA397870BC80FD50E6A99DA3E(L_7, /*hidden argument*/GameObject_AddComponent_TisPlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_m89B34D1AD3E162ECA397870BC80FD50E6A99DA3E_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_instance_4(L_8);
		goto IL_004f;
	}

IL_003d:
	{
		// instance = new PlayGamesHelperObject();
		PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * L_9 = (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F *)il2cpp_codegen_object_new(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject__ctor_m6F2894860B6C363E51DFB53E692ADCDCCB10D9B1(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_instance_4(L_9);
		// sIsDummy = true;
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sIsDummy_5((bool)1);
	}

IL_004f:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_Awake_m38AAE71B9B1F9F4D3F98E863119E2235A3B8251F (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_Awake_m38AAE71B9B1F9F4D3F98E863119E2235A3B8251F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DontDestroyOnLoad(gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_OnDisable_m395CECEDE06F5F33F23B85418FCFB7FDBFE8B82C (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_OnDisable_m395CECEDE06F5F33F23B85418FCFB7FDBFE8B82C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (instance == this)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// instance = null;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_instance_4((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F *)NULL);
	}

IL_0018:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_RunCoroutine_mCDC6CBF287B6D8EBE869A970C5770808A95DC194 (RuntimeObject* ___action0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RunCoroutine_mCDC6CBF287B6D8EBE869A970C5770808A95DC194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 * V_0 = NULL;
	bool V_1 = false;
	{
		U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 * L_0 = (U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass10_0__ctor_mC187ED07653BC33D8A6BA353FC0F6A74C1628F91(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 * L_1 = V_0;
		RuntimeObject* L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		// if (instance != null)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * L_3 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		// RunOnGameThread(() => instance.StartCoroutine(action));
		U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 * L_6 = V_0;
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_7 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass10_0_U3CRunCoroutineU3Eb__0_m4C5D12A9EFD2B686E793203491194E13C6C5A46D_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE(L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunOnGameThread(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * V_2 = NULL;
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (action == null)
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_0 = ___action0;
		V_0 = (bool)((((RuntimeObject*)(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// throw new ArgumentNullException("action");
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, _stringLiteral34EB4C4EF005207E8B8F916B9F1FFFACCCD6945E, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, PlayGamesHelperObject_RunOnGameThread_m71BD8D4C0C9B9374134CE0A5C971C6CF768BB1CE_RuntimeMethod_var);
	}

IL_0015:
	{
		// if (sIsDummy)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		bool L_3 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sIsDummy_5();
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// return;
		goto IL_0055;
	}

IL_0021:
	{
		// lock (sQueue)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_5 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sQueue_6();
		V_2 = L_5;
		V_3 = (bool)0;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_6 = V_2;
		Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_6, (bool*)(&V_3), /*hidden argument*/NULL);
		// sQueue.Add(action);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_7 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sQueue_6();
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_8 = ___action0;
		NullCheck(L_7);
		List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B(L_7, L_8, /*hidden argument*/List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B_RuntimeMethod_var);
		// sQueueEmpty = false;
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sQueueEmpty_8(0);
		IL2CPP_LEAVE(0x55, FINALLY_004a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_3;
			if (!L_9)
			{
				goto IL_0054;
			}
		}

IL_004d:
		{
			List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_10 = V_2;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		}

IL_0054:
		{
			IL2CPP_END_FINALLY(74)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x55, IL_0055)
	}

IL_0055:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_Update_m803AA51E52E865F91C722354EE7156072705F5EE (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_Update_m803AA51E52E865F91C722354EE7156072705F5EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	bool V_4 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	int32_t G_B3_0 = 0;
	{
		// if (sIsDummy || sQueueEmpty)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sIsDummy_5();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		bool L_1 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sQueueEmpty_8();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		V_0 = (bool)G_B3_0;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		// return;
		goto IL_009c;
	}

IL_001c:
	{
		// localQueue.Clear();
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_3 = __this->get_localQueue_7();
		NullCheck(L_3);
		List_1_Clear_m851B12AA396220AB3F093C8B3D84B0E815E8A38B(L_3, /*hidden argument*/List_1_Clear_m851B12AA396220AB3F093C8B3D84B0E815E8A38B_RuntimeMethod_var);
		// lock (sQueue)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_4 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sQueue_6();
		V_1 = L_4;
		V_2 = (bool)0;
	}

IL_0030:
	try
	{ // begin try (depth: 1)
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_5 = V_1;
		Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_5, (bool*)(&V_2), /*hidden argument*/NULL);
		// localQueue.AddRange(sQueue);
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_6 = __this->get_localQueue_7();
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_7 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sQueue_6();
		NullCheck(L_6);
		List_1_AddRange_m8AFF779F458D803CEE3FD8A0A2CCD5B5C18B2ED8(L_6, L_7, /*hidden argument*/List_1_AddRange_m8AFF779F458D803CEE3FD8A0A2CCD5B5C18B2ED8_RuntimeMethod_var);
		// sQueue.Clear();
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_8 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sQueue_6();
		NullCheck(L_8);
		List_1_Clear_m851B12AA396220AB3F093C8B3D84B0E815E8A38B(L_8, /*hidden argument*/List_1_Clear_m851B12AA396220AB3F093C8B3D84B0E815E8A38B_RuntimeMethod_var);
		// sQueueEmpty = true;
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sQueueEmpty_8(1);
		IL2CPP_LEAVE(0x6C, FINALLY_0061);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_2;
			if (!L_9)
			{
				goto IL_006b;
			}
		}

IL_0064:
		{
			List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_10 = V_1;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		}

IL_006b:
		{
			IL2CPP_END_FINALLY(97)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
	}

IL_006c:
	{
		// for (int i = 0; i < localQueue.Count; i++)
		V_3 = 0;
		goto IL_0088;
	}

IL_0070:
	{
		// localQueue[i].Invoke();
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_11 = __this->get_localQueue_7();
		int32_t L_12 = V_3;
		NullCheck(L_11);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_13 = List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_RuntimeMethod_var);
		NullCheck(L_13);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_13, /*hidden argument*/NULL);
		// for (int i = 0; i < localQueue.Count; i++)
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0088:
	{
		// for (int i = 0; i < localQueue.Count; i++)
		int32_t L_15 = V_3;
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_16 = __this->get_localQueue_7();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_inline(L_16, /*hidden argument*/List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_RuntimeMethod_var);
		V_4 = (bool)((((int32_t)L_15) < ((int32_t)L_17))? 1 : 0);
		bool L_18 = V_4;
		if (L_18)
		{
			goto IL_0070;
		}
	}

IL_009c:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationFocus(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_OnApplicationFocus_m9A19C80383E682429043888202B65FD0BD998994 (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * __this, bool ___focused0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_OnApplicationFocus_m9A19C80383E682429043888202B65FD0BD998994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_1 = NULL;
	Exception_t * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// foreach (Action<bool> cb in sFocusCallbackList)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sFocusCallbackList_10();
		NullCheck(L_0);
		Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE  L_1 = List_1_GetEnumerator_m1A6BEB03A9E4B76F009EACE9E2DC8202C1B00B0D(L_0, /*hidden argument*/List_1_GetEnumerator_m1A6BEB03A9E4B76F009EACE9E2DC8202C1B00B0D_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_000f:
		{
			// foreach (Action<bool> cb in sFocusCallbackList)
			Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_2 = Enumerator_get_Current_mEDF66107B1E3BECA631A37771397FAC62B4BB21B_inline((Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *)(&V_0), /*hidden argument*/Enumerator_get_Current_mEDF66107B1E3BECA631A37771397FAC62B4BB21B_RuntimeMethod_var);
			V_1 = L_2;
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			// cb(focused);
			Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_3 = V_1;
			bool L_4 = ___focused0;
			NullCheck(L_3);
			Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F(L_3, L_4, /*hidden argument*/Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_RuntimeMethod_var);
			goto IL_004a;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_0024;
			throw e;
		}

CATCH_0024:
		{ // begin catch(System.Exception)
			// catch (Exception e)
			V_2 = ((Exception_t *)__exception_local);
			// Logger.e("Exception in OnApplicationFocus:" +
			//                e.Message + "\n" + e.StackTrace);
			Exception_t * L_5 = V_2;
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_5);
			Exception_t * L_7 = V_2;
			NullCheck(L_7);
			String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Exception::get_StackTrace() */, L_7);
			String_t* L_9 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(_stringLiteral025A63FFA169F26B605C16CA69D0B1E95EC4FB19, L_6, _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC, L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
			Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8(L_9, /*hidden argument*/NULL);
			goto IL_004a;
		} // end catch (depth: 2)

IL_004a:
		{
		}

IL_004b:
		{
			// foreach (Action<bool> cb in sFocusCallbackList)
			bool L_10 = Enumerator_MoveNext_m08F3FE51A64369D57EC067889997CBA5F81DCC73((Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m08F3FE51A64369D57EC067889997CBA5F81DCC73_RuntimeMethod_var);
			if (L_10)
			{
				goto IL_000f;
			}
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m0F7AA651E81830E11A33BA5B2CACFFBE25F610A9((Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *)(&V_0), /*hidden argument*/Enumerator_Dispose_m0F7AA651E81830E11A33BA5B2CACFFBE25F610A9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x65, IL_0065)
	}

IL_0065:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationPause(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_OnApplicationPause_mADFF6C814E67628969732FC37BCC560B296D4AB6 (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * __this, bool ___paused0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_OnApplicationPause_mADFF6C814E67628969732FC37BCC560B296D4AB6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * V_1 = NULL;
	Exception_t * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// foreach (Action<bool> cb in sPauseCallbackList)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sPauseCallbackList_9();
		NullCheck(L_0);
		Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE  L_1 = List_1_GetEnumerator_m1A6BEB03A9E4B76F009EACE9E2DC8202C1B00B0D(L_0, /*hidden argument*/List_1_GetEnumerator_m1A6BEB03A9E4B76F009EACE9E2DC8202C1B00B0D_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_000f:
		{
			// foreach (Action<bool> cb in sPauseCallbackList)
			Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_2 = Enumerator_get_Current_mEDF66107B1E3BECA631A37771397FAC62B4BB21B_inline((Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *)(&V_0), /*hidden argument*/Enumerator_get_Current_mEDF66107B1E3BECA631A37771397FAC62B4BB21B_RuntimeMethod_var);
			V_1 = L_2;
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			// cb(paused);
			Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_3 = V_1;
			bool L_4 = ___paused0;
			NullCheck(L_3);
			Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F(L_3, L_4, /*hidden argument*/Action_1_Invoke_m45E8F9900F9DB395C48A868A7C6A83BDD7FC692F_RuntimeMethod_var);
			goto IL_004a;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_0024;
			throw e;
		}

CATCH_0024:
		{ // begin catch(System.Exception)
			// catch (Exception e)
			V_2 = ((Exception_t *)__exception_local);
			// Logger.e("Exception in OnApplicationPause:" +
			//                e.Message + "\n" + e.StackTrace);
			Exception_t * L_5 = V_2;
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_5);
			Exception_t * L_7 = V_2;
			NullCheck(L_7);
			String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Exception::get_StackTrace() */, L_7);
			String_t* L_9 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(_stringLiteral58FAF6730CD731832DEB85A8C0E16ACC72A8A179, L_6, _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC, L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t3E750EF4B5D1A8799801389FCD60CD6336A82C1B_il2cpp_TypeInfo_var);
			Logger_e_mB9E2D5031D7C7D375E366C6D42AC1D71292CE6D8(L_9, /*hidden argument*/NULL);
			goto IL_004a;
		} // end catch (depth: 2)

IL_004a:
		{
		}

IL_004b:
		{
			// foreach (Action<bool> cb in sPauseCallbackList)
			bool L_10 = Enumerator_MoveNext_m08F3FE51A64369D57EC067889997CBA5F81DCC73((Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m08F3FE51A64369D57EC067889997CBA5F81DCC73_RuntimeMethod_var);
			if (L_10)
			{
				goto IL_000f;
			}
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m0F7AA651E81830E11A33BA5B2CACFFBE25F610A9((Enumerator_t27459B5A39D3647660C4CF7FEC3DECAF818C69DE *)(&V_0), /*hidden argument*/Enumerator_Dispose_m0F7AA651E81830E11A33BA5B2CACFFBE25F610A9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x65, IL_0065)
	}

IL_0065:
	{
		// }
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::AddFocusCallback(System.Action`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_AddFocusCallback_m6E1E5A60B199070EC622D2D47B50D8C20319B82E (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_AddFocusCallback_m6E1E5A60B199070EC622D2D47B50D8C20319B82E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (!sFocusCallbackList.Contains(callback))
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sFocusCallbackList_10();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m1087577496C9953C2B9FE83421C010D93E1E39DE(L_0, L_1, /*hidden argument*/List_1_Contains_m1087577496C9953C2B9FE83421C010D93E1E39DE_RuntimeMethod_var);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		// sFocusCallbackList.Add(callback);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_4 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sFocusCallbackList_10();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_5 = ___callback0;
		NullCheck(L_4);
		List_1_Add_mBF59C2344C951987CF2A02BFE8F7F1BF795434FB(L_4, L_5, /*hidden argument*/List_1_Add_mBF59C2344C951987CF2A02BFE8F7F1BF795434FB_RuntimeMethod_var);
	}

IL_0021:
	{
		// }
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::RemoveFocusCallback(System.Action`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayGamesHelperObject_RemoveFocusCallback_mEA5550B390C0301AB6AE9CFB43379E80E9326979 (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RemoveFocusCallback_mEA5550B390C0301AB6AE9CFB43379E80E9326979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// return sFocusCallbackList.Remove(callback);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sFocusCallbackList_10();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Remove_m201C000881D6CDC3645867A7B25986ADF9614086(L_0, L_1, /*hidden argument*/List_1_Remove_m201C000881D6CDC3645867A7B25986ADF9614086_RuntimeMethod_var);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		// }
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::AddPauseCallback(System.Action`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject_AddPauseCallback_m97D958B110BFCA92A31FA362904CEF6AA1FCD00A (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_AddPauseCallback_m97D958B110BFCA92A31FA362904CEF6AA1FCD00A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (!sPauseCallbackList.Contains(callback))
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sPauseCallbackList_9();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m1087577496C9953C2B9FE83421C010D93E1E39DE(L_0, L_1, /*hidden argument*/List_1_Contains_m1087577496C9953C2B9FE83421C010D93E1E39DE_RuntimeMethod_var);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		// sPauseCallbackList.Add(callback);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_4 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sPauseCallbackList_9();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_5 = ___callback0;
		NullCheck(L_4);
		List_1_Add_mBF59C2344C951987CF2A02BFE8F7F1BF795434FB(L_4, L_5, /*hidden argument*/List_1_Add_mBF59C2344C951987CF2A02BFE8F7F1BF795434FB_RuntimeMethod_var);
	}

IL_0021:
	{
		// }
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::RemovePauseCallback(System.Action`1<System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayGamesHelperObject_RemovePauseCallback_m941669D42C479896AE60A61F2800C97DABE2EA5F (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RemovePauseCallback_m941669D42C479896AE60A61F2800C97DABE2EA5F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// return sPauseCallbackList.Remove(callback);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_sPauseCallbackList_9();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Remove_m201C000881D6CDC3645867A7B25986ADF9614086(L_0, L_1, /*hidden argument*/List_1_Remove_m201C000881D6CDC3645867A7B25986ADF9614086_RuntimeMethod_var);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		// }
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject__ctor_m6F2894860B6C363E51DFB53E692ADCDCCB10D9B1 (PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject__ctor_m6F2894860B6C363E51DFB53E692ADCDCCB10D9B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// List<System.Action> localQueue = new List<System.Action>();
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_0 = (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *)il2cpp_codegen_object_new(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_il2cpp_TypeInfo_var);
		List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968(L_0, /*hidden argument*/List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968_RuntimeMethod_var);
		__this->set_localQueue_7(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesHelperObject__cctor_mED148D40A157EBA3FC978F09D87A3225C30ADF32 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject__cctor_mED148D40A157EBA3FC978F09D87A3225C30ADF32_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static PlayGamesHelperObject instance = null;
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_instance_4((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F *)NULL);
		// private static bool sIsDummy = false;
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sIsDummy_5((bool)0);
		// private static List<System.Action> sQueue = new List<Action>();
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_0 = (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *)il2cpp_codegen_object_new(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_il2cpp_TypeInfo_var);
		List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968(L_0, /*hidden argument*/List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968_RuntimeMethod_var);
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sQueue_6(L_0);
		// private volatile static bool sQueueEmpty = true;
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sQueueEmpty_8(1);
		// private static List<Action<bool>> sPauseCallbackList =
		//     new List<Action<bool>>();
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_1 = (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 *)il2cpp_codegen_object_new(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89_il2cpp_TypeInfo_var);
		List_1__ctor_m2806B6CAA6E2D1E105FDA5CCB083EFDD8591339E(L_1, /*hidden argument*/List_1__ctor_m2806B6CAA6E2D1E105FDA5CCB083EFDD8591339E_RuntimeMethod_var);
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sPauseCallbackList_9(L_1);
		// private static List<Action<bool>> sFocusCallbackList =
		//     new List<Action<bool>>();
		List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 * L_2 = (List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89 *)il2cpp_codegen_object_new(List_1_tF6FD4CFFD312BA9104E0B9CB1A6620233FEF9E89_il2cpp_TypeInfo_var);
		List_1__ctor_m2806B6CAA6E2D1E105FDA5CCB083EFDD8591339E(L_2, /*hidden argument*/List_1__ctor_m2806B6CAA6E2D1E105FDA5CCB083EFDD8591339E_RuntimeMethod_var);
		((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->set_sFocusCallbackList_10(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject_<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_mC187ED07653BC33D8A6BA353FC0F6A74C1628F91 (U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject_<>c__DisplayClass10_0::<RunCoroutine>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0_U3CRunCoroutineU3Eb__0_m4C5D12A9EFD2B686E793203491194E13C6C5A46D (U3CU3Ec__DisplayClass10_0_t3458BDF7065D2C47F4DC11DCF21C8836C14D41D2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass10_0_U3CRunCoroutineU3Eb__0_m4C5D12A9EFD2B686E793203491194E13C6C5A46D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// RunOnGameThread(() => instance.StartCoroutine(action));
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F * L_0 = ((PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_StaticFields*)il2cpp_codegen_static_fields_for(PlayGamesHelperObject_tD1050B62E48DADEEC8592DB770B2E997921FD18F_il2cpp_TypeInfo_var))->get_instance_4();
		RuntimeObject* L_1 = __this->get_action_0();
		NullCheck(L_0);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.PluginVersion::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PluginVersion__ctor_m492A36F5A6C804F8254096C80B59312EAFE1AC76 (PluginVersion_tB0E519E7845CD35129B206E909BC0EF6A24D6BE9 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoginMenuHandler::SetVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069 (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, bool ___visible0, const RuntimeMethod* method)
{
	{
		// this.Parent.SetActive(visible);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_Parent_0();
		bool L_1 = ___visible0;
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LoginMenuHandler::SetLoadingMessage(System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, bool ___visible0, String_t* ___message1, const RuntimeMethod* method)
{
	{
		// this.LoadingMessageParent.SetActive(visible);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_LoadingMessageParent_3();
		bool L_1 = ___visible0;
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, L_1, /*hidden argument*/NULL);
		// this.LoadingMessageLabel.text = message;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_2 = __this->get_LoadingMessageLabel_5();
		String_t* L_3 = ___message1;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		// }
		return;
	}
}
// System.Void LoginMenuHandler::SetSignInWithAppleButton(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608 (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, bool ___visible0, bool ___enabled1, const RuntimeMethod* method)
{
	{
		// this.SignInWithAppleParent.SetActive(visible);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_SignInWithAppleParent_1();
		bool L_1 = ___visible0;
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, L_1, /*hidden argument*/NULL);
		// this.SignInWithAppleButton.enabled = enabled;
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_2 = __this->get_SignInWithAppleButton_2();
		bool L_3 = ___enabled1;
		NullCheck(L_2);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LoginMenuHandler::UpdateLoadingMessage(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler_UpdateLoadingMessage_m9F88D8943762FB49A8DCB56CDEC6BC63F35E7083 (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, float ___deltaTime0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (!this.LoadingMessageParent.activeSelf)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_LoadingMessageParent_3();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		// return;
		goto IL_0032;
	}

IL_0015:
	{
		// this.LoadingIconTransform.Rotate(0.0f, 0.0f, -360.0f * deltaTime);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = __this->get_LoadingIconTransform_4();
		float L_4 = ___deltaTime0;
		NullCheck(L_3);
		Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9(L_3, (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)(-360.0f), (float)L_4)), /*hidden argument*/NULL);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void LoginMenuHandler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginMenuHandler__ctor_m7363D92585401D5357294C6D50A145D5F9905FD5 (LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * V_1 = NULL;
	{
		// if (AppleAuthManager.IsCurrentPlatformSupported)
		IL2CPP_RUNTIME_CLASS_INIT(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C_il2cpp_TypeInfo_var);
		bool L_0 = AppleAuthManager_get_IsCurrentPlatformSupported_m4B3FB905E225F53F9FB03208A10FA86EB8C6CBB4(/*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		// var deserializer = new PayloadDeserializer();
		PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * L_2 = (PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E *)il2cpp_codegen_object_new(PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E_il2cpp_TypeInfo_var);
		PayloadDeserializer__ctor_m3DD225AE33F56326A8DF099FAEDDD75F65469D31(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		// this._appleAuthManager = new AppleAuthManager(deserializer);
		PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * L_3 = V_1;
		AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * L_4 = (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C *)il2cpp_codegen_object_new(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C_il2cpp_TypeInfo_var);
		AppleAuthManager__ctor_m4597D08EC5F35812DB3BAF881BF569B77C15EC87(L_4, L_3, /*hidden argument*/NULL);
		__this->set__appleAuthManager_5(L_4);
	}

IL_001e:
	{
		// this.InitializeLoginMenu();
		MainMenu_InitializeLoginMenu_m6DF9DFCD30D84C572C0FAE153A293E972409D333(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (this._appleAuthManager != null)
		RuntimeObject* L_0 = __this->get__appleAuthManager_5();
		V_0 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		// this._appleAuthManager.Update();
		RuntimeObject* L_2 = __this->get__appleAuthManager_5();
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(6 /* System.Void AppleAuth.IAppleAuthManager::Update() */, IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		// this.LoginMenu.UpdateLoadingMessage(Time.deltaTime);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_3 = __this->get_LoginMenu_6();
		float L_4 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		NullCheck(L_3);
		LoginMenuHandler_UpdateLoadingMessage_m9F88D8943762FB49A8DCB56CDEC6BC63F35E7083(L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::SignInWithAppleButtonPressed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SignInWithAppleButtonPressed_mE369BF33E96F249C0B362FB5F5CF020951ADCFFB (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	{
		// this.SetupLoginMenuForAppleSignIn();
		MainMenu_SetupLoginMenuForAppleSignIn_m314FDA3B2F46F68904F84841840033848ADB585D(__this, /*hidden argument*/NULL);
		// this.SignInWithApple();
		MainMenu_SignInWithApple_m54E769319FF0899CCD150E6BB1602E3E5ECFBE94(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::InitializeLoginMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_InitializeLoginMenu_m6DF9DFCD30D84C572C0FAE153A293E972409D333 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_InitializeLoginMenu_m6DF9DFCD30D84C572C0FAE153A293E972409D333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		// this.LoginMenu.SetVisible(visible: true);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_0 = __this->get_LoginMenu_6();
		NullCheck(L_0);
		LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069(L_0, (bool)1, /*hidden argument*/NULL);
		// this.GameMenu.SetVisible(visible: false);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_1 = __this->get_GameMenu_7();
		NullCheck(L_1);
		GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C(L_1, (bool)0, /*hidden argument*/NULL);
		// if (this._appleAuthManager == null)
		RuntimeObject* L_2 = __this->get__appleAuthManager_5();
		V_0 = (bool)((((RuntimeObject*)(RuntimeObject*)L_2) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		// this.SetupLoginMenuForUnsupportedPlatform();
		MainMenu_SetupLoginMenuForUnsupportedPlatform_m80C029D8C208B629AF2849559B8D653AD4FCF374(__this, /*hidden argument*/NULL);
		// return;
		goto IL_0086;
	}

IL_0032:
	{
		// this._appleAuthManager.SetCredentialsRevokedCallback(result =>
		// {
		//     Debug.Log("Received revoked callback " + result);
		//     this.SetupLoginMenuForSignInWithApple();
		//     PlayerPrefs.DeleteKey(AppleUserIdKey);
		// });
		RuntimeObject* L_4 = __this->get__appleAuthManager_5();
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_5, __this, (intptr_t)((intptr_t)MainMenu_U3CInitializeLoginMenuU3Eb__7_0_mC33C351ABC83249E18FA1ED7E2C2010992174DFE_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		NullCheck(L_4);
		InterfaceActionInvoker1< Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * >::Invoke(5 /* System.Void AppleAuth.IAppleAuthManager::SetCredentialsRevokedCallback(System.Action`1<System.String>) */, IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var, L_4, L_5);
		// if (PlayerPrefs.HasKey(AppleUserIdKey))
		bool L_6 = PlayerPrefs_HasKey_mD87D3051ACE7EC6F5B54F4FC9E18572C917CA0D1(_stringLiteral359BBFFBCF9FF96C06E2E9D27055844D66667163, /*hidden argument*/NULL);
		V_1 = L_6;
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_0076;
		}
	}
	{
		// var storedAppleUserId = PlayerPrefs.GetString(AppleUserIdKey);
		String_t* L_8 = PlayerPrefs_GetString_m3031AD2D5DEAB97677A9EF629618541437F079F1(_stringLiteral359BBFFBCF9FF96C06E2E9D27055844D66667163, /*hidden argument*/NULL);
		V_2 = L_8;
		// this.SetupLoginMenuForCheckingCredentials();
		MainMenu_SetupLoginMenuForCheckingCredentials_m16AEE25404D91E530C121EFFFF3257C074EEBF25(__this, /*hidden argument*/NULL);
		// this.CheckCredentialStatusForUserId(storedAppleUserId);
		String_t* L_9 = V_2;
		MainMenu_CheckCredentialStatusForUserId_m33A8D3E7479359851349A1532CC5031D03AFE3B2(__this, L_9, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0076:
	{
		// this.SetupLoginMenuForQuickLoginAttempt();
		MainMenu_SetupLoginMenuForQuickLoginAttempt_m351D9B48275D2D2587C3FFD3F5F4D6BF002C948F(__this, /*hidden argument*/NULL);
		// this.AttemptQuickLogin();
		MainMenu_AttemptQuickLogin_m1C4F040B20F22A88A09469378F69540EFEAFB883(__this, /*hidden argument*/NULL);
	}

IL_0086:
	{
		// }
		return;
	}
}
// System.Void MainMenu::SetupLoginMenuForUnsupportedPlatform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForUnsupportedPlatform_m80C029D8C208B629AF2849559B8D653AD4FCF374 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_SetupLoginMenuForUnsupportedPlatform_m80C029D8C208B629AF2849559B8D653AD4FCF374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.LoginMenu.SetVisible(visible: true);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_0 = __this->get_LoginMenu_6();
		NullCheck(L_0);
		LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069(L_0, (bool)1, /*hidden argument*/NULL);
		// this.GameMenu.SetVisible(visible: false);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_1 = __this->get_GameMenu_7();
		NullCheck(L_1);
		GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C(L_1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetSignInWithAppleButton(visible: false, enabled: false);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_2 = __this->get_LoginMenu_6();
		NullCheck(L_2);
		LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608(L_2, (bool)0, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetLoadingMessage(visible: true, message: "Unsupported platform");
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_3 = __this->get_LoginMenu_6();
		NullCheck(L_3);
		LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B(L_3, (bool)1, _stringLiteralE8697BF486B289F063DC8C65D77D61852BE0C0A5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::SetupLoginMenuForSignInWithApple()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.LoginMenu.SetVisible(visible: true);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_0 = __this->get_LoginMenu_6();
		NullCheck(L_0);
		LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069(L_0, (bool)1, /*hidden argument*/NULL);
		// this.GameMenu.SetVisible(visible: false);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_1 = __this->get_GameMenu_7();
		NullCheck(L_1);
		GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C(L_1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetSignInWithAppleButton(visible: true, enabled: true);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_2 = __this->get_LoginMenu_6();
		NullCheck(L_2);
		LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608(L_2, (bool)1, (bool)1, /*hidden argument*/NULL);
		// this.LoginMenu.SetLoadingMessage(visible: false, message: string.Empty);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_3 = __this->get_LoginMenu_6();
		String_t* L_4 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_3);
		LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B(L_3, (bool)0, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::SetupLoginMenuForCheckingCredentials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForCheckingCredentials_m16AEE25404D91E530C121EFFFF3257C074EEBF25 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_SetupLoginMenuForCheckingCredentials_m16AEE25404D91E530C121EFFFF3257C074EEBF25_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.LoginMenu.SetVisible(visible: true);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_0 = __this->get_LoginMenu_6();
		NullCheck(L_0);
		LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069(L_0, (bool)1, /*hidden argument*/NULL);
		// this.GameMenu.SetVisible(visible: false);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_1 = __this->get_GameMenu_7();
		NullCheck(L_1);
		GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C(L_1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetSignInWithAppleButton(visible: true, enabled: false);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_2 = __this->get_LoginMenu_6();
		NullCheck(L_2);
		LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608(L_2, (bool)1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetLoadingMessage(visible: true, message: "Checking Apple Credentials");
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_3 = __this->get_LoginMenu_6();
		NullCheck(L_3);
		LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B(L_3, (bool)1, _stringLiteralB45C7AA548BE5CFE94BE16743D453E6201708610, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::SetupLoginMenuForQuickLoginAttempt()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForQuickLoginAttempt_m351D9B48275D2D2587C3FFD3F5F4D6BF002C948F (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_SetupLoginMenuForQuickLoginAttempt_m351D9B48275D2D2587C3FFD3F5F4D6BF002C948F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.LoginMenu.SetVisible(visible: true);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_0 = __this->get_LoginMenu_6();
		NullCheck(L_0);
		LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069(L_0, (bool)1, /*hidden argument*/NULL);
		// this.GameMenu.SetVisible(visible: false);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_1 = __this->get_GameMenu_7();
		NullCheck(L_1);
		GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C(L_1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetSignInWithAppleButton(visible: true, enabled: false);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_2 = __this->get_LoginMenu_6();
		NullCheck(L_2);
		LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608(L_2, (bool)1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetLoadingMessage(visible: true, message: "Attempting Quick Login");
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_3 = __this->get_LoginMenu_6();
		NullCheck(L_3);
		LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B(L_3, (bool)1, _stringLiteral9BF54B3696FB70DB7DC021E3F63F039E59C9C412, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::SetupLoginMenuForAppleSignIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupLoginMenuForAppleSignIn_m314FDA3B2F46F68904F84841840033848ADB585D (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_SetupLoginMenuForAppleSignIn_m314FDA3B2F46F68904F84841840033848ADB585D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.LoginMenu.SetVisible(visible: true);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_0 = __this->get_LoginMenu_6();
		NullCheck(L_0);
		LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069(L_0, (bool)1, /*hidden argument*/NULL);
		// this.GameMenu.SetVisible(visible: false);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_1 = __this->get_GameMenu_7();
		NullCheck(L_1);
		GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C(L_1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetSignInWithAppleButton(visible: true, enabled: false);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_2 = __this->get_LoginMenu_6();
		NullCheck(L_2);
		LoginMenuHandler_SetSignInWithAppleButton_mE95A326549569F72B6B9A1EE4374521B3FCF4608(L_2, (bool)1, (bool)0, /*hidden argument*/NULL);
		// this.LoginMenu.SetLoadingMessage(visible: true, message: "Signing In with Apple");
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_3 = __this->get_LoginMenu_6();
		NullCheck(L_3);
		LoginMenuHandler_SetLoadingMessage_mBD48B5F80FBAC1BE1DD26E5F2A547F5510F60A6B(L_3, (bool)1, _stringLiteral237EA81755C751F30D0CF842D4B244CF71A0A5CB, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::SetupGameMenu(System.String,AppleAuth.Interfaces.ICredential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SetupGameMenu_m9141BCE4B7637E206CF1718C18189AC0D6DDCFF9 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, String_t* ___appleUserId0, RuntimeObject* ___credential1, const RuntimeMethod* method)
{
	{
		// this.LoginMenu.SetVisible(visible: false);
		LoginMenuHandler_tD1366C888E50FC2F56FFC0F57F1117FAF00E8F65 * L_0 = __this->get_LoginMenu_6();
		NullCheck(L_0);
		LoginMenuHandler_SetVisible_mC11AA20D31D56B1C9BD4681F2189C288F6831069(L_0, (bool)0, /*hidden argument*/NULL);
		// this.GameMenu.SetVisible(visible: true);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_1 = __this->get_GameMenu_7();
		NullCheck(L_1);
		GameMenuHandler_SetVisible_m30DEA55E579AE67476247CE3B2FE4D1D5BD7F56C(L_1, (bool)1, /*hidden argument*/NULL);
		// this.GameMenu.SetupAppleData(appleUserId, credential);
		GameMenuHandler_t754FF0E609ED3E7A934C2C952A617529B731F106 * L_2 = __this->get_GameMenu_7();
		String_t* L_3 = ___appleUserId0;
		RuntimeObject* L_4 = ___credential1;
		NullCheck(L_2);
		GameMenuHandler_SetupAppleData_m5878285CA5056522A0FD6BDC58739DCEC59E6BC2(L_2, L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::CheckCredentialStatusForUserId(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_CheckCredentialStatusForUserId_m33A8D3E7479359851349A1532CC5031D03AFE3B2 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, String_t* ___appleUserId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_CheckCredentialStatusForUserId_m33A8D3E7479359851349A1532CC5031D03AFE3B2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * L_0 = (U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass14_0__ctor_m036E1E18C115482EC760CB2A2851B38E75EAA653(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * L_2 = V_0;
		String_t* L_3 = ___appleUserId0;
		NullCheck(L_2);
		L_2->set_appleUserId_1(L_3);
		// this._appleAuthManager.GetCredentialState(
		//     appleUserId,
		//     state =>
		//     {
		//         switch (state)
		//         {
		//             
		//             case CredentialState.Authorized:
		//                 this.SetupGameMenu(appleUserId, null);
		//                 return;
		// 
		//             
		//             
		//             case CredentialState.Revoked:
		//             case CredentialState.NotFound:
		//                 this.SetupLoginMenuForSignInWithApple();
		//                 PlayerPrefs.DeleteKey(AppleUserIdKey);
		//                 return;
		//         }
		//     },
		//     error =>
		//     {
		//         var authorizationErrorCode = error.GetAuthorizationErrorCode();
		//         Debug.LogWarning("Error while trying to get credential state " + authorizationErrorCode.ToString() + " " + error.ToString());
		//         this.SetupLoginMenuForSignInWithApple();
		//     });
		RuntimeObject* L_4 = __this->get__appleAuthManager_5();
		U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = L_5->get_appleUserId_1();
		U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * L_7 = V_0;
		Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * L_8 = (Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 *)il2cpp_codegen_object_new(Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88_il2cpp_TypeInfo_var);
		Action_1__ctor_mC614E47849BEBE0AF8A23568BBA0E74B035427AC(L_8, L_7, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__0_m0E51F1E824212F9985EDD569F5D0E2947979B370_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mC614E47849BEBE0AF8A23568BBA0E74B035427AC_RuntimeMethod_var);
		U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * L_9 = V_0;
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_10 = (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 *)il2cpp_codegen_object_new(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3_il2cpp_TypeInfo_var);
		Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055(L_10, L_9, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__1_m2FAFDE5F0F9B6B2AEDFE777D0850E452BFB896B6_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055_RuntimeMethod_var);
		NullCheck(L_4);
		InterfaceActionInvoker3< String_t*, Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 *, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * >::Invoke(4 /* System.Void AppleAuth.IAppleAuthManager::GetCredentialState(System.String,System.Action`1<AppleAuth.Enums.CredentialState>,System.Action`1<AppleAuth.Interfaces.IAppleError>) */, IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var, L_4, L_6, L_8, L_10);
		// }
		return;
	}
}
// System.Void MainMenu::AttemptQuickLogin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_AttemptQuickLogin_m1C4F040B20F22A88A09469378F69540EFEAFB883 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_AttemptQuickLogin_m1C4F040B20F22A88A09469378F69540EFEAFB883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var quickLoginArgs = new AppleAuthQuickLoginArgs();
		il2cpp_codegen_initobj((&V_0), sizeof(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A ));
		// this._appleAuthManager.QuickLogin(
		//     quickLoginArgs,
		//     credential =>
		//     {
		//         
		//         var appleIdCredential = credential as IAppleIDCredential;
		//         if (appleIdCredential != null)
		//         {
		//             PlayerPrefs.SetString(AppleUserIdKey, credential.User);
		//         }
		// 
		//         this.SetupGameMenu(credential.User, credential);
		//     },
		//     error =>
		//     {
		//         
		//         var authorizationErrorCode = error.GetAuthorizationErrorCode();
		//         Debug.LogWarning("Quick Login Failed " + authorizationErrorCode.ToString() + " " + error.ToString());
		//         this.SetupLoginMenuForSignInWithApple();
		//     });
		RuntimeObject* L_0 = __this->get__appleAuthManager_5();
		AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  L_1 = V_0;
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_2 = (Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *)il2cpp_codegen_object_new(Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F_il2cpp_TypeInfo_var);
		Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4(L_2, __this, (intptr_t)((intptr_t)MainMenu_U3CAttemptQuickLoginU3Eb__15_0_mE971ABB7EF757755E9980F507531CAB5D16A2549_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4_RuntimeMethod_var);
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_3 = (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 *)il2cpp_codegen_object_new(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3_il2cpp_TypeInfo_var);
		Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055(L_3, __this, (intptr_t)((intptr_t)MainMenu_U3CAttemptQuickLoginU3Eb__15_1_m3FFF75DA7F2BA4DB719F829F8C4623FFC639B467_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055_RuntimeMethod_var);
		NullCheck(L_0);
		InterfaceActionInvoker3< AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A , Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * >::Invoke(1 /* System.Void AppleAuth.IAppleAuthManager::QuickLogin(AppleAuth.AppleAuthQuickLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>) */, IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		// }
		return;
	}
}
// System.Void MainMenu::SignInWithApple()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SignInWithApple_m54E769319FF0899CCD150E6BB1602E3E5ECFBE94 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_SignInWithApple_m54E769319FF0899CCD150E6BB1602E3E5ECFBE94_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);
		AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54((AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 *)(&V_0), 3, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
		// this._appleAuthManager.LoginWithAppleId(
		//     loginArgs,
		//     credential =>
		//     {
		//         
		//         PlayerPrefs.SetString(AppleUserIdKey, credential.User);
		//         this.SetupGameMenu(credential.User, credential);
		//     },
		//     error =>
		//     {
		//         var authorizationErrorCode = error.GetAuthorizationErrorCode();
		//         Debug.LogWarning("Sign in with Apple failed " + authorizationErrorCode.ToString() + " " + error.ToString());
		//         this.SetupLoginMenuForSignInWithApple();
		//     });
		RuntimeObject* L_0 = __this->get__appleAuthManager_5();
		AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  L_1 = V_0;
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_2 = (Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *)il2cpp_codegen_object_new(Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F_il2cpp_TypeInfo_var);
		Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4(L_2, __this, (intptr_t)((intptr_t)MainMenu_U3CSignInWithAppleU3Eb__16_0_mE59B75DF412205BE9CF77711BDBB75A56461D83F_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4_RuntimeMethod_var);
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_3 = (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 *)il2cpp_codegen_object_new(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3_il2cpp_TypeInfo_var);
		Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055(L_3, __this, (intptr_t)((intptr_t)MainMenu_U3CSignInWithAppleU3Eb__16_1_m2E75C508FE4FBF8F3789A238CD92E5CC8516620B_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055_RuntimeMethod_var);
		NullCheck(L_0);
		InterfaceActionInvoker3< AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 , Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * >::Invoke(3 /* System.Void AppleAuth.IAppleAuthManager::LoginWithAppleId(AppleAuth.AppleAuthLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>) */, IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		// }
		return;
	}
}
// System.Void MainMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::<InitializeLoginMenu>b__7_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_U3CInitializeLoginMenuU3Eb__7_0_mC33C351ABC83249E18FA1ED7E2C2010992174DFE (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, String_t* ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_U3CInitializeLoginMenuU3Eb__7_0_mC33C351ABC83249E18FA1ED7E2C2010992174DFE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Received revoked callback " + result);
		String_t* L_0 = ___result0;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral4278CC782CD0D8EE6104E01420569B266CD0F43C, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_1, /*hidden argument*/NULL);
		// this.SetupLoginMenuForSignInWithApple();
		MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC(__this, /*hidden argument*/NULL);
		// PlayerPrefs.DeleteKey(AppleUserIdKey);
		PlayerPrefs_DeleteKey_mE0D76FF284F638715170DB52728B7595B41B6E8C(_stringLiteral359BBFFBCF9FF96C06E2E9D27055844D66667163, /*hidden argument*/NULL);
		// });
		return;
	}
}
// System.Void MainMenu::<AttemptQuickLogin>b__15_0(AppleAuth.Interfaces.ICredential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_U3CAttemptQuickLoginU3Eb__15_0_mE971ABB7EF757755E9980F507531CAB5D16A2549 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, RuntimeObject* ___credential0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_U3CAttemptQuickLoginU3Eb__15_0_mE971ABB7EF757755E9980F507531CAB5D16A2549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	{
		// var appleIdCredential = credential as IAppleIDCredential;
		RuntimeObject* L_0 = ___credential0;
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var));
		// if (appleIdCredential != null)
		RuntimeObject* L_1 = V_0;
		V_1 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_1) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		// PlayerPrefs.SetString(AppleUserIdKey, credential.User);
		RuntimeObject* L_3 = ___credential0;
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_3);
		PlayerPrefs_SetString_m7AC4E332A5DCA04E0AD91544AF836744BA8C2583(_stringLiteral359BBFFBCF9FF96C06E2E9D27055844D66667163, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// this.SetupGameMenu(credential.User, credential);
		RuntimeObject* L_5 = ___credential0;
		NullCheck(L_5);
		String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_5);
		RuntimeObject* L_7 = ___credential0;
		MainMenu_SetupGameMenu_m9141BCE4B7637E206CF1718C18189AC0D6DDCFF9(__this, L_6, L_7, /*hidden argument*/NULL);
		// },
		return;
	}
}
// System.Void MainMenu::<AttemptQuickLogin>b__15_1(AppleAuth.Interfaces.IAppleError)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_U3CAttemptQuickLoginU3Eb__15_1_m3FFF75DA7F2BA4DB719F829F8C4623FFC639B467 (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, RuntimeObject* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_U3CAttemptQuickLoginU3Eb__15_1_m3FFF75DA7F2BA4DB719F829F8C4623FFC639B467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// var authorizationErrorCode = error.GetAuthorizationErrorCode();
		RuntimeObject* L_0 = ___error0;
		int32_t L_1 = AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// Debug.LogWarning("Quick Login Failed " + authorizationErrorCode.ToString() + " " + error.ToString());
		RuntimeObject * L_2 = Box(AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_0 = *(int32_t*)UnBox(L_2);
		RuntimeObject* L_4 = ___error0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		String_t* L_6 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(_stringLiteral90FD22C37BF4C83D185F8535C863E072DFB4ED39, L_3, _stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_6, /*hidden argument*/NULL);
		// this.SetupLoginMenuForSignInWithApple();
		MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC(__this, /*hidden argument*/NULL);
		// });
		return;
	}
}
// System.Void MainMenu::<SignInWithApple>b__16_0(AppleAuth.Interfaces.ICredential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_U3CSignInWithAppleU3Eb__16_0_mE59B75DF412205BE9CF77711BDBB75A56461D83F (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, RuntimeObject* ___credential0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_U3CSignInWithAppleU3Eb__16_0_mE59B75DF412205BE9CF77711BDBB75A56461D83F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PlayerPrefs.SetString(AppleUserIdKey, credential.User);
		RuntimeObject* L_0 = ___credential0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_0);
		PlayerPrefs_SetString_m7AC4E332A5DCA04E0AD91544AF836744BA8C2583(_stringLiteral359BBFFBCF9FF96C06E2E9D27055844D66667163, L_1, /*hidden argument*/NULL);
		// this.SetupGameMenu(credential.User, credential);
		RuntimeObject* L_2 = ___credential0;
		NullCheck(L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_2);
		RuntimeObject* L_4 = ___credential0;
		MainMenu_SetupGameMenu_m9141BCE4B7637E206CF1718C18189AC0D6DDCFF9(__this, L_3, L_4, /*hidden argument*/NULL);
		// },
		return;
	}
}
// System.Void MainMenu::<SignInWithApple>b__16_1(AppleAuth.Interfaces.IAppleError)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_U3CSignInWithAppleU3Eb__16_1_m2E75C508FE4FBF8F3789A238CD92E5CC8516620B (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * __this, RuntimeObject* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_U3CSignInWithAppleU3Eb__16_1_m2E75C508FE4FBF8F3789A238CD92E5CC8516620B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// var authorizationErrorCode = error.GetAuthorizationErrorCode();
		RuntimeObject* L_0 = ___error0;
		int32_t L_1 = AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// Debug.LogWarning("Sign in with Apple failed " + authorizationErrorCode.ToString() + " " + error.ToString());
		RuntimeObject * L_2 = Box(AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_0 = *(int32_t*)UnBox(L_2);
		RuntimeObject* L_4 = ___error0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		String_t* L_6 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(_stringLiteralDEAE2C9F6284B3727A6AF84C2BF44F20A87C0A0D, L_3, _stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_6, /*hidden argument*/NULL);
		// this.SetupLoginMenuForSignInWithApple();
		MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC(__this, /*hidden argument*/NULL);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu_<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_m036E1E18C115482EC760CB2A2851B38E75EAA653 (U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu_<>c__DisplayClass14_0::<CheckCredentialStatusForUserId>b__0(AppleAuth.Enums.CredentialState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__0_m0E51F1E824212F9985EDD569F5D0E2947979B370 (U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * __this, int32_t ___state0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__0_m0E51F1E824212F9985EDD569F5D0E2947979B370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (state)
		int32_t L_0 = ___state0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_002c;
			}
			case 1:
			{
				goto IL_0017;
			}
			case 2:
			{
				goto IL_002c;
			}
		}
	}
	{
		goto IL_0045;
	}

IL_0017:
	{
		// this.SetupGameMenu(appleUserId, null);
		MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * L_2 = __this->get_U3CU3E4__this_0();
		String_t* L_3 = __this->get_appleUserId_1();
		NullCheck(L_2);
		MainMenu_SetupGameMenu_m9141BCE4B7637E206CF1718C18189AC0D6DDCFF9(L_2, L_3, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		// return;
		goto IL_0045;
	}

IL_002c:
	{
		// this.SetupLoginMenuForSignInWithApple();
		MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * L_4 = __this->get_U3CU3E4__this_0();
		NullCheck(L_4);
		MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC(L_4, /*hidden argument*/NULL);
		// PlayerPrefs.DeleteKey(AppleUserIdKey);
		PlayerPrefs_DeleteKey_mE0D76FF284F638715170DB52728B7595B41B6E8C(_stringLiteral359BBFFBCF9FF96C06E2E9D27055844D66667163, /*hidden argument*/NULL);
		// return;
		goto IL_0045;
	}

IL_0045:
	{
		// },
		return;
	}
}
// System.Void MainMenu_<>c__DisplayClass14_0::<CheckCredentialStatusForUserId>b__1(AppleAuth.Interfaces.IAppleError)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__1_m2FAFDE5F0F9B6B2AEDFE777D0850E452BFB896B6 (U3CU3Ec__DisplayClass14_0_t1BBEB060C6D8DC600C4587D999275DFC4B0417D6 * __this, RuntimeObject* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass14_0_U3CCheckCredentialStatusForUserIdU3Eb__1_m2FAFDE5F0F9B6B2AEDFE777D0850E452BFB896B6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// var authorizationErrorCode = error.GetAuthorizationErrorCode();
		RuntimeObject* L_0 = ___error0;
		int32_t L_1 = AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// Debug.LogWarning("Error while trying to get credential state " + authorizationErrorCode.ToString() + " " + error.ToString());
		RuntimeObject * L_2 = Box(AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_0 = *(int32_t*)UnBox(L_2);
		RuntimeObject* L_4 = ___error0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		String_t* L_6 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(_stringLiteralB92F72B9FD3D0FA7FF9E7A63992988FBDF5675DD, L_3, _stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_6, /*hidden argument*/NULL);
		// this.SetupLoginMenuForSignInWithApple();
		MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105 * L_7 = __this->get_U3CU3E4__this_0();
		NullCheck(L_7);
		MainMenu_SetupLoginMenuForSignInWithApple_m0BEFD9EB064AF41A77D70CD0E596AD89F9E80ACC(L_7, /*hidden argument*/NULL);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebelinxAppleAuthController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxAppleAuthController_Start_mC5EE45F17610B28F7978FD18AFC8504770675FA2 (WebelinxAppleAuthController_t8256E6D120C43A6DCBD58B2252E235C187CD0280 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxAppleAuthController_Start_mC5EE45F17610B28F7978FD18AFC8504770675FA2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * V_2 = NULL;
	{
		// Debug.Log("Da li je platforma supported: " + AppleAuthManager.IsCurrentPlatformSupported);
		IL2CPP_RUNTIME_CLASS_INIT(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C_il2cpp_TypeInfo_var);
		bool L_0 = AppleAuthManager_get_IsCurrentPlatformSupported_m4B3FB905E225F53F9FB03208A10FA86EB8C6CBB4(/*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301((bool*)(&V_0), /*hidden argument*/NULL);
		String_t* L_2 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral52049EFA43D99F2C3B47EFCD7FCF0510C8A323E0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_2, /*hidden argument*/NULL);
		// if (AppleAuthManager.IsCurrentPlatformSupported)
		bool L_3 = AppleAuthManager_get_IsCurrentPlatformSupported_m4B3FB905E225F53F9FB03208A10FA86EB8C6CBB4(/*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		// PayloadDeserializer deserializer = new PayloadDeserializer();
		PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * L_5 = (PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E *)il2cpp_codegen_object_new(PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E_il2cpp_TypeInfo_var);
		PayloadDeserializer__ctor_m3DD225AE33F56326A8DF099FAEDDD75F65469D31(L_5, /*hidden argument*/NULL);
		V_2 = L_5;
		// this.appleAuthManager = new AppleAuthManager(deserializer);
		PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * L_6 = V_2;
		AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * L_7 = (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C *)il2cpp_codegen_object_new(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C_il2cpp_TypeInfo_var);
		AppleAuthManager__ctor_m4597D08EC5F35812DB3BAF881BF569B77C15EC87(L_7, L_6, /*hidden argument*/NULL);
		__this->set_appleAuthManager_4(L_7);
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void WebelinxAppleAuthController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxAppleAuthController_Update_m6DCB8BF987E305E3FE6BDFFF0857C5AB7349C25D (WebelinxAppleAuthController_t8256E6D120C43A6DCBD58B2252E235C187CD0280 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxAppleAuthController_Update_m6DCB8BF987E305E3FE6BDFFF0857C5AB7349C25D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (this.appleAuthManager != null)
		RuntimeObject* L_0 = __this->get_appleAuthManager_4();
		V_0 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		// this.appleAuthManager.Update();
		RuntimeObject* L_2 = __this->get_appleAuthManager_4();
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(6 /* System.Void AppleAuth.IAppleAuthManager::Update() */, IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var, L_2);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Void WebelinxAppleAuthController::AppleAuthHandler(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxAppleAuthController_AppleAuthHandler_m9C86C49F406E5269A87D21CDF0E43AEF6280BA07 (WebelinxAppleAuthController_t8256E6D120C43A6DCBD58B2252E235C187CD0280 * __this, bool ___isLinking0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebelinxAppleAuthController_AppleAuthHandler_m9C86C49F406E5269A87D21CDF0E43AEF6280BA07_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * G_B2_0 = NULL;
	AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	RuntimeObject* G_B2_2 = NULL;
	Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * G_B1_0 = NULL;
	AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	RuntimeObject* G_B1_2 = NULL;
	Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * G_B4_0 = NULL;
	Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * G_B4_1 = NULL;
	AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  G_B4_2;
	memset((&G_B4_2), 0, sizeof(G_B4_2));
	RuntimeObject* G_B4_3 = NULL;
	Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * G_B3_0 = NULL;
	Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * G_B3_1 = NULL;
	AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	RuntimeObject* G_B3_3 = NULL;
	{
		// AppleAuthLoginArgs loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);
		AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54((AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 *)(&V_0), 3, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
		// this.appleAuthManager.LoginWithAppleId(loginArgs, (ICredential credential) =>
		//     {
		//         
		//         IAppleIDCredential appleIdCredential = credential as IAppleIDCredential;
		// 
		//         if (appleIdCredential != null)
		//         {
		//             
		//             
		//             string userId = appleIdCredential.User;
		//             
		// 
		//             
		//             string email = appleIdCredential.Email;
		//             Debug.Log("Email log: " + email);
		// 
		//             
		//             IPersonName fullName = appleIdCredential.FullName;
		// 
		//             Debug.Log("Full name log: " + fullName);
		// 
		//             
		//             string identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken, 0, appleIdCredential.IdentityToken.Length);
		//             Debug.Log("Identity token log: " + fullName);
		// 
		//             
		//             string authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode, 0, appleIdCredential.AuthorizationCode.Length);
		//             Debug.Log("Authorization Code log: " + fullName);
		// 
		//             
		//         }
		//     },
		//     error =>
		//     {
		//         
		//         AuthorizationErrorCode authorizationErrorCode = error.GetAuthorizationErrorCode();
		//     });
		RuntimeObject* L_0 = __this->get_appleAuthManager_4();
		AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var);
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_2 = ((U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var))->get_U3CU3E9__3_0_1();
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = L_1;
		G_B1_2 = L_0;
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = L_1;
			G_B2_2 = L_0;
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var);
		U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * L_4 = ((U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_5 = (Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *)il2cpp_codegen_object_new(Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F_il2cpp_TypeInfo_var);
		Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_0_m7CB32875FC16DF9BC460CA91228508118DC534AD_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m45F01B3B118F33360C337DA637601A78E955AEB4_RuntimeMethod_var);
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_6 = L_5;
		((U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var))->set_U3CU3E9__3_0_1(L_6);
		G_B2_0 = L_6;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var);
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_7 = ((U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var))->get_U3CU3E9__3_1_2();
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_8 = L_7;
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		if (L_8)
		{
			G_B4_0 = L_8;
			G_B4_1 = G_B2_0;
			G_B4_2 = G_B2_1;
			G_B4_3 = G_B2_2;
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var);
		U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * L_9 = ((U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_10 = (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 *)il2cpp_codegen_object_new(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3_il2cpp_TypeInfo_var);
		Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055(L_10, L_9, (intptr_t)((intptr_t)U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_1_m7A93743A0A6D77967CFF416B59225ED7EDAF2E93_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mB425BB46EC5ECEE1D3C6924D00661ECE4EB18055_RuntimeMethod_var);
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_11 = L_10;
		((U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var))->set_U3CU3E9__3_1_2(L_11);
		G_B4_0 = L_11;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		G_B4_3 = G_B3_3;
	}

IL_0050:
	{
		NullCheck(G_B4_3);
		InterfaceActionInvoker3< AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 , Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * >::Invoke(3 /* System.Void AppleAuth.IAppleAuthManager::LoginWithAppleId(AppleAuth.AppleAuthLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>) */, IAppleAuthManager_t3E01C00C9A0454E56BB325251A87C6A7D9B2F287_il2cpp_TypeInfo_var, G_B4_3, G_B4_2, G_B4_1, G_B4_0);
		// }
		return;
	}
}
// System.Void WebelinxAppleAuthController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebelinxAppleAuthController__ctor_m353F2BF00DFF1AAA58B346725B2553036BC95C9E (WebelinxAppleAuthController_t8256E6D120C43A6DCBD58B2252E235C187CD0280 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebelinxAppleAuthController_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m18EE7693094C336A370250B287B562DF94875253 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m18EE7693094C336A370250B287B562DF94875253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * L_0 = (U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 *)il2cpp_codegen_object_new(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m7D4447F7EDCE4B591674E4A6048A309B5758BA08(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void WebelinxAppleAuthController_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7D4447F7EDCE4B591674E4A6048A309B5758BA08 (U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebelinxAppleAuthController_<>c::<AppleAuthHandler>b__3_0(AppleAuth.Interfaces.ICredential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_0_m7CB32875FC16DF9BC460CA91228508118DC534AD (U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * __this, RuntimeObject* ___credential0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_0_m7CB32875FC16DF9BC460CA91228508118DC534AD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	{
		// IAppleIDCredential appleIdCredential = credential as IAppleIDCredential;
		RuntimeObject* L_0 = ___credential0;
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var));
		// if (appleIdCredential != null)
		RuntimeObject* L_1 = V_0;
		V_1 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_1) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_00a8;
		}
	}
	{
		// string userId = appleIdCredential.User;
		RuntimeObject* L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.ICredential::get_User() */, ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44_il2cpp_TypeInfo_var, L_3);
		V_2 = L_4;
		// string email = appleIdCredential.Email;
		RuntimeObject* L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(5 /* System.String AppleAuth.Interfaces.IAppleIDCredential::get_Email() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_5);
		V_3 = L_6;
		// Debug.Log("Email log: " + email);
		String_t* L_7 = V_3;
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral615D43B81CED22F65E6ECF062ABD62E3F0D89EDC, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_8, /*hidden argument*/NULL);
		// IPersonName fullName = appleIdCredential.FullName;
		RuntimeObject* L_9 = V_0;
		NullCheck(L_9);
		RuntimeObject* L_10 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(4 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IAppleIDCredential::get_FullName() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_9);
		V_4 = L_10;
		// Debug.Log("Full name log: " + fullName);
		RuntimeObject* L_11 = V_4;
		String_t* L_12 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral4A354D2B71364BACE1F77A0943FFC64AE95AC84A, L_11, /*hidden argument*/NULL);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_12, /*hidden argument*/NULL);
		// string identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken, 0, appleIdCredential.IdentityToken.Length);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_13 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		RuntimeObject* L_14 = V_0;
		NullCheck(L_14);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(0 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_IdentityToken() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_14);
		RuntimeObject* L_16 = V_0;
		NullCheck(L_16);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_17 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(0 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_IdentityToken() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_16);
		NullCheck(L_17);
		NullCheck(L_13);
		String_t* L_18 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(31 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_13, L_15, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))));
		V_5 = L_18;
		// Debug.Log("Identity token log: " + fullName);
		RuntimeObject* L_19 = V_4;
		String_t* L_20 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralDEDB5B33E3227FE4457EF07E6806353303A1FE2F, L_19, /*hidden argument*/NULL);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_20, /*hidden argument*/NULL);
		// string authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode, 0, appleIdCredential.AuthorizationCode.Length);
		Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * L_21 = Encoding_get_UTF8_m67C8652936B681E7BC7505E459E88790E0FF16D9(/*hidden argument*/NULL);
		RuntimeObject* L_22 = V_0;
		NullCheck(L_22);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(1 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizationCode() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_22);
		RuntimeObject* L_24 = V_0;
		NullCheck(L_24);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_25 = InterfaceFuncInvoker0< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* >::Invoke(1 /* System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizationCode() */, IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E_il2cpp_TypeInfo_var, L_24);
		NullCheck(L_25);
		NullCheck(L_21);
		String_t* L_26 = VirtFuncInvoker3< String_t*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(31 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_21, L_23, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))));
		V_6 = L_26;
		// Debug.Log("Authorization Code log: " + fullName);
		RuntimeObject* L_27 = V_4;
		String_t* L_28 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral1988A1C5E0F5737FC0FD47973E5DD7EE29CE933B, L_27, /*hidden argument*/NULL);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_28, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		// },
		return;
	}
}
// System.Void WebelinxAppleAuthController_<>c::<AppleAuthHandler>b__3_1(AppleAuth.Interfaces.IAppleError)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAppleAuthHandlerU3Eb__3_1_m7A93743A0A6D77967CFF416B59225ED7EDAF2E93 (U3CU3Ec_t8EAD9EF03978EBAC5ED431693DD64092E0188C55 * __this, RuntimeObject* ___error0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// AuthorizationErrorCode authorizationErrorCode = error.GetAuthorizationErrorCode();
		RuntimeObject* L_0 = ___error0;
		int32_t L_1 = AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_UserUUID_mC1824612BC320BA142FDBE242726D2712BFCD902_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string UserUUID { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CUserUUIDU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_DisplayName_m4F58543D0F421804556204E391108297C3C51D50_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_Email_m69A025C44E648B762D68463C85FA74144897CE93_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_IsAnonymous_mEEC1E03C2960F04B8A8DCE046775D85100AC522D_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsAnonymous { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CIsAnonymousU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserFirebaseInfo_set_PhotoUri_m2A865C067ACE928BE0C30A959AF3EB5727157E6A_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = ___value0;
		__this->set_U3CPhotoUriU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_UserUUID_m1BA830CB5DC3B933B6DA95CF15A6D1883D78629C_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public string UserUUID { get; set; }
		String_t* L_0 = __this->get_U3CUserUUIDU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_DisplayName_m7D6DB48B0328C4B1084A2264A5054EFCD0B77C46_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserFirebaseInfo_get_Email_m62E73F5D92D3EB4471A5FCA58760FEA2ADFD18AF_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool SignedInUserFirebaseInfo_get_IsAnonymous_m064F66405EE6B554D78E364FD555E180BDF2F0DB_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public bool IsAnonymous { get; set; }
		bool L_0 = __this->get_U3CIsAnonymousU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * SignedInUserFirebaseInfo_get_PhotoUri_m1E1E86EA4BE2FA79D9C2D15578692F1E88ADE5F8_inline (SignedInUserFirebaseInfo_t878E215F3B66C7FE87C6ADC0DC08695A28FE3CD3 * __this, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = __this->get_U3CPhotoUriU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_UserProviderID_mC93C141881ABA67D15BCF271FE0D0F546D01C165_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string UserProviderID { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CUserProviderIDU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_ProviderNameID_m8422083B4FAAB0D2C1FB9F208CB1A7BB7C559828_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string ProviderNameID { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CProviderNameIDU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_DisplayName_mDB53CCA9AB396016EF447DE6440A54CE0928EB81_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CDisplayNameU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_Email_m851CCA9B5D5F346D62D5EF68A9F9E9A2B7DAFE74_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CEmailU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void SignedInUserProviderInfo_set_PhotoUri_mE69209E187C563DC763A3349D67E7DDCA0D60AFC_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = ___value0;
		__this->set_U3CPhotoUriU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_ProviderNameID_mD11C21B821492EF3EDC289324BDE73FB5E427E3E_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string ProviderNameID { get; set; }
		String_t* L_0 = __this->get_U3CProviderNameIDU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_UserProviderID_m4D91634A20FC5A4B6D78749E72CC14153B9C962D_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string UserProviderID { get; set; }
		String_t* L_0 = __this->get_U3CUserProviderIDU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_Email_mBD64A4E4340E7B14DBDE3805D44A37BFEEFF711D_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string Email { get; set; }
		String_t* L_0 = __this->get_U3CEmailU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* SignedInUserProviderInfo_get_DisplayName_mFDF9B959B7FB8CE83439AB5BAAC99B15BD7E2C7B_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public string DisplayName { get; set; }
		String_t* L_0 = __this->get_U3CDisplayNameU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * SignedInUserProviderInfo_get_PhotoUri_mBBA748CD89585D8F1526008220027539659F8355_inline (SignedInUserProviderInfo_tBEA8C895C791C1E641D897EA1D8F0DBF07D917D1 * __this, const RuntimeMethod* method)
{
	{
		// public Uri PhotoUri { get; set; }
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = __this->get_U3CPhotoUriU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m619F23350E60DA452BA9F320E9EA38E1D933663D_gshared_inline (Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return (bool)L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = ((EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
