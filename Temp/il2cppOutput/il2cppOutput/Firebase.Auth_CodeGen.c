﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF_RuntimeMethod_var;
extern const RuntimeMethod* FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7_RuntimeMethod_var;
extern const RuntimeMethod* Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String Firebase.Auth.IUserInfo::get_DisplayName()
// 0x00000002 System.String Firebase.Auth.IUserInfo::get_Email()
// 0x00000003 System.Uri Firebase.Auth.IUserInfo::get_PhotoUrl()
// 0x00000004 System.String Firebase.Auth.IUserInfo::get_ProviderId()
// 0x00000005 System.String Firebase.Auth.IUserInfo::get_UserId()
// 0x00000006 System.Void Firebase.Auth.UserInfoInterfaceList::.ctor(System.IntPtr,System.Boolean)
extern void UserInfoInterfaceList__ctor_m969BCAE5B0171B9F24C435611C2D57CEF93C4F54 (void);
// 0x00000007 System.Void Firebase.Auth.UserInfoInterfaceList::Finalize()
extern void UserInfoInterfaceList_Finalize_mD536EF30C4A7836DC7AE6F949107D1F770E34BB6 (void);
// 0x00000008 System.Void Firebase.Auth.UserInfoInterfaceList::Dispose()
extern void UserInfoInterfaceList_Dispose_mC25B3B40D335F63D189E23B81DEEA426C0E76350 (void);
// 0x00000009 System.Void Firebase.Auth.UserInfoInterfaceList::Dispose(System.Boolean)
extern void UserInfoInterfaceList_Dispose_m6903C9BF5E1050559288FD30150C273778B4DC05 (void);
// 0x0000000A System.Boolean Firebase.Auth.UserInfoInterfaceList::get_IsReadOnly()
extern void UserInfoInterfaceList_get_IsReadOnly_m97CCE213B0867EC6B07AF2E0AFCFE5D33DC82844 (void);
// 0x0000000B Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::get_Item(System.Int32)
extern void UserInfoInterfaceList_get_Item_m2CF8D552EFD6768C39D2579B76356659F2C570DD (void);
// 0x0000000C System.Void Firebase.Auth.UserInfoInterfaceList::set_Item(System.Int32,Firebase.Auth.UserInfoInterface)
extern void UserInfoInterfaceList_set_Item_mA5E90884FF1A01C43CCE62ABD40CD296DC000ED5 (void);
// 0x0000000D System.Int32 Firebase.Auth.UserInfoInterfaceList::get_Count()
extern void UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074 (void);
// 0x0000000E System.Void Firebase.Auth.UserInfoInterfaceList::CopyTo(Firebase.Auth.UserInfoInterface[],System.Int32)
extern void UserInfoInterfaceList_CopyTo_m11CCDBDABE905725AC2B4D5FBCF19AA926C4F25A (void);
// 0x0000000F System.Void Firebase.Auth.UserInfoInterfaceList::CopyTo(System.Int32,Firebase.Auth.UserInfoInterface[],System.Int32,System.Int32)
extern void UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E (void);
// 0x00000010 System.Collections.Generic.IEnumerator`1<Firebase.Auth.UserInfoInterface> Firebase.Auth.UserInfoInterfaceList::global::System.Collections.Generic.IEnumerable<Firebase.Auth.UserInfoInterface>.GetEnumerator()
extern void UserInfoInterfaceList_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CFirebase_Auth_UserInfoInterfaceU3E_GetEnumerator_mCDDED8A7BA953D28567C3DE977F584DC759DE4C3 (void);
// 0x00000011 System.Collections.IEnumerator Firebase.Auth.UserInfoInterfaceList::global::System.Collections.IEnumerable.GetEnumerator()
extern void UserInfoInterfaceList_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m0D58B068226E88BDF4B38BF06648A191AC0E75F9 (void);
// 0x00000012 Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator Firebase.Auth.UserInfoInterfaceList::GetEnumerator()
extern void UserInfoInterfaceList_GetEnumerator_m34C89390756E8DCA0D4FDE9EC4020AF7AFBB4021 (void);
// 0x00000013 System.Void Firebase.Auth.UserInfoInterfaceList::Clear()
extern void UserInfoInterfaceList_Clear_mF43F449B72C454169BE9001F0BDB87A43B9BC711 (void);
// 0x00000014 System.Void Firebase.Auth.UserInfoInterfaceList::Add(Firebase.Auth.UserInfoInterface)
extern void UserInfoInterfaceList_Add_m272A420019BB6431562C0EE4CF84E86280F2BBD9 (void);
// 0x00000015 System.UInt32 Firebase.Auth.UserInfoInterfaceList::size()
extern void UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B (void);
// 0x00000016 Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::getitemcopy(System.Int32)
extern void UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00 (void);
// 0x00000017 Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::getitem(System.Int32)
extern void UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA (void);
// 0x00000018 System.Void Firebase.Auth.UserInfoInterfaceList::setitem(System.Int32,Firebase.Auth.UserInfoInterface)
extern void UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F (void);
// 0x00000019 System.Void Firebase.Auth.UserInfoInterfaceList::Insert(System.Int32,Firebase.Auth.UserInfoInterface)
extern void UserInfoInterfaceList_Insert_mEF354596EFD206D50AE0F69B9A76E1A6B4DC1878 (void);
// 0x0000001A System.Void Firebase.Auth.UserInfoInterfaceList::RemoveAt(System.Int32)
extern void UserInfoInterfaceList_RemoveAt_mBE2A2F80ACBA832F24F7B066A861019BFC1F4E0B (void);
// 0x0000001B System.Boolean Firebase.Auth.UserInfoInterfaceList::Contains(Firebase.Auth.UserInfoInterface)
extern void UserInfoInterfaceList_Contains_m3D0CA1C4D5D72A7D59BE69C5067AAE50CF4063F8 (void);
// 0x0000001C System.Int32 Firebase.Auth.UserInfoInterfaceList::IndexOf(Firebase.Auth.UserInfoInterface)
extern void UserInfoInterfaceList_IndexOf_m814BFA29319E2C0C125EE6E87B0FD742C890CB86 (void);
// 0x0000001D System.Boolean Firebase.Auth.UserInfoInterfaceList::Remove(Firebase.Auth.UserInfoInterface)
extern void UserInfoInterfaceList_Remove_m6AE184FAF9D7EC2568042FF0304127885EC5F422 (void);
// 0x0000001E System.Void Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::.ctor(Firebase.Auth.UserInfoInterfaceList)
extern void UserInfoInterfaceListEnumerator__ctor_mB52655D936C618ADDC4D506EF165F8ADAFAF350E (void);
// 0x0000001F Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::get_Current()
extern void UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720 (void);
// 0x00000020 System.Object Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void UserInfoInterfaceListEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mA488DE8E00C60FC4C370D97E7084C9609FF0368E (void);
// 0x00000021 System.Boolean Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::MoveNext()
extern void UserInfoInterfaceListEnumerator_MoveNext_mB0E76F84C535182420B86E45C821E100E902F0A9 (void);
// 0x00000022 System.Void Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::Reset()
extern void UserInfoInterfaceListEnumerator_Reset_m89E71EC357585556E719F9C537D6A4954D0804E4 (void);
// 0x00000023 System.Void Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::Dispose()
extern void UserInfoInterfaceListEnumerator_Dispose_mA619C0FC99C494EE549F5868CBDCDE2E02F5CF03 (void);
// 0x00000024 System.Void Firebase.Auth.Future_User::.ctor(System.IntPtr,System.Boolean)
extern void Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A (void);
// 0x00000025 System.Void Firebase.Auth.Future_User::Dispose(System.Boolean)
extern void Future_User_Dispose_m82E8E0011FC005AB27FDF878D851C18D22FB19E1 (void);
// 0x00000026 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.Future_User::GetTask(Firebase.Auth.Future_User)
extern void Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506 (void);
// 0x00000027 System.Void Firebase.Auth.Future_User::ThrowIfDisposed()
extern void Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE (void);
// 0x00000028 System.Void Firebase.Auth.Future_User::SetOnCompletionCallback(Firebase.Auth.Future_User_Action)
extern void Future_User_SetOnCompletionCallback_mA4F7DBBBBBA68E9A54C2231034CCE80415DD80A6 (void);
// 0x00000029 System.Void Firebase.Auth.Future_User::SetCompletionData(System.IntPtr)
extern void Future_User_SetCompletionData_m7B693E6CE4A587D18797FE4A21D00A4C823DB7BC (void);
// 0x0000002A System.Void Firebase.Auth.Future_User::SWIG_CompletionDispatcher(System.Int32)
extern void Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47 (void);
// 0x0000002B System.IntPtr Firebase.Auth.Future_User::SWIG_OnCompletion(Firebase.Auth.Future_User_SWIG_CompletionDelegate,System.Int32)
extern void Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A (void);
// 0x0000002C System.Void Firebase.Auth.Future_User::SWIG_FreeCompletionData(System.IntPtr)
extern void Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A (void);
// 0x0000002D Firebase.Auth.FirebaseUser Firebase.Auth.Future_User::GetResult()
extern void Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902 (void);
// 0x0000002E System.Void Firebase.Auth.Future_User::.cctor()
extern void Future_User__cctor_m4576D93855EFC88AA0CD018753370EBD1F2BE0EA (void);
// 0x0000002F System.Void Firebase.Auth.Future_User_Action::.ctor(System.Object,System.IntPtr)
extern void Action__ctor_m9AE0C8C97132B6FFDB6D84FDC9A8802717222F39 (void);
// 0x00000030 System.Void Firebase.Auth.Future_User_Action::Invoke()
extern void Action_Invoke_m8B7DBC95C4F8A5255426959F447C3FDDC7DFB2B5 (void);
// 0x00000031 System.IAsyncResult Firebase.Auth.Future_User_Action::BeginInvoke(System.AsyncCallback,System.Object)
extern void Action_BeginInvoke_m153941CDA9F036DD9B908B6294DAFAB3A9C763E4 (void);
// 0x00000032 System.Void Firebase.Auth.Future_User_Action::EndInvoke(System.IAsyncResult)
extern void Action_EndInvoke_m49FAC74EBAB03687FE2F0001B201C163432F6DBC (void);
// 0x00000033 System.Void Firebase.Auth.Future_User_SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIG_CompletionDelegate__ctor_m9FCE4E269635690B019F5FF509AFF343FA13DBDC (void);
// 0x00000034 System.Void Firebase.Auth.Future_User_SWIG_CompletionDelegate::Invoke(System.Int32)
extern void SWIG_CompletionDelegate_Invoke_mDEC298EADEA6875C62E559FACECB441130831BE7 (void);
// 0x00000035 System.IAsyncResult Firebase.Auth.Future_User_SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void SWIG_CompletionDelegate_BeginInvoke_m1F1D032BA7DC7797D7B332090F05CF043978EA1F (void);
// 0x00000036 System.Void Firebase.Auth.Future_User_SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
extern void SWIG_CompletionDelegate_EndInvoke_m78A511D0EE58036E4D5A03C2690C362F49A84CE4 (void);
// 0x00000037 System.Void Firebase.Auth.Future_User_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m69741D9DAF70E435DFBFF0CFFA06B08796349300 (void);
// 0x00000038 System.Void Firebase.Auth.Future_User_<>c__DisplayClass4_0::<GetTask>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CGetTaskU3Eb__0_mC165A76D4A3E3CA94F84D8F613ECE4E75D2EF7DF (void);
// 0x00000039 System.Void Firebase.Auth.Credential::.ctor(System.IntPtr,System.Boolean)
extern void Credential__ctor_mBC968251CEBE306DF7CD89C85E74D1B1D451CFC7 (void);
// 0x0000003A System.Runtime.InteropServices.HandleRef Firebase.Auth.Credential::getCPtr(Firebase.Auth.Credential)
extern void Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42 (void);
// 0x0000003B System.Void Firebase.Auth.Credential::Finalize()
extern void Credential_Finalize_mD6808D5E0D70311F7639F026600E220CF64ED657 (void);
// 0x0000003C System.Void Firebase.Auth.Credential::Dispose()
extern void Credential_Dispose_m76866FF791102DEEC0E64363C978654CA2E0DD87 (void);
// 0x0000003D System.Void Firebase.Auth.Credential::Dispose(System.Boolean)
extern void Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5 (void);
// 0x0000003E System.Boolean Firebase.Auth.Credential::IsValid()
extern void Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039 (void);
// 0x0000003F System.Void Firebase.Auth.FacebookAuthProvider::.cctor()
extern void FacebookAuthProvider__cctor_mF5D3E8AA0DA69CEDDE05F5547497DC789089ECF6 (void);
// 0x00000040 Firebase.Auth.Credential Firebase.Auth.FacebookAuthProvider::GetCredential(System.String)
extern void FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5 (void);
// 0x00000041 System.Void Firebase.Auth.PlayGamesAuthProvider::.cctor()
extern void PlayGamesAuthProvider__cctor_m8EE6103C650CD01A16F0ECFB31FD4E50E6E17CBA (void);
// 0x00000042 Firebase.Auth.Credential Firebase.Auth.PlayGamesAuthProvider::GetCredential(System.String)
extern void PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33 (void);
// 0x00000043 System.Void Firebase.Auth.UserInfoInterface::.ctor(System.IntPtr,System.Boolean)
extern void UserInfoInterface__ctor_m7E75B0EBDFD46089D31A77871A8B74520EEBC04C (void);
// 0x00000044 System.Runtime.InteropServices.HandleRef Firebase.Auth.UserInfoInterface::getCPtr(Firebase.Auth.UserInfoInterface)
extern void UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680 (void);
// 0x00000045 System.Void Firebase.Auth.UserInfoInterface::Finalize()
extern void UserInfoInterface_Finalize_m9DFDC97F9560711EAD0F29882ACDB60CCDA56D66 (void);
// 0x00000046 System.Void Firebase.Auth.UserInfoInterface::Dispose()
extern void UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B (void);
// 0x00000047 System.Void Firebase.Auth.UserInfoInterface::Dispose(System.Boolean)
extern void UserInfoInterface_Dispose_m8532EF4241914613A205B8E2E1720C39FD85C1AA (void);
// 0x00000048 System.Uri Firebase.Auth.UserInfoInterface::get_PhotoUrl()
extern void UserInfoInterface_get_PhotoUrl_m459FA1AD2C0AB62EFEF0B264F4ED9532CC15484C (void);
// 0x00000049 System.String Firebase.Auth.UserInfoInterface::get_UserId()
extern void UserInfoInterface_get_UserId_m3A02719809CE964AFB9ABECF26FFCF81D2415C9D (void);
// 0x0000004A System.String Firebase.Auth.UserInfoInterface::get_Email()
extern void UserInfoInterface_get_Email_m02D9D14CC54BD35802499A248FD7382F9E95C6AC (void);
// 0x0000004B System.String Firebase.Auth.UserInfoInterface::get_DisplayName()
extern void UserInfoInterface_get_DisplayName_m3DCD08821470A2241296F8AE5E40E28935E61832 (void);
// 0x0000004C System.String Firebase.Auth.UserInfoInterface::get_PhotoUrlInternal()
extern void UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024 (void);
// 0x0000004D System.String Firebase.Auth.UserInfoInterface::get_ProviderId()
extern void UserInfoInterface_get_ProviderId_mA71AA9527D83D2FBACFBCB38ACACA341F197660C (void);
// 0x0000004E System.Void Firebase.Auth.FirebaseUser::.ctor(System.IntPtr,System.Boolean)
extern void FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5 (void);
// 0x0000004F System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseUser::getCPtr(Firebase.Auth.FirebaseUser)
extern void FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D (void);
// 0x00000050 System.Void Firebase.Auth.FirebaseUser::Dispose(System.Boolean)
extern void FirebaseUser_Dispose_m347979011C289888965AA04311F9CD818CB5994A (void);
// 0x00000051 System.Uri Firebase.Auth.FirebaseUser::get_PhotoUrl()
extern void FirebaseUser_get_PhotoUrl_mAC0F8F1390483E1FD30D932527542C89E9C389C6 (void);
// 0x00000052 System.Threading.Tasks.Task Firebase.Auth.FirebaseUser::UpdateEmailAsync(System.String)
extern void FirebaseUser_UpdateEmailAsync_m8D0B4CA7BE4605CF684B74CEE6C5A2ADA2E1F5A7 (void);
// 0x00000053 System.Threading.Tasks.Task Firebase.Auth.FirebaseUser::UpdateUserProfileAsync(Firebase.Auth.UserProfile)
extern void FirebaseUser_UpdateUserProfileAsync_mACFB318A8FA0A47B70F317197B0C5884D5772D9D (void);
// 0x00000054 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseUser::LinkWithCredentialAsync(Firebase.Auth.Credential)
extern void FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627 (void);
// 0x00000055 System.String Firebase.Auth.FirebaseUser::get_DisplayName()
extern void FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB (void);
// 0x00000056 System.String Firebase.Auth.FirebaseUser::get_Email()
extern void FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06 (void);
// 0x00000057 System.Boolean Firebase.Auth.FirebaseUser::get_IsAnonymous()
extern void FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902 (void);
// 0x00000058 System.String Firebase.Auth.FirebaseUser::get_PhotoUrlInternal()
extern void FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F (void);
// 0x00000059 System.Collections.Generic.IEnumerable`1<Firebase.Auth.IUserInfo> Firebase.Auth.FirebaseUser::get_ProviderData()
extern void FirebaseUser_get_ProviderData_mE5D70ADC73F94F71D686939C53A8984CCEC682D0 (void);
// 0x0000005A System.String Firebase.Auth.FirebaseUser::get_UserId()
extern void FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD (void);
// 0x0000005B System.Void Firebase.Auth.UserProfile::.ctor(System.IntPtr,System.Boolean)
extern void UserProfile__ctor_m3F41707B2949CA717EB71681388E002A3106801A (void);
// 0x0000005C System.Runtime.InteropServices.HandleRef Firebase.Auth.UserProfile::getCPtr(Firebase.Auth.UserProfile)
extern void UserProfile_getCPtr_m43D66A8F09ED56F3026B25C5DFEEEA8A3BEB7CEC (void);
// 0x0000005D System.Void Firebase.Auth.UserProfile::Finalize()
extern void UserProfile_Finalize_m47BE7F05B7BA57E259D3FECCCBBC7AEEE0351DB1 (void);
// 0x0000005E System.Void Firebase.Auth.UserProfile::Dispose()
extern void UserProfile_Dispose_m8B47F62EA95EDA7AF6819F157ED0C3043944380C (void);
// 0x0000005F System.Void Firebase.Auth.UserProfile::Dispose(System.Boolean)
extern void UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3 (void);
// 0x00000060 System.Void Firebase.Auth.UserProfile::set_PhotoUrl(System.Uri)
extern void UserProfile_set_PhotoUrl_mCDA8A14D63367925AD4D59808B472B88086D977A (void);
// 0x00000061 System.Void Firebase.Auth.UserProfile::.ctor()
extern void UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809 (void);
// 0x00000062 System.Void Firebase.Auth.UserProfile::set_DisplayName(System.String)
extern void UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557 (void);
// 0x00000063 System.Void Firebase.Auth.UserProfile::set_PhotoUrlInternal(System.String)
extern void UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363 (void);
// 0x00000064 System.Void Firebase.Auth.FirebaseAuth::.ctor(System.IntPtr,System.Boolean)
extern void FirebaseAuth__ctor_m5A11B7E7ACB1FF9296510387F4A203804EC9B5D0 (void);
// 0x00000065 System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseAuth::getCPtr(Firebase.Auth.FirebaseAuth)
extern void FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9 (void);
// 0x00000066 System.Void Firebase.Auth.FirebaseAuth::Finalize()
extern void FirebaseAuth_Finalize_m5E134A851F020A7EA755620983EF8D2879DCE270 (void);
// 0x00000067 System.Void Firebase.Auth.FirebaseAuth::Dispose()
extern void FirebaseAuth_Dispose_mDAE8E88D96D92B5D08A8E3165DFA4F3F4DCF30DA (void);
// 0x00000068 System.Void Firebase.Auth.FirebaseAuth::Dispose(System.Boolean)
extern void FirebaseAuth_Dispose_m31BB65B92A235E510C6FBC6B250BFE0D5D4D4A18 (void);
// 0x00000069 Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::ProxyFromAppCPtr(System.IntPtr)
extern void FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15 (void);
// 0x0000006A System.Void Firebase.Auth.FirebaseAuth::ThrowIfNull()
extern void FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC (void);
// 0x0000006B Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuth(Firebase.FirebaseApp)
extern void FirebaseAuth_GetAuth_mEFC135E9B1D37E4739D3E2A8D30A1C80DDE3F157 (void);
// 0x0000006C System.Void Firebase.Auth.FirebaseAuth::OnAppDisposed(System.Object,System.EventArgs)
extern void FirebaseAuth_OnAppDisposed_m432A4ECCD62471F891EC24B73F0BE81794DCA4EB (void);
// 0x0000006D System.Void Firebase.Auth.FirebaseAuth::DisposeInternal()
extern void FirebaseAuth_DisposeInternal_m5DD53E6F76144CC4B417157E6C3475F7704117DC (void);
// 0x0000006E System.Void Firebase.Auth.FirebaseAuth::ForwardStateChange(System.IntPtr,System.Action`1<Firebase.Auth.FirebaseAuth>)
extern void FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C (void);
// 0x0000006F System.Void Firebase.Auth.FirebaseAuth::StateChangedFunction(System.IntPtr)
extern void FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7 (void);
// 0x00000070 System.Void Firebase.Auth.FirebaseAuth::IdTokenChangedFunction(System.IntPtr)
extern void FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF (void);
// 0x00000071 Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::get_DefaultInstance()
extern void FirebaseAuth_get_DefaultInstance_mA307D08C178B8C5A8EA7BE0F3222668A544FA228 (void);
// 0x00000072 System.Void Firebase.Auth.FirebaseAuth::add_StateChanged(System.EventHandler)
extern void FirebaseAuth_add_StateChanged_mFA3F7A1FE096865FF349DBD64644371C0C8CCB34 (void);
// 0x00000073 System.Void Firebase.Auth.FirebaseAuth::remove_StateChanged(System.EventHandler)
extern void FirebaseAuth_remove_StateChanged_mF960C837B4BD78DAF5716BC2348EF9702AD2B736 (void);
// 0x00000074 System.Void Firebase.Auth.FirebaseAuth::add_stateChangedImpl(System.EventHandler)
extern void FirebaseAuth_add_stateChangedImpl_m1387CE8F8F0F4796FFDF97ADD352101FEE55335C (void);
// 0x00000075 System.Void Firebase.Auth.FirebaseAuth::remove_stateChangedImpl(System.EventHandler)
extern void FirebaseAuth_remove_stateChangedImpl_m6BC5FB994A66D606B5783D721FE3D17FD3D5B4D2 (void);
// 0x00000076 Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::UpdateCurrentUser(Firebase.Auth.FirebaseUser)
extern void FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF (void);
// 0x00000077 Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::get_CurrentUser()
extern void FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF (void);
// 0x00000078 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInWithCredentialAsync(Firebase.Auth.Credential)
extern void FirebaseAuth_SignInWithCredentialAsync_m0C8097559DE43C62ED1AAC3D6105B155B02C948F (void);
// 0x00000079 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInAnonymouslyAsync()
extern void FirebaseAuth_SignInAnonymouslyAsync_m6963AC820328CD24A672D8800D1B51BD98FF0995 (void);
// 0x0000007A System.Void Firebase.Auth.FirebaseAuth::CompleteFirebaseUserTask(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>,System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>)
extern void FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF (void);
// 0x0000007B System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInWithCredentialInternalAsync(Firebase.Auth.Credential)
extern void FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B (void);
// 0x0000007C System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInAnonymouslyInternalAsync()
extern void FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C (void);
// 0x0000007D System.Void Firebase.Auth.FirebaseAuth::SignOut()
extern void FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D (void);
// 0x0000007E Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuthInternal(Firebase.FirebaseApp,Firebase.InitResult&)
extern void FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D (void);
// 0x0000007F System.Void Firebase.Auth.FirebaseAuth::LogHeartbeatInternal(Firebase.FirebaseApp)
extern void FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145 (void);
// 0x00000080 System.Void Firebase.Auth.FirebaseAuth::ReleaseReferenceInternal(Firebase.Auth.FirebaseAuth)
extern void FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B (void);
// 0x00000081 Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::get_CurrentUserInternal()
extern void FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74 (void);
// 0x00000082 System.Void Firebase.Auth.FirebaseAuth::.cctor()
extern void FirebaseAuth__cctor_m9714CC491B97AB4DE8C0D5763ACB8BAEB9E80936 (void);
// 0x00000083 System.Void Firebase.Auth.FirebaseAuth_StateChangedDelegate::.ctor(System.Object,System.IntPtr)
extern void StateChangedDelegate__ctor_m5A465ECFE2F3056DE914C988625D85656F431F1C (void);
// 0x00000084 System.Void Firebase.Auth.FirebaseAuth_StateChangedDelegate::Invoke(System.IntPtr)
extern void StateChangedDelegate_Invoke_m76E4AE64047B20E5DBA21576DB6C0B780E2C2729 (void);
// 0x00000085 System.IAsyncResult Firebase.Auth.FirebaseAuth_StateChangedDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void StateChangedDelegate_BeginInvoke_m57C156934DDB33999507D45527F6E84DF9F6BA35 (void);
// 0x00000086 System.Void Firebase.Auth.FirebaseAuth_StateChangedDelegate::EndInvoke(System.IAsyncResult)
extern void StateChangedDelegate_EndInvoke_mD4D5CA766ED9C02ED543990F366C99E187DA5E3A (void);
// 0x00000087 System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mACC4F54E5C54C1F4E1983D751939FEE59B3EAE4D (void);
// 0x00000088 System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_1::.ctor()
extern void U3CU3Ec__DisplayClass19_1__ctor_m1C29623F09C98E18908C68A9AAEA1F77D1CC820F (void);
// 0x00000089 System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_1::<GetAuth>b__0()
extern void U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A (void);
// 0x0000008A System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m3362924530F3A7340346D7C34BC4B0E66A70A061 (void);
// 0x0000008B System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_1::.ctor()
extern void U3CU3Ec__DisplayClass22_1__ctor_mE1858D9A2B9F52467C9D68DF38C2EFDCC99DE22D (void);
// 0x0000008C System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_1::<ForwardStateChange>b__0()
extern void U3CU3Ec__DisplayClass22_1_U3CForwardStateChangeU3Eb__0_m4958CF79DC4F966EB6B44A3100894727D07F5D49 (void);
// 0x0000008D System.Void Firebase.Auth.FirebaseAuth_<>c::.cctor()
extern void U3CU3Ec__cctor_m07998BEFD2CCB1494ECB6037ACA9B180B78E0086 (void);
// 0x0000008E System.Void Firebase.Auth.FirebaseAuth_<>c::.ctor()
extern void U3CU3Ec__ctor_m17B1FF4992C7A229A6F9003C935994D8A8687BCB (void);
// 0x0000008F System.Void Firebase.Auth.FirebaseAuth_<>c::<StateChangedFunction>b__23_0(Firebase.Auth.FirebaseAuth)
extern void U3CU3Ec_U3CStateChangedFunctionU3Eb__23_0_m4DF5A2B9C4EF839583FBDD03461848F5EF489F4E (void);
// 0x00000090 System.Void Firebase.Auth.FirebaseAuth_<>c::<IdTokenChangedFunction>b__24_0(Firebase.Auth.FirebaseAuth)
extern void U3CU3Ec_U3CIdTokenChangedFunctionU3Eb__24_0_mBDB096D23092A8E0948E7EEABA9ECE55DD9C92FB (void);
// 0x00000091 System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_m2347AE3FAE11B4DFFD2348CD75D799A145AFEDCC (void);
// 0x00000092 System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass48_0::<SignInWithCredentialAsync>b__0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void U3CU3Ec__DisplayClass48_0_U3CSignInWithCredentialAsyncU3Eb__0_m732B3D2DD97881C4CEEF6D96D047F28D1DE67CAE (void);
// 0x00000093 System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass50_0::.ctor()
extern void U3CU3Ec__DisplayClass50_0__ctor_m91E489361C82BCFD50E78CF12F492893906F7B5A (void);
// 0x00000094 System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass50_0::<SignInAnonymouslyAsync>b__0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void U3CU3Ec__DisplayClass50_0_U3CSignInAnonymouslyAsyncU3Eb__0_m1D34F38B75501F86230E71CD29B059F63C572E4A (void);
// 0x00000095 System.Void Firebase.Auth.AuthUtilPINVOKE::.cctor()
extern void AuthUtilPINVOKE__cctor_mB959A2D2962E2F91E1F004B060423C21556019E0 (void);
// 0x00000096 System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Clear(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_Clear_m0477B48FDCFEEC92FC007E3F43EC65647F9E240A (void);
// 0x00000097 System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_Add_m235699A22C7864EC52A0A694E8549F1C4C1AE089 (void);
// 0x00000098 System.UInt32 Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_size(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_size_m4022FD61AA1F01B2682E30DD2660D39144EE3875 (void);
// 0x00000099 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_getitemcopy_mB07EF1F7557FF0370E3ACE819AEE2612D4CB524D (void);
// 0x0000009A System.IntPtr Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_getitem_m57D02F5BE29828EB28F2B8B3DC5591207B34BEE9 (void);
// 0x0000009B System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_setitem_mCB2A5B2BF72BD7E40187C4B07E9CC8FA9344A441 (void);
// 0x0000009C System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_Insert_mF8231269D3DB49CE00D815C59748E6D77C5E8A6D (void);
// 0x0000009D System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_RemoveAt_m3E7CC088E5D8F62308D0A7193D34DE0DA9215179 (void);
// 0x0000009E System.Boolean Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Contains(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_Contains_mACD9BBB51A6B1007C9107D484C7963ACC01C1803 (void);
// 0x0000009F System.Int32 Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_IndexOf(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_IndexOf_mC0DFA98501EF7C4F2D513ACF7B513877B71E69D3 (void);
// 0x000000A0 System.Boolean Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Remove(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterfaceList_Remove_m58DD7F22CAFF88AABC98EBB2B4003720BCDEE7BC (void);
// 0x000000A1 System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserInfoInterfaceList(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_delete_UserInfoInterfaceList_mF4C1DD6531A015AC1E86A763A37E9E2EDC5B1A51 (void);
// 0x000000A2 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.Auth.Future_User_SWIG_CompletionDelegate,System.Int32)
extern void AuthUtilPINVOKE_Future_User_SWIG_OnCompletion_mA4865E7D9C1E147E26B7FD5F81078FA99F7ADF32 (void);
// 0x000000A3 System.Void Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AuthUtilPINVOKE_Future_User_SWIG_FreeCompletionData_m4E8D0B09D212ED4E404E86B97439D0AF51879DB1 (void);
// 0x000000A4 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_GetResult(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_Future_User_GetResult_m36CFE956376E3A4DC187BCC9D6F2732417615FE9 (void);
// 0x000000A5 System.Void Firebase.Auth.AuthUtilPINVOKE::delete_Future_User(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_delete_Future_User_m09EF8AC9392014A450B9A39F9237AB703460AE42 (void);
// 0x000000A6 System.Void Firebase.Auth.AuthUtilPINVOKE::delete_Credential(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_delete_Credential_m02588ECD12EF7FBADE68921F2AC6F2DBA0E37C88 (void);
// 0x000000A7 System.Boolean Firebase.Auth.AuthUtilPINVOKE::Credential_IsValid(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_Credential_IsValid_m590E8B8C5882B73A5D6C253E2E6591D789A61081 (void);
// 0x000000A8 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FacebookAuthProvider_GetCredential(System.String)
extern void AuthUtilPINVOKE_FacebookAuthProvider_GetCredential_m6C17A5533AB95072B52FEFFEF1BC520C8B3B89BF (void);
// 0x000000A9 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::PlayGamesAuthProvider_GetCredential(System.String)
extern void AuthUtilPINVOKE_PlayGamesAuthProvider_GetCredential_mF15F71E21D192B4940648E872A8F7F8E8713A0CA (void);
// 0x000000AA System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserInfoInterface(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_delete_UserInfoInterface_m6EECB261E56A9D7F8BB61C69988423AD41B16747 (void);
// 0x000000AB System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_UserId_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterface_UserId_get_m3B7A2BC624181BC02ACF3481BF437903B1EFC954 (void);
// 0x000000AC System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_Email_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterface_Email_get_m49D0AB8130C3E528C1D44B66CC7D681B2F2E804B (void);
// 0x000000AD System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_DisplayName_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterface_DisplayName_get_m40F5D1957524C394F0A8022F47B399931BE4DFF7 (void);
// 0x000000AE System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_PhotoUrlInternal_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterface_PhotoUrlInternal_get_mA1C69290B7452547D58BA4675DAE501090CCA5B7 (void);
// 0x000000AF System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_ProviderId_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_UserInfoInterface_ProviderId_get_mB0AC6D00DCC186EB3D315809B0727D666F24C9B0 (void);
// 0x000000B0 System.Void Firebase.Auth.AuthUtilPINVOKE::delete_FirebaseUser(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_delete_FirebaseUser_m21C9A5C887AD120B4228AC2E15E7EA499F745A9F (void);
// 0x000000B1 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UpdateEmail(System.Runtime.InteropServices.HandleRef,System.String)
extern void AuthUtilPINVOKE_FirebaseUser_UpdateEmail_mE4BC3966DE87BDA371C7CE7A0F23D91BEC313CCB (void);
// 0x000000B2 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UpdateUserProfile(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_UpdateUserProfile_m828FCDD053CA923BA34246E6679C39F6BCBF076C (void);
// 0x000000B3 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_LinkWithCredential(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_LinkWithCredential_mBC153FDCFAC9320B9628591B105955EFCCA5E03F (void);
// 0x000000B4 System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_DisplayName_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_DisplayName_get_mDD546C27F066D7A7C9F9612FB9CC18C6E8D9E2BF (void);
// 0x000000B5 System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_Email_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_Email_get_m1C74E1D469EF27F32E2810738DBF9CA1BA23582D (void);
// 0x000000B6 System.Boolean Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_IsAnonymous_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_IsAnonymous_get_m62129D05B6F8B37AF44E526484F3F44094D907D8 (void);
// 0x000000B7 System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_PhotoUrlInternal_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_PhotoUrlInternal_get_m12B7DDC9E668B2B40694C4DEB10A2C4EAC81FD00 (void);
// 0x000000B8 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_ProviderData_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_ProviderData_get_mD388B7BE41C3645C7464C3C0DAA81290CF2CED47 (void);
// 0x000000B9 System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UserId_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseUser_UserId_get_mA5AC5F6494A1F4A6A3F0DBC90E0D4D1006F5CF5A (void);
// 0x000000BA System.IntPtr Firebase.Auth.AuthUtilPINVOKE::new_UserProfile()
extern void AuthUtilPINVOKE_new_UserProfile_mF862E0AB1127CCB19BEA8E520C8EC4E4AD85FF1C (void);
// 0x000000BB System.Void Firebase.Auth.AuthUtilPINVOKE::UserProfile_DisplayName_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void AuthUtilPINVOKE_UserProfile_DisplayName_set_m2E3AA6A6AFEED65380B898B3BFD7FDD033CB5B59 (void);
// 0x000000BC System.Void Firebase.Auth.AuthUtilPINVOKE::UserProfile_PhotoUrlInternal_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void AuthUtilPINVOKE_UserProfile_PhotoUrlInternal_set_mA5391563F2C002A4D053CF4BC28C2B7548E699B0 (void);
// 0x000000BD System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserProfile(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_delete_UserProfile_mEBF4B6D168A27F787149E7D71C5F6DA18AA6104E (void);
// 0x000000BE System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignInWithCredentialInternal(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseAuth_SignInWithCredentialInternal_m286B534BC062DE3FD14436764BCE057923965B67 (void);
// 0x000000BF System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignInAnonymouslyInternal(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseAuth_SignInAnonymouslyInternal_m769C6AFE4B52A39BFFB28D8298F6664514DB39C6 (void);
// 0x000000C0 System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignOut(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseAuth_SignOut_m03EDFAB11CF0A459DD7CA6A3E097790BA3D91458 (void);
// 0x000000C1 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_GetAuthInternal(System.Runtime.InteropServices.HandleRef,System.Int32&)
extern void AuthUtilPINVOKE_FirebaseAuth_GetAuthInternal_m9CA73CC68A18B4B86AFFB925E37E8F6A0DAE72AA (void);
// 0x000000C2 System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_LogHeartbeatInternal(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseAuth_LogHeartbeatInternal_m40FB5B32F8D0549946D5FA26631C4D88338C6D5C (void);
// 0x000000C3 System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseAuth_ReleaseReferenceInternal_m77FAC25E87B2644CA0EF2B9BFF0201C2F38349D9 (void);
// 0x000000C4 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_CurrentUserInternal_get(System.Runtime.InteropServices.HandleRef)
extern void AuthUtilPINVOKE_FirebaseAuth_CurrentUserInternal_get_m1646DA22D3C6243710AFD36C183194346B494495 (void);
// 0x000000C5 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::CreateAuthStateListener(System.Runtime.InteropServices.HandleRef,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
extern void AuthUtilPINVOKE_CreateAuthStateListener_m093D034502CF32CE850E6A9E286C15EAE6785C2B (void);
// 0x000000C6 System.Void Firebase.Auth.AuthUtilPINVOKE::DestroyAuthStateListener(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AuthUtilPINVOKE_DestroyAuthStateListener_m632301CCA03C97183D31D2E819196742BBB8BB1B (void);
// 0x000000C7 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::CreateIdTokenListener(System.Runtime.InteropServices.HandleRef,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
extern void AuthUtilPINVOKE_CreateIdTokenListener_mA59101F16559583539311A7139734B88B26CF10B (void);
// 0x000000C8 System.Void Firebase.Auth.AuthUtilPINVOKE::DestroyIdTokenListener(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AuthUtilPINVOKE_DestroyIdTokenListener_m7CACC17CE6A8EB523C7FC9DC96051AB52A6571F1 (void);
// 0x000000C9 System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIGUpcast(System.IntPtr)
extern void AuthUtilPINVOKE_Future_User_SWIGUpcast_m9F5C5F23C6908B207633FFC69E6B33329CCDD5FC (void);
// 0x000000CA System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_SWIGUpcast(System.IntPtr)
extern void AuthUtilPINVOKE_FirebaseUser_SWIGUpcast_m2E75C4601FD7E308456F2F0BF75ECB73F92D0E44 (void);
// 0x000000CB System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AuthUtil(Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AuthUtil_mD13FDDDDC03DD49540B6AFFC935A58DE0B534762 (void);
// 0x000000CC System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AuthUtil(Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AuthUtil_m85D92F4B1140B7B2228AB92E057C747B9A07FCA7 (void);
// 0x000000CD System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625 (void);
// 0x000000CE System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145 (void);
// 0x000000CF System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944 (void);
// 0x000000D0 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888 (void);
// 0x000000D1 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719 (void);
// 0x000000D2 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D (void);
// 0x000000D3 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2 (void);
// 0x000000D4 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9 (void);
// 0x000000D5 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E (void);
// 0x000000D6 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2 (void);
// 0x000000D7 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43 (void);
// 0x000000D8 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77 (void);
// 0x000000D9 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032 (void);
// 0x000000DA System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7 (void);
// 0x000000DB System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_mC03977EF4F677D23618BA72DEE999DAD6233D0CE (void);
// 0x000000DC System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_mFF2CE315200639030B26C32F018A7E7354B81663 (void);
// 0x000000DD System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D (void);
// 0x000000DE System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_m797B0B4E66994FAF224C4EB881F468E142D745B7 (void);
// 0x000000DF System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ExceptionDelegate_BeginInvoke_mAC113B823C70AA10A3D5344072B3FDD0101EA845 (void);
// 0x000000E0 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionDelegate_EndInvoke_mD9FB06B5011432C53792DCFFC3AA5F23D16703EE (void);
// 0x000000E1 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_mC3898DBFD267903C5001A38AD119C7DA5FE561F2 (void);
// 0x000000E2 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_m6928DBD3A8C7DD5AEF69A157C7833B46B927DA6D (void);
// 0x000000E3 System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void ExceptionArgumentDelegate_BeginInvoke_mBF7ADADE9B2D349A22A925C4C1728A92B154C42D (void);
// 0x000000E4 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionArgumentDelegate_EndInvoke_m70375B45334E455A99D59904D6A945A04B6886EF (void);
// 0x000000E5 System.Boolean Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_m746EAD61F79AF2A98C4350069D4507EAC1D251DE (void);
// 0x000000E6 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C (void);
// 0x000000E7 System.Exception Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD (void);
// 0x000000E8 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::.cctor()
extern void SWIGPendingException__cctor_mC42B091F09F3F6365C73742A66AD460617B4B7EB (void);
// 0x000000E9 System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::SWIGRegisterStringCallback_AuthUtil(Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_mCF54A64D405E0EE541CB782629AE4D3EFFF01E69 (void);
// 0x000000EA System.String Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134 (void);
// 0x000000EB System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_mE675A6A15898F4EF6BD38CADB60C065692FF609E (void);
// 0x000000EC System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_m22B2245AE72469FC2016E3B70329E4117E38AF96 (void);
// 0x000000ED System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_m34FFAA789252BBCC866BE5712BD2072DEC39AA75 (void);
// 0x000000EE System.String Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_mCB669A3F17E62E6075E1CCAD17E777E903F5739C (void);
// 0x000000EF System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void SWIGStringDelegate_BeginInvoke_mA37707372B07564EB2B162155D8F88A43C9F89BF (void);
// 0x000000F0 System.String Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern void SWIGStringDelegate_EndInvoke_m70160AEB75A18A0445E89204A64C3CFEF21F767E (void);
// 0x000000F1 System.IntPtr Firebase.Auth.AuthUtil::CreateAuthStateListener(Firebase.Auth.FirebaseAuth,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
extern void AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828 (void);
// 0x000000F2 System.Void Firebase.Auth.AuthUtil::DestroyAuthStateListener(Firebase.Auth.FirebaseAuth,System.IntPtr)
extern void AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7 (void);
// 0x000000F3 System.IntPtr Firebase.Auth.AuthUtil::CreateIdTokenListener(Firebase.Auth.FirebaseAuth,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
extern void AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187 (void);
// 0x000000F4 System.Void Firebase.Auth.AuthUtil::DestroyIdTokenListener(Firebase.Auth.FirebaseAuth,System.IntPtr)
extern void AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22 (void);
static Il2CppMethodPointer s_methodPointers[244] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UserInfoInterfaceList__ctor_m969BCAE5B0171B9F24C435611C2D57CEF93C4F54,
	UserInfoInterfaceList_Finalize_mD536EF30C4A7836DC7AE6F949107D1F770E34BB6,
	UserInfoInterfaceList_Dispose_mC25B3B40D335F63D189E23B81DEEA426C0E76350,
	UserInfoInterfaceList_Dispose_m6903C9BF5E1050559288FD30150C273778B4DC05,
	UserInfoInterfaceList_get_IsReadOnly_m97CCE213B0867EC6B07AF2E0AFCFE5D33DC82844,
	UserInfoInterfaceList_get_Item_m2CF8D552EFD6768C39D2579B76356659F2C570DD,
	UserInfoInterfaceList_set_Item_mA5E90884FF1A01C43CCE62ABD40CD296DC000ED5,
	UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074,
	UserInfoInterfaceList_CopyTo_m11CCDBDABE905725AC2B4D5FBCF19AA926C4F25A,
	UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E,
	UserInfoInterfaceList_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CFirebase_Auth_UserInfoInterfaceU3E_GetEnumerator_mCDDED8A7BA953D28567C3DE977F584DC759DE4C3,
	UserInfoInterfaceList_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m0D58B068226E88BDF4B38BF06648A191AC0E75F9,
	UserInfoInterfaceList_GetEnumerator_m34C89390756E8DCA0D4FDE9EC4020AF7AFBB4021,
	UserInfoInterfaceList_Clear_mF43F449B72C454169BE9001F0BDB87A43B9BC711,
	UserInfoInterfaceList_Add_m272A420019BB6431562C0EE4CF84E86280F2BBD9,
	UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B,
	UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00,
	UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA,
	UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F,
	UserInfoInterfaceList_Insert_mEF354596EFD206D50AE0F69B9A76E1A6B4DC1878,
	UserInfoInterfaceList_RemoveAt_mBE2A2F80ACBA832F24F7B066A861019BFC1F4E0B,
	UserInfoInterfaceList_Contains_m3D0CA1C4D5D72A7D59BE69C5067AAE50CF4063F8,
	UserInfoInterfaceList_IndexOf_m814BFA29319E2C0C125EE6E87B0FD742C890CB86,
	UserInfoInterfaceList_Remove_m6AE184FAF9D7EC2568042FF0304127885EC5F422,
	UserInfoInterfaceListEnumerator__ctor_mB52655D936C618ADDC4D506EF165F8ADAFAF350E,
	UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720,
	UserInfoInterfaceListEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mA488DE8E00C60FC4C370D97E7084C9609FF0368E,
	UserInfoInterfaceListEnumerator_MoveNext_mB0E76F84C535182420B86E45C821E100E902F0A9,
	UserInfoInterfaceListEnumerator_Reset_m89E71EC357585556E719F9C537D6A4954D0804E4,
	UserInfoInterfaceListEnumerator_Dispose_mA619C0FC99C494EE549F5868CBDCDE2E02F5CF03,
	Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A,
	Future_User_Dispose_m82E8E0011FC005AB27FDF878D851C18D22FB19E1,
	Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506,
	Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE,
	Future_User_SetOnCompletionCallback_mA4F7DBBBBBA68E9A54C2231034CCE80415DD80A6,
	Future_User_SetCompletionData_m7B693E6CE4A587D18797FE4A21D00A4C823DB7BC,
	Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47,
	Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A,
	Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A,
	Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902,
	Future_User__cctor_m4576D93855EFC88AA0CD018753370EBD1F2BE0EA,
	Action__ctor_m9AE0C8C97132B6FFDB6D84FDC9A8802717222F39,
	Action_Invoke_m8B7DBC95C4F8A5255426959F447C3FDDC7DFB2B5,
	Action_BeginInvoke_m153941CDA9F036DD9B908B6294DAFAB3A9C763E4,
	Action_EndInvoke_m49FAC74EBAB03687FE2F0001B201C163432F6DBC,
	SWIG_CompletionDelegate__ctor_m9FCE4E269635690B019F5FF509AFF343FA13DBDC,
	SWIG_CompletionDelegate_Invoke_mDEC298EADEA6875C62E559FACECB441130831BE7,
	SWIG_CompletionDelegate_BeginInvoke_m1F1D032BA7DC7797D7B332090F05CF043978EA1F,
	SWIG_CompletionDelegate_EndInvoke_m78A511D0EE58036E4D5A03C2690C362F49A84CE4,
	U3CU3Ec__DisplayClass4_0__ctor_m69741D9DAF70E435DFBFF0CFFA06B08796349300,
	U3CU3Ec__DisplayClass4_0_U3CGetTaskU3Eb__0_mC165A76D4A3E3CA94F84D8F613ECE4E75D2EF7DF,
	Credential__ctor_mBC968251CEBE306DF7CD89C85E74D1B1D451CFC7,
	Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42,
	Credential_Finalize_mD6808D5E0D70311F7639F026600E220CF64ED657,
	Credential_Dispose_m76866FF791102DEEC0E64363C978654CA2E0DD87,
	Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5,
	Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039,
	FacebookAuthProvider__cctor_mF5D3E8AA0DA69CEDDE05F5547497DC789089ECF6,
	FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5,
	PlayGamesAuthProvider__cctor_m8EE6103C650CD01A16F0ECFB31FD4E50E6E17CBA,
	PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33,
	UserInfoInterface__ctor_m7E75B0EBDFD46089D31A77871A8B74520EEBC04C,
	UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680,
	UserInfoInterface_Finalize_m9DFDC97F9560711EAD0F29882ACDB60CCDA56D66,
	UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B,
	UserInfoInterface_Dispose_m8532EF4241914613A205B8E2E1720C39FD85C1AA,
	UserInfoInterface_get_PhotoUrl_m459FA1AD2C0AB62EFEF0B264F4ED9532CC15484C,
	UserInfoInterface_get_UserId_m3A02719809CE964AFB9ABECF26FFCF81D2415C9D,
	UserInfoInterface_get_Email_m02D9D14CC54BD35802499A248FD7382F9E95C6AC,
	UserInfoInterface_get_DisplayName_m3DCD08821470A2241296F8AE5E40E28935E61832,
	UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024,
	UserInfoInterface_get_ProviderId_mA71AA9527D83D2FBACFBCB38ACACA341F197660C,
	FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5,
	FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D,
	FirebaseUser_Dispose_m347979011C289888965AA04311F9CD818CB5994A,
	FirebaseUser_get_PhotoUrl_mAC0F8F1390483E1FD30D932527542C89E9C389C6,
	FirebaseUser_UpdateEmailAsync_m8D0B4CA7BE4605CF684B74CEE6C5A2ADA2E1F5A7,
	FirebaseUser_UpdateUserProfileAsync_mACFB318A8FA0A47B70F317197B0C5884D5772D9D,
	FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627,
	FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB,
	FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06,
	FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902,
	FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F,
	FirebaseUser_get_ProviderData_mE5D70ADC73F94F71D686939C53A8984CCEC682D0,
	FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD,
	UserProfile__ctor_m3F41707B2949CA717EB71681388E002A3106801A,
	UserProfile_getCPtr_m43D66A8F09ED56F3026B25C5DFEEEA8A3BEB7CEC,
	UserProfile_Finalize_m47BE7F05B7BA57E259D3FECCCBBC7AEEE0351DB1,
	UserProfile_Dispose_m8B47F62EA95EDA7AF6819F157ED0C3043944380C,
	UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3,
	UserProfile_set_PhotoUrl_mCDA8A14D63367925AD4D59808B472B88086D977A,
	UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809,
	UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557,
	UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363,
	FirebaseAuth__ctor_m5A11B7E7ACB1FF9296510387F4A203804EC9B5D0,
	FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9,
	FirebaseAuth_Finalize_m5E134A851F020A7EA755620983EF8D2879DCE270,
	FirebaseAuth_Dispose_mDAE8E88D96D92B5D08A8E3165DFA4F3F4DCF30DA,
	FirebaseAuth_Dispose_m31BB65B92A235E510C6FBC6B250BFE0D5D4D4A18,
	FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15,
	FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC,
	FirebaseAuth_GetAuth_mEFC135E9B1D37E4739D3E2A8D30A1C80DDE3F157,
	FirebaseAuth_OnAppDisposed_m432A4ECCD62471F891EC24B73F0BE81794DCA4EB,
	FirebaseAuth_DisposeInternal_m5DD53E6F76144CC4B417157E6C3475F7704117DC,
	FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C,
	FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7,
	FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF,
	FirebaseAuth_get_DefaultInstance_mA307D08C178B8C5A8EA7BE0F3222668A544FA228,
	FirebaseAuth_add_StateChanged_mFA3F7A1FE096865FF349DBD64644371C0C8CCB34,
	FirebaseAuth_remove_StateChanged_mF960C837B4BD78DAF5716BC2348EF9702AD2B736,
	FirebaseAuth_add_stateChangedImpl_m1387CE8F8F0F4796FFDF97ADD352101FEE55335C,
	FirebaseAuth_remove_stateChangedImpl_m6BC5FB994A66D606B5783D721FE3D17FD3D5B4D2,
	FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF,
	FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF,
	FirebaseAuth_SignInWithCredentialAsync_m0C8097559DE43C62ED1AAC3D6105B155B02C948F,
	FirebaseAuth_SignInAnonymouslyAsync_m6963AC820328CD24A672D8800D1B51BD98FF0995,
	FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF,
	FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B,
	FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C,
	FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D,
	FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D,
	FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145,
	FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B,
	FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74,
	FirebaseAuth__cctor_m9714CC491B97AB4DE8C0D5763ACB8BAEB9E80936,
	StateChangedDelegate__ctor_m5A465ECFE2F3056DE914C988625D85656F431F1C,
	StateChangedDelegate_Invoke_m76E4AE64047B20E5DBA21576DB6C0B780E2C2729,
	StateChangedDelegate_BeginInvoke_m57C156934DDB33999507D45527F6E84DF9F6BA35,
	StateChangedDelegate_EndInvoke_mD4D5CA766ED9C02ED543990F366C99E187DA5E3A,
	U3CU3Ec__DisplayClass19_0__ctor_mACC4F54E5C54C1F4E1983D751939FEE59B3EAE4D,
	U3CU3Ec__DisplayClass19_1__ctor_m1C29623F09C98E18908C68A9AAEA1F77D1CC820F,
	U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A,
	U3CU3Ec__DisplayClass22_0__ctor_m3362924530F3A7340346D7C34BC4B0E66A70A061,
	U3CU3Ec__DisplayClass22_1__ctor_mE1858D9A2B9F52467C9D68DF38C2EFDCC99DE22D,
	U3CU3Ec__DisplayClass22_1_U3CForwardStateChangeU3Eb__0_m4958CF79DC4F966EB6B44A3100894727D07F5D49,
	U3CU3Ec__cctor_m07998BEFD2CCB1494ECB6037ACA9B180B78E0086,
	U3CU3Ec__ctor_m17B1FF4992C7A229A6F9003C935994D8A8687BCB,
	U3CU3Ec_U3CStateChangedFunctionU3Eb__23_0_m4DF5A2B9C4EF839583FBDD03461848F5EF489F4E,
	U3CU3Ec_U3CIdTokenChangedFunctionU3Eb__24_0_mBDB096D23092A8E0948E7EEABA9ECE55DD9C92FB,
	U3CU3Ec__DisplayClass48_0__ctor_m2347AE3FAE11B4DFFD2348CD75D799A145AFEDCC,
	U3CU3Ec__DisplayClass48_0_U3CSignInWithCredentialAsyncU3Eb__0_m732B3D2DD97881C4CEEF6D96D047F28D1DE67CAE,
	U3CU3Ec__DisplayClass50_0__ctor_m91E489361C82BCFD50E78CF12F492893906F7B5A,
	U3CU3Ec__DisplayClass50_0_U3CSignInAnonymouslyAsyncU3Eb__0_m1D34F38B75501F86230E71CD29B059F63C572E4A,
	AuthUtilPINVOKE__cctor_mB959A2D2962E2F91E1F004B060423C21556019E0,
	AuthUtilPINVOKE_UserInfoInterfaceList_Clear_m0477B48FDCFEEC92FC007E3F43EC65647F9E240A,
	AuthUtilPINVOKE_UserInfoInterfaceList_Add_m235699A22C7864EC52A0A694E8549F1C4C1AE089,
	AuthUtilPINVOKE_UserInfoInterfaceList_size_m4022FD61AA1F01B2682E30DD2660D39144EE3875,
	AuthUtilPINVOKE_UserInfoInterfaceList_getitemcopy_mB07EF1F7557FF0370E3ACE819AEE2612D4CB524D,
	AuthUtilPINVOKE_UserInfoInterfaceList_getitem_m57D02F5BE29828EB28F2B8B3DC5591207B34BEE9,
	AuthUtilPINVOKE_UserInfoInterfaceList_setitem_mCB2A5B2BF72BD7E40187C4B07E9CC8FA9344A441,
	AuthUtilPINVOKE_UserInfoInterfaceList_Insert_mF8231269D3DB49CE00D815C59748E6D77C5E8A6D,
	AuthUtilPINVOKE_UserInfoInterfaceList_RemoveAt_m3E7CC088E5D8F62308D0A7193D34DE0DA9215179,
	AuthUtilPINVOKE_UserInfoInterfaceList_Contains_mACD9BBB51A6B1007C9107D484C7963ACC01C1803,
	AuthUtilPINVOKE_UserInfoInterfaceList_IndexOf_mC0DFA98501EF7C4F2D513ACF7B513877B71E69D3,
	AuthUtilPINVOKE_UserInfoInterfaceList_Remove_m58DD7F22CAFF88AABC98EBB2B4003720BCDEE7BC,
	AuthUtilPINVOKE_delete_UserInfoInterfaceList_mF4C1DD6531A015AC1E86A763A37E9E2EDC5B1A51,
	AuthUtilPINVOKE_Future_User_SWIG_OnCompletion_mA4865E7D9C1E147E26B7FD5F81078FA99F7ADF32,
	AuthUtilPINVOKE_Future_User_SWIG_FreeCompletionData_m4E8D0B09D212ED4E404E86B97439D0AF51879DB1,
	AuthUtilPINVOKE_Future_User_GetResult_m36CFE956376E3A4DC187BCC9D6F2732417615FE9,
	AuthUtilPINVOKE_delete_Future_User_m09EF8AC9392014A450B9A39F9237AB703460AE42,
	AuthUtilPINVOKE_delete_Credential_m02588ECD12EF7FBADE68921F2AC6F2DBA0E37C88,
	AuthUtilPINVOKE_Credential_IsValid_m590E8B8C5882B73A5D6C253E2E6591D789A61081,
	AuthUtilPINVOKE_FacebookAuthProvider_GetCredential_m6C17A5533AB95072B52FEFFEF1BC520C8B3B89BF,
	AuthUtilPINVOKE_PlayGamesAuthProvider_GetCredential_mF15F71E21D192B4940648E872A8F7F8E8713A0CA,
	AuthUtilPINVOKE_delete_UserInfoInterface_m6EECB261E56A9D7F8BB61C69988423AD41B16747,
	AuthUtilPINVOKE_UserInfoInterface_UserId_get_m3B7A2BC624181BC02ACF3481BF437903B1EFC954,
	AuthUtilPINVOKE_UserInfoInterface_Email_get_m49D0AB8130C3E528C1D44B66CC7D681B2F2E804B,
	AuthUtilPINVOKE_UserInfoInterface_DisplayName_get_m40F5D1957524C394F0A8022F47B399931BE4DFF7,
	AuthUtilPINVOKE_UserInfoInterface_PhotoUrlInternal_get_mA1C69290B7452547D58BA4675DAE501090CCA5B7,
	AuthUtilPINVOKE_UserInfoInterface_ProviderId_get_mB0AC6D00DCC186EB3D315809B0727D666F24C9B0,
	AuthUtilPINVOKE_delete_FirebaseUser_m21C9A5C887AD120B4228AC2E15E7EA499F745A9F,
	AuthUtilPINVOKE_FirebaseUser_UpdateEmail_mE4BC3966DE87BDA371C7CE7A0F23D91BEC313CCB,
	AuthUtilPINVOKE_FirebaseUser_UpdateUserProfile_m828FCDD053CA923BA34246E6679C39F6BCBF076C,
	AuthUtilPINVOKE_FirebaseUser_LinkWithCredential_mBC153FDCFAC9320B9628591B105955EFCCA5E03F,
	AuthUtilPINVOKE_FirebaseUser_DisplayName_get_mDD546C27F066D7A7C9F9612FB9CC18C6E8D9E2BF,
	AuthUtilPINVOKE_FirebaseUser_Email_get_m1C74E1D469EF27F32E2810738DBF9CA1BA23582D,
	AuthUtilPINVOKE_FirebaseUser_IsAnonymous_get_m62129D05B6F8B37AF44E526484F3F44094D907D8,
	AuthUtilPINVOKE_FirebaseUser_PhotoUrlInternal_get_m12B7DDC9E668B2B40694C4DEB10A2C4EAC81FD00,
	AuthUtilPINVOKE_FirebaseUser_ProviderData_get_mD388B7BE41C3645C7464C3C0DAA81290CF2CED47,
	AuthUtilPINVOKE_FirebaseUser_UserId_get_mA5AC5F6494A1F4A6A3F0DBC90E0D4D1006F5CF5A,
	AuthUtilPINVOKE_new_UserProfile_mF862E0AB1127CCB19BEA8E520C8EC4E4AD85FF1C,
	AuthUtilPINVOKE_UserProfile_DisplayName_set_m2E3AA6A6AFEED65380B898B3BFD7FDD033CB5B59,
	AuthUtilPINVOKE_UserProfile_PhotoUrlInternal_set_mA5391563F2C002A4D053CF4BC28C2B7548E699B0,
	AuthUtilPINVOKE_delete_UserProfile_mEBF4B6D168A27F787149E7D71C5F6DA18AA6104E,
	AuthUtilPINVOKE_FirebaseAuth_SignInWithCredentialInternal_m286B534BC062DE3FD14436764BCE057923965B67,
	AuthUtilPINVOKE_FirebaseAuth_SignInAnonymouslyInternal_m769C6AFE4B52A39BFFB28D8298F6664514DB39C6,
	AuthUtilPINVOKE_FirebaseAuth_SignOut_m03EDFAB11CF0A459DD7CA6A3E097790BA3D91458,
	AuthUtilPINVOKE_FirebaseAuth_GetAuthInternal_m9CA73CC68A18B4B86AFFB925E37E8F6A0DAE72AA,
	AuthUtilPINVOKE_FirebaseAuth_LogHeartbeatInternal_m40FB5B32F8D0549946D5FA26631C4D88338C6D5C,
	AuthUtilPINVOKE_FirebaseAuth_ReleaseReferenceInternal_m77FAC25E87B2644CA0EF2B9BFF0201C2F38349D9,
	AuthUtilPINVOKE_FirebaseAuth_CurrentUserInternal_get_m1646DA22D3C6243710AFD36C183194346B494495,
	AuthUtilPINVOKE_CreateAuthStateListener_m093D034502CF32CE850E6A9E286C15EAE6785C2B,
	AuthUtilPINVOKE_DestroyAuthStateListener_m632301CCA03C97183D31D2E819196742BBB8BB1B,
	AuthUtilPINVOKE_CreateIdTokenListener_mA59101F16559583539311A7139734B88B26CF10B,
	AuthUtilPINVOKE_DestroyIdTokenListener_m7CACC17CE6A8EB523C7FC9DC96051AB52A6571F1,
	AuthUtilPINVOKE_Future_User_SWIGUpcast_m9F5C5F23C6908B207633FFC69E6B33329CCDD5FC,
	AuthUtilPINVOKE_FirebaseUser_SWIGUpcast_m2E75C4601FD7E308456F2F0BF75ECB73F92D0E44,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AuthUtil_mD13FDDDDC03DD49540B6AFFC935A58DE0B534762,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AuthUtil_m85D92F4B1140B7B2228AB92E057C747B9A07FCA7,
	SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625,
	SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145,
	SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888,
	SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719,
	SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D,
	SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2,
	SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E,
	SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2,
	SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43,
	SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77,
	SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7,
	SWIGExceptionHelper__cctor_mC03977EF4F677D23618BA72DEE999DAD6233D0CE,
	SWIGExceptionHelper__ctor_mFF2CE315200639030B26C32F018A7E7354B81663,
	ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D,
	ExceptionDelegate_Invoke_m797B0B4E66994FAF224C4EB881F468E142D745B7,
	ExceptionDelegate_BeginInvoke_mAC113B823C70AA10A3D5344072B3FDD0101EA845,
	ExceptionDelegate_EndInvoke_mD9FB06B5011432C53792DCFFC3AA5F23D16703EE,
	ExceptionArgumentDelegate__ctor_mC3898DBFD267903C5001A38AD119C7DA5FE561F2,
	ExceptionArgumentDelegate_Invoke_m6928DBD3A8C7DD5AEF69A157C7833B46B927DA6D,
	ExceptionArgumentDelegate_BeginInvoke_mBF7ADADE9B2D349A22A925C4C1728A92B154C42D,
	ExceptionArgumentDelegate_EndInvoke_m70375B45334E455A99D59904D6A945A04B6886EF,
	SWIGPendingException_get_Pending_m746EAD61F79AF2A98C4350069D4507EAC1D251DE,
	SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C,
	SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD,
	SWIGPendingException__cctor_mC42B091F09F3F6365C73742A66AD460617B4B7EB,
	SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_mCF54A64D405E0EE541CB782629AE4D3EFFF01E69,
	SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134,
	SWIGStringHelper__cctor_mE675A6A15898F4EF6BD38CADB60C065692FF609E,
	SWIGStringHelper__ctor_m22B2245AE72469FC2016E3B70329E4117E38AF96,
	SWIGStringDelegate__ctor_m34FFAA789252BBCC866BE5712BD2072DEC39AA75,
	SWIGStringDelegate_Invoke_mCB669A3F17E62E6075E1CCAD17E777E903F5739C,
	SWIGStringDelegate_BeginInvoke_mA37707372B07564EB2B162155D8F88A43C9F89BF,
	SWIGStringDelegate_EndInvoke_m70160AEB75A18A0445E89204A64C3CFEF21F767E,
	AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828,
	AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7,
	AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187,
	AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22,
};
static const int32_t s_InvokerIndices[244] = 
{
	14,
	14,
	14,
	14,
	14,
	120,
	23,
	23,
	31,
	102,
	34,
	62,
	10,
	124,
	416,
	14,
	14,
	14,
	23,
	26,
	10,
	34,
	34,
	62,
	62,
	32,
	9,
	104,
	9,
	26,
	14,
	14,
	102,
	23,
	23,
	120,
	31,
	0,
	23,
	26,
	7,
	121,
	1251,
	7,
	14,
	3,
	163,
	23,
	101,
	26,
	163,
	32,
	519,
	26,
	23,
	23,
	120,
	1252,
	23,
	23,
	31,
	102,
	3,
	0,
	3,
	0,
	120,
	1252,
	23,
	23,
	31,
	14,
	14,
	14,
	14,
	14,
	14,
	120,
	1252,
	31,
	14,
	28,
	28,
	28,
	14,
	14,
	102,
	14,
	14,
	14,
	120,
	1252,
	23,
	23,
	31,
	26,
	23,
	26,
	26,
	120,
	1252,
	23,
	23,
	31,
	18,
	23,
	0,
	27,
	23,
	1263,
	25,
	25,
	4,
	26,
	26,
	26,
	26,
	28,
	14,
	28,
	14,
	27,
	28,
	14,
	23,
	613,
	111,
	111,
	14,
	3,
	163,
	7,
	750,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	26,
	26,
	23,
	26,
	23,
	26,
	3,
	1253,
	1264,
	1254,
	1265,
	1265,
	1266,
	1266,
	1267,
	1268,
	1269,
	1268,
	1253,
	1256,
	1257,
	1270,
	1253,
	1253,
	1271,
	24,
	24,
	1253,
	1255,
	1255,
	1255,
	1255,
	1255,
	1253,
	1272,
	1273,
	1273,
	1255,
	1255,
	1271,
	1255,
	1270,
	1255,
	681,
	1274,
	1274,
	1253,
	1273,
	1270,
	1253,
	1275,
	1253,
	1253,
	1270,
	1272,
	1257,
	1272,
	1257,
	1165,
	1165,
	1258,
	144,
	111,
	111,
	111,
	111,
	111,
	111,
	111,
	111,
	111,
	111,
	111,
	122,
	122,
	122,
	3,
	23,
	163,
	26,
	166,
	26,
	163,
	27,
	202,
	26,
	49,
	111,
	4,
	3,
	111,
	0,
	3,
	23,
	163,
	28,
	166,
	28,
	1276,
	886,
	1276,
	886,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[18] = 
{
	{ 0x0600002A, 36,  (void**)&Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47_RuntimeMethod_var, 0 },
	{ 0x0600006F, 34,  (void**)&FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7_RuntimeMethod_var, 0 },
	{ 0x06000070, 35,  (void**)&FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF_RuntimeMethod_var, 0 },
	{ 0x060000CD, 19,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625_RuntimeMethod_var, 0 },
	{ 0x060000CE, 20,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145_RuntimeMethod_var, 0 },
	{ 0x060000CF, 21,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944_RuntimeMethod_var, 0 },
	{ 0x060000D0, 22,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888_RuntimeMethod_var, 0 },
	{ 0x060000D1, 23,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719_RuntimeMethod_var, 0 },
	{ 0x060000D2, 24,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D_RuntimeMethod_var, 0 },
	{ 0x060000D3, 25,  (void**)&SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2_RuntimeMethod_var, 0 },
	{ 0x060000D4, 26,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9_RuntimeMethod_var, 0 },
	{ 0x060000D5, 27,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E_RuntimeMethod_var, 0 },
	{ 0x060000D6, 28,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2_RuntimeMethod_var, 0 },
	{ 0x060000D7, 29,  (void**)&SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43_RuntimeMethod_var, 0 },
	{ 0x060000D8, 30,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77_RuntimeMethod_var, 0 },
	{ 0x060000D9, 31,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032_RuntimeMethod_var, 0 },
	{ 0x060000DA, 32,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7_RuntimeMethod_var, 0 },
	{ 0x060000EA, 33,  (void**)&SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134_RuntimeMethod_var, 0 },
};
extern const Il2CppCodeGenModule g_Firebase_AuthCodeGenModule;
const Il2CppCodeGenModule g_Firebase_AuthCodeGenModule = 
{
	"Firebase.Auth.dll",
	244,
	s_methodPointers,
	s_InvokerIndices,
	18,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
