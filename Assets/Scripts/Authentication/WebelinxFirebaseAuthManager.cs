using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using UnityEngine.UI;
using System.Threading.Tasks;
using UnityEngine.Events;
#if FACEBOOK
using Facebook.Unity;
#endif
using System;

namespace Authentication
{
    public enum Providers { GooglePlay, Apple, Facebook };

    public class WebelinxFirebaseAuthManager : MonoBehaviour
    {
        public static WebelinxFirebaseAuthManager Instance;

        public UnityAction OnSignOut;

        // Flag that providers set depending on whether they are trying to sign in or link accounts
        public bool isLinkingAccounts;

        public FirebaseAuth auth;

        // Reference to auth.CurrentUser
        public FirebaseUser user;

        public SignedInUserFirebaseInfo userFirebaseInfo;
        public SignedInUserProviderInfo userProviderInfo;


        private void Awake()
        {
            isLinkingAccounts = true;

            if (Instance == null) Instance = this;

            isLinkingAccounts = false;

            InitializeFirebase();
        }


        public void InitializeFirebase()
        {
            auth = FirebaseAuth.DefaultInstance;

            user = null;

            auth.StateChanged += AuthStateChanged;

            //AuthStateChanged(this, null);
        }

        public void SignInThroughProvider(string accessToken, Providers provider)
        {
            Credential credential = GetCredential(accessToken, provider);

            if(credential == null || !credential.IsValid())
            {
                Debug.LogError("Invalid Credential");
                return;
            }

            auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Login canceled");

                    Debug.LogError("You canceled your Login.");
                    return;
                }

                if (task.IsFaulted)
                {
                    WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Login error");

                    Debug.LogError("Login error " + task.Exception);
                    return;
                }

                user = task.Result;
            });
        }

        /// <summary>
        /// Links currently logged in account with new provider
        /// Linking will fail if you are trying to link with the provider that already has an account with given credentials
        /// </summary>
        /// <param name="accessToken">Provider's accessToken</param>
        /// <param name="provider">Provider type</param>
        public void LinkAccountWithAnotherProvider(string accessToken, Providers provider)
        {
            Credential credential = GetCredential(accessToken, provider);

            if (credential == null || !credential.IsValid())
            {
                Debug.LogError("Invalid Credential");
                return;
            }

            auth.CurrentUser.LinkWithCredentialAsync(credential).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("LinkWithCredentialAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
                    return;
                }
                
                // When linking accounts, auth's state is not changed, so this function needs to be called manually
                AuthStateChanged(this, null);
            });
        }


        /// <summary>
        /// Creates new anonymous firebase user. 
        /// If you sign out and sign in again, even from the same device, new account will be created.
        /// Only way that data can be saved when you log out and log in again is through linking anonymous account with any provider and then signing in with that provider
        /// </summary>
        public void SignInAnonymously()
        {
            auth.SignInAnonymouslyAsync().ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("SignInAnonymouslyAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                    return;
                }

                user = task.Result;
            });
        }

        public void SignOut()
        {
            if (auth.CurrentUser != null && user != null)
            {
                // TODO: REMOVE -  TEST ONLY
                if (PlayerPrefs.HasKey("ProfilePicture"))
                    PlayerPrefs.DeleteKey("ProfilePicture");

                user = null;
                auth.SignOut();
                OnSignOut.Invoke();
            }
        }

        public Credential GetCredential(string accessToken, Providers provider)
        {
            Credential credential = null;

            switch (provider)
            {
                case Providers.Facebook:
                    credential = FacebookAuthProvider.GetCredential(accessToken);
                    break;
                case Providers.GooglePlay:
                    credential = PlayGamesAuthProvider.GetCredential(accessToken);
                    break;
                default:
                    break;
            }

            return credential;
        }

        private void AuthStateChanged(object sender, System.EventArgs eventArgs)
        {
            if (auth.CurrentUser == null)
            {
                user = null;
                WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "You are not signed in");

                return;
            }

            // TEST: If linking is done, sign user out 
            if(isLinkingAccounts)
            {
                isLinkingAccounts = false;

                WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Linking accounts done. Sign in again");

                user = null;
                userFirebaseInfo = null;
                userProviderInfo = null;
                auth.SignOut();
                //OnSignOut.Invoke();

                return;
            }

            if(auth.CurrentUser != null)
            {
                user = auth.CurrentUser;

                WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, user.IsAnonymous ? "You signed in anonymously!" : "You signed in through provider");

                userFirebaseInfo = new SignedInUserFirebaseInfo();
                
                SetUserInformations(user);
                WebelinxFirebaseAuthView.Instance.PrintUserInformations();
            }
        }

        private void SetUserInformations(FirebaseUser user)
        {
            userFirebaseInfo = new SignedInUserFirebaseInfo();
            userProviderInfo = new SignedInUserProviderInfo();

            userFirebaseInfo.SetData(user);

            if(!user.IsAnonymous)
            {
                userProviderInfo.SetData(user);
                SyncFirebaseUserDataWithProviderData();
            }
        }

        /// <summary>
        /// Sync some basic informations from provider's user data to Firebase user data and update Firebase user
        /// If there are not differences, there is no need to call update
        /// If user is anonymous there is no data to be Synced
        /// </summary>
        private void SyncFirebaseUserDataWithProviderData()
        {
            Firebase.Auth.UserProfile profile = new UserProfile();
            bool isAnythingChanged = false;
            bool isEmailChanged = false;

            if (userFirebaseInfo.DisplayName != userProviderInfo.DisplayName)
            {
                userFirebaseInfo.DisplayName = userProviderInfo.DisplayName;
                profile.DisplayName = userFirebaseInfo.DisplayName;
                isAnythingChanged = true;
            }

            if (userFirebaseInfo.PhotoUri != userProviderInfo.PhotoUri)
            {
                userFirebaseInfo.PhotoUri = userProviderInfo.PhotoUri;
                profile.PhotoUrl = userFirebaseInfo.PhotoUri;
                isAnythingChanged = true;
            }

            if (userFirebaseInfo.Email != userProviderInfo.Email)
            {
                userFirebaseInfo.Email = userProviderInfo.Email;
                isEmailChanged = true;
            }
            

            if(isAnythingChanged)
            {
                user.UpdateUserProfileAsync(profile).ContinueWith(task =>
                {
                    if (task.IsCanceled)
                    {
                        Debug.LogError("Profile update canceled");
                        return;
                    }
                    if (task.IsFaulted)
                    {
                        Debug.LogError("Error update-ing profile : "+task.Exception);
                        return;
                    }

                    // Handle successful update here
                    WebelinxFirebaseAuthView.Instance.PrintUserInformations();
                });
            }

            if(isEmailChanged)
            {
                user.UpdateEmailAsync(userFirebaseInfo.Email).ContinueWith(task =>
                {
                    if (task.IsCanceled)
                    {
                        Debug.LogError("UpdateEmailAsync was canceled.");
                        return;
                    }
                    if (task.IsFaulted)
                    {
                        Debug.LogError("UpdateEmailAsync encountered an error: " + task.Exception);
                        return;
                    }

                    // Handle successful update here
                    WebelinxFirebaseAuthView.Instance.PrintUserInformations();
                });
            }


        }

        void OnDestroy()
        {
            auth.StateChanged -= AuthStateChanged;
            auth = null;
        }
    };

    public class SignedInUserFirebaseInfo
    {
        public string UserUUID { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public bool IsAnonymous { get; set; }
        public Uri PhotoUri { get; set; }

        public SignedInUserFirebaseInfo() { }
        public void SetData(FirebaseUser user)
        {
            UserUUID = user.UserId;
            DisplayName = user.DisplayName;
            Email = user.Email;
            IsAnonymous = user.IsAnonymous;
            PhotoUri = user.PhotoUrl;
        }
        public override string ToString()
        {
            return $"Firebase UUID: {UserUUID}\nDisplay Name: {DisplayName}\n Email: {Email}\n Is Anonymous? : {IsAnonymous}\n Photo URI : {PhotoUri}";
        }
    }

    public class SignedInUserProviderInfo
    {
        public string ProviderNameID { get; set; }
        public string UserProviderID { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public Uri PhotoUri { get; set; }

        public SignedInUserProviderInfo() { }
        public void SetData(FirebaseUser user)
        {
            foreach (IUserInfo profile in user.ProviderData)
            {
                UserProviderID = profile.UserId;
                ProviderNameID = profile.ProviderId;
                DisplayName = profile.DisplayName;
                Email = profile.Email;
                PhotoUri = profile.PhotoUrl;
            }
        }
        public override string ToString()
        {
            return $"Provider name: {ProviderNameID}\n User's provider ID: {UserProviderID}\n Email: {Email}\n Display Name: {DisplayName}\n Photo URI : {PhotoUri}";
        }
    }

}
