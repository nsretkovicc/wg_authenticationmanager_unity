
#if FACEBOOK
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System;
using Facebook.Unity;
using UnityEngine.Events;

namespace Authentication
{
    public class WebelinxFacebookAuthController : MonoBehaviour
    {
        public static WebelinxFacebookAuthController Instance;

        public string appId;
        // WARNING --- SHOULD BE HIDDEN
        public string appSecret;

        private List<string> permissions = new List<string>() { "email", "public_profile" };

        void Awake()
        {
            if(Instance == null)
                Instance = this;

            if (FB.IsInitialized)
                FB.ActivateApp();
            else
            {
                //Handle FB.Init
                FB.Init(() =>
                {
                    FB.ActivateApp();
                });
            }
        }

        public void FacebookLogin(bool isLinking)
        {
            WebelinxFirebaseAuthManager.Instance.isLinkingAccounts = isLinking;

            FB.LogInWithReadPermissions(permissions, FacebookLoginCallback);
        }


        public void FacebookLoginCallback(ILoginResult result)
        {
            if (FB.IsLoggedIn)
            {
                // AccessToken class has session details
                AccessToken aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
                
                // If we are not linking accounts, just sign in
                if(!WebelinxFirebaseAuthManager.Instance.isLinkingAccounts)
                {
                    WebelinxFirebaseAuthManager.Instance.SignInThroughProvider(aToken.TokenString, Providers.Facebook);
                    //GetUserInformation(aToken.UserId);
                }
                else
                    WebelinxFirebaseAuthManager.Instance.LinkAccountWithAnotherProvider(aToken.TokenString, Providers.Facebook);
            }
            else if (result.Cancelled)
            {
                WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Facebook login canceled");
            }
            else
            {
                WebelinxFirebaseAuthView.Instance.UpdateUserStatusText(null, "Facebook Login error");
            }
        }


#region Test - delete
        public void GetUserInformation(string userId)
        {
            // Display profile picture
            // Gets user's profile picture from graph API request, could be saved in user's firebase data but the problem occurs when user updates its profile pic
            FB.API($"/{userId}/picture?type=square&height=128&width=128&access_token={appId}|{appSecret}", HttpMethod.GET, UpdateUserProfilePicture);
        }

        public void UpdateUserProfilePicture(IGraphResult result)
        {
            if (result.Texture == null) return;

            byte[] texBytes = result.Texture.EncodeToPNG();
            string texString = Convert.ToBase64String(texBytes);

            PlayerPrefs.SetString("ProfilePicture", texString);
        }
#endregion
    }
}
#endif