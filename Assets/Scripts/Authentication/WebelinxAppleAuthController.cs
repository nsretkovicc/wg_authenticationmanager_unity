﻿using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class WebelinxAppleAuthController : MonoBehaviour
{
    private IAppleAuthManager appleAuthManager;

    void Start()
    {
        Debug.Log("Da li je platforma supported: " + AppleAuthManager.IsCurrentPlatformSupported);
        // If the current platform is supported
        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
            PayloadDeserializer deserializer = new PayloadDeserializer();
            // Creates an Apple Authentication manager with the deserializer
            this.appleAuthManager = new AppleAuthManager(deserializer);
        }
    }

    void Update()
    {
        // Updates the AppleAuthManager instance to execute
        // pending callbacks inside Unity's execution loop
        if (this.appleAuthManager != null)
        {
            this.appleAuthManager.Update();
        }
    }

    public void AppleAuthHandler(bool isLinking)
    {
        AppleAuthLoginArgs loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

        this.appleAuthManager.LoginWithAppleId(loginArgs, (ICredential credential) =>
            {
                // Obtained credential, cast it to IAppleIDCredential
                IAppleIDCredential appleIdCredential = credential as IAppleIDCredential;
                
                if (appleIdCredential != null)
                {
                    // Apple User ID
                    // You should save the user ID somewhere in the device
                    string userId = appleIdCredential.User;
                    //PlayerPrefs.SetString("AppleUserIdKey", userId);

                    // Email (Received ONLY in the first login)
                    string email = appleIdCredential.Email;
                    Debug.Log("Email log: " + email);

                    // Full name (Received ONLY in the first login)
                    IPersonName fullName = appleIdCredential.FullName;

                    Debug.Log("Full name log: " + fullName);

                    // Identity token
                    string identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken, 0, appleIdCredential.IdentityToken.Length);
                    Debug.Log("Identity token log: " + fullName);

                    // Authorization code
                    string authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode, 0, appleIdCredential.AuthorizationCode.Length);
                    Debug.Log("Authorization Code log: " + fullName);

                    // And now you have all the information to create/login a user in your system
                }
            },
            error =>
            {
                // Something went wrong
                AuthorizationErrorCode authorizationErrorCode = error.GetAuthorizationErrorCode();
            });
    }
}
