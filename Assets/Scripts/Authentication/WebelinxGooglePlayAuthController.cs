﻿#if UNITY_ANDROID
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System.Threading.Tasks;

namespace Authentication
{
    public class WebelinxGooglePlayAuthController : MonoBehaviour
    {
        public WebelinxGooglePlayAuthController Instance;

        public string accessToken;

        void Start()
        {
            if(Instance == null)
                Instance = this;

            WebelinxFirebaseAuthManager.Instance.OnSignOut += OnFirebaseSignOut;

            PlayGamesPlatform.DebugLogEnabled = true;
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().RequestServerAuthCode(false).Build();


            accessToken = "";

            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.Activate();

            
        }

        public void GooglePlayAuthHandler(bool isLinking)
        {
            WebelinxFirebaseAuthManager.Instance.isLinkingAccounts = isLinking;

            Social.localUser.Authenticate((success) =>
            {
                if (success)
                {
                    accessToken = PlayGamesPlatform.Instance.GetServerAuthCode();
                    if (isLinking)
                        WebelinxFirebaseAuthManager.Instance.LinkAccountWithAnotherProvider(accessToken, Providers.GooglePlay);
                    else
                        WebelinxFirebaseAuthManager.Instance.SignInThroughProvider(accessToken, Providers.GooglePlay);
                }
                else
                {
#if !UNITY_EDITOR
                    WebelinxFirebaseAuthView.Instance.statusText.text = "Error with Google Play Sign In. (Not Firebase, Google Play)";
#endif
                 }
            });

            // In case token fails
            //PlayGamesPlatform.Instance.GetAnotherServerAuthCode(true, (code) =>
            //{
            //    WebelinxFirebaseAuthManager.Instance.SignInThroughProvider(code, Providers.GooglePlay);
            //});
        }

        private void GetServerAuthCode(bool isLinking)
        {
            // Always re-authenticate
            PlayGamesPlatform.Instance.GetAnotherServerAuthCode(true, (token) =>
            {

            });
        }

        public void OnFirebaseSignOut()
        {
            PlayGamesPlatform.Instance.SignOut();
        }
    }
}
#endif