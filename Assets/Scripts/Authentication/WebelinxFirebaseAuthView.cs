﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using UnityEngine.UI;
using System;

namespace Authentication
{
    public class WebelinxFirebaseAuthView : MonoBehaviour
    {
        public static WebelinxFirebaseAuthView Instance;

        public Image userImage;
        public Text statusText;
        public Text linkingStatusText;

        public Text firebaseUserInfo;
        public Text providerUserInfo;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;

            //RefreshUserInfo();
        }

        private void Start()
        {
            WebelinxFirebaseAuthManager.Instance.OnSignOut += OnUserSignedOut;
        }

        public void UpdateUserStatusText(FirebaseUser user, string specialMessage)
        {
            if (specialMessage != "")
            {
                statusText.enabled = false;
                statusText.text = specialMessage;
                statusText.color = Color.red;
                statusText.enabled = true;
                
                return;
            }

            //if (user == null)
            //    statusText.text = $"You signed out {user.Email}";
            //else
            //    statusText.text = $"You signed in: {user.Email} : {user.DisplayName}";
            //
            //statusText.text += $"\nFirebase UID: {user.UserId}";
            //statusText.color = Color.white;
        }

        public void PrintUserInformations()
        {
            firebaseUserInfo.text = WebelinxFirebaseAuthManager.Instance.userFirebaseInfo.ToString();
            providerUserInfo.text = WebelinxFirebaseAuthManager.Instance.userProviderInfo.ToString();
        }


        public void DisplayUserProfilePicture()
        {
            if (!PlayerPrefs.HasKey("ProfilePicture")) return;

            string texString = PlayerPrefs.GetString("ProfilePicture");

            byte[] texBytes = Convert.FromBase64String(texString);

            Texture2D tex = new Texture2D(128, 128);
            tex.LoadImage(texBytes);

            userImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
        }

        private void OnUserSignedOut()
        {
            firebaseUserInfo.text = "";
            providerUserInfo.text = "";
            statusText.text = "";
        }
    }
}
