﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Authentication
{
    public class WebelinxAnonymousAuthController : MonoBehaviour
    {
        public static WebelinxAnonymousAuthController Instance;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public void LoginAnonymously()
        {
            WebelinxFirebaseAuthManager.Instance.SignInAnonymously();
        }
    }
}
