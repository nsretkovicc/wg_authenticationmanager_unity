# WebelinxGames Firebase Authentication Manager

Firebase authentication manager that supports signing in as Guest or through providers such as Facebook (iOS & Android), Google Play Games (Android), Game Center and Apple (iOS) 

## Requirements

- Unity 2019.4.20f1+
- Firebase-Auth Unity SDK and SDKs for Authentication providers
- Configured desired Authentication providers in Firebase/Authentication/Sign-in method console

#### Using Unity Package Manager
Add git@gitlab.com:webelinx/games/common/firebase-authentication.git

## Auth SDKs/Plugins used in projects - Download links
**No native SDKs, everything should be imported in Unity**
- **Facebook** - Used v11.0 - Compatible with v11.0.0+ with constraint that **minimum Unity version has to be 2019.4.39f1 for versions above 12.0.0**
    - Download link: https://developers.facebook.com/docs/unity/downloads/
- **Google Play Games Plugin** - Used v10.14 - **Not compatible with v11.01 (latest) for now** - Getting Server Auth code in v11.01 is inconsistent
    - Download link: https://github.com/playgameservices/play-games-plugin-for-unity/releases
- **Apple SDK** - Used v1.4.2 Latest
    - Download link: https://github.com/lupidan/apple-signin-unity/releases/tag/v1.4.2
- **Game Center** - Used with Unity's SocialPlatform

## Using
- Drag prefab to the scene from prefabs folder **and do not destroy it.** (DontDestroyOnLoad is called in WebelinxGamesFierebaseAuthManager's Awake)
- Each provider's script has AuthHandler method that should be called when user clicks on desired provider
- When signing in, isLinking should be set to false. Each provider obtains accessToken and sends it to **WebelinxGamesFierebaseAuthManager** where token is handled and credential is created
- If credential is valid, user gets signed in through chosen provider
- User can also sign in anonymously/as guest. Anonymous provider should be added to Firebase Console / Authentication / Sign-In method
- When user is signed in **AuthStateChanged** callback is called, this is a function where you should handle next steps
- When user is signed in, there are two types of data that we have about user, its Firebase User data and its Provider Data which is more detailed data about authenticated user. When user signs in, Manager checks and syncs more detailed provider data to Firebase User data, for example if user changes his username in Google Play account, this change will reflect next time he signs in with Google Play.
- If user is signed in with, for example Google Play then changes GP username and play the game again, this change will not be seen until user re-authenticate again
### Fields
- When user is signed in, two types of objects are created - **SignedInUserFirebaseInfo - userFirebaseInfo** and **SignedInUserProviderInfo userProviderInfo** where you can access user's Provider/Firebase data mentioned above separately
- **FirebaseAuth - auth** - Field that controlls authentication
- **FirebaseUser - user** - Reference to auth.CurrentUser - currently signed in user
### Linking accounts
- Linking is most often done when user wants to link his guest account to some provider so that he can always restore his progress, but linking could also be done between any two providers
- isLinking bool flag should be set to true if user is linking his current account to another provider
- Linking accounts doc: https://firebase.google.com/docs/auth/unity/account-linking

# Setup providers
## Facebook Setup
- **This plugin is supported for both, iOS and Android devices**
- Import unity plugin from download link above, you will see Menu item "Facebook" in toolbar and there you can configure Facebook SO
- Tested for Facebook apps that are either _Consumer_ or _None_ type
- Go to project's Facebook App, in Dashboard tab scroll down and find **Add products to your app** section, add Facebook Login
- Facebook Login tab will appear on the left sidebar, click and go settings
- Find **Valid OAuth Redirect URIs**, go to Firebase/Authentication/Sign-in method/Add provider and click Facebook
- Firebase will generate **OAuth redirect URI**, copy it, should look like this:
```
https://solitaire-7042a.firebaseapp.com/__/auth/handler
```
- Go back to Facebook application's Login settings and paste it into **Valid OAuth Redirect URIs** text box
- Under Settings/Basic tab, scroll down, add and configure platform (Android and/or iOS)

**Setup Facebook as provider in Firebase**
- Firebase / Authentication / Sign-in method / Facebook
- To setup a provider in Firebase, go to project's facebook app and get App id and App secret (under Settings/Basic tab)

**Setup Facebook in Unity**
- When you import FB SDK in Unity, "Facebook" Menu Item in toolbar will be added, click Edit Settings and configure scriptable object
- Most important fields are _Facebook App Id_ and _Client Token_
- Click _Regenerate Android Manifest_ for Android

## Google Play Setup
- **This plugin is only supported for Android devices**
- Plugin documentation: https://github.com/playgameservices/play-games-plugin-for-unity
- Enable Google Play SignIn in Play Console of your game
- Application needs to be uploaded to Google Play console, then it will be re-signed (SHA1 will be created)
- One test achievement or test leaderboard needs to be created in Play Console


**Getting client ID and client secret**

- Open Game's Application in **Google Cloud Console**
- In Play Console find SHA1 where application is signed (should be under Settings/AppIntegrity - SHA1 certificate fingerprint)
- Go to Credentials / Create Credential / OAuth Client ID
- For Application Type select **Web Application** (not android, else GooglePlay will return empty Server Auth Code)
- Paste SHA1 copied from **Play Console**, name credential and create it
- Google Will create _clientsecretXYZ123.json_ file, download it
- Copy **client_id** and **client_secret** values from this file

**Setup Google Play as provider in Firebase**

- Firebase / Authentication / Sign-in method / Google Play
- Select Google Play provider, enter _client_id_ and _client_secret_ and click Save to add provider
    
**Google Play setup in Unity**

- Import plugin package from  _Auth SDKs/Plugins used in projects_ section
- In Unity, go to Window / Google Play Games / Setup / Android Setup
- One achievement or leaderboard has to be created to be able to setup Google Play, copy setup XML and paste it into a big text box of Google Play's dialogue
- At the bottom of the dialogue, there is Client ID field, enter _client_id_ from downloaded json file
- Click Setup

Client ID example:
```
126591668506-teotns5tutchbhcu2u1nlchcc2ie3g0n.apps.googleusercontent.com
``` 
Client Secret example:
```
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
```
Achievement created XML for Setup example:
```
<?xml version="1.0" encoding="utf-8"?>
<!--Google Play game services IDs. Save this file as res/values/games-ids.xml in your project.-->
<resources>
  <!--app_id-->
  <string name="app_id" translatable="false">126591668506</string>
  <!--package_name-->
  <string name="package_name" translatable="false">com.WebelinxGames.Solitaire</string>
  <!--achievement Test achievement-->
  <string name="achievement_test_achievement" translatable="false">CgkImqLOy9cDEAIQAg</string>
</resources>

```
## Apple Setup
- **This plugin is only supported for iOS devices**
- Plugin documentation: https://github.com/lupidan/apple-signin-unity
- Firebase documentation: https://firebase.google.com/docs/auth/unity/apple
- Configure iOS part of Firebase project settings such as App Store ID / Team ID etc...
- Import GoogleService-Info.plist into the project

**Setup Apple as provider in Firebase**
- Firebase / Authentication / Sign-in method / Apple
- Select Apple provider, **don't enter anything** in any fields as this is only for iOS devices
- Click save to add provider

**Setup Apple SignIn capabilities**
- In your generated Xcode project, select the main app Unity-iPhone target and select the option Signing And Capabilities. You should see there an option to add a capability from a list. Just locate Sign In With Apple and add it to your project.
- Add Apple SignIn capability in Apple Developers Console

## Game Center Setup
- **Only supported for iOS devices**
- Done through Unity's SocialPlatform

**Setup Apple SignIn capabilities**
- In your generated Xcode project, select the main app Unity-iPhone target and select the option GameCenter. You should see there an option to add a capability from a list. Just locate GameCenter and add it to your project.
- Add GameCenter capability in Apple Developers Console
- Create test achievement/leaderboard in Apple Developers Console for GameCenter to work

**Specific situation that needs to be handled**
- Documentation about Social.localUser.Authenticate: https://docs.unity3d.com/ScriptReference/SocialPlatforms.ILocalUser.Authenticate.html

Important note:
```
On certain platforms (including but not limited to iOS and tvOS), the callback is only invoked on the first call to Authenticate(). 
Subsequent calls to Authenticate() on such platforms results in no callback being triggered. 
This can occur if, for example, the user or the OS cancels the authentication operation before it has completed. 
Please ensure you test for this situation.

```
